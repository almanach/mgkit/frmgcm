#!/bin/sh
running_instance=$(ps aux | grep '[c]pm.jar' | awk '{print $2}')
# if running_instance is empty do not kill
if [ -n "$running_instance" ]; then 
  kill -9 $(ps aux | grep '[c]pm.jar' | awk '{print $2}')
fi

dirname=`dirname $0`/
config_file=$dirname"appdata/cpm.config"
java=java
java_opt=""
jar=$dirname"build/cpm.jar"
nohup $java $java_opt -server -cp $jar fr.inria.alpage.frmgcm.AppMain $config_file > "${dirname}cpm.log" 2>&1&
