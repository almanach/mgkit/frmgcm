WD = .
PACKAGE = fr/inria/alpage/frmgcm/dpp
javacc = ./tools/javacc/bin/lib/javacc.jar

dpath_parser:
	java -cp $(javacc) jjtree -OUTPUT_DIRECTORY=$(WD)/src/$(PACKAGE) $(WD)/GrammarDefinition/dpath.jjt
	java -cp $(javacc) javacc -OUTPUT_DIRECTORY=$(WD)/src/$(PACKAGE) $(WD)/src/$(PACKAGE)/dpath.jj 
