## Overview

Projet GForge : http://gforge.inria.fr/projects/frmgcm/

Documentation : dans les sources du projet, dans doc (point d'entrée *index.html*), ou en [accès local sur marc](http://marc.inria.fr/frmgcm_doc).

### Pré-requis

Installation de Java 7 ([oracle](http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html) ou [openjdk](http://openjdk.java.net/projects/jdk7u/))

Installation de [mongodb](http://www.mongodb.org/) et[ elasticsearch](http://www.elasticsearch.org/)

### Build

Importer le projet sous eclipse (File-&gt;Import, Choisir Général -&gt; Existing Projects into workspace, entrer le chemin vers la racine du projet récupéré par git)

Si le fichier dpath.jjt (dans GrammarDefinition) est modifier, exécuter 'make dpath_parser'

Compiler et packager avec eclipse : (Clic droit sur le dossier racine du projet -&gt; Export -&gt; Java -&gt; Runnable jar file)

Si aucune "Launch Configuration" n'est disponible, on peut créer rapidement la configuration par défaut en appuyant sur F11 (ou dans Project -&gt; Debug/Run).

S'assurer que les librairies sont extraites dans le jar (options par défaut lors de l'export).

Le jar résultant se trouve à l'endroit choisi lors de l'export.

#### Dépendances (incluses)

- javacc (https://javacc.java.net/) : générateur de parseur (utilisé pour dpath)

- Apache HttpComponents (http://hc.apache.org/) : serveur http

- Commons CLI (http://commons.apache.org/proper/commons-cli/) : gestion des arguments en ligne de commandes du programme

- JSch (http://www.jcraft.com/jsch/) : gestion des connexions via SSH

- mongodriver (http://docs.mongodb.org/ecosystem/drivers/java/) : driver java API pour la base de donnée mongo

- Commons Compress (http://commons.apache.org/proper/commons-compress/) : (dé)compression de fichiers

- boilerpipe (https://code.google.com/p/boilerpipe/) : extraction de texte dans les fichier html

### Deploiement

Binaire : build/cpm.jar (sur marc : /home/pinot/alpage/buiquang/install/frmgcm/cpm.jar)

Script d'éxécution : run.sh (sur marc : /home/pinot/alpage/buiquang/install/frmgcm/run.sh)

Fichier de configuration : appdata/cpm.config (sur marc : /home/pinot/alpage/buiquang/install/frmgcm/cpm.config)

Répertoire principal : appdata (sur marc : /home/pinot/alpage/buiquang/frmgcm/appdata)

Avant de lancer le script d'éxécution, il faut avoir bien configuré le fichier de configuration (doc inline) ET la configuration présente dans le script d'éxécution (doc inline)

## Webservice API Reference

### Corpus

La classe qui définit les webservices suivant est fr.inria.alpage.frmgcm.services.CorpusService

<table border="1" cellpadding="1" cellspacing="1" height="193" style="width: 100%;" width="859">
	<thead>
		<tr>
			<th scope="col" style="width: 181px;">Methode</th>
			<th scope="col" style="width: 143px;">Requête HTTP</th>
			<th scope="col" style="width: 688px;">Corps de requête</th>
			<th scope="col" style="width: 518px;">Réponse</th>
			<th scope="col" style="width: 213px;">Description</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="align-center" colspan="4" style="width: 1431px;">Les urls sont relatives à http://localhost:PORT/corpus/</td>
			<td style="width: 213px;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 181px;">create</td>
			<td style="width: 143px;">POST _create</td>
			<td style="width: 688px;">
			<pre class="other " data-pbcklang="other">

{               
    mgwiki_user:string, // le nom d'utilisateur mgwiki               
    cid:string, // l'id du corpus dans mgwiki               
    path:string, // le chemin local vers le corpus (voir module mgwiki_corpus)               
    name:string, // le nom du corpus (doit être unique)               
}</pre>
			</td>
			<td style="width: 518px;">
			<pre class="xml " data-pbcklang="xml">

{success:true}</pre>

OU (si echec)

			<pre class="xml " data-pbcklang="xml">

{error:true}</pre>

&nbsp;

			</td>
			<td style="width: 213px;">crée un nouveau corpus et lance automatiquement le processus de dépaquetage/conversion des fichiers originaux</td>
		</tr>
		<tr>
			<td style="width: 181px;">get</td>
			<td style="width: 143px;">GET {cid}</td>
			<td style="width: 688px;">-</td>
			<td style="width: 518px;">

Le json représentant l'objet **Corpus** demandé

OU

			<pre class="other " data-pbcklang="other">

{error:string // message d'erreur}</pre>

&nbsp;

			</td>
			<td style="width: 213px;">retourne l'objet **Corpus**</td>
		</tr>
		<tr>
			<td style="width: 181px;">delete</td>
			<td style="width: 143px;">GET {cid}/_delete</td>
			<td style="width: 688px;">-</td>
			<td style="width: 518px;">
			<pre class="other " data-pbcklang="other">

{success:boolean}</pre>
			</td>
			<td style="width: 213px;">

supprime le corpus (index, objet en db)

todo : supprimer le répertoire correspondant sur rioc

			</td>
		</tr>
		<tr>
			<td style="width: 181px;">getTagset</td>
			<td style="width: 143px;">GET {cid}/_tagset</td>
			<td style="width: 688px;">-</td>
			<td style="width: 518px;">
			<pre class="other " data-pbcklang="other">

{               
    _id:mongoid, // the mongo object id for this tagset               
    edge:               
        (key): // feature name               
            [string], // list of values               
    node:               
        (key): // feature name               
            [string], // list of values               
    name:string // tagset name               
}</pre>

OU si non défini

objet vide {}

			</td>
			<td style="width: 213px;">retourne le tagset associé à un corpus selon le format dans lequel il a été analysé (passage, conll, depconll ou depxml)</td>
		</tr>
		<tr>
			<td style="width: 181px;">process</td>
			<td style="width: 143px;">POST {cid}/_process</td>
			<td style="width: 688px;">
			<pre class="other " data-pbcklang="other">

{               
    mgwiki_user:string, // le nom d'utilisateur de mgwiki               
    pid:string, // id du processus à lancer               
    /* 0 : préparation (automatique dès la création du corpus)               
    *  1 : analyse syntaxique               
    *  2 : extraction de terme (enlevé)               
    *  3 : indexation (à ajouter)               
    */               
    format:string, // format dans lequel analyser le corpus               
    termextract:string // (optionel) si renseigné, enchaine l'analyse sémantique après l'analyse syntaxique               
    reset:boolean // redémarre le processus si celui ci était déja en cours               
}</pre>
			</td>
			<td style="width: 518px;">
			<pre class="other " data-pbcklang="other">

{state:"ok"}</pre>

OU si une erreur est survenue

			<pre class="other " data-pbcklang="other">

{               
    error:true,               
    message:string//message d'erreur               
}</pre>

&nbsp;

			</td>
			<td style="width: 213px;">lance un processus sur le corpus</td>
		</tr>
		<tr>
			<td style="width: 181px;">index</td>
			<td style="width: 143px;">GET {cid}/_index</td>
			<td style="width: 688px;">&nbsp;</td>
			<td style="width: 518px;">
			<pre class="other " data-pbcklang="other">

{launched:"ok"}</pre>
			</td>
			<td style="width: 213px;">lance l'indexation du corpus</td>
		</tr>
		<tr>
			<td style="width: 181px;">getTasksInfo</td>
			<td style="width: 143px;">GET {cid}/task</td>
			<td style="width: 688px;">&nbsp;</td>
			<td style="width: 518px;">
			<pre class="other " data-pbcklang="other">

[               
    {               
        _id:mongoid, // l'id mongo de la tâche               
        name:string, // nom de la tâche (vide par défaut)               
        tasks:[ // liste des sous processus constituant la tâche               
            {               
                status:string, // status du processus               
                type:string, // type de processus               
                command:string, // commande du processus               
                args:[string], // liste d'arguments du processus               
                refObjectId:string, // id (mongo id) du corpus associé               
                stdout:string, // chemin vers le fichier contenant la sortie standard               
                stderr:string, // chemin vers le fichier contenant la sortie d'erreur               
            }               
        ]               
]</pre>
			</td>
			<td style="width: 213px;">retourne les informations sur les tâches associées au corpus</td>
		</tr>
		<tr>
			<td style="width: 181px;">getTaskOutput</td>
			<td style="width: 143px;">GET {cid}/task/{pid}/{tid}/stdout</td>
			<td style="width: 688px;">&nbsp;</td>
			<td style="width: 518px;">Le chemin vers le fichier d'output de la tâche sélectionnée</td>
			<td style="width: 213px;">retourne le chemin vers le fichier d'output de la tâche sélectionnée</td>
		</tr>
		<tr>
			<td style="width: 181px;">getTaskError</td>
			<td style="width: 143px;">GET {cid}/task/{pid}/{tid}/stderr</td>
			<td style="width: 688px;">&nbsp;</td>
			<td style="width: 518px;">

Le chemin vers le fichier d'erreur de la tâche sélectionnée

			</td>
			<td style="width: 213px;">

retourne le chemin vers le fichier d'erreur de la tâche sélectionnée

			</td>
		</tr>
		<tr>
			<td style="width: 181px;">getReport</td>
			<td style="width: 143px;">GET {cid}/report</td>
			<td style="width: 688px;">&nbsp;</td>
			<td style="width: 518px;">Le rapport généré par le script "report.pl"</td>
			<td style="width: 213px;">retourne le contenu du rapport d'analyse du corpus</td>
		</tr>
		<tr>
			<td style="width: 181px;">search</td>
			<td style="width: 143px;">POST {cid}/_search</td>
			<td style="width: 688px;">
			<pre class="other " data-pbcklang="other">

{               
    cma-reloaded:string // optionel, (pris en compte si renseigné) effectue une recherche asynchrone               
    query:string // le contenu de la requête               
    searchType:string // le type de la requête (id, sentence, dpath)               
    from:int // le nombre de résultat à sauter               
    max:int // le nombre de résultat à retourner               
    export:string // optionel, (pris en compte si renseigné) effectue un export des résultats               
}</pre>
			</td>
			<td style="width: 518px;">
			<pre class="xml " data-pbcklang="xml">

{               
    total:int, // nombre total de résultats               
    status:string, // pour la recherhce asynchrone, indique si cet objet est complet               
    hits:[{ // liste des résultats               
        _score:int, // non utilisé, toujours égal à 1               
        _source:{ // données du résultat               
            sentence:string, // phrase représentative du graphe               
            did:string, // id de la collection à laquelle appartient le graphe               
            sid:string, // id du graphe               
        }               
    }]               
}</pre>
			</td>
			<td style="width: 213px;">effectue une recherche, par phrase, id de graphe, ou dpath</td>
		</tr>
		<tr>
			<td style="width: 181px;">getDocumentsList</td>
			<td style="width: 143px;">GET {cid}/document</td>
			<td style="width: 688px;">&nbsp;</td>
			<td style="width: 518px;">
			<pre class="other " data-pbcklang="other">

{      
  path : string , // here the path of the root element will always be "/"     
  children : {      
     (name) : {     
      file : boolean , // indicate if this child is a file or directory     
      path : string , // relative path of this file/folder      
      did : string , // (optional) document id if this is a file       
      children : { } , // list of children      
      fullpath : string , // full path relative to the root     
      scid : string // (optional) collection id that is associated with this document     
    }     
  }     
}</pre>
			</td>
			<td style="width: 213px;">récupère le json définissant l'arborescence des fichiers du corpus qui ont été traités</td>
		</tr>
		<tr>
			<td style="width: 181px;">getDocumentHTML</td>
			<td style="width: 143px;">GET {cid}/document/{did}/(annotated)</td>
			<td style="width: 688px;">&nbsp;</td>
			<td style="width: 518px;">

un fichier html

			</td>
			<td style="width: 213px;">récupère le html annoté qui relie le texte original et les phrases analysées</td>
		</tr>
		<tr>
			<td style="width: 181px;">saveRevision</td>
			<td style="width: 143px;">POST {cid}/document/{did}/_save</td>
			<td style="width: 688px;">
			<pre class="other " data-pbcklang="other">

{     
  name:string, // the name of the revision     
  description:string, // (optional) the description of the revision     
  mgwiki_user:string // a mgwiki user name     
}</pre>
			</td>
			<td style="width: 518px;">a boolean</td>
			<td style="width: 213px;">crée une sauvegarde de l'état courant du document (ensemble des changements opérés sur les graphes)</td>
		</tr>
		<tr>
			<td style="width: 181px;">getRevisions</td>
			<td style="width: 143px;">GET {cid}/document/{did}/revisions</td>
			<td style="width: 688px;">&nbsp;</td>
			<td style="width: 518px;">une liste de **DocumentRevision**</td>
			<td style="width: 213px;">retourne la liste de toutes les révisions de ce document</td>
		</tr>
		<tr>
			<td style="width: 181px;">getGraph</td>
			<td style="width: 143px;">POST {cid}/document/{did}/graph</td>
			<td style="width: 688px;">
			<pre class="xml " data-pbcklang="xml">

{     
  gid:string,// the id of the graph as it is found in the analysis results     
  vid:int, // (optional) the version id of the graph (number between 0, original version and n the last revision version)     
}     
OR (for fragment selection, currently works only on conll graph format)     
{     
  slist:[string], // list of graph ids that span over the fragment selection     
  ts:string, // the token id at the beginning of the fragment (in the form ExFy)     
  te:string // the token id at the end of the fragment (in the form ExFy)     
}</pre>
			</td>
			<td style="width: 518px;">

le chemin vers le fichier source du graph

OU

un objet **Depgraph**

&nbsp;

			</td>
			<td style="width: 213px;">selon les arguments passés, permet d'obtenir le chemin vers le fichier source du graphe demandé OU le contenu d'un fragment de graphe(s)</td>
		</tr>
		<tr>
			<td style="width: 181px;">saveGraph</td>
			<td style="width: 143px;">POST {cid}/document/{did}/graph/save</td>
			<td style="width: 688px;">
			<pre class="other " data-pbcklang="other">

{     
  mgwiki_user:string, // a mgwiki user name     
  data:string, // the new data to update the graph with     
  gid:string, // the graph id as it is found in the analysis results     
}</pre>

&nbsp;

			</td>
			<td style="width: 518px;">a boolean</td>
			<td style="width: 213px;">crée une nouvelle révision pour un graphe et crée automatiquement une nouvelle révision du document</td>
		</tr>
	</tbody>
</table>

&nbsp;

### Collections

Ce webservice plus générique permet d'effectuer des recherches et de manager des collections de graphes indépendemment de la construction "corpus/document/graphes".

<table border="1" cellpadding="1" cellspacing="1" style="width: 100%;">
	<thead>
		<tr>
			<th scope="col">Méthode</th>
			<th scope="col">Requête HTTP</th>
			<th scope="col">Corps de requête</th>
			<th scope="col">Réponse</th>
			<th scope="col">Description</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="align-center" colspan="5">Les urls sont relatives à http://localhost:PORT/sc/</td>
		</tr>
		<tr>
			<td>create</td>
			<td>POST _create</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>crée une nouvelle collection</td>
		</tr>
		<tr>
			<td>delete</td>
			<td>POST _delete</td>
			<td>&nbsp;</td>
			<td>
			<pre class="other " data-pbcklang="other">

{acknowledge:true}</pre>
			</td>
			<td>supprime la collection (supprime les graphes associés)</td>
		</tr>
		<tr>
			<td>list</td>
			<td>POST _list</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>liste les collection pour un utilisateur (et possiblement selon un namespace)</td>
		</tr>
		<tr>
			<td>addResources</td>
			<td>POST {cid}/_add</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>ajoute une ou plusieurs ressource(s) de graphes à une collection (crée la collection si aucune collection de ce nom existe)</td>
		</tr>
		<tr>
			<td>index</td>
			<td>POST {cid}/_index</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>(ré-)indexe les graphes de la collection</td>
		</tr>
		<tr>
			<td>search</td>
			<td>POST {cid}/_search</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>effectue une recherche dans les graphes de la collection</td>
		</tr>
		<tr>
			<td>filter</td>
			<td>POST {cid}/_filter</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>effectue un filtre à partir d'une requête dpath</td>
		</tr>
		<tr>
			<td>getGraph</td>
			<td>POST {cid}/_graph</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>retourne des informations (selon les paramètres) sur un graph</td>
		</tr>
		<tr>
			<td>listGraphs</td>
			<td>POST {cid}/_graph/_all</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>liste tous les graphes (avec possibilité de filtre selon les paramètres) de la collection</td>
		</tr>
		<tr>
			<td>createGraph</td>
			<td>POST {cid}/_graph/_create</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>crée un graphe en base de donnée et l'ajoute à la collection</td>
		</tr>
		<tr>
			<td>deleteGraph</td>
			<td>POST {cid}/_graph/_delete</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>supprime un graphe</td>
		</tr>
		<tr>
			<td>getVotesGraph</td>
			<td>POST {cid}/_graph/_vote</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>retourne la liste des votes sur un graphe</td>
		</tr>
		<tr>
			<td>putVoteGraph</td>
			<td>POST {cid}/_graph/_vote/_put</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>ajoute ou modifie un vote sur un graphe</td>
		</tr>
		<tr>
			<td>getNotesGraph</td>
			<td>POST {cid}/_graph/_note</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>retourne la liste des notes/commentaires sur un graphe</td>
		</tr>
		<tr>
			<td>addNoteGraph</td>
			<td>POST {cid}/_graph/_note/_add</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>ajoute un commenaire sur le graphe</td>
		</tr>
		<tr>
			<td>deleteNoteGraph</td>
			<td>POST {cid}/_graph/_note/_remove</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>supprime un commentaire sur le graphe</td>
		</tr>
		<tr>
			<td>update</td>
			<td>POST {cid}/_graph/_update</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>met à jour un graphe avec de nouvelles données</td>
		</tr>
		<tr>
			<td>getTagset</td>
			<td>POST _tagset</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>retourne le contenu d'un tagset</td>
		</tr>
		<tr>
			<td class="align-center" colspan="5">Les points d'entrée suivant sont des services créés ad hoc pour mgwiki et devraient être déplacé dans un autre service ou refactorisés avec les précédents.</td>
		</tr>
		<tr>
			<td>mgwikiSearch</td>
			<td>POST _mgwiki/_search</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>effectue une recherche sur les graphes depxml d'mgwiki</td>
		</tr>
		<tr>
			<td>mgwikiIndex</td>
			<td>POST _mgwiki/_index</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>indexe les graphes depxml d'mgwiki</td>
		</tr>
		<tr>
			<td>mgwikiMatchSignature</td>
			<td>POST _mgwiki/_matchSignature</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>retourne la liste des pages liées à une phrase (après l'avoir parsée et comparée au signatures enregistrée d'mgwiki)</td>
		</tr>
		<tr>
			<td>mgwikiGetGraph</td>
			<td>POST _mgwiki/_graph</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>retourne des informations sur un graphe</td>
		</tr>
	</tbody>
</table>

&nbsp;

### Macros

Petit webservice permettant de gérer les macros dpath.

<table border="1" cellpadding="1" cellspacing="1" style="width: 100%;">
	<thead>
		<tr>
			<th scope="col">Méthode</th>
			<th scope="col">Requête HTTP</th>
			<th scope="col">Corpus de requête</th>
			<th scope="col">Réponse</th>
			<th scope="col">Description</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="align-center" colspan="5">Les urls sont relatives à http://localhost:PORT/dpathlib/</td>
		</tr>
		<tr>
			<td>create</td>
			<td>POST _create</td>
			<td>
			<pre class="other " data-pbcklang="other">

{           
    mgwiki_user:string,// le nom d'utilisateur mgwiki           
    name:string, // le nom unique de la macro           
    publicv:boolean, // indique si la macro est publique ou non           
    description:string, // optionnel, description de la macro           
    query:string, // contenu de la requête           
}</pre>
			</td>
			<td>
			<pre class="other " data-pbcklang="other">

{           
    success:boolean, // indique si l'opération s'est bien déroulée           
    message:string // message d'erreur           
}</pre>
			</td>
			<td>crée une nouvelle macro</td>
		</tr>
		<tr>
			<td>list</td>
			<td>POST _all</td>
			<td>
			<pre class="other " data-pbcklang="other">

{           
    mgwiki_user:string // l'utilisateur mgwiki           
}</pre>
			</td>
			<td>
			<pre class="other " data-pbcklang="other">

[{DPathMacro}] // liste de DPathMacro</pre>
			</td>
			<td>liste les macro visible par l'utilisateur courant</td>
		</tr>
		<tr>
			<td>getHistory</td>
			<td>POST _history</td>
			<td>
			<pre class="other " data-pbcklang="other">

{           
    mgwiki_user:string // l'utilisateur mgwiki           
}</pre>
			</td>
			<td>
			<pre class="other " data-pbcklang="other">

[{string}] // liste de requêtes dpath</pre>
			</td>
			<td>liste l'historique des requêtes dpath effectuées par l'utilisateur courant</td>
		</tr>
		<tr>
			<td>get</td>
			<td>POST {macroName}</td>
			<td>&nbsp;</td>
			<td>Le json représentant l'objet **DPathMacro** demandé</td>
			<td>retourne les information sur une macro</td>
		</tr>
		<tr>
			<td>search</td>
			<td>POST _search</td>
			<td>
			<pre class="other " data-pbcklang="other">

{    
  mgwiki_user:string, // l'utilisateur mgwiki    
  query:string // le texte à rechercher (description ou titre)    
}    
  </pre>
			</td>
			<td>
			<pre class="other " data-pbcklang="other">

[{DPathMacro}] // la liste des macro correspondant à la requête</pre>
			</td>
			<td>effectue une recherche sur le nom et la description des macros.</td>
		</tr>
		<tr>
			<td>edit</td>
			<td>POST {macroName}/_edit</td>
			<td>
			<pre class="other " data-pbcklang="other">

{           
    mgwiki_user:string,// le nom d'utilisateur mgwiki           
    name:string, // le nom unique de la macro           
    publicv:boolean, // indique si la macro est publique ou non           
    description:string, // optionnel, description de la macro           
    query:string, // contenu de la requête           
}</pre>
			</td>
			<td>
			<pre class="other " data-pbcklang="other">

{           
    success:boolean, // indique si l'opération s'est bien déroulée           
    message:string // message d'erreur           
}</pre>
			</td>
			<td>modifie une macro existante</td>
		</tr>
		<tr>
			<td>delete</td>
			<td>POST {macroName}/_delete</td>
			<td>
			<pre class="other " data-pbcklang="other">

{           
    mgwiki_user:string // l'utilisateur mgwiki           
}</pre>
			</td>
			<td>
			<pre class="other " data-pbcklang="other">

{           
    success:boolean, // indique si l'opération s'est bien déroulée           
    message:string // message d'erreur           
}</pre>
			</td>
			<td>supprime une macro</td>
		</tr>
	</tbody>
</table>

## Architecture logicielle

[svg:FrMGClassDiagram.svg]

&nbsp;

## ElasticSearch

[Référence d'elasticsearch](http://www.elasticsearch.org/guide/en/elasticsearch/reference/current/index.html)

### Schema d'indexation

Script de suppression de l'index : *appHome*/scripts/es_index_deletion.pl

Script d'initialisation de l'index : *appHome*/scripts/es_index_creation.pl

Le script d'initialisation contient le json qui définit le schema d'indexation.

<pre class="other " data-pbcklang="other">

{                 
   "did":{                 
      "type":"string",                 
      "index":"not_analyzed",                 
      "norms":{                 
         "enabled":false                 
      },                 
      "index_options":"docs"                 
   },                 
   "edges":{                 
      "properties":{                 
         "id":{                 
            "type":"string"                 
         },                 
         "label":{                 
            "type":"string",                 
            "index":"not_analyzed",                 
            "norms":{                 
               "enabled":false                 
            },                 
            "index_options":"docs"                 
         },                 
         "source":{                 
            "type":"string",                 
            "index":"not_analyzed",                 
            "norms":{                 
               "enabled":false                 
            },                 
            "index_options":"docs"                 
         },                 
         "target":{                 
            "type":"string",                 
            "index":"not_analyzed",                 
            "norms":{                 
               "enabled":false                 
            },                 
            "index_options":"docs"                 
         },                 
         "type":{                 
            "type":"string"                 
         }                 
      }                 
   },                 
   "nodes":{                 
      "properties":{                 
         "cat":{                 
            "type":"string",                 
            "index":"not_analyzed"                 
         },                 
         "cpos":{                 
            "type":"string",                 
            "index":"not_analyzed",                 
            "norms":{                 
               "enabled":false                 
            },                 
            "index_options":"docs"                 
         },                 
         "features":{                 
            "properties":{                 
               "m":{                 
                  "type":"string",                 
                  "index":"not_analyzed",                 
                  "norms":{                 
                     "enabled":false                 
                  },                 
                  "index_options":"docs"                 
               },                 
               "n":{                 
                  "type":"string",                 
                  "index":"not_analyzed",                 
                  "norms":{                 
                     "enabled":false                 
                  },                 
                  "index_options":"docs"                 
               },                 
               "p":{                 
                  "type":"string",                 
                  "index":"not_analyzed",                 
                  "norms":{                 
                     "enabled":false                 
                  },                 
                  "index_options":"docs"                 
               },                 
               "s":{                 
                  "type":"string",                 
                  "index":"not_analyzed",                 
                  "norms":{                 
                     "enabled":false                 
                  },                 
                  "index_options":"docs"                 
               },                 
               "t":{                 
                  "type":"string",                 
                  "index":"not_analyzed",                 
                  "norms":{                 
                     "enabled":false                 
                  },                 
                  "index_options":"docs"                 
               }                 
            }                 
         },                 
         "form":{                 
            "type":"string",                 
            "index":"not_analyzed",                 
            "norms":{                 
               "enabled":false                 
            },                 
            "index_options":"docs"                 
         },                 
         "id":{                 
            "type":"string",                 
            "index":"not_analyzed",                 
            "norms":{                 
               "enabled":false                 
            },                 
            "index_options":"docs"                 
         },                 
         "lemma":{                 
            "type":"string",                 
            "index":"not_analyzed",                 
            "norms":{                 
               "enabled":false                 
            },                 
            "index_options":"docs"                 
         },                 
         "pos":{                 
            "type":"string",                 
            "index":"not_analyzed",                 
            "norms":{                 
               "enabled":false                 
            },                 
            "index_options":"docs"                 
         },                 
         "token":{                 
            "type":"string"                 
         },                 
         "xcat":{                 
            "type":"string"                 
         }                 
      }                 
   },                 
   "sentence":{                 
      "type":"string"                 
   },                 
   "sid":{                 
      "type":"string",                 
      "index":"not_analyzed",                 
      "norms":{                 
         "enabled":false                 
      },                 
      "index_options":"docs"                 
   }                 
}</pre>

Le point important est que les champs ne soient pas analysés (sauf le champ "sentence") sinon les requêtes donnent de faux et incomplets résultats.
