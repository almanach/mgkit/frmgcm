 #!/usr/bin/perl

use strict;
use warnings;
use AppConfig;


use LWP::UserAgent;
 
my $ua = LWP::UserAgent->new;
 
my $server_endpoint = "http://localhost:9200/sentences";
 
my $req = HTTP::Request->new(PUT => $server_endpoint);
$req->header('content-type' => 'application/json');
 
#my $post_data = $json;
my $post_data = '{"mappings":{"graph":{"properties":{"did":{"type":"string","index":"not_analyzed","norms":{"enabled":false},"index_options":"docs"},"edges":{"properties":{"id":{"type":"string"},"label":{"type":"string","index":"not_analyzed","norms":{"enabled":false},"index_options":"docs"},"source":{"type":"string","index":"not_analyzed","norms":{"enabled":false},"index_options":"docs"},"target":{"type":"string","index":"not_analyzed","norms":{"enabled":false},"index_options":"docs"},"type":{"type":"string"}}},"nodes":{"properties":{"cat":{"type":"string","index":"not_analyzed"},"cpos":{"type":"string","index":"not_analyzed","norms":{"enabled":false},"index_options":"docs"},"features":{"properties":{"m":{"type":"string","index":"not_analyzed","norms":{"enabled":false},"index_options":"docs"},"n":{"type":"string","index":"not_analyzed","norms":{"enabled":false},"index_options":"docs"},"p":{"type":"string","index":"not_analyzed","norms":{"enabled":false},"index_options":"docs"},"s":{"type":"string","index":"not_analyzed","norms":{"enabled":false},"index_options":"docs"},"t":{"type":"string","index":"not_analyzed","norms":{"enabled":false},"index_options":"docs"}}},"form":{"type":"string","index":"not_analyzed","norms":{"enabled":false},"index_options":"docs"},"id":{"type":"string","index":"not_analyzed","norms":{"enabled":false},"index_options":"docs"},"lemma":{"type":"string","index":"not_analyzed","norms":{"enabled":false},"index_options":"docs"},"pos":{"type":"string","index":"not_analyzed","norms":{"enabled":false},"index_options":"docs"},"token":{"type":"string"},"xcat":{"type":"string"}}},"sentence":{"type":"string"},"sid":{"type":"string","index":"not_analyzed","norms":{"enabled":false},"index_options":"docs"}}}}}';
#my $post_data = '{"graph":{"properties":{"did":{"type":"string","index":"not_analyzed","norms":{"enabled":false},"index_options":"docs"},"edges":{"properties":{"id":{"type":"string"},"label":{"type":"string","index":"not_analyzed","norms":{"enabled":false},"index_options":"docs"},"source":{"type":"string","index":"not_analyzed","norms":{"enabled":false},"index_options":"docs"},"target":{"type":"string","index":"not_analyzed","norms":{"enabled":false},"index_options":"docs"},"type":{"type":"string"}}},"nodes":{"properties":{"cat":{"type":"string","index":"not_analyzed"},"cpos":{"type":"string","index":"not_analyzed","norms":{"enabled":false},"index_options":"docs"},"features":{"properties":{"m":{"type":"string","index":"not_analyzed","norms":{"enabled":false},"index_options":"docs"},"n":{"type":"string","index":"not_analyzed","norms":{"enabled":false},"index_options":"docs"},"p":{"type":"string","index":"not_analyzed","norms":{"enabled":false},"index_options":"docs"},"s":{"type":"string","index":"not_analyzed","norms":{"enabled":false},"index_options":"docs"},"t":{"type":"string","index":"not_analyzed","norms":{"enabled":false},"index_options":"docs"}}},"form":{"type":"string","index":"not_analyzed","norms":{"enabled":false},"index_options":"docs"},"id":{"type":"string","index":"not_analyzed","norms":{"enabled":false},"index_options":"docs"},"lemma":{"type":"string","index":"not_analyzed","norms":{"enabled":false},"index_options":"docs"},"pos":{"type":"string","index":"not_analyzed","norms":{"enabled":false},"index_options":"docs"},"token":{"type":"string"},"xcat":{"type":"string"}}},"sentence":{"type":"string"},"sid":{"type":"string","index":"not_analyzed","norms":{"enabled":false},"index_options":"docs"}}}}';
#$req->content();
$req->content($post_data);
 
my $resp = $ua->request($req);

if ($resp->is_success) {
    my $message = $resp->decoded_content;
    print "Received reply: $message\n";
}
else {
  print $resp->decoded_content;
    print "HTTP POST error code: ", $resp->code, "\n";
    print "HTTP POST error message: ", $resp->message, "\n";
}
