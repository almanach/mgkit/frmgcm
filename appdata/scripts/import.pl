 #!/usr/bin/perl

use strict;
use warnings;
use AppConfig;

use Cwd 'abs_path';

my $config = AppConfig->new('format=s',
                              'folder=f',
                              'corpus_name=s');

$config->args;

my $directory = abs_path($config->folder);
my $corpus_name = $config->corpus_name;

my $format = $config->format;
#print $directory;

#opendir (DIR, $directory) or die $!;

my $json = '{"docs":[';
$json.= '{"path":"file:///'.$directory.'","format":"'.$format.'","saveLocalCopy":false,"autoIndex":true}';
=pod  
while (my $file = readdir(DIR)) {
  if($file =~ /(.*?)\.tar\.gz$/){
    $json .= '{"path":"'.$directory.'/'.$file.'","format":"'.$format.'"},';
  }
  
}

$json = substr($json,0,length($json)-1);
=cut
$json .= ']}';

#closedir(DIR);


use LWP::UserAgent;
 
my $ua = LWP::UserAgent->new;
 
my $server_endpoint = "http://localhost:8082/sc/".$corpus_name."/_add";
 
my $req = HTTP::Request->new(POST => $server_endpoint);
$req->header('content-type' => 'application/json');
 
my $post_data = $json;
$req->content($post_data);
 
my $resp = $ua->request($req);
