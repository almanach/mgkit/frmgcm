#!/bin/sh
running_instance=$(ps aux | grep '[c]pm.jar' | awk '{print $2}')
# if running_instance is empty do not kill
echo $running_instance
kill -9 $(ps aux | grep '[c]pm.jar' | awk '{print $2}')

config_file="../cpm.config"
java=java
jar="../../build/cpm.jar"
nohup $java -server -cp $jar fr.inria.alpage.frmgcm.AppMain $config_file > ~/var/cpm/nohuplog.out 2>&1&
