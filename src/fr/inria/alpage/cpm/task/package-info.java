/**
 * This package encompass classes related to task processing. It is used for handling corpus processing.
 */
package fr.inria.alpage.cpm.task;