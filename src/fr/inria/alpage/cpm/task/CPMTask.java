package fr.inria.alpage.cpm.task;

import java.util.ArrayList;
import java.util.Iterator;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.util.JSON;

import fr.inria.alpage.frmgcm.db.CPMDB;
import fr.inria.alpage.frmgcm.utils.Utils;

/**
 * This class defines a list of subprocess {@see TaskUnit}
 * @author buiquang
 *
 */
public class CPMTask extends Thread{
  /**
   * the list of processes defined in this task
   */
  public ArrayList<CPMTaskUnit> processes = null;
  /**
   * The id of the task
   */
  public String id;
  /**
   * The name of the task
   */
  public String name;
  /**
   * The json db object reprenting this task
   */
  public BasicDBObject taskObject = null;
  
  /**
   * Constructor : create a new task
   */
  public CPMTask(){
    super("CPM Task");
    this.taskObject = new BasicDBObject();
    DBCollection collection = CPMDB.GetInstance().db.getCollection("tasks");
    collection.insert(taskObject);
    ObjectId id = (ObjectId)taskObject.get( "_id" );
    this.id = id.toString();
  }
  
  /**
   * Constructor : retrieve a task from an id
   * @param id
   */
  public CPMTask(String id){
    DBCollection coll = CPMDB.GetInstance().db.getCollection("tasks");
    BasicDBObject q = new BasicDBObject("_id",new ObjectId(id));
    this.taskObject = (BasicDBObject) coll.findOne(q);
    this.id = id;
  }
  
  /**
   * add a task unit to the list of processes defined in this task
   * @param tu the TaskUnit defining the new process to append to the task
   */
  public void addTaskUnit(CPMTaskUnit tu){
    if(this.processes == null){
      this.processes = new ArrayList<CPMTaskUnit>();
    }
    this.processes.add(tu);
    BasicDBList list = (BasicDBList) taskObject.get("tasks");
    if(list==null){
      list = new BasicDBList();
    }
    tu.index = list.size();
    list.add(tu.taskUnitObject);
    taskObject.put("tasks", list);
    save();

    
  }
  
  /**
   * Save modification to the task in db
   */
  private void save(){
    DBCollection coll = CPMDB.GetInstance().db.getCollection("tasks");
    coll.save(this.taskObject);
  }
  
  /**
   * run the task. check if not already running. skip finished task.
   */
  public void run(){
    if(isRunning()){
      Utils.Log("Task ".concat(id).concat(" already running"));
      return;
    }else{
      setRunningStatus(true);
      // reset the task running status to false when exiting the server
      Runtime.getRuntime().addShutdownHook(new Thread(){
          @Override
          public void run(){
            setRunningStatus(false);
          }
      });
    }
    Utils.Log("Launching task ".concat(id));
    Iterator<CPMTaskUnit> it = this.processes.iterator();
    boolean forceReset = false;
    while(it.hasNext()){
      CPMTaskUnit process = it.next();
      if(process.getTaskObject().getString("status").equals("0") && !forceReset){
        Utils.Log("Skipping process ".concat(process.getName()));
        continue;
      }
      forceReset = true; // the first process launched force the reset of all subsequent process
      Utils.Log("Launching process ".concat(process.getName()));
      process.start();
      try {
        process.join();
        Utils.Log("Finished process ".concat(process.getName()));
      } catch (InterruptedException e) {
        process.updateStatus("1");
        e.printStackTrace();
      }
    }
    
    setRunningStatus(false);
  }
  
  /**
   * 
   * @param data a json serialized representation of task defining information, namely :
   * - id : the id of the task,
   * - task : the index of the taskunit (optional)
   * - action : the kind of information wanted ("status"[default], "info", "fullinfo", "output" and "stderr") 
   * @return a json serialized representation of information about the task :
   * - info : returns the db representation of the task
   * - fullinfo : returns the name, the outputs and status a taskunit
   * - status : returns the status a taskunit
   * - output : return the output of a taskunit
   * - stderr : return the stderr of a taskunit 
   */
  public static String getInfo(String data){
    Utils.Log("Retrieving Task");
    BasicDBObject taskInfo = (BasicDBObject) JSON.parse(data);
    
    
    String action = taskInfo.getString("action");
    if(action == null){
    	action = "status";
    }
    
    
    String output = "";

    DBCollection coll = CPMDB.GetInstance().db.getCollection("tasks");

    BasicDBObject q = new BasicDBObject("_id",new ObjectId((String)taskInfo.get("id")));

    DBCursor curs = coll.find(q);
    
    BasicDBList response = new BasicDBList();

    if(curs.hasNext()){
      BasicDBObject taskObject = (BasicDBObject) curs.next();
      
      if(action.equals("info")){
        return JSON.serialize(taskObject);
      }
      
      BasicDBList list = (BasicDBList) taskObject.get("tasks");
      String task_id_obj = (String)taskInfo.get("task");
      int task_id = -1;
      if(task_id_obj != null){
        task_id = Integer.parseInt(task_id_obj);
      }
      Iterator<Object> it = list.iterator();
      int task_index = -1;
      while(it.hasNext()){
        BasicDBObject process = (BasicDBObject) it.next();
        task_index++;
        if(task_index != task_id && task_id != -1){
          continue;
        }
        
        BasicDBObject unitResponse = new BasicDBObject();
        
        if(action.equals("status")){
            Object status = process.get("status");
            if(status != null){
              unitResponse.put("status", (String)status);
            }else{
              unitResponse.put("status", (String)"unknown");
            }
        }else if(action.equals("output")){        	
          unitResponse.put("output", CPMTaskUnit.getOutput(process));
        }else if(action.equals("stderr")){
          unitResponse.put("output", CPMTaskUnit.getErr(process));
        }else if(action.equals("fullinfo")){
          Object status = process.get("status");
          if(status != null){
            unitResponse.put("status", (String)status);
          }else{
            unitResponse.put("status", (String)"unknown");
          }
          unitResponse.put("stdout", CPMTaskUnit.getOutput(process));
          unitResponse.put("stderr", CPMTaskUnit.getErr(process));
          unitResponse.put("name", process.get("name"));
        }
        
        response.add(unitResponse);
        
      }
      
    }
    output = JSON.serialize(response);
    return output;
  }
  
  /**
   * Create or update a task with a json object definition/representation of a task
   * @param taskObject the json db representation of a task
   * @return the task as defined by the taskObject argument
   */
  public static CPMTask parseData(BasicDBObject taskObject){
    Utils.Log("Creating Task",3);
    BasicDBList list = (BasicDBList) taskObject.get("tasks");

    DBCollection collection = CPMDB.GetInstance().db.getCollection("tasks");
    if(taskObject.get("_id")==null){
      collection.insert(taskObject);
    }
    ObjectId id = (ObjectId)taskObject.get( "_id" );
    CPMTask task = new CPMTask(id.toString());
    
    
    task.processes = new ArrayList<CPMTaskUnit>();
    Iterator<Object> it = list.iterator();
    int index = 0;
    while(it.hasNext()){
      BasicDBObject obj = (BasicDBObject) it.next();
      CPMTaskUnit process = CPMTaskUnit.create(obj,id,index);
      index++;
      task.processes.add(process);
    }
    
    collection.save(taskObject);
    return task;
  }
  
  /**
   * Create or update a task with a serialized json object definition/representation of a task
   * @param taskObject the serialized json db representation of a task
   * @return the task as defined by the data argument
   */
  public static CPMTask parseData(String data){
    
    
    BasicDBObject taskObject = (BasicDBObject) JSON.parse(data);
    
    
    return parseData(taskObject);
  }
  
  /**
   * checks in db if the task is currently running
   * @return true if the task is running, else false
   */
  public boolean isRunning(){
    DBCollection coll = CPMDB.GetInstance().db.getCollection("tasks");
    BasicDBObject q = new BasicDBObject("_id",new ObjectId(id));
    this.taskObject = (BasicDBObject) coll.findOne(q);
    return this.taskObject.getBoolean("running");
  }
  
  /**
   * set the running status of the task in db
   * @param running the running status value (true or false)
   */
  public void setRunningStatus(boolean running){
    DBCollection coll = CPMDB.GetInstance().db.getCollection("tasks");
    BasicDBObject q = new BasicDBObject("_id",new ObjectId(id));
    this.taskObject = (BasicDBObject) coll.findOne(q);
    this.taskObject.put("running", running);
    this.save();
  }

  
}
