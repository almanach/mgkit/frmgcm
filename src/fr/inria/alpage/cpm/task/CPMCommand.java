package fr.inria.alpage.cpm.task;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;

import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import java.util.UUID;

import com.mongodb.BasicDBObject;


import fr.inria.alpage.frmgcm.utils.Utils;

/**
 * This class is used to execute bash command as a task unit 
 * @author buiquang
 *
 */
public class CPMCommand extends CPMTaskUnit{
  /**
   * The bash command to be executed
   */
  public String command = null;
  /**
   * The json db object representing this task unit 
   */
  public BasicDBObject dbobject = null;
  
  /**
   * Constructor
   * @param taskId the reference id to the task containing this task unit
   * @param obj the json object defining this task unit (must contain "name" and "command" attributes)
   */
  public CPMCommand(String taskId, BasicDBObject obj) {
    super(taskId);
    this.dbobject = obj;
    this.setName((String)obj.getString("name"));
    this.command = (String) obj.get("command");
  }

  @Override
  public void run() {
    try {
      
      String scriptfile = exportToScript(this.command);
      Process process = Runtime.getRuntime().exec("sh ".concat(scriptfile));
      this.updateStatus("running");
      process.waitFor();
      this.updateStatus(String.valueOf(process.exitValue()));
	    new File(scriptfile).delete();
	    Utils.Log("Deleted temporary script file",3);
      BufferedReader out = new BufferedReader(new InputStreamReader(process.getInputStream()));
      BufferedReader err = new BufferedReader(new InputStreamReader(process.getErrorStream()));
      this.writeOutput(out,err);

      
    } catch (Exception e) {
      System.err.println("processing subtask ".concat(this.getName()).concat(" failed"));
      e.printStackTrace();
    }
    return;
  }

  /**
   * Create a script file in /tmp containing a bash command 
   * @param command a bash command
   * @return the filepath of the script containing the command
   */
  public static String exportToScript(String command){
    UUID uid = UUID.randomUUID();
    String filename = "/tmp/"+uid.toString();
    PrintWriter writer =null;
    try {
      writer = new PrintWriter(filename, "UTF-8");
      writer.println("#!/bin/bash");
      writer.println(command);
      writer.close();
    } catch (FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (UnsupportedEncodingException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return filename;
  }
  

 
}
