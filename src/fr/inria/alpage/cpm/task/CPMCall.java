package fr.inria.alpage.cpm.task;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;

import com.jcraft.jsch.JSchException;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;

import fr.inria.alpage.frmgcm.corpus.Corpus;
import fr.inria.alpage.frmgcm.utils.SSH;
import fr.inria.alpage.frmgcm.utils.Utils;

/**
 * This class is used to call java method as a task unit 
 * @author buiquang
 *
 */
public class CPMCall extends CPMTaskUnit{

  /**
   * Corpus id reference
   */
  public String refObjectId = null;
  /**
   * Method name to be called (belongs to the class {@see Corpus}
   */
  public String function = null;
  /**
   * List of arguments to be passed to the function (optional)
   */
  public ArrayList<Object> args = null;
  
  /**
   * Constructor
   * @param taskId the task id {@see CPMTask} in which this task unit belongs to 
   * @param corpus_ref the corpus id {@see fr.inria.alpage.frmgcm.corpus.Corpus} over which the taskunit will process
   * @param method the java method name (declared in the class Corpus or in this class) which will be called
   * @param args optional array of arguments passed to the method
   */
  public CPMCall(String taskId,String corpus_ref,String method,ArrayList<Object> args){
    super(taskId);
    this.refObjectId = corpus_ref;
    this.function = method;
    if(args == null){
      args = new ArrayList<Object>();
    }
    this.args = args;
    this.taskUnitObject.put("refObjectId", corpus_ref);
    this.taskUnitObject.put("name", method);
    this.taskUnitObject.put("type", "call");
    this.taskUnitObject.put("command", method);
    BasicDBList dbargs = new BasicDBList();
    dbargs.addAll(args);
    this.taskUnitObject.put("args", dbargs);
  }
  
  /**
   * Constructor (passing definition as a jsonobject)
   * @param taskId the task id {@see CPMTask} in which this task unit belongs to
   * @param obj the object defining the taskunit. Must contain the properties "command" and "refObjectId" (if command belongs to the class {@see Corpus})
   */
  public CPMCall(String taskId,BasicDBObject obj) {
    super(taskId);
    String name = (String)obj.getString("name");
    if(name == null){
      name = "undefined";
    }
    this.setName(name);
    this.args = new ArrayList<Object>();
    Object raw_args = obj.get("args");
    BasicDBList args = (BasicDBList) raw_args;
    Iterator<Object> it = args.iterator();
    while(it.hasNext()){
      String arg = (String)it.next();
      this.args.add(arg);
    }
    this.function = (String) obj.get("command");
    this.refObjectId = obj.getString("refObjectId");
  }

  @SuppressWarnings({ "rawtypes" })
  @Override
  public void run() {
    String scriptName = this.function;
    int argc = this.args.size();
    Class[] args = new Class[argc];
    for(int i =0; i< argc ; i++){
      args[i] = String.class;
    }
    Utils.Log("Running "+scriptName,3);
    Method method;
    try {
      Object object = null;
      if(this.refObjectId== null){
        method = CPMCall.class.getMethod(scriptName, args);
        object = this;
      }else{
        method = Corpus.class.getMethod(scriptName, args);
        object = Corpus.Retrieve(this.refObjectId);
      }
      
      try {
        this.updateStatus("running");
        String[] output = (String[]) method.invoke(object, this.args.toArray());
        this.writeOutput(output[0], output[1]);
        this.updateStatus("0");
      } catch (Exception e) {
        this.updateStatus("1");
        e.printStackTrace();
      } 
    } catch (NoSuchMethodException e) {
      this.updateStatus("1");
      e.printStackTrace();
    } catch (SecurityException e) {
      this.updateStatus("1");
      e.printStackTrace();
    }
    
    return;
  }

 
  
  
  //////////////////////////////////////////////////////////////////////////////////////////////////
  //                                     S C R I P T S                                            //
  //////////////////////////////////////////////////////////////////////////////////////////////////
  
  public String[] riocExec(String command){
    SSH ssh = new SSH(this);
    try {
      ssh.exec(command);
    } catch (JSchException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return new String[]{"",""};
  }

  
  public String[] convertAllFileInDir(String dir) throws Exception{
    ArrayList<String> args = new ArrayList<String>();
    args.add(dir);
    Utils.mapReduceFolder(new File(dir.concat("/uncompressed")), new Corpus.MapReduceFolderConvert(), args);
    ArrayList<Object[]> processes = new ArrayList<Object[]>();
    Iterator<String> it = args.iterator();
    while(it.hasNext()){
      String command = (String) it.next();
      String scriptfile = CPMCommand.exportToScript(command);
      Process process = null;
      try {
        process = Runtime.getRuntime().exec("sh ".concat(scriptfile));
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
      Object[] pbundle = new Object[2];
      pbundle[0] = process;
      pbundle[1] = scriptfile;
      processes.add(pbundle);
      
      
    }
    
    Iterator<Object[]> pit = processes.iterator();
    while(pit.hasNext()){
      Object[] pbundle = (Object[]) pit.next();
      Process p = (Process) pbundle[0];
      String scriptfile = (String) pbundle[1];
      try {
        p.waitFor();
        BufferedReader out = new BufferedReader(new InputStreamReader(p.getInputStream()));
        BufferedReader err = new BufferedReader(new InputStreamReader(p.getErrorStream()));
        this.writeOutput(out,err);
      } catch (InterruptedException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
      new File(scriptfile).delete();
      Utils.Log("Deleted temporary script file",3);
    }

    return new String[]{"",""};
  }
  
  

  

  

}
