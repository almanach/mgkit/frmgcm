package fr.inria.alpage.cpm.task;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.UUID;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;


import fr.inria.alpage.frmgcm.Configuration;
import fr.inria.alpage.frmgcm.db.CPMDB;
import fr.inria.alpage.frmgcm.utils.Utils;

/**
 * Abstract class used to define a task unit that will be process as part of a {@see fr.inria.alpage.cpm.task.Task}
 * @author buiquang
 *
 */
public abstract class CPMTaskUnit extends Thread{
  /**
   * Reference to the id of the task that contains this task unit
   */
  public String taskId = null;
  /**
   * Index of this task unit in the global task it belongs to
   */
  public int index;
  /**
   * JSON object storing information about this task unit (namely : 
   * - stdout : the path to the file where the standard output of the task will be written
   * - stderr : the path to the file where the standard error of the task will be written
   * - status : the status of the task ("waiting", "running" or the result of the process (0 => ok))
   */
  public BasicDBObject taskUnitObject = null;
  
  /**
   * Abstract Constructor
   * @param id the id of the task this task unit belongs to
   */
  protected CPMTaskUnit(String id){
    taskUnitObject = new BasicDBObject();
    taskUnitObject.put("status", "waiting");
    
    UUID uid = UUID.randomUUID();
    File file = new File(Configuration.appHome+"/outputs/"+id);
    if(!file.exists()){
      file.mkdir();
    }
    String errfilename = Configuration.appHome+"/outputs/"+id+"/stderr_".concat(uid.toString());
    String outfilename = Configuration.appHome+"/outputs/"+id+"/stdout_".concat(uid.toString());
    taskUnitObject.put("stdout", outfilename);
    taskUnitObject.put("stderr", errfilename);
    this.taskId = id;
  }

  /**
   * 
   * @return the status of the task (found in db object at property "status")
   */
  public String getStatus(){
    DBCollection coll = CPMDB.GetInstance().db.getCollection("tasks");
    BasicDBObject q = new BasicDBObject("_id",this.taskId);
    DBCursor curs = coll.find(q);
    if(curs.hasNext()){
      BasicDBObject task = (BasicDBObject) curs.next();
      BasicDBList list = (BasicDBList) task.get("tasks");
      BasicDBObject process = (BasicDBObject) list.get(this.index);
      Object status = process.get("status");
      if(status != null){
        return (String) status;
      }else{
        return "unknown";
      }
    }
    return "unknown";
  }
  
  /**
   * Task unit factory
   * @param obj the definition object of the task unit
   * @param id the ObjectId of the task which this task unit belongs to
   * @param index the index of the task unit within its task
   * @return a newly created task unit instance
   */
  public static CPMTaskUnit create(BasicDBObject obj,ObjectId id,int index) {
    Utils.Log("Creating process",3);
    CPMTaskUnit process = null;
    // create the folder containing the file used for writing the output of tasks
    if(!obj.containsField("stdout")){
      obj.put("status", "waiting");
      
      UUID uid = UUID.randomUUID();
      File file = new File(Configuration.appHome+"/outputs/"+id.toString());
      if(!file.exists()){
        file.mkdir();
      }
      String errfilename = Configuration.appHome+"/outputs/"+id.toString()+"/stderr_".concat(uid.toString());
      String outfilename = Configuration.appHome+"/outputs/"+id.toString()+"/stdout_".concat(uid.toString());
      obj.put("stdout", outfilename);
      obj.put("stderr", errfilename);
    }
    
    if(obj.get("type").equals("command")){
      process = new CPMCommand(id.toString(),obj);
    }else{
      process = new CPMCall(id.toString(),obj);
    }
    process.index = index;
    return process;
  }
  
  /**
   * 
   * @param taskInfo the db json object representing a taskunit
   * @return the output of the task unit
   */
  public static String getOutput(BasicDBObject taskInfo){
    
    String outFilename = taskInfo.getString("stdout");
    if(!(new File(outFilename).exists())){
      return "cpm_stdout_not_found";
    }
    if(!taskInfo.getString("status").equals("0")){
      return "not finished";
    }
    StringBuilder sb = new StringBuilder();
    try {
      BufferedReader out = new BufferedReader(new FileReader(outFilename));
      String line = null;
      try {
        while((line = out.readLine())!=null){
          sb.append(line.concat("\n"));
        }
        out.close();
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
      return sb.toString().trim();
    } catch (FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return "error reading output";
  }
  
  /**
   * 
   * @param taskInfo the db json object representing a taskunit
   * @return the error output of the task unit
   */
  public static String getErr(BasicDBObject taskInfo){
    
    String outFilename = taskInfo.getString("stderr");
    if(!(new File(outFilename).exists())){
      return "cpm_stderr_not_found";
    }
    if(!taskInfo.getString("status").equals("0")){
      return "not finished";
    }
    StringBuilder sb = new StringBuilder();
    try {
      BufferedReader out = new BufferedReader(new FileReader(outFilename));
      String line = null;
      try {
        while((line = out.readLine())!=null){
          sb.append(line.concat("\n"));
        }
        out.close();
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
      return sb.toString().trim();
    } catch (FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return "error reading output";
  }

  /**
   * write the output (error and normal) from buffered readers to string buffers 
   * @param outBuffer The buffer reader containing the standard output
   * @param errBuffer The buffer reader containing the error output
   * @param outString The string buffer where the standard output will be written
   * @param errString The string buffer where the error output will be written
   */
  public static void writeOutput(BufferedReader outBuffer, BufferedReader errBuffer,StringBuffer outString, StringBuffer errString){
    String inputline = null;
    try {
      while((inputline = outBuffer.readLine())!=null){
        outString.append(inputline);
        outString.append("\n");
      }
    } catch (IOException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
    try {
      while((inputline = errBuffer.readLine())!=null){
        errString.append(inputline);
        errString.append("\n");
      }
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }
  
  /**
   * write the output (error and normal) from buffered readers to files
   * @param outBuffer The buffer reader containing the standard output
   * @param errBuffer The buffer reader containing the error output
   * @param outFilename The path of the file in which to write the standard output
   * @param errFilename The path of the file in which to write the error output
   */
  public static void writeOutput(BufferedReader outBuffer, BufferedReader errBuffer,String outFilename, String errFilename){
    PrintWriter writer =null;
    String inputline = null;
    try {
      writer = new PrintWriter(new FileWriter(outFilename,true));
      while((inputline = outBuffer.readLine())!=null){
        writer.println(inputline);
      }
      outBuffer.close();
    } catch (IOException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    } finally{
      writer.close();
    }
    try {
      writer = new PrintWriter(new FileWriter(errFilename, true));
      while((inputline = errBuffer.readLine())!=null){
        writer.println(inputline);
      }
      errBuffer.close();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } finally{
      writer.close();
    }
  }
  
  /**
   * write the outputs of the taskunit to the file defined by the properties "stderr" and "stdout"
   * @param outBuffer The buffer reader containing the standard output
   * @param errBuffer The buffer reader containing the error output
   */
  public void writeOutput(BufferedReader outBuffer, BufferedReader errBuffer){
    BasicDBObject taskInfo = getTaskObject();
  	String errFilename = taskInfo.getString("stderr");
  	String outFilename = taskInfo.getString("stdout");
  	CPMTaskUnit.writeOutput(outBuffer, errBuffer,outFilename, errFilename);
  }
  
  /**
   * write the outputs of the taskunit to the file defined by the properties "stderr" and "stdout"
   * @param out The string containing the standard output
   * @param err The string containing the error output
   */
  public void writeOutput(String out, String err){
    BasicDBObject taskInfo = getTaskObject();
    String errFilename = taskInfo.getString("stderr");
    String outFilename = taskInfo.getString("stdout");
    PrintWriter writer =null;
    try {
      writer = new PrintWriter(new FileWriter(outFilename, true));
      writer.println(out);
    } catch (IOException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
    writer.close();
    try {
      writer = new PrintWriter(new FileWriter(errFilename, true));
      writer.println(err);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }
  
  /**
   * Update in database the status of the process defined by this task unit
   * @param status
   */
  public void updateStatus(String status){
    DBCollection coll = CPMDB.GetInstance().db.getCollection("tasks");
    BasicDBObject q = new BasicDBObject("_id",new ObjectId(this.taskId));
    BasicDBObject task = (BasicDBObject)coll.findOne(q);
    BasicDBList list = (BasicDBList) task.get("tasks");
    BasicDBObject process = (BasicDBObject) list.get(this.index);
    process.put("status", status);
    coll.save(task);
  }
  
  /**
   * Get the json db object representing this task unit in db
   * @return
   */
  public BasicDBObject getTaskObject(){
    DBCollection coll = CPMDB.GetInstance().db.getCollection("tasks");
    BasicDBObject q = new BasicDBObject("_id",new ObjectId(this.taskId));
    BasicDBObject task = (BasicDBObject)coll.findOne(q);
    BasicDBList list = (BasicDBList) task.get("tasks");
    BasicDBObject process = (BasicDBObject) list.get(this.index);
    return process;
  }

  
}
