/* Generated By:JJTree: Do not edit this line. ASTCoordExpression.java Version 6.0 */
/* JavaCCOptions:MULTI=true,NODE_USES_PARSER=false,VISITOR=false,TRACK_TOKENS=false,NODE_PREFIX=AST,NODE_EXTENDS=,NODE_FACTORY=,SUPPORT_CLASS_VISIBILITY_PUBLIC=true */
package fr.inria.alpage.frmgcm.dpp;

import java.util.ArrayList;

public
class ASTCoordExpression extends SimpleNode {
  public ASTCoordExpression(int id) {
    super(id);
  }

  public ASTCoordExpression(DPathParser p, int id) {
    super(p, id);
  }
  
  public ArrayList<String> coords = new ArrayList<String>();


  public void addCoord(String image) {
    coords.add(image);
    
  }
  
  public String toString() {
    String coordNames = "";
    for(String coordName :coords){
      coordNames += coordName + " ";
    }
    return DPathParserTreeConstants.jjtNodeName[id]+ " : "+coordNames;
  }

}
/* JavaCC - OriginalChecksum=37d1d4a9851f4aa3efd51a20b3aa3ad0 (do not edit this line) */
