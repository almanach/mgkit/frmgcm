package fr.inria.alpage.frmgcm.dpp;
/* Generated By:JJTree: Do not edit this line. ASTDPathNavigation.java Version 6.0 */
/* JavaCCOptions:MULTI=true,NODE_USES_PARSER=false,VISITOR=false,TRACK_TOKENS=false,NODE_PREFIX=AST,NODE_EXTENDS=,NODE_FACTORY=,SUPPORT_CLASS_VISIBILITY_PUBLIC=true */
public
class ASTDPathNavigation extends SimpleNode {
  public ASTDPathNavigation(int id) {
    super(id);
  }

  public ASTDPathNavigation(DPathParser p, int id) {
    super(p, id);
  }
  
public String name ;
  
  public void setName(String image) {
    name = image;
    
  }
  
  public String toString() {
    return DPathParserTreeConstants.jjtNodeName[id]+ " : "+name;
  }

}
/* JavaCC - OriginalChecksum=548682bec4151cfe1c66101e7f02cc98 (do not edit this line) */
