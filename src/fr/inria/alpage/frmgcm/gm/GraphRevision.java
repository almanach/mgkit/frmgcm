package fr.inria.alpage.frmgcm.gm;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.ReflectionDBObject;

import fr.inria.alpage.frmgcm.corpus.format.AbstractGraph;
import fr.inria.alpage.frmgcm.db.CPMDB;
import fr.inria.alpage.frmgcm.utils.Utils;
import fr.inria.alpage.frmgcm.utils.Utils.URI.URI_TYPE;

/**
 * This class handles revisions of {@see Graph} instances, reflects with mongo db object
 * @author buiquang
 *
 */
public class GraphRevision extends ReflectionDBObject {

  /**
   * The optional (if uri is provided) binary data content representing the graph structure 
   */
  private byte[] data;
  /**
   * A json object used to store additional_data such as the sentence string or frmg parser options
   */
  public BasicDBObject additional_data;
  /**
   * The optional (if data is provided) uri (file:/// or http://) locating the graph revision data
   */
  private String uri;

  public BasicDBObject getadditional_data() {
    return additional_data;
  }

  public void setadditional_data(BasicDBObject additional_data) {
    this.additional_data = additional_data;
  }

  public String geturi() {
    return uri;
  }

  public void seturi(String uri) {
    this.uri = uri;
  }

  public void setdata(byte[] data) {
    this.data = data;
  }
  
  public byte[] getdata() {
    return data;
  }

  
  public GraphRevision(){
    super();
  }
  
  /**
   * Construct a new graph revision from a definition
   * @param definition the json object defining the revision, must contain either data or uri
   */
  public GraphRevision(BasicDBObject definition){
    String datastr = definition.getString("data");
    if(datastr != null){
      this.data = datastr.getBytes();
    }
    this.uri = definition.getString("uri");
    this.additional_data = (BasicDBObject)definition.get("additional_data");
  }
  
  /**
   * Update the graph revision from a definition
   * @param def the json object defining the new data for the revision, must contain either data or uri
   */
  public void update(BasicDBObject def){
    String datastr = def.getString("data");
    if(datastr != null){
      this.data = datastr.getBytes();
    }
    this.uri = def.getString("uri");
    this.additional_data = (BasicDBObject)def.get("additional_data");
    this.save();
  }
  
  /**
   * Validate the content of the graph revision (make sure there is either data or uri not null)
   * @return a validation object
   */
  private Utils.ValidationBean validate(){
    Utils.ValidationBean validation = new Utils.ValidationBean();
    if(this.data != null || this.uri != null){
      validation.success = true;
    }
    return validation;
  }

  /**
   * Retrieve the data that define the structure of the graph
   * @return the raw data that define the structure of the graph
   */
  public String getData(){
    if(this.data!=null){
      return new String(this.data);
    }else{
      String data = fetchRemoteData();
      return data;
    }
  }
  
  /**
   * Retrieve the data that is to be found at the uri location defined for this graph revision
   * @return the data that define the structure of the graph
   */
  private String fetchRemoteData(){
    Utils.URI uri = new Utils.URI(this.uri);
    if(uri.type == URI_TYPE.FILEPATH){
      return Utils.fetchFromFile(uri);
    }else if(uri.type == URI_TYPE.URL){
      return Utils.fetchFromURL(uri);
    }
    return null;
  }

  /**
   * Save the graph revision in mongo db
   * @return a boolean indicating if operation was a success
   */
  public boolean save(){
    Utils.ValidationBean valid = this.validate();
    if(valid.success){
      DBCollection coll = CPMDB.GetInstance().db.getCollection(GraphManager.graphRevisionCollection);
      coll.setObjectClass(this.getClass());
      coll.save(this);
    }
    return valid.success;
  }

  /**
   * Operate a diff between the data of this graph revision and newly provided data
   * @param updateData the new data to operate the diff with
   * @todo implement this using full signature diff (implemented in mgwiki php code)
   * @return a boolean indicating if (true) there is a difference between the two data or not (false)
   */
  public boolean diff(BasicDBObject updateData) {
    //@todo
    return true;
  }
}
