package fr.inria.alpage.frmgcm.gm;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;

import fr.inria.alpage.frmgcm.corpus.Corpus;
import fr.inria.alpage.frmgcm.db.CPMDB;
import fr.inria.alpage.frmgcm.utils.Utils;
import fr.inria.alpage.frmgcm.utils.Utils.ValidationBean;

/**
 * This class handles most of crud operation in graphs stored in mongo db
 * @author buiquang
 *
 */
public class GraphManager {
  /**
   * The graph mongo db collection name
   */
  public final static String graphCollection = "graphs";
  /**
   * The graph revision mongo db collection name
   */
  public final static String graphRevisionCollection = "graph_revisions";
  
  /**
   * Create a graph from a definition 
   * @param definition the graph defintion json object (see {@see Graph} for defintion fields)
   * @return the newly created graph
   */
  public static Graph createGraph(BasicDBObject definition){
    Graph graph = Graph.createNewGraph(definition);
    if(graph.save()){
      return graph;
    }
    return null;
  }
  
  /**
   * Update a graph 
   * @param uid the id of the graph
   * @param updateInfo the update information json object (similar to the one used to define a graph during its creation, see {@see Graph} )
   * @param createIfNotFound a boolean indicating whether a newly graph should be created if none was found with the corresponding uid
   * @return a validation bean indicating if the operation went well
   */
  public static Utils.ValidationBean updateGraph(String uid,BasicDBObject updateInfo,boolean createIfNotFound){
    Graph graph = getGraph(uid);
    Utils.ValidationBean ok = new ValidationBean();
    if(graph == null){
      if(createIfNotFound){
        graph = createGraph(updateInfo);
        ok.success = graph.save();
      }else{
        ok.success = false;
        ok.message = "graph not found";
      }
    }else{
      ok = graph.update(updateInfo); 
    }
    return ok;
  }
  
  /**
   * delete a graph
   * @param uid the uid of the graph to delete
   */
  public static void deleteGraph(String uid){
    DBCollection coll = CPMDB.GetInstance().db.getCollection(graphCollection);
    coll.setObjectClass(Graph.class);
    BasicDBObject q = new BasicDBObject("_id",new ObjectId(uid));
    Graph graph = (Graph) coll.findOne(q);
    graph.delete();
  }
  
  /**
   * Retrieve a graph from a filter definition
   * @param definition the filter to match the graph
   * @return the first graph found that match the filter definition
   */
  public static Graph getGraph(BasicDBObject definition){
    DBCollection coll = CPMDB.GetInstance().db.getCollection(graphCollection);
    coll.setObjectClass(Graph.class);
    BasicDBObject q = definition;
    Graph graph = (Graph) coll.findOne(q);
    return graph;
  }
  
  /**
   * Retrieve a graph from an id
   * @param uid the id of the graph
   * @return the graph with the corresponding id or null if not found
   */
  public static Graph getGraph(String uid){
    DBCollection coll = CPMDB.GetInstance().db.getCollection(graphCollection);
    coll.setObjectClass(Graph.class);
    BasicDBObject q = new BasicDBObject("_id",new ObjectId(uid));
    Graph graph = (Graph) coll.findOne(q);
    return graph;
  }

}
