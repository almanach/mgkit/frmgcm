package fr.inria.alpage.frmgcm.gm;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.ReflectionDBObject;

import fr.inria.alpage.frmgcm.corpus.Corpus;
import fr.inria.alpage.frmgcm.corpus.format.AbstractGraph;
import fr.inria.alpage.frmgcm.db.CPMDB;
import fr.inria.alpage.frmgcm.db.ElasticSearchProxy;
import fr.inria.alpage.frmgcm.utils.Utils;
import fr.inria.alpage.frmgcm.utils.Utils.ValidationBean;

/**
 * This class reflects graph object (part of the GraphServer project refactoring) stored in mongodb 
 * @author paul
 *
 */
public class Graph extends ReflectionDBObject {
  /**
   * The id of the sentence linked to the graph (currently not used)
   */
  public String sid;
  /**
   * The external id (relative to the collection owner of the graph), allow retrieving graph within a collection {@see SentenceCollection}
   */
  public String eid;
  /**
   * The unique id of the {@see SentenceCollection} the graph is bound to
   */
  public String cid;
  /**
   * The format of the graph, see {@see AbstractGraph} for available formats
   */
  public String format;
  /**
   * Boolean indicating if the graph has been indexed or not
   */
  public Boolean bIndexed;
  /**
   * The list of {@see GraphRevision} id that contain the actual data (or data pointer) of the graph
   */
  public BasicDBList revisions;
  /**
   * List of note objects (have two fields : name and data)
   */
  public BasicDBList notes;
  /**
   * List of vote objects (have two fields : name and vote)
   */
  public BasicDBList votes;
  
  public BasicDBList getnotes(){
    if(this.notes == null){
      this.notes = new BasicDBList();
    }
    return this.notes;
  }
  
  public void setnotes(BasicDBList notes){
    this.notes = notes;
  }
  
  public BasicDBList getvotes(){
    if(this.votes == null){
      this.votes = new BasicDBList();
    }
    return this.votes;
  }
  
  public void setvotes(BasicDBList votes){
    this.votes = votes;
  }
  
  public String getsid(){
    return sid;
  }
  
  public void setsid(String sid){
    this.sid = sid;
  }
  
  public String geteid() {
    return eid;
  }

  public void seteid(String eid) {
    this.eid = eid;
  }

  public String getcid() {
    return cid;
  }

  public void setcid(String cid) {
    this.cid = cid;
  }

  public String getformat() {
    return format;
  }

  public void setformat(String format) {
    this.format = format;
  }

  public boolean getbIndexed() {
    return bIndexed;
  }

  public void setbIndexed(boolean bIndexed) {
    this.bIndexed = bIndexed;
  }

  public BasicDBList getrevisions() {
    return revisions;
  }

  public void setrevisions(BasicDBList revisions) {
    this.revisions = revisions;
  }

  /**
   * Graph factory
   * @param definition the definition of the graph containing mandatory properties "eid", "cid" and "format"
   * @return the graph
   */
  public static Graph createNewGraph(BasicDBObject definition){
    Graph g = new Graph(definition);
    return g;
  }
  
  /**
   * Constructor used by mongo for db reflection
   */
  public Graph(){
    super();
  }
  
  /**
   * Construct graph and create the initial {@see GraphRevision) associated
   * @param definition
   */
  private Graph(BasicDBObject definition){
    this.bIndexed = false;
    this.format = definition.getString("format");
    this.sid = definition.getString("sid");
    this.eid = definition.getString("eid");
    this.cid = definition.getString("cid");
    this.revisions = new BasicDBList();
    createRevision(definition);
  }
  
  /**
   * Index the graph
   */
  public void index(){
    index(true);
  }
  
  /**
   * Index the graph
   * @param overwrite a boolean indicating if the previous indexation must be overwritten or not
   */
  public void index(boolean overwrite){
    if(!bIndexed || overwrite){
      int revisionHead = revisions.size()-1;
      try {
        AbstractGraph graph = getFormatedGraph();
        graph.indexSentence(cid, this.get_id().toString(), String.valueOf(revisionHead), bIndexed && overwrite);
        bIndexed = true;
        this.save();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }
 
  /**
   * Retrieve the data associated with the last revision of this graph
   * @return the data that define this graph structure
   */
  public String getData(){
    int revisionHead = revisions.size()-1;
    return getData(revisionHead);
  }
  
  /**
   * Retrieve the additional data associated with the last revision of this graph (ex frmg parser options, sentence string)
   * @return the additional data associated with the last revision of this graph
   */
  public BasicDBObject getAdditionalData(){
    int revisionHead = revisions.size()-1;
    GraphRevision lastRevision = (GraphRevision) getRevision(revisionHead);
    return lastRevision.additional_data;
  }
  
  /**
   * Return an instance of a subtype of {@see AbstractGraph} that handle the format of the last revision of this graph
   * @return the instance of a subtype of {@see AbstractGraph}
   * @throws Exception
   */
  private AbstractGraph getFormatedGraph() throws Exception{
    int revisionHead = revisions.size()-1;
    return getFormatedGraph(revisionHead);
  }
  
  /**
   * Return an instance of a subtype of {@see AbstractGraph} that handle the format of the selected revision of this graph
   * @param revision the revision number to create the {@see AbstractGraph} from
   * @return the instance of a subtype of {@see AbstractGraph}
   * @throws Exception
   */
  private AbstractGraph getFormatedGraph(int revision) throws Exception{
    GraphRevision lastRevision = (GraphRevision) getRevision(revision);
    AbstractGraph graph = AbstractGraph.Create(lastRevision, this.format);
    return graph;
  }
  
  /**
   * Retrieve the data in depgraph format (json data used directly by the depgraph visualisation library)
   * @return the json data representing this graph in the depgraph format
   * @throws Exception
   */
  public BasicDBObject getDepgraphSrcData() throws Exception{
    AbstractGraph graph = getFormatedGraph();
    return graph.getDepGraphData();
  }
  
  /**
   * Retrieve the data associated with the selected revision of this graph
   * @param revision the revision number to retrieve the data from
   * @return the data that define this graph structure
   */
  public String getData(int revision){
    GraphRevision lastRevision = (GraphRevision) getRevision(revision);
    String data = lastRevision.getData();
    if(format.equals("passage") || format.equals("depxml")){
      data = data.replace("&", "&amp;");
    }
    return data;
  }
  
  /**
   * Retrieve the sentence represented by this graph (from the graph structure or given data found in 'additional_data' field)
   * @return the sentence represented by this graph
   * @throws Exception
   */
  public String getSentence() throws Exception{
    int revisionHead = revisions.size()-1;
    GraphRevision lastRevision = (GraphRevision) getRevision(revisionHead);
    String sentence =  null;
    if(lastRevision.additional_data!=null){
      sentence = lastRevision.additional_data.getString("sentence");
    }
    if(sentence==null){
      AbstractGraph graph = getFormatedGraph();
      sentence = graph.getSentenceString();
    }
    
    return sentence;
  }
  
  /**
   * Create a new revision of this graph
   * @param dataInfo the data that will define the revision content
   * @return the new {@see GraphRevision} or null if couldn't be created
   */
  private GraphRevision createRevision(BasicDBObject dataInfo){
    GraphRevision newRevision = new GraphRevision(dataInfo);
    if(newRevision.save()){
      this.revisions.add(newRevision.get_id().toString());
      return newRevision;
    }
    return null;
  }
  
  /**
   * Update the graph with new data
   * @param updateData the new data to upgrade the graph with
   * @return a validation object
   */
  public Utils.ValidationBean update(BasicDBObject updateData){
    return update(updateData,false);
  }
  
  /**
   * Get the dpath signature of a graph
   * @todo implement method 'getSignature' in every formats
   * @return the dpath string signature of the graph
   */
  public String getSignature(){
    int revisionHead = revisions.size()-1;
    GraphRevision lastRevision = (GraphRevision) getRevision(revisionHead);
    AbstractGraph graph = AbstractGraph.Create(lastRevision, this.format);
    return graph.getSignature(lastRevision.additional_data.getString("highlighting"));
  }
  
  /**
   * Update the graph with new data
   * @param updateData the new data to update the graph from
   * @param createNewRevision boolean indicating if a new revision should be created with this update
   * @return a validation object
   */
  public Utils.ValidationBean update(BasicDBObject updateData,boolean createNewRevision){
    Utils.ValidationBean ok = new ValidationBean();
    int revisionHead = revisions.size()-1;
    GraphRevision lastRevision = (GraphRevision) getRevision(revisionHead);
    if(updateData.getString("uri") == null && (updateData.getString("data")==null || updateData.getString("data").trim().equals(""))){
      updateData.put("data", lastRevision.getData());
    }else if(updateData.getString("data") == null && (updateData.getString("uri")==null || updateData.getString("uri").trim().equals(""))){
      updateData.put("uri", lastRevision.geturi());
    }
    
    if(lastRevision.diff(updateData) || createNewRevision){
      GraphRevision newRevision = null;
      if(!createNewRevision){
        newRevision = lastRevision;
        newRevision.update(updateData);
      }else{
        newRevision = this.createRevision(updateData);
      }
      try {
        AbstractGraph.Create(newRevision, this.format).indexSentence(this.cid, this.get_id().toString(), String.valueOf(revisionHead), true);
      } catch (Exception e) {
        Utils.Log("error when indexing sentence");
        e.printStackTrace();
      }
      ok.success = true;
    }
    return ok;
  }
  
  /**
   * Get a selected revision of this graph
   * @param rid the revision number to retrieve
   * @return the graph revision or null if not found
   */
  public GraphRevision getRevision(int rid){
    DBCollection coll = CPMDB.GetInstance().db.getCollection(GraphManager.graphRevisionCollection);
    coll.setObjectClass(GraphRevision.class);
    if(revisions.size()>rid){
      return (GraphRevision)coll.findOne(new BasicDBObject("_id",new ObjectId(((String)revisions.get(rid)))));
    }else{
      return null;
    }
  }
  
  /**
   * Validate the content of the graph (must contain a cid, format and a working revision)
   * @return a validation object
   */
  private Utils.ValidationBean validate(){
    Utils.ValidationBean validation = new Utils.ValidationBean();
    if(this.cid != null && this.format != null && this.revisions.size()>0){
      validation.success = true;
    }
    return validation;
  }
  
  /**
   * Save this graph into db
   * @return a boolean indicating if the operation was a success
   */
  public boolean save(){
    Utils.ValidationBean valid = this.validate();
    if(valid.success){
      DBCollection coll = CPMDB.GetInstance().db.getCollection(GraphManager.graphCollection);
      coll.setObjectClass(Graph.class);
      coll.save(this);
    }
    return valid.success;
  }

  /**
   * Delete all revisions associated with this graph
   */
  public void deleteRevisions() {
    for(Object obj:revisions){
      String id = (String)obj;
      DBCollection coll = CPMDB.GetInstance().db.getCollection(GraphManager.graphRevisionCollection);
      coll.setObjectClass(GraphRevision.class);
      coll.remove(new BasicDBObject("_id",new ObjectId(id)));
    }
  }
  
  /**
   * Add a note to this graph
   * @param username the author of the note
   * @param content the content of the note
   */
  public void addNote(String username, String content){
    BasicDBList notes = this.getnotes();
    BasicDBObject note = new BasicDBObject();
    note.put("user", username);
    note.put("data", content);
    notes.add(note);
    this.save();
  }
  
  /**
   * remove a note
   * @param index the index of the note to remove
   */
  public void removeNote(Integer index){
    BasicDBList notes = this.getnotes();
    notes.remove(index);
    this.save();
  }
  
  /**
   * Add a vote
   * @param username the user rating the graph
   * @param val the value of the vote (positive or negative integer, 0 for unvoting)
   */
  public void vote(String username, Integer val){
    BasicDBList votes = this.getvotes();
    for(Object obj:votes){
      BasicDBObject vote = (BasicDBObject)obj;
      if(vote.getString("name").equals(username)){
        vote.put("val", val);
        this.save();
        return;
      }
    }
    BasicDBObject vote = new BasicDBObject();
    vote.put("name", username);
    vote.put("val", val);
    votes.add(vote);
    this.save();
  }

  /**
   * delete this graph from db (keeping it in the index, this is intended when this graph is deleted within collection deletion that already remove all its graph from index)
   */
  public void delete(){
    delete(false);
  }
  
  /**
   * delete this graph from db
   * @param removeFromIndex if set to true, also remove from index
   */
  public void delete(boolean removeFromIndex) {
    if(removeFromIndex){
      removeFromIndex();
    }
    deleteRevisions();
    DBCollection coll = CPMDB.GetInstance().db.getCollection(GraphManager.graphCollection);
    coll.setObjectClass(Graph.class);
    BasicDBObject q = new BasicDBObject("_id",new ObjectId(this.get_id().toString()));
    coll.remove(q);
  }
  
  /**
   * Remove this graph from elasticsearch index
   */
  public void removeFromIndex(){
    BasicDBObject query = new BasicDBObject();
    BasicDBObject queryInfo = new BasicDBObject();
    BasicDBObject did = new BasicDBObject("term",new BasicDBObject("did",this.cid));
    BasicDBObject sid = new BasicDBObject("term",new BasicDBObject("sid",this.get_id().toString()));
    BasicDBList match = new BasicDBList();
    match.add(did);
    match.add(sid);
    queryInfo.put("and", match);
    BasicDBObject filter = new BasicDBObject("filter", queryInfo);
    BasicDBObject const_score = new BasicDBObject("constant_score",filter);
    query.put("query", const_score);
    
    BasicDBObject result = null; 
    try {
      result = ElasticSearchProxy.search(query);
    } catch (IOException e) {
      e.printStackTrace();
    }
    
    BasicDBObject hitsInfo = (BasicDBObject)result.get("hits");
    if(result.get("error")!=null || hitsInfo.getInt("total")==0){
      return;
    }
    
    query.put("size", hitsInfo.getInt("total"));
    
    try {
      result = ElasticSearchProxy.search(query);
    } catch (IOException e) {
      e.printStackTrace();
    }
    
    BasicDBList hits = (BasicDBList)((BasicDBObject)result.get("hits")).get("hits");
    String bulkDelete = "";
    for(Object hit:hits){
      bulkDelete += "{\"delete\":{\"_id\":\""+((BasicDBObject) hit).getString("_id")+"\"}}\n";
    }
    ElasticSearchProxy.callBulk(bulkDelete);
  }

}
