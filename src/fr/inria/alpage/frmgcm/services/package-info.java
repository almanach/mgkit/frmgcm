/**
 * This package contains classes defining service endpoints reachable via the http server run by the application.
 */
package fr.inria.alpage.frmgcm.services;