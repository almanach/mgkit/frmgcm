package fr.inria.alpage.frmgcm.services;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HttpContext;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.util.JSON;

import fr.inria.alpage.frmgcm.Configuration;
import fr.inria.alpage.frmgcm.search.DPathSearchMod;
import fr.inria.alpage.frmgcm.server.FRMGCMService;
import fr.inria.alpage.frmgcm.utils.Utils;

public class CMAWebService extends FRMGCMService {

  public CMAWebService(){
    super("/app/(.*)");
  }
  
  public CMAWebService(String uriPattern) {
    super(uriPattern);
    // TODO Auto-generated constructor stub
  }

  @Override
  public void handleRequest(HttpRequest request,String requestedURI, HttpEntity entity,
      HttpResponse response, HttpContext context,StringBuilder test) {

    // the path requested less "/ap/"
    String path = requestedURI.substring(5);
    String method = request.getRequestLine().getMethod().toUpperCase(Locale.ENGLISH);
    // returned content string
    String content = null;
    response.setStatusCode(HttpStatus.SC_OK); // default presumed ok
    
    // request values
    String [] queryRequest = path.split("\\?");
    String[] pathParts = queryRequest[0].split("/");
    BasicDBObject getValues = new BasicDBObject();
    if(queryRequest.length>1){
      String[] values = queryRequest[1].split("&");
      for(String pair:values){
        String[] pairSplitted = pair.split("=");
        getValues.put(pairSplitted[0],pairSplitted[1]);
      }
    }
    
    
    BasicDBObject postValues = new BasicDBObject();
    
    // init postValues from the request entity
    if(entity != null){
      try {
        InputStream is = entity.getContent();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line = null;
        String postContent = "";
        while((line=br.readLine())!=null){
          postContent += line+"\n";
        }
        postValues = (BasicDBObject) JSON.parse(postContent);
      } catch (Exception e1) {
        
        Utils.Log("error when initializing postValues");
        e1.printStackTrace();
      }
    }

    try{
      // execute function depending on path
      content = route(pathParts,getValues,postValues,method).toString();
      
    }catch(Exception e){
      if(content==null){
        content = e.getMessage();
      }
      e.printStackTrace();
    }
    
    response.setStatusCode(HttpStatus.SC_OK);
    StringEntity body = new StringEntity(content, ContentType.create("text/html", (Charset) null));
    response.setEntity(body);
  }

  private Object route(String[] route, BasicDBObject getValues, BasicDBObject postValues, String method) {
    if(route.length==0){
      
    }else if(route[0].equals("_dpath")){
      // apply dpath on sentence given
      DPathSearchMod searchModule = new DPathSearchMod(postValues.getString("query"));
      try {
        searchModule.execute((BasicDBList)postValues.get("sentences"));
        return searchModule.getResults().toString();
      } catch (Exception e) {
        e.printStackTrace();
        return "{\"error\":true,\"message\":\"The query reported an error!\"}";
      }
    }
    return "{\"error\":true,\"message\":\"There is no service at this url.\"}";
  }

}
