package fr.inria.alpage.frmgcm.services;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.Locale;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HttpContext;
import org.bson.types.ObjectId;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

import fr.inria.alpage.frmgcm.Configuration;
import fr.inria.alpage.frmgcm.corpus.Corpus;
import fr.inria.alpage.frmgcm.corpus.Document;
import fr.inria.alpage.frmgcm.corpus.SentenceCollection;
import fr.inria.alpage.frmgcm.corpus.format.AbstractGraph;
import fr.inria.alpage.frmgcm.corpus.format.PassageGraph;
import fr.inria.alpage.frmgcm.db.CPMDB;
import fr.inria.alpage.frmgcm.gm.Graph;
import fr.inria.alpage.frmgcm.gm.GraphManager;
import fr.inria.alpage.frmgcm.search.DPathSearchMod;
import fr.inria.alpage.frmgcm.search.DPathSearchMacroManager;
import fr.inria.alpage.frmgcm.search.AsyncSearch;
import fr.inria.alpage.frmgcm.server.FRMGCMService;
import fr.inria.alpage.frmgcm.utils.Utils;

public class SentenceCollectionService extends FRMGCMService {

  public SentenceCollectionService() {
    super("/sc/?(.*)");
  }
  
  public SentenceCollectionService(String uriPattern) {
    super(uriPattern);
  }

  @Override
  public void handleRequest(HttpRequest request, String requestedURI,
      HttpEntity entity, HttpResponse response, HttpContext context,
      StringBuilder corpus) {
    // the path requested less "/sc/"
    String path = "";
    if(requestedURI.length()>4){
      path = requestedURI.substring(4);
    }
    String method = request.getRequestLine().getMethod().toUpperCase(Locale.ENGLISH);
    // returned content string
    String content = null;
    response.setStatusCode(HttpStatus.SC_OK); // default presumed ok
    
    // request values
    String [] queryRequest = path.split("\\?");
    String[] pathParts = queryRequest[0].split("/");
    BasicDBObject getValues = new BasicDBObject();
    if(queryRequest.length>1){
      String[] values = queryRequest[1].split("&");
      for(String pair:values){
        String[] pairSplitted = pair.split("=");
        if(pairSplitted.length>1){
          try {
            getValues.put(pairSplitted[0],URLDecoder.decode(pairSplitted[1],"utf-8"));
          } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
          }
        }
      }
    }
    
    
    BasicDBObject postValues = new BasicDBObject();
    String postData = null;
    // init postValues from the request entity
    if(entity != null){
      try {
        InputStream is = entity.getContent();
        
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line = null;
        String postContent = "";
        while((line=br.readLine())!=null){
          postContent += line+"\n";
        }
        postData = postContent;
        Header contentTypeHeader = entity.getContentType();
        String postContentType = null;
        if(contentTypeHeader!=null){
          postContentType = contentTypeHeader.getValue();
        }
        if(postContentType!=null && (postContentType.equals("text/json") || postContentType.equals("application/json"))){
          postValues = (BasicDBObject) JSON.parse(postContent);
        }
        
      } catch (Exception e1) {
        postValues = new BasicDBObject();
        Utils.Log("error when initializing postValues");
        e1.printStackTrace();
      }
    }

    try{
      // execute function depending on path
      content = route(pathParts,getValues,postValues,postData,method).toString();
      
    }catch(Exception e){
      if(content==null){
        content = e.getMessage();
      }
      e.printStackTrace();
    }
    
    response.setStatusCode(HttpStatus.SC_OK);
    StringEntity body = new StringEntity(content, ContentType.create("text/html", (Charset) null));
    response.setEntity(body);

  }

  private Object route(String[] route, BasicDBObject getValues, BasicDBObject postValues,String postData, String method) {
    String user = null;
    if(postValues != null){
      user = postValues.getString("mgwiki_user");
      if(user==null){
        user = getValues.getString("mgwiki_user");
      }
    }
    Object retVal = null;
    if(route.length==0){
      
      // impossible
    }else if(route[0].equals("")){
      retVal = "";
      BufferedReader br = null;
      try {
         br = new BufferedReader(new FileReader(Configuration.cmaHtmlIndexFile));
         String line;
         while ((line = br.readLine()) != null) {
           retVal += line+"\n";
         }
      } catch (Exception e) {
         return "error";
      } finally {
        try {
          br.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }else if(route[0].equals("_mgwiki")){
      if(route.length==1){
        
      }else if(route[1].equals("_index")){
        // when indexing a mgwiki sentence, ask for highlighting2signature information and index it.
        // foreach sentence, related pages must be indicated 
        Runnable indexingProcess = new Runnable(){

          @Override
          public void run() {
            try {
              SentenceCollection.MGWikiIndex();
            } catch (Exception e) {
              e.printStackTrace();
            }
          }
          
        };
        Thread t = new Thread(indexingProcess);
        t.start();
        retVal = "{\"launched\":\"ok\"}";
        
      }else if(route[1].equals("_search")){
        DPathSearchMacroManager.putHistory(postValues.getString("query"),user);
        BasicDBList subcollectionIds = new BasicDBList();
        subcollectionIds.add("frmgwiki");
        AsyncSearch s = new AsyncSearch(postValues.getString("query"),subcollectionIds,"dpath");
        String from = postValues.getString("from");
        String max = postValues.getString("max");
        int imax = 20;
        int ifrom = 0;
        if(max != null){
          imax = Integer.valueOf(max);
          if(from!=null){
            ifrom = Integer.valueOf(from);
          }
        }
        retVal = s.execute(ifrom, imax);
        
        /*
        DPathSearch mod = new DPathSearch(postValues.getString("query"));
        BasicDBList subcollectionIds = new BasicDBList();
        subcollectionIds.add("frmgwiki");
        mod.setTarget(subcollectionIds);

        try {
          mod.execute();
        } catch (Exception e) {
          e.printStackTrace();
        }
        String from = postValues.getString("from");
        String max = postValues.getString("max");
        if(max != null){
          int imax = Integer.valueOf(max);
          int ifrom = 0;
          if(from!=null){
            ifrom = Integer.valueOf(from);
          }
          retVal = mod.getJSONResults(ifrom,imax);
        }*/
      }else if(route[1].equals("_matchSignature")){
        retVal = new BasicDBList();
        try {
          retVal = SentenceCollection.MGWikiMatchSignatures(postValues.getString("data"), postValues.getString("options"));
        } catch (Exception e) {
          e.printStackTrace();
        }
        
        
        
      }else if(route[1].equals("_graph")){
        String sid = postValues.getString("sid");
        Graph g = GraphManager.getGraph(sid);
        if(g != null){
          if(postValues.getString("depgraphsrc")!=null){
            try {
              retVal = g.getDepgraphSrcData();
            } catch (Exception e) {
              retVal = new BasicDBObject("error","error when fetching graph");
              e.printStackTrace();
            }
          }else{
            retVal = g.getData();
          }
        }else{
          retVal = new BasicDBObject("error","couldn't find the graph");
        }
        
      }
    }else if(route[0].equals("_tagset")){
      DBCollection coll = CPMDB.GetInstance().db.getCollection("tagsets");
      DBCursor cursor = coll.find();
      String formatName = postValues.getString("format");
      if(formatName != null && formatName.equals("dis_xmldep")){
        formatName = "depxml";
      }
      if(formatName==null){
        formatName = "conll";
      }
      while(cursor.hasNext()){
        BasicDBObject tagset = (BasicDBObject)cursor.next();
        if(tagset.getString("name").equals(formatName)){
          retVal = tagset.toString();
          break;
        }
      }
      if(retVal == null){
        retVal = "{}";
      }
  }else if(route[0].equals("_list")){
      // remove this 
      /*DBCollection coll = CPMDB.GetInstance().db.getCollection("stcollection");
      DBCursor cursor = coll.find();
      while(cursor.hasNext()){
        BasicDBObject doc = (BasicDBObject)cursor.next();
        doc.remove("docs");
        coll.save(doc);
      }
      Utils.Log("this code part must be remove");*/
      // -- end to remove code
      // list collections
      BasicDBList collections = SentenceCollection.RetrieveAll(new BasicDBObject("namespace",postValues.getString("namespace")));
      BasicDBList restrictedCollections = new BasicDBList();
      BasicDBList corpusAdded = new BasicDBList();
      for(Object obj:collections){
        SentenceCollection collection = (SentenceCollection)obj;
        Boolean b = collection.getisCMADocument();
        if(b==null || !b){
          restrictedCollections.add(collection);
        }else{
          Document doc = Document.Retrieve(collection.getcmaDocumentId());
          if(doc == null){
            collection.delete();
            continue;
          }
          Corpus corpus = doc.getCorpusOwner();
          if(corpus!=null && corpus.isPublic()){
            if(corpusAdded.contains(corpus.externId)){
              continue;
            }
            BasicDBObject corpusCollection = new BasicDBObject();
            String name = corpus.name;
            if(name == null){
              name = corpus.externId;
            }
            corpusCollection.put("name", name);
            corpusCollection.put("cid", corpus.externId);
            corpusAdded.add(corpus.externId);
            BasicDBList formats = new BasicDBList();
            formats.add(corpus.outputFormat);
            corpusCollection.put("formats",formats);
            corpusCollection.put("cmaCorpus", true);
            restrictedCollections.add(corpusCollection);
          }
        }
      }
      //restrictedCollections.add(new BasicDBObject("name","_mgwiki"));
      retVal = restrictedCollections.toString();
    }else if(route[0].equals("_create")){
      // create collection
      SentenceCollection collection = SentenceCollection.CreateNew(postValues);
      if(collection != null){
        retVal = new BasicDBObject("acknowledge",true);
      }else{
        retVal = new BasicDBObject("acknowledge",false);
      }
    }else{
      // collection actions
      String collection_name = route[0].trim();
      SentenceCollection collection = SentenceCollection.Retrieve(collection_name);
      if(collection==null){
        collection = SentenceCollection.CreateNew(new BasicDBObject("name",collection_name));
      }
      if(route.length==1){
        // return collection info
      }else if(route[1].equals("_delete")){
        collection.delete();
        retVal = new BasicDBObject("acknowledge",true);
      }else if(route[1].equals("_add")){
        BasicDBList docs = (BasicDBList)postValues.get("docs");
        retVal = new BasicDBObject("acknowledge",true);
        for(Object obj:docs){
          BasicDBObject doc = (BasicDBObject)obj;
          String path = doc.getString("path"); // mandatory (can be parse or path to directory or file)
          String format = doc.getString("format"); // mandatory format of sentences
          boolean saveLocalCopy = doc.getBoolean("saveLocalCopy");
          boolean autoIndex = doc.getBoolean("autoIndex");
          retVal = collection.addResources(path, format, saveLocalCopy, autoIndex);
        }
        
      }else if(route[1].equals("_index")){
        final SentenceCollection thecollection = SentenceCollection.Retrieve(collection_name);
        Runnable indexingProcess = new Runnable(){

          @Override
          public void run() {
            thecollection.index(true);
          }
          
        };
        Thread t = new Thread(indexingProcess);
        t.start();
        retVal = "{\"launched\":\"ok\"}";
      }else if(route[1].equals("_graph")){
        
        if(route.length==2){
          String sid = postValues.getString("sid");
          String rid = postValues.getString("rid");
          Graph g = null;
          try{
            g = GraphManager.getGraph(sid);
            if(g != null){
              if(postValues.getString("depgraphsrc")!=null){
                try {
                  retVal = g.getDepgraphSrcData();
                } catch (Exception e) {
                  retVal = new BasicDBObject("error","error when fetching graph");
                  e.printStackTrace();
                }
              }else if(postValues.getString("signature")!=null){
                try {
                  retVal = g.getSignature();
                } catch (Exception e) {
                  retVal = new BasicDBObject("error","error when fetching graph");
                  e.printStackTrace();
                }
              }else if(postValues.getString("info")!=null){
                try {
                  BasicDBObject graphObj = new BasicDBObject();
                  graphObj.put("votes", g.votes);
                  graphObj.put("notes", g.notes);
                  graphObj.put("format",g.format);
                  graphObj.put("additional_data", g.getAdditionalData());

                  try {
                    graphObj.put("sentence",g.getSentence());
                  } catch (Exception e) {
                    e.printStackTrace();
                  }
                  retVal = graphObj;
                } catch (Exception e) {
                  retVal = new BasicDBObject("error","error when fetching graph");
                  e.printStackTrace();
                }
              }else if(postValues.getString("complete")!=null){
                try {
                  BasicDBObject graphObj = new BasicDBObject("id",g.get_id().toString());
                  graphObj.put("votes", g.votes);
                  graphObj.put("notes", g.notes);
                  graphObj.put("eid", g.eid);
                  

                  try {
                    graphObj.put("data", g.getDepgraphSrcData());
                    graphObj.put("sentence",g.getSentence());
                  } catch (Exception e) {
                    e.printStackTrace();
                  }
                  retVal = graphObj;
                } catch (Exception e) {
                  retVal = new BasicDBObject("error","error when fetching graph");
                  e.printStackTrace();
                }
              }else{
                retVal = g.getData();
              }
            }else{
              retVal = new BasicDBObject("error","couldn't find the graph");
            }
          }catch(Exception e){
            // legacy for cma corpus manager
            if(collection.isCMADocument!=null && collection.isCMADocument){
              Document doc = Document.Retrieve(collection.cmaDocumentId);
              if(postValues.getString("depgraphsrc")!=null){
                try {
                  AbstractGraph ag = doc.getSentence(sid);
                  retVal = ag.getDepGraphData();
                } catch (Exception e2) {
                  retVal = new BasicDBObject("error","error when fetching graph");
                  e.printStackTrace();
                }
              }else{
                Object[] fileinfo = doc.getGraphFileInfo(sid, rid);
                retVal = Utils.fetchFromFile(new Utils.URI("file:///"+fileinfo[0]));
              }
            }
          }  
        }else{
          if(route[2].equals("_all")){
            Integer from = Integer.valueOf(postValues.getString("from"));
            Integer max = Integer.valueOf(postValues.getString("max"));
            DBCollection coll = CPMDB.GetInstance().db.getCollection(GraphManager.graphCollection);
            coll.setObjectClass(Graph.class);
            
            BasicDBObject q = new BasicDBObject();
            
            BasicDBList filters = new BasicDBList();
            filters.add(new BasicDBObject("cid",collection.get_id().toString()));
            
            BasicDBList voteVals = new BasicDBList();
            voteVals.add(1);
            voteVals.add(-1);
            if(postValues.getString("hideVoted")!=null){
              filters.add(new BasicDBObject("votes",new BasicDBObject ("$not",new BasicDBObject("$elemMatch",new BasicDBObject("val",new BasicDBObject("$in",voteVals))))));
            }
            if(postValues.getString("hideUnVoted")!=null){
              filters.add(new BasicDBObject("votes",new BasicDBObject("$elemMatch",new BasicDBObject("val",new BasicDBObject("$in",voteVals)))));
            }
            q.put("$and", filters);
            
            
            
            
            BasicDBObject sort = new BasicDBObject("eid",1);
            DBCursor curs = coll.find(q).sort(sort).limit(max).skip(from); 
            BasicDBObject response = new BasicDBObject("total",coll.find(q).count());
            BasicDBList graphs = new BasicDBList();
            response.put("graphs", graphs);
            while(curs.hasNext()){
              Graph graph = (Graph)curs.next();
              BasicDBObject graphObj = new BasicDBObject("id",graph.get_id().toString());
              graphObj.put("votes", graph.votes);
              graphObj.put("notes", graph.notes);
              /*try {
                graphObj.put("data", graph.getDepgraphSrcData());
              } catch (Exception e) {
                e.printStackTrace();
              }*/
              graphs.add(graphObj);
            }
            retVal = response;
            
          }else if(route[2].equals("_vote")){
            Graph g = GraphManager.getGraph(postValues.getString("sid"));
            if(route.length==3){
              retVal = g.getvotes();
            }else if(route[3].equals("_put")){
              Integer val = Integer.valueOf(postValues.getString("val"));
              g.vote(user,val);
              retVal = new BasicDBObject("acknowledge",true); 
            }
          }else if(route[2].equals("_note")){
            Graph g = GraphManager.getGraph(postValues.getString("sid"));
            if(route.length==3){
              retVal = g.getnotes();
            }else if(route[3].equals("_add")){
              g.addNote(user, postValues.getString("data"));
              retVal = new BasicDBObject("acknowledge",true);
            }else if(route[3].equals("_remove")){
              g.removeNote(Integer.valueOf(postValues.getString("index")));
              retVal = new BasicDBObject("acknowledge",true);
            }
          }else if(route[2].equals("_create")){
            BasicDBObject retInfo = new BasicDBObject();
            BasicDBObject graphDefinition = getDefinitionFromParams(postValues, postData,getValues);
            graphDefinition.put("cid", collection.get_id().toString());
            
            Graph g = GraphManager.createGraph(graphDefinition);
            String index = getValues.getString("autoindex");
            if(g != null){
              if(index!=null){
                g.index(true);
              }
              retInfo.put("uid", g.get_id().toString());
              retInfo.put("success" ,true);
            }else{
              retInfo.put("success",false);
            }
            retVal = retInfo;
          }else if(route[2].equals("_delete")){
            String sid = getValues.getString("uid");
            String eid = getValues.getString("eid");
            Graph g = null;
            if(eid != null){
              
              BasicDBObject graphdef = new BasicDBObject("eid",eid);
              graphdef.put("cid", collection.get_id().toString());
              g = GraphManager.getGraph(graphdef);
            }else{
              g = GraphManager.getGraph(sid);
            }
            if(g != null){
              g.delete(true);
            }
          }else if(route[2].equals("_update")){
            String sid = getValues.getString("uid");
            String eid = getValues.getString("eid");
            BasicDBObject graphDefinition = getDefinitionFromParams(postValues, postData,getValues);
            graphDefinition.put("cid", collection.get_id().toString());
            Graph g = null;
            if(eid != null){
              // update signature for mgwiki graphs
              String signature = getValues.getString("signature");
              if(signature != null && !signature.trim().equals("")){
                String page_refs = getValues.getString("page_ref_ids");
                String[] page_ref_ids = null;
                if(page_refs != null && !page_refs.trim().equals("")){
                  page_ref_ids = page_refs.split("\\|");
                }
                String signatureDescription = null;
                String graph_title = getValues.getString("title");
                if(graph_title != null && !graph_title.trim().equals("")){
                  signatureDescription = graph_title;
                }
                collection.updateSignature(eid, signature, "mgwiki", page_ref_ids, signatureDescription);
              }
              
              BasicDBObject graphdef = new BasicDBObject("eid",eid);
              graphdef.put("cid", collection.get_id().toString());
              g = GraphManager.getGraph(graphdef);
            }else if(sid!=null){
              g = GraphManager.getGraph(sid);
            }
            if(g == null){
              if(getValues.getString("autocreate")!=null){
                g = GraphManager.createGraph(graphDefinition);
              }
            }else{
              g.update(graphDefinition);
            }
            BasicDBObject retInfo = new BasicDBObject();
            if(g!=null){
              retInfo.put("uid", g.get_id().toString());
              retInfo.put("success" ,true);
            }else{
              retInfo.put("success",false);
            }
            retVal = retInfo;
          }
        }
        
        
      }else if(route[1].equals("_search")){
        DPathSearchMacroManager.putHistory(postValues.getString("query"),user);
        BasicDBList subcollectionIds = new BasicDBList();
        subcollectionIds.add(collection.get_id().toString());
        AsyncSearch s = new AsyncSearch(postValues.getString("query"),subcollectionIds,"dpath");
        if(postValues.getString("abort")!=null){
          retVal = s.abort().toJSON();
        }else if(postValues.getString("export")!=null){
          retVal = s.export();
        }else{
          String from = postValues.getString("from");
          String max = postValues.getString("max");
          int imax = 20;
          int ifrom = 0;
          if(max != null){
            imax = Integer.valueOf(max);
            if(from!=null){
              ifrom = Integer.valueOf(from);
            }
          }
          retVal = s.execute(ifrom, imax);
        }
        
        // search collection (only filter, or apply full dpath)
        /*
        DPathSearch mod = new DPathSearch(postValues.getString("query"));
        BasicDBList subcollectionIds = new BasicDBList();
        subcollectionIds.add(collection.get_id().toString());
        mod.setTarget(subcollectionIds);
        
        try {
          mod.execute();
        } catch (Exception e) {
          e.printStackTrace();
        }
        String from = postValues.getString("from");
        String max = postValues.getString("max");
        if(max != null){
          int imax = Integer.valueOf(max);
          int ifrom = 0;
          if(from!=null){
            ifrom = Integer.valueOf(from);
          }
          retVal = mod.getJSONResults(ifrom,imax);
        }else{
          retVal = collection.formatResults(mod.getResults());
        }*/
      }else if(route[1].equals("_filter")){
        // search collection (only filter, or apply full dpath)
        DPathSearchMod mod = new DPathSearchMod(postValues.getString("query"));
        BasicDBList subcollectionIds = new BasicDBList();
        subcollectionIds.add(collection.get_id().toString());
        mod.setTarget(subcollectionIds);
        try {
          retVal = collection.formatResults(mod.filter());
          //retVal = mod.filter();
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
    
    if(retVal == null){
      return "{\"error\":true,\"message\":\"There is no service at this url.\"}";
    }
    return retVal;
  }
  
  public static BasicDBObject getDefinitionFromParams(BasicDBObject postValues,String postData,BasicDBObject getValues){
    BasicDBObject graphDefinition = new BasicDBObject();
    String eid = getValues.getString("eid");
    if(eid!=null){
      graphDefinition.put("eid", eid);
    }
    
    graphDefinition.put("format", getValues.getString("format"));
    
    
    graphDefinition.put("data", postData);
    
    BasicDBObject additional_data = new BasicDBObject();
    String sentence = getValues.getString("sentence");
    String highlightings = getValues.getString("highlighting");
    String frmgparams = getValues.getString("frmgparams");
    String viewmode = getValues.getString("viewmode");
    if(highlightings!=null){
      additional_data.put("highlighting", highlightings);
    }
    if(frmgparams!=null){
      additional_data.put("frmgparams", frmgparams);
    }
    if(viewmode != null){
      additional_data.put("viewmode", viewmode);
    }
    if(sentence != null){
      additional_data.put("sentence", sentence);
    }
    
    
    graphDefinition.put("additional_data", additional_data);
    return graphDefinition;
  }
  
}
