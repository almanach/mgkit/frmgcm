package fr.inria.alpage.frmgcm.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HttpContext;

import com.jcraft.jsch.JSchException;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.util.JSON;

import fr.inria.alpage.frmgcm.corpus.Corpus;
import fr.inria.alpage.frmgcm.corpus.Document;
import fr.inria.alpage.frmgcm.db.CPMDB;
import fr.inria.alpage.frmgcm.search.DPathSearchMacroManager;
import fr.inria.alpage.frmgcm.search.DPathSearchMacroManager.DPathMacro;
import fr.inria.alpage.frmgcm.server.FRMGCMService;
import fr.inria.alpage.frmgcm.utils.Utils;

public class DPathQueryMacroLibService extends FRMGCMService {

  public DPathQueryMacroLibService() {
    super("/dpathlib/(.*)");
  }
  
  public DPathQueryMacroLibService(String uriPattern) {
    super(uriPattern);
  }

  @Override
  public void handleRequest(HttpRequest request, String requestedURI,HttpEntity entity, HttpResponse response, HttpContext context,StringBuilder corpus) {
    String path = requestedURI.substring(10);
    
    // returned content string
    String content = null;
    response.setStatusCode(HttpStatus.SC_OK); // default presumed ok
    
    // request values
    String[] pathParts = path.split("/");
    
    BasicDBObject postValues = new BasicDBObject();
    
    // init postValues from the request entity
    if(entity != null){
      try {
        InputStream is = entity.getContent();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line = null;
        String postContent = "";
        while((line=br.readLine())!=null){
          postContent += line+"\n";
        }
        postValues = (BasicDBObject) JSON.parse(postContent);
      } catch (Exception e1) {
        
        Utils.Log("error when initializing postValues");
        e1.printStackTrace();
      }
    }
    
    try{
      // execute function depending on path
      if(pathParts.length==0){
      }else if(pathParts[0].equals("_create")){
        String name = postValues.getString("name").trim();
        DPathMacro macro = new DPathMacro();
        macro.created = new java.util.Date().toString();
        macro.description = postValues.getString("description");
        macro.lastModified = macro.created;
        macro.name = name;
        macro.query = postValues.getString("query");
        macro.user = postValues.getString("mgwiki_user");
        macro.ispublic = postValues.get("publicv").equals("true");
        Utils.ValidationBean validation = macro.validate(true);
        if(validation.success){
          macro.save();
        }
        content = "{\"success\":"+validation.success+",\"message\":\""+validation.message+"\"}";
      }else if(pathParts[0].equals("_all")){
        content = DPathSearchMacroManager.getAll(postValues.getString("mgwiki_user")).toString();
      }
      else if(pathParts[0].equals("_search")){
        content = DPathSearchMacroManager.search(postValues.getString("query"),postValues.getString("mgwiki_user")).toString();
      }else if(pathParts[0].equals("_history")){
        content = DPathSearchMacroManager.getHistory(postValues.getString("mgwiki_user")).toString();
      }else{
        String macroName = pathParts[0].trim();
        DPathMacro existingMacro = DPathMacro.RetrieveFromName(macroName);
        if(pathParts.length==1){
          content = existingMacro.toString();
          // retrieve macro
        }
        else if(pathParts[1].equals("_edit")){
          String name = postValues.getString("name").trim();
          boolean validateName = false;
          if(!macroName.equals(name)){
            validateName = true;
          }
          existingMacro.description = postValues.getString("description");
          existingMacro.lastModified = new java.util.Date().toString();
          existingMacro.name = name;
          existingMacro.query = postValues.getString("query");
          existingMacro.ispublic = postValues.get("publicv").equals("true");
          Utils.ValidationBean validation = existingMacro.validate(validateName);
          if(!postValues.get("mgwiki_user").equals(existingMacro.user)){
            validation.success = false;
            validation.message = "You are not the owner of this macro, you cannot edit it.";
          }
          if(validation.success){
            existingMacro.save();
          }
          content = "{\"success\":"+validation.success+",\"message\":\""+validation.message+"\"}";
          
          
        }else if(pathParts[1].equals("_delete")){
          boolean allowed = true;
          String message = "";
          if(!postValues.get("mgwiki_user").equals(existingMacro.user)){
            allowed = false;
            message = "You are not the owner of this macro, you cannot delete it.";
          }else{
            DPathMacro.Delete(macroName);
          }
          content = "{\"success\":"+allowed+",\"message\":\""+message+"\"}";
        }
      }
    }catch(Exception e){
      if(content==null){
        content = e.getMessage();
      }
      e.printStackTrace();
    }
    
    response.setStatusCode(HttpStatus.SC_OK);
    StringEntity body = new StringEntity(content, ContentType.create("text/html", (Charset) null));
    response.setEntity(body);

  }

}
