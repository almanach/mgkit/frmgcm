package fr.inria.alpage.frmgcm.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;

import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HttpContext;

import com.jcraft.jsch.JSchException;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.util.JSON;

import fr.inria.alpage.frmgcm.AppMain;
import fr.inria.alpage.frmgcm.corpus.Corpus;
import fr.inria.alpage.frmgcm.corpus.Document;
import fr.inria.alpage.frmgcm.corpus.SentenceCollection;
import fr.inria.alpage.frmgcm.db.CPMDB;
import fr.inria.alpage.frmgcm.search.DPathSearchMacroManager;
import fr.inria.alpage.frmgcm.search.AsyncSearch;
import fr.inria.alpage.frmgcm.server.FRMGCMService;
import fr.inria.alpage.frmgcm.utils.Utils;
import fr.inria.alpage.frmgcm.utils.Utils.ValidationBean;

public class CorpusService extends FRMGCMService {
  
  public CorpusService(){
    super("/corpus/(.*)");
  }
  
  public CorpusService(String uriPattern) {
    super(uriPattern);
    // TODO Auto-generated constructor stub
  }

  @Override
  public void handleRequest(HttpRequest request,String requestedURI, HttpEntity entity, HttpResponse response, HttpContext context,StringBuilder test) {
    // the path requested less "/corpus/"
    String path = requestedURI.substring(8);
    String method = request.getRequestLine().getMethod().toUpperCase(Locale.ENGLISH);
    // returned content string
    String content = null;
    response.setStatusCode(HttpStatus.SC_OK); // default presumed ok
    
    // request values
    String [] queryRequest = path.split("\\?");
    String[] pathParts = queryRequest[0].split("/");
    BasicDBObject getValues = new BasicDBObject();
    if(queryRequest.length>1){
      String[] values = queryRequest[1].split("&");
      for(String pair:values){
        String[] pairSplitted = pair.split("=");
        getValues.put(pairSplitted[0],pairSplitted[1]);
      }
    }
    
    
    BasicDBObject postValues = new BasicDBObject();
    
    // init postValues from the request entity
    if(entity != null){
      try {
        InputStream is = entity.getContent();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line = null;
        String postContent = "";
        while((line=br.readLine())!=null){
          postContent += line+"\n";
        }
        postValues = (BasicDBObject) JSON.parse(postContent);
      } catch (Exception e1) {
        
        Utils.Log("error when initializing postValues");
        e1.printStackTrace();
      }
    }

    try{
      // execute function depending on path
      content = route(pathParts,getValues,postValues,method).toString();
      
    }catch(Exception e){
      if(content==null){
        content = e.getMessage();
      }
      e.printStackTrace();
    }
    
    response.setStatusCode(HttpStatus.SC_OK);
    StringEntity body = new StringEntity(content, ContentType.create("text/html", (Charset) null));
    response.setEntity(body);
  }
  
  private Object route(String[] route,BasicDBObject get,BasicDBObject post,String method) throws Exception{
    
    String user = null;
    if(post != null){
      user = post.getString("mgwiki_user");
    }
    Object retVal = null;
    if(route.length==0){
      
    }else if(route[0].equals("_create")){
      //create new corpus
      Utils.Log("Creating new Corpus");
      Corpus corpus = Corpus.CreateCorpus(post);
      if(corpus != null){
        retVal = new BasicDBObject("success",true);
      }else{
        retVal = new BasicDBObject("error",true);
      }
    }else{
      // corpus actions
      String corpusEID = route[0].trim();
      final Corpus corpus = Corpus.RetrieveFromExternalID(corpusEID);
      
      if(corpus == null){
        retVal = "{\"error\":\"couldn't retrieve corpus ("+corpusEID+")\"}";
        throw new Exception("couldn't retrieve corpus ("+corpusEID+")");
      }
      if(route.length==1){
        // corpus info
        Utils.Log("Retrieving corpus info");
        retVal = JSON.serialize(corpus);

      }else if(route[1].equals("_delete")){
        corpus.delete();
        retVal = new BasicDBObject("success",true);
      }else if(route[1].equals("_tagset")){
          DBCollection coll = CPMDB.GetInstance().db.getCollection("tagsets");
          DBCursor cursor = coll.find();
          while(cursor.hasNext()){
            BasicDBObject tagset = (BasicDBObject)cursor.next();
            String formatName = corpus.getoutputFormat();
            if(formatName.equals("dis_xmldep")){
              formatName = "depxml";
            }
            if(tagset.getString("name").equals(formatName)){
              retVal = tagset.toString();
              break;
            }
          }
          if(retVal == null){
            retVal = "{}";
          }
      }else if(route[1].equals("_process")){
        // process
        boolean hasPermission = AppMain.havePermission(user,"admin",corpusEID);
        if(hasPermission){
          retVal = corpusProcess(route,post,corpus);
        }else{
          retVal = "{\"status\":\"error\",\"message\":\"You don't have the permission to process this corpus\"}";
        }
        
      }else if(route[1].equals("_index")){
        // process
        Runnable indexingProcess = new Runnable(){

          @Override
          public void run() {
            corpus.index();
            
          }
          
        };
        Thread t = new Thread(indexingProcess);
        t.start();
        retVal = "{\"launched\":\"ok\"}";
      }else if(route[1].equals("_download")){
        ValidationBean ok = corpus.prepareDownload();
        retVal = ok.toJSON();
      }else if(route[1].equals("status")){
        
      }else if(route[1].equals("task")){
        if(route.length>3){
          // task output
          String staskId = route[2];
          String staskunitId = route[3];
          if(route.length > 4){
            if(route[4].equals("stderr")){
              retVal = corpus.getTaskOutput(staskId, staskunitId,true);
              
            }else if(route[4].equals("stdout")){
              retVal = corpus.getTaskOutput(staskId, staskunitId,false);
            }
          }
        }else{
          // tasks status
          retVal = corpus.getDefaultTaskInfo();
        }
      }else if(route[1].equals("report")){
        retVal = "report:";
        try {
          retVal = corpus.getReport()[0];
        } catch (JSchException e) {
          e.printStackTrace();
        } catch (IOException e) {
          e.printStackTrace();
        } 
      }else if(route[1].equals("_search")){
        DPathSearchMacroManager.putHistory(post.getString("query"),user);
        if(post.getString("cma-reloaded") != null){
          BasicDBList subcollectionIds = new BasicDBList();
          for(Entry<String,Object> entry:corpus.getdocuments().entrySet()){
            SentenceCollection coll = SentenceCollection.Retrieve(entry.getKey());
            String id = entry.getKey();
            if(coll != null){
              id = coll.get_id().toString();
            }
            subcollectionIds.add(id);
          }
          AsyncSearch s = new AsyncSearch(post.getString("query"),subcollectionIds,"dpath");
          if(post.getString("abort")!=null){
            retVal = s.abort().toJSON();
          }else if(post.getString("export")!=null){
            retVal = s.export();
          }else{
            String from = post.getString("from");
            String max = post.getString("max");
            int imax = 20;
            int ifrom = 0;
            if(max != null){
              imax = Integer.valueOf(max);
              if(from!=null){
                ifrom = Integer.valueOf(from);
              }
            }
            retVal = s.execute(ifrom, imax);
          }
        }else{
          retVal = corpus.search(post.getString("query"),post.getString("searchType"),post.getString("from"),post.getString("max"),post.getString("did"),post.getString("export"));
        }
        
      }else if(route[1].equals("document")){
        
        //document need doc id
        if(route.length == 2){
          try {
            retVal = corpus.getDocumentList();
          } catch (Exception e) {
            retVal = e.getMessage();
            e.printStackTrace();
          }
        }else{
          // document actions
          Document doc = Document.Retrieve(route[2]);
          
          if(route.length==3){
            
            retVal = doc.getAnnotatedDoc();
          }else if(route[3].equals("_save")){
            retVal = String.valueOf(doc.saveRevision(post.getString("name"), post.getString("description"),post.getString("mgwiki_user")));
          }else if(route[3].equals("revisions")){
            retVal = doc.getRevisions().toString();
          }else if(route[3].equals("annotated")){
            retVal = doc.getAnnotatedDoc();
          } else if(route[3].equals("graph")){
            if(route.length == 4){
              // get Graph
              if(post.containsField("gid")){
                retVal = corpus.getGraphFilePath(route[2],post.getString("gid"),post.getString("vid"));
              }else{
                BasicDBList slist = (BasicDBList) post.get("slist");
                retVal = corpus.getGraph(route[2], post.getString("ts"), post.getString("te"), slist);
              }
            }else{
              String action = route[4];
              if(action.equals("save")){
                if(post.get("gid")!=null){
                  boolean success = doc.updateSentence(post.getString("gid"),post.getString("data"),post.getString("mgwiki_user"));
                  retVal = String.valueOf(success);
                }else{
                  retVal = "no gid";
                }
                
              }
            }
          }
        }        
      }
      else{
      }
    }
    return retVal;
  }
  

  
  private String corpusProcess(String[] pathParts, BasicDBObject postValues,Corpus corpus) {
    Integer processId = Integer.valueOf(postValues.getString("pid"));
    //Integer subProcessId = postValues.getInt("subpid");
    
   
    
    BasicDBObject options = new BasicDBObject(postValues);
    if(options.containsField("format")){
      corpus.outputFormat = options.getString("format");
      corpus.save();
    }
    
    if(options.containsField("termextract")){
      corpus.termExtractProcessEnabled = true;
      corpus.save();
    }
    
    corpus.startTask(processId,postValues);
    return "{\"state\":\"ok\"}";  
  }

  
  
  private String authenticate(HashMap<String,String> postValues){
    return String.valueOf(AppMain.AddUser(postValues.get("uid"), postValues.get("name"), postValues.get("key")));
  }


}
