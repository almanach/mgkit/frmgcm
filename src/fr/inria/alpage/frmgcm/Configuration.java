package fr.inria.alpage.frmgcm;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;

import fr.inria.alpage.frmgcm.utils.Utils;

public class Configuration {
  
  /**
   * Read a config file and override default values
   * @param configfilepath
   * @todo implement this!
   * @return a boolean indicating if the process went well
   */
  public static Utils.ValidationBean Init(String configfilepath){
    Utils.ValidationBean ok = new Utils.ValidationBean();
    ok.success = true;
    File configfile = new File(configfilepath);
    Field[] fields = Configuration.class.getDeclaredFields();
    try {
      BufferedReader br = new BufferedReader(new FileReader(configfile));
      String line = null;
      while((line = br.readLine())!=null){
        if(line.trim().startsWith("#")){
          continue;
        }
        if(line.indexOf("#")!=-1){
          line = line.substring(0,line.indexOf("#"));
        }
        String[] parts = line.split("=");
        if(parts.length>=2){
          String property = parts[0].trim();
          String value = parts[1].trim();
          for(Field field:fields){
            if(field.getName().equals(property)){
              try {
                if(field.getType().isPrimitive()){
                  if(field.getName().equals("verbose") || field.getName().equals("noRioc")){
                    field.set(null, Boolean.valueOf(value));
                  }else{
                    field.set(null, Integer.valueOf(value));
                  }
                }else{
                  field.set(null, value);
                }
                //Utils.Log("initializing property \""+property+"\" with value \""+value+"\"");
              } catch (IllegalArgumentException | IllegalAccessException e) {
                //Utils.Log("error when initializing property \""+property+"\" with value \""+value+"\"");
                e.printStackTrace();
              }
            }
          }
        }
      }
    } catch (FileNotFoundException e) {
      ok.success = false;
      ok.message = "The file "+configfilepath+" doesn't exist!";
      return ok;
    } catch (IOException e) {
      ok.success = false;
      ok.message = "Error when reading config file "+configfilepath+"";
      e.printStackTrace();
    } 
    return ok;
  }
  
  public static String home = "/home/pinot/alpage/buiquang/";//"/home/paul/";// "/home/buiquang/";// 

  /**
   * home var directory of the app (used for logs, process outputs, resources, etc)
   */
  public static String appHome = home+"var/cpm";
  
  /**
   * Host url of mgwiki instance
   */
  public static String mgwikiHost = "http://alpage.inria.fr/frmgwiki";//"http://localhost/mgwiki";//"http://localhost/mgwiki-dev";//
  
  /**
   * elasticsearch hostname
   */
  public static String elasticSearchHost = "cognac.inria.fr";//"localhost";//
  /**
   * elasticsearch listening port
   */
  public static String elasticSearchPort = "9200";
  
  /**
   * Listening webservice port
   */
  public static int serverPort = 8082;
  /**
   * Listening websocket port
   */
  public static int websocketPort = 8081;
  
  
  //public final static String neo4jDbPath = home+"var/cpm/neo4jdb/graph.db";//"var/cpm/neo4jdb/graph.db";//"install/neo4j-community-2.0.1/data/graph.db";//
  
  //public static int searchResultsDefaultLimit = 20;
  
  /**
   * Main log file
   */
  public static String logFile = appHome+"/cpm.log";
  /**
   * Log level 
   */
  public static int logLevel = 5;
  /**
   * display log in standard output
   */
  public static boolean verbose = true;
  
  /**
   * boolean used for local dev
   */
  public static boolean noRioc = false;
  /**
   * Default host for ssh execution {@see Utils.SSH}
   */
  public static String clusterHost = "buiquang@rioc.inria.fr";
  /**
   * Location of the public key (no password) used for connecting through ssh to the cluster host
   */
  public static String id_rioc = home+".ssh/id_rioc";
  
  /**
   * Base directory for corpus processing in the cluster host
   */
  public static String riocServerPath = "/scratch/buiquang/mgwiki";
  
  /**
   * file path where to save the gathered tagsets
   */
  public static String tagsetsFilePath = appHome +"/resources/tagsets.json";
  
  /**
   * Path file of the base template for corpus process configuration 
   */
  public static String templateDispatchConfFilepath = Configuration.appHome+"/resources/dispatch.dummy.conf";
  /**
   * Path file of the base template for the script that will be called when corpus process is finished (basically call a mgwiki webservice) 
   */
  public static String templateEndHandlerFilepath = Configuration.appHome+"/resources/endHandler.sh";

  public static int dpathQueryHistoryMaxSize = 20;

  
  public static String permissionServiceURL = mgwikiHost + "/mgwiki_corpus_user_permission";

  public static String cmaHtmlIndexFile = appHome+"/www-data/index.html";
  
  public static String frmgparserproxyurl = "http://alpage.inria.fr/newparserdemo/process.xml";
  
  public static String mgwikiSentencesUrl = mgwikiHost + "/parser/ws/listSentences";
  
  public static String d3jsUrl =mgwikiHost + "/d3js/ws_post";

  public static int DPATH_RESULT_CACHE_MAX_SIZE = 10000;

  
}
