package fr.inria.alpage.frmgcm.utils;

import java.util.ArrayList;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

import fr.inria.alpage.frmgcm.db.CPMDB;
import fr.inria.alpage.frmgcm.utils.AbstractCacheManager.Cacheable;
import fr.inria.alpage.frmgcm.utils.AbstractCacheManager.CompositeCacheable;

/**
 * This class is used to cache data in database
 * @author buiquang
 *
 */
public class DBCacheManager {
  /**
   * the db collection used to store data
   */
  private static DBCollection dbcache = CPMDB.GetInstance().db.getCollection("cache");
  /**
   * a lock object
   */
  private static Object lock = new Object();
  
  static{
    try{
      dbcache.setObjectClass(Cacheable.class);
      Thread threadCleanerUpper = new Thread(
      new Runnable(){
        int milliSecondSleepTime = 5000;
        public void run(){
          try{
            while (true){
              DBCursor cursor = dbcache.find();
              ArrayList<Object> expiredKeys = new ArrayList<Object>();
              while(cursor.hasNext()){
                Cacheable cacheObject = (Cacheable)cursor.next();
                
                if (cacheObject.isExpired()){
                  expiredKeys.add(cacheObject.get_id());
                }
              }
              for(Object expiredKey:expiredKeys){
                synchronized(lock){
                  dbcache.remove(new BasicDBObject("_id",expiredKey));
                }
              }
              Thread.sleep(this.milliSecondSleepTime);
            }
          }
          catch (Exception e){
              e.printStackTrace();
          }
          return;
        } 
      }); 
      threadCleanerUpper.setPriority(Thread.MIN_PRIORITY);
      threadCleanerUpper.start();
    }
    catch(Exception e){
      
      System.out.println("CacheManager.Static Block: " + e);
    }
  }
  
  /**
   * put a composite cacheable object in the cache (this is used because mongodb limits size of object stored in db)
   * @param object the {@see CompositeCacheable} object
   */
  public static void putCache(CompositeCacheable object){
    for(Object objectPart:object.cacheParts){
      putCache((Cacheable)objectPart);
    }
    putCache((Cacheable)object);
  }
 
  /**
   * put a cacheable object in the cache
   * @param object the cacheable object
   */
  public static void putCache(Cacheable object){
    dbcache.setObjectClass(Cacheable.class);
    dbcache.insert(object);
  }

  /**
   * retrieve a previously cached object (automatically reconstruct {@see CompositeCacheable} objects)
   * @param identifier the identifier of the cacheable object
   * @return a cacheable object if found or not expired
   */
  public static Cacheable getCache(Object identifier){
    Cacheable object = null;
    synchronized (lock){
      dbcache.setObjectClass(Cacheable.class);
       object = (Cacheable)dbcache.findOne(new BasicDBObject("key",identifier));
    }
    if (object == null || object.isExpired()){
      return null;
    }
    else{
      if(object.data != null && object.data.equals(AbstractCacheManager.CompositeCacheable.COMPOSITE_CACHEABLE_MARKUP)){
        int i = 0;
        BasicDBList compositeCache = new BasicDBList();
        while(true){
          Cacheable cachepart = (Cacheable)dbcache.findOne(new BasicDBObject("key",identifier+AbstractCacheManager.CompositeCacheable.COMPOSITE_CACHEABLE_PART+i));
          if(cachepart==null){
            break;
          }
          compositeCache.addAll((BasicDBList)cachepart.data);
          i++;
        }
        java.util.Calendar cal = java.util.Calendar.getInstance();
        cal.setTime(object.getexpirationDate());
        long diff = cal.getTime().getTime() - new java.util.Date().getTime();
        int diffMinutes = (int) (diff / (60 * 1000) % 60);
        return new Cacheable(compositeCache,identifier,diffMinutes);
      }
      return object;
    }
  }
}
