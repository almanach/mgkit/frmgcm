package fr.inria.alpage.frmgcm.utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.URLEncoder;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.jcraft.jsch.JSchException;
import com.mongodb.BasicDBObject;

import fr.inria.alpage.cpm.task.CPMCommand;
import fr.inria.alpage.frmgcm.Configuration;
import fr.inria.alpage.frmgcm.corpus.Corpus.OARSubParams;

/**
 * Class encompassing numerous static utility methods
 * @author buiquang
 *
 */
public class Utils {
  
  /**
   * Execute a bash command in the rioc server using ssh.
   * @param cmd the bash command to execute
   * @return an array of two strings. the first is the stdout and the second is the stderr of the process.
   */
  public static String[] ssh_exec(String cmd){
    StringBuffer[] sbout = new StringBuffer[2];
    sbout[0]=new StringBuffer();
    sbout[1]=new StringBuffer();
    SSH ssh = new SSH(sbout[0],sbout[1]);
    try {
      ssh.exec(cmd);
    } catch (JSchException | IOException e) {
      sbout[1].append(e.getMessage());
      e.printStackTrace();
    }
    return new String[]{sbout[0].toString(),sbout[1].toString()};
  }
  
  /**
   * Abstract class used for browsing a tar file.
   * @author buiquang
   *
   */
  public static abstract class BrowseTarFileHandler{
    /**
     * a property used to store internal process data
     */
    public Object data = null;
    /**
     * a property used to store external data sent to the instance of the tar browser
     */
    public Object eData = null;
    /**
     * Constructor
     * @param data the data object sent to the instance
     */
    public BrowseTarFileHandler(Object data) {
      eData = data;
    }
    /**
     * Constructor
     */
    public BrowseTarFileHandler() {
    }
    /**
     * The handler that will be called at every step of the browsing process
     * @param filename the current filename processed
     * @param fileStream the corresponding file stream
     * @return boolean information used to inform about what happened in the handling process (used to stop browsing if needed {@see searchTar})
     */
    public abstract boolean handleFile(String filename,InputStream fileStream);
  }
  
  /**
   * Search within a tar / tar.gz file and apply a callback when the file is found.
   * @param is current input stream processed
   * @param curFileName current filename represented by the input stream
   * @param targetFilename the filename that is searched within the tar/tar.gz 
   * @param matchCallback the callback that is called when the file is found
   * @return a boolean indicating if the search should continue (used internally when recursing the method)
   * @throws IOException
   */
  public static boolean searchTar(InputStream is,String curFileName,String targetFilename,Utils.BrowseTarFileHandler matchCallback) throws IOException{
    boolean doContinue = true;
    
    
    if(targetFilename.equals(curFileName)){
      matchCallback.handleFile(curFileName, is);
      return false;
    }
    
    if(curFileName.endsWith("tar") || curFileName.endsWith("tar.gz")){
      
      BufferedInputStream in = new BufferedInputStream(is);
      
      TarArchiveInputStream tarIn =  null;
      GzipCompressorInputStream gzIn = null;
      if(curFileName.endsWith("tar.gz")){
        Utils.Log("processing "+curFileName);
         gzIn = new GzipCompressorInputStream(in);
        tarIn = new TarArchiveInputStream(gzIn);
      }else{
        tarIn = new TarArchiveInputStream(in);
      }
      
      
      TarArchiveEntry tarEntry = null;
      
      while((tarEntry = tarIn.getNextTarEntry())!=null){
        String name = tarEntry.getName();
        if(!targetFilename.startsWith(curFileName+"#"+name)){
          continue;
        }
        doContinue = searchTar(tarIn, curFileName+"#"+name, targetFilename,matchCallback);
        if(!doContinue){
          return doContinue;
        }
      }
    }
    
    return doContinue;
  }
  
  /**
   * Browse a tar file an apply a callback to each of its elements
   * @param is the current input stream processed
   * @param curFileName the current filename represented by the input stream
   * @param callback the callback that will process each file. if this callback returns the false value, the browsing is interrupted.
   * @return a boolean indicating if the browsing should continue (used internally when recursing the method)
   * @throws Exception
   */
  public static boolean browseTar(InputStream is,String curFileName,Utils.BrowseTarFileHandler callback) throws Exception{
    boolean doContinue = true;
    if(curFileName.endsWith("tar") || curFileName.endsWith("tar.gz")){
      
      
      BufferedInputStream in = new BufferedInputStream(is);
      
      TarArchiveInputStream tarIn =  null;
      GzipCompressorInputStream gzIn = null;
      if(curFileName.endsWith("tar.gz")){
        Utils.Log("processing "+curFileName);
         try {
          gzIn = new GzipCompressorInputStream(in);
        } catch (IOException e) {
          Utils.Log("Error with reading file "+curFileName+" as a Gziped file.",1);
          e.printStackTrace();
          return doContinue;
        }
        tarIn = new TarArchiveInputStream(gzIn);
      }else{
        tarIn = new TarArchiveInputStream(in);
      }
      
      
      TarArchiveEntry tarEntry = null;
      
      while((tarEntry = tarIn.getNextTarEntry())!=null){
        String name = tarEntry.getName();
        doContinue = browseTar(tarIn, curFileName+"#"+name, callback);
        if(!doContinue){
          return doContinue;
        }
      }
    }else if(curFileName.endsWith("bz2")){
      BZip2CompressorInputStream bzIn;
      try {
        bzIn = new BZip2CompressorInputStream(is);
      } catch (IOException e) {
        Utils.Log("Error with reading file "+curFileName+" as a Bzip2 file.",1);
        e.printStackTrace();
        return doContinue;
      }
      doContinue = callback.handleFile(curFileName, bzIn);
    }else{
      doContinue = callback.handleFile(curFileName, is);
    }
    return doContinue;
  }
  
  /**
   * Handle indifferently local filepath and url for data retrieval
   * @author paul
   *
   */
  public static class URI{
    /**
     * the location of the data
     */
    public String location = null;
    /**
     * for local files get the tar archive path if the filepath is indicating embed file
     */
    public String locationEndPoint = null;
    /**
     * Types of uri handled
     * @author buiquang
     *
     */
    public enum URI_TYPE{
      URL,
      FILEPATH,
      UNDEFINED
    };
    /**
     * The type of resource location (url or local file)
     */
    public URI_TYPE type = URI_TYPE.UNDEFINED;
    /**
     * Constructor from a string definition of the path 
     * @param path (url => http://path, local file => file:///pathtoarchive(#pathwithinarchive)?)
     */
    public URI(String path){
      if(path.startsWith("file:///")){
        location = path.substring(8);
        String[] inArchive = location.split("#");
        locationEndPoint = inArchive[0];
        type = URI_TYPE.FILEPATH;
      }else if(path.startsWith("http://")){
        location = path;
        type = URI_TYPE.URL;
      }else{
        location = path;
      }
    }
    
    /**
     * Retrive a input stream from the location asked 
     * @return the input stream or null if an error happened
     */
    public InputStream getStream(){
      switch(type){
      case URL:
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet getRequest = new HttpGet(location);
        


        HttpResponse response;
        StringBuilder output = new StringBuilder();
        BufferedReader br = null;
        try {
          response = httpClient.execute(getRequest);
          br = new BufferedReader(
              new InputStreamReader((response.getEntity().getContent())));
          String line = null;
          while ((line = br.readLine()) != null) {
              output.append(line+"\n");
          }
          
          br.close();
          httpClient.close();
        } catch (IOException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
        
        
        String stdata = output.toString();
        ByteArrayInputStream stream = new ByteArrayInputStream(stdata.getBytes(StandardCharsets.UTF_8));
        return stream;
      case FILEPATH:
        
        FileInputStream fin = null;
        try {
          fin = new FileInputStream(locationEndPoint);
        } catch (FileNotFoundException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
        return fin;
      default:
        return null;
      }
    }
  }
  
  /**
   * Retrieve the string content of file
   * @param uri the location of the resource
   * @return the content of the file
   */
  public static String fetchFromFile(final Utils.URI uri){
    Utils.BrowseTarFileHandler op = new Utils.BrowseTarFileHandler() {
      
      @Override
      public boolean handleFile(String filename, InputStream fileStream) {
        if(filename.equals(uri.location)){
          byte[] btoRead = new byte[1024];
          StringBuffer sb = new StringBuffer();
          int len = 0;
          InputStream is = fileStream;
          if(filename.endsWith("bz2")){
            try {
              is = new BZip2CompressorInputStream(fileStream);
            } catch (IOException e) {
              Utils.Log("Failed read bz2 stream for file "+filename);
              e.printStackTrace();
            }
          }
          try {
            while ((len = is.read(btoRead)) != -1) {
              String s = new String(btoRead,Charset.forName("ISO-8859-1"));
              sb.append(s);
              btoRead = new byte[1024];
            }
          } catch (IOException e) {
            e.printStackTrace();
          } finally{
            try {
              is.close();
            } catch (IOException e) {
              e.printStackTrace();
            }
          }
          this.data = sb.toString();
          return false;
        }
        return true;
      }
    };
    
    InputStream is = uri.getStream();
    try {
      Utils.searchTar(is, uri.locationEndPoint,uri.location,op);
    } catch (Exception e) {
      e.printStackTrace();
    } finally{
      try {
        is.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    return (String)op.data;
  }
  
  /**
   * Retrieve the string content of a file located at some url
   * @param uri the location of the file
   * @return the string content of the resource
   */
  public static String fetchFromURL(Utils.URI uri){
    InputStream is = uri.getStream();
    byte[] btoRead = new byte[1024];
    StringBuffer sb = new StringBuffer();
    int len = 0;
    try {
      while ((len = is.read(btoRead)) != -1) {
        String s = new String(btoRead,Charset.forName("UTF-8"));
        sb.append(s);
        btoRead = new byte[1024];
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return sb.toString();
  }
  
  /**
   * Convenient simple structure used as generic validation object message
   * @author buiquang
   *
   */
  public static class ValidationBean{
    /**
     * boolean indicating if the message is a success
     */
    public boolean success;
    /**
     * data transmitted as a string
     */
    public String message;
    
    public BasicDBObject toJSON() {
      BasicDBObject obj = new BasicDBObject();
      obj.put("message", message);
      obj.put("success", success);
      return obj;
    }
  }
  
  /**
   * Uncompress a file (bz2)
   * @param file the file
   * @return the filepath of the newly uncompressed file
   */
  public static String uncompressFile(File file){
    String filepath = file.getAbsolutePath();
    try {
      filepath = file.getCanonicalPath();
    } catch (IOException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
    // compressed file
    String cmd = "bzip2 -fd "+filepath ;
    
    Utils.Log(cmd);
    String scriptfile = CPMCommand.exportToScript(cmd);
    Process process = null;
    try {
      process = Runtime.getRuntime().exec("sh ".concat(scriptfile));
    } catch (IOException e) {
      e.printStackTrace();
    }
    try {
      process.waitFor();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    
    return filepath.replace(".bz2", "");
  }

  /**
   * Execute a bash command
   * @param cmd the bash command
   * @return an array of two string. the first is the stdout and the second is the stderr of the process.
   */
  public static String[] exec(String cmd){
    String [] output = new String[]{"",""};
    Utils.Log("Executing command : "+cmd);
    String scriptfile = CPMCommand.exportToScript(cmd);
    Process process = null;
    try {
      process = Runtime.getRuntime().exec("sh ".concat(scriptfile));
    } catch (IOException e) {
      output[1] = output[1].concat(e.getMessage());
      e.printStackTrace();
    }
    try {
      BufferedReader bout = new BufferedReader(new InputStreamReader(process.getInputStream()));
      BufferedReader berr = new BufferedReader(new InputStreamReader(process.getInputStream()));
      String line = null;
      while((line = bout.readLine())!=null){
        output[0] = output[0].concat(line+"\n");
      }
      while((line = berr.readLine())!=null){
        output[1] = output[1].concat(line+"\n");
      }
      output[0] = output[0].concat(String.valueOf(process.waitFor()));
    } catch (InterruptedException e) {
      output[1] = output[1].concat(e.getMessage());
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    new File(scriptfile).delete();
    return output;
  }
  
  /**
   * Read and optionnaly modify a file line by line
   * @param baseFile the entry file
   * @param outFile the path of the modified file
   * @param processor the callback that will decide to modify or not each line
   * @param params an array of objects passed to the callback
   * @return an array of two string representing the status of the process (stdout & stderr)
   */
  public static String[] readModifyWrite(String baseFile,String outFile,ReadModifyWriteProcessorInterface processor,ArrayList<Object> params){
    String[] output = new String[]{"",""};

    BufferedReader br = null;
    BufferedWriter bw = null;
    try {
       br = new BufferedReader(new FileReader(baseFile));
       bw = new BufferedWriter(new FileWriter(outFile));
       String line;
       while ((line = br.readLine()) != null) {
         String modifiedline = processor.processLine(line,params);
         bw.write(modifiedline+"\n");
       }
    } catch (Exception e) {
       output[1] = output[1].concat(e.getMessage());
       return output;
    } finally {
       try {
          if(br != null)
             br.close();
       } catch (IOException e) {
         output[1] = output[1].concat(e.getMessage());
       }
       try {
          if(bw != null)
             bw.close();
       } catch (IOException e) {
         output[1] = output[1].concat(e.getMessage());
       }
    }
    return output;
  }
  
  /**
   * Interface used for the method {@see readModifyWrite}
   * @author buiquang
   *
   */
  public interface ReadModifyWriteProcessorInterface{
    /**
     * Process a line and return the new line that will replace the entry data
     * @param line the entry line data
     * @param params an array of parameters that can be passed to this callback
     * @return the new line content
     */
    public String processLine(String line,ArrayList<Object> params);
  }
  
  /**
   * Copy a file
   * @param source the original file path
   * @param dest the file path of the copy
   * @throws IOException
   */
  public static void copy(String source,String dest) throws IOException{
    File sourceFile = new File(source);
    if(sourceFile.isDirectory()){
      File[] files = sourceFile.listFiles();
      for(int i=0; i< files.length; i++){
        Utils.copy(files[i].getCanonicalPath(), dest+"/"+files[i].getName());
      }
    }else{
      File destFile = new File(dest);
      destFile.getParentFile().mkdirs();
      Utils.copyFile(sourceFile, destFile);
    }
  }
  
  /**
   * Copy a file
   * @param source the original file
   * @param dest the destination file
   * @throws IOException
   */
  public static void copyFile(File source, File dest)
      throws IOException {
    FileChannel inputChannel = null;
    FileChannel outputChannel = null;
    try {
      inputChannel = new FileInputStream(source).getChannel();
      outputChannel = new FileOutputStream(dest).getChannel();
      outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
    } finally {
      inputChannel.close();
      outputChannel.close();
    }
  }

  /**
   * Get the name of a file without its extension
   * @param file the file
   * @return the name of a file without its extension
   */
  public static String getShortName(File file){
    String name = file.getName();
    int extensionsIndex = name.lastIndexOf(".");
    if(extensionsIndex != -1){
      return name.substring(0, extensionsIndex);
    }
    return name;
  }
  
  /**
   * Get the name of a file without its extension
   * @param filepath the filepath
   * @return the name of the file without its extension
   */
  public static String getShortName(String filepath){
    int extensionsIndex = filepath.lastIndexOf(".");
    if(extensionsIndex != -1){
      return filepath.substring(0, extensionsIndex);
    }
    return filepath;
  }
  
  /**
   * Log a message to the log of the application
   * @param message
   */
  public static void Log(String message){
    Log(message,0);
  }
  
  
  /**
   * Apply a function over files within a folder
   * @param file the folder / file being processed
   * @param callback the callback to apply to each file
   * @param args an array of string parameters to be sent to the callback
   * @return boolean indicating if the mapping should continue (used internally)
   * @throws Exception
   */
  public static boolean mapReduceFolder(File file, MapReduceFolderDelegate callback, ArrayList<String> args) throws Exception{
    if(file.isDirectory()){
      //Utils.Log("map reduce folder process folder "+file.getPath());
      File[] files = file.listFiles();
      for(int i = 0 ; i< files.length ; ++i){
        boolean doContinue = Utils.mapReduceFolder(files[i], callback, args);
        if(!doContinue){
          return false;
        }
      }
    }else{
      //Utils.Log("map reduce folder process file "+file.getName());
      return callback.apply(file,args);
    }
    return true;
  }
  
  /**
   * The interface used in {@see mapReduceFolder} to process files
   * @author buiquang
   *
   */
  public static interface MapReduceFolderDelegate{
    /**
     * The callback applied to each file.
     * @param file the current file to process
     * @param args an array of string parameters sent to the callback. can be used to store returnable values.
     * @return a boolean set to true to continue processing the other files or false to stop the mapping process
     * @throws Exception
     */
    public abstract boolean apply(File file,ArrayList<String> args) throws Exception;
  }
  
  /**
   * Log string data into a file
   * @param message the string data to store
   * @param logfile the name of the log that will contain the data logged (if the file exist, the content will be appended)
   */
  public static void Log(String message,String logfile){
    String logDir = Configuration.appHome+"/logs/";
    Date d = new Date();
    String datedMessage = "[ " + d.toString() + " ] " + message;
    
    File logFile = new File(logDir+logfile);
    boolean fileExist = logFile.exists();
    PrintWriter pw = null;
    try {
      pw = new PrintWriter(new FileWriter(logFile, fileExist));
      pw.println(datedMessage);
      pw.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }
  
  /**
   * Log a string data message to the defaul application log
   * @param message the message to log
   * @param loglevel (0=stdin,1=error,2=warning,3=info,4=debug)
   */
  public static void Log(String message,int loglevel){
    Date d = new Date();
    String type = null;
    switch(loglevel){
    case 0:
      type = "";
      break;
    case 1:
      type = "ERROR => ";
      break;
    case 2:
      type = "WARNING => ";
      break;
    case 3:
      type = "INFO => ";
      break;
    case 4:
    default:
      type = "DEBUG => ";
      break;
    }
    
    String datedMessage = "[ " + d.toString() + " ] " + type + message;
    
    File logFile = new File(Configuration.logFile);
    PrintWriter pw = null;
    try {
      pw = new PrintWriter(new FileWriter(logFile, true));
      pw.println(datedMessage);
      pw.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    if(Configuration.verbose && loglevel < Configuration.logLevel){
      if(loglevel == 1){
        System.err.println(datedMessage);
      }else{
        System.out.println(datedMessage);
      }
    }
  }
}
