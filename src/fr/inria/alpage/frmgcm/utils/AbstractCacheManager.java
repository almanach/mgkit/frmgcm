package fr.inria.alpage.frmgcm.utils;

import com.mongodb.BasicDBList;
import com.mongodb.ReflectionDBObject;

/**
 * This class only contain an inner class used by different type of cache manager
 * @author buiquang
 *
 */
public class AbstractCacheManager {
  
  
  /**
   * Instance of this class is used to embed data for caching purpose
   * @author buiquang
   *
   */
  public static class Cacheable extends ReflectionDBObject{
    /**
     * the expiration date of the data cached
     */
    private java.util.Date expirationDate = null;
    public java.util.Date getexpirationDate() {
      return expirationDate;
    }

    public void setexpirationDate(java.util.Date expirationDate) {
      this.expirationDate = expirationDate;
    }

    public Object getdata() {
      return data;
    }

    public void setdata(Object data) {
      this.data = data;
    }

    public void setkey(Object key) {
      this.key = key;
    }
    /**
     * the key used to retrieve the data in cache (equality will be tested using the method equals())
     */
    private Object key = null;
    /**
     * the data object to store in cache
     */
    public Object data;
    
    
    /**
     * Constructor
     */
    public Cacheable(){
      super();
    }
    
    /**
     * Constructor
     * @param data the data to store in cache
     * @param key the key to identify the object in cache
     * @param expirationTime the expiration time expressed in minutes
     */
    public Cacheable(Object data,Object key,int expirationTime){
      this.data = data;
      this.key = key;
      // minutesToLive of 0 means it lives on indefinitely.
      if (expirationTime != 0)
      {
        this.expirationDate = new java.util.Date();
        java.util.Calendar cal = java.util.Calendar.getInstance();
        cal.setTime(this.expirationDate);
        cal.add(java.util.Calendar.MINUTE, expirationTime);
        this.expirationDate = cal.getTime();
      }
    }
    
    /**
     * Check if the cache object is due to expiration
     * @return true if expirationDate is exceeded
     */
    public boolean isExpired(){
      if (expirationDate != null){
        if (expirationDate.before(new java.util.Date())){
          return true;
        }
        else{
          return false;
        }
      }
      else{
        return false;
      }
    }

    public Object getkey() {
      return key;
    }
    
  }
  
  /**
   * Instances of this class is used to embed LARGE data for caching purpose (by automatically creating multiple cache segments)
   * @author buiquang
   *
   */
  public static class CompositeCacheable extends Cacheable{
    /**
     * The data content that will indicate that this cache object if a composite cache
     */
    public static final String COMPOSITE_CACHEABLE_MARKUP = "==COMPOSITE_CACHEABLE_MARKUP==";
    /**
     * The string that will be appended to the key for cache part objects
     */
    public static final String COMPOSITE_CACHEABLE_PART = ".part";
    /**
     * List of {@see Cacheable} object that were created to represent this composite object 
     */
    public BasicDBList cacheParts = new BasicDBList();
    
    public CompositeCacheable(){
      super();
    }
    
    /**
     * Default constructor only use BasicDBList (array) in entry
     * @param data the BasicDBList of items to cache
     * @param key the key to identify the object in cache
     * @param maxSize the max size of chunks of data per cache part
     * @param expirationTime the expiration time expressed in minutes
     */
    public CompositeCacheable(BasicDBList data,Object key,int maxSize,int expirationTime){
      super(COMPOSITE_CACHEABLE_MARKUP,key,expirationTime);
      int i = 0;
      int part = 0;
      BasicDBList cachePart = new BasicDBList();
      for(Object dataChunk:data){
        cachePart.add(dataChunk);
        i++;
        if(i>maxSize){
          cacheParts.add(new Cacheable(cachePart, key+COMPOSITE_CACHEABLE_PART+part, expirationTime));
          cachePart = new BasicDBList();
          i=0;
          part++;
        }
      }
      if(cachePart.size()>0){
        cacheParts.add(new Cacheable(cachePart, key+COMPOSITE_CACHEABLE_PART+part, expirationTime));
      }
    }
  }
  
}
