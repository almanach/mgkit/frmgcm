package fr.inria.alpage.frmgcm.utils;

import java.util.ArrayList;

import com.mongodb.DBCollection;

import fr.inria.alpage.frmgcm.db.CPMDB;
import fr.inria.alpage.frmgcm.utils.AbstractCacheManager.Cacheable;

/**
 * This class is used to cache data in memory
 * @author buiquang
 *
 */
public class StaticCacheManager {
  /**
   * The cache object that will store the data
   */
  private static java.util.HashMap<Object,Object> cacheHashMap = new java.util.HashMap<Object,Object>();
  /**
   * a lock object
   */
  private static Object lock = new Object();
  
  static{
    try{
      Thread threadCleanerUpper = new Thread(
      new Runnable(){
        int milliSecondSleepTime = 5000;
        public void run(){
          try{
            while (true){
              java.util.Set<Object> keySet = cacheHashMap.keySet();
              java.util.Iterator<Object> keys = keySet.iterator();
              ArrayList<Object> expiredKeys = new ArrayList<Object>();
              while(keys.hasNext()){
                Object key = keys.next();
                Cacheable value = (Cacheable)cacheHashMap.get(key);
                if (value.isExpired()){
                  expiredKeys.add(key);
                }
              }
              for(Object expiredKey:expiredKeys){
                synchronized(lock){
                  cacheHashMap.remove(expiredKey);
                }
              }
              Thread.sleep(this.milliSecondSleepTime);
            }
          }
          catch (Exception e){
              e.printStackTrace();
          }
          return;
        } 
      }); 
      threadCleanerUpper.setPriority(Thread.MIN_PRIORITY);
      threadCleanerUpper.start();
    }
    catch(Exception e){
      
      System.out.println("CacheManager.Static Block: " + e);
    }
  }
  
  /**
   * put a cacheable object in the cache
   * @param object the cacheable object
   */
  public static void putCache(Cacheable object){
    cacheHashMap.put(object.getkey(), object);
  }

  /**
   * retrieve a previously cached object 
   * @param identifier the identifier of the cacheable object
   * @return a cacheable object if found or not expired
   */
  public static Cacheable getCache(Object identifier){
    Cacheable object = null;
    synchronized (lock){ 
       object = (Cacheable)cacheHashMap.get(identifier);
    }
    if (object == null || object.isExpired()){
      return null;
    }
    else{
      return object;
    }
  }
  
  

}
