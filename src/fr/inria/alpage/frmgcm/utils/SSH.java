package fr.inria.alpage.frmgcm.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import fr.inria.alpage.cpm.task.CPMTaskUnit;
import fr.inria.alpage.frmgcm.Configuration;

/**
 * This class handles the execution of bash command over ssh (default is the rioc server)
 * @author paul
 *
 */
public class SSH {
/**
 * optional reference to a {@see CPMTaskUnit} This is used to retrieve the file destinations for stdout and stderr of the command launched
 */
  public CPMTaskUnit refTaskUnit = null;
  /**
   * Used to store standard error stream
   */
  public StringBuffer stderrstring = null;
  /**
   * Used to store standard output stream
   */
  public StringBuffer stdoutstring = null;
  
  /**
   * Constructor passing a {@see CPMTaskUnit) as argument for command output storage
   * @param task
   */
  public SSH(CPMTaskUnit task){
    this.refTaskUnit = task;
  }
  
  /**
   * Constructor passing two string buffers for command output storage
   * @param stdoutstring
   * @param stderrstring
   */
  public SSH(StringBuffer stdoutstring,StringBuffer stderrstring){
    this.stderrstring = stderrstring;
    this.stdoutstring = stdoutstring;
  }

  /**
   * Execute a bash command over ssh
   * @param command
   * @throws JSchException
   * @throws IOException
   */
  public void exec(String command) throws JSchException, IOException{
    
    if(Configuration.noRioc){ // to remove. Only for local dev
      if(this.refTaskUnit != null){
        this.refTaskUnit.writeOutput("norioc", "noRioc");
      }else if(this.stdoutstring != null && this.stderrstring != null){
        stdoutstring.append("noRioc");
        stderrstring.append("noRioc");
      }
      return;
    }
    
    
    java.util.Properties config = new java.util.Properties(); 
    config.put("StrictHostKeyChecking", "no");
    
    JSch jsch=new JSch();
    //URL id_url = this.getClass().getResource("/id_rioc");
    String id_filepath = Configuration.id_rioc;//id_url.getFile();
    
    jsch.addIdentity(id_filepath);

    String host=Configuration.clusterHost;
    String user=host.substring(0, host.indexOf('@'));
    host=host.substring(host.indexOf('@')+1);
     
    Session session=jsch.getSession(user, host, 22);

    session.setConfig(config);

    session.connect();
    
    Channel channel=session.openChannel("exec");
    ((ChannelExec)channel).setCommand(command);
    
    channel.setInputStream(null);
    
  //channel.setOutputStream(System.out);
   
  //FileOutputStream fos=new FileOutputStream("/tmp/stderr");
  //((ChannelExec)channel).setErrStream(fos);
  ((ChannelExec)channel).setErrStream(System.err);
     
    InputStream in=channel.getInputStream();
    BufferedReader outBuffer = new BufferedReader(new InputStreamReader(in));
    BufferedReader errBuffer = new BufferedReader(new InputStreamReader(((ChannelExec)channel).getErrStream()));
    
    Utils.Log("executing cmd : "+command);
    
    channel.connect();
     
    if(this.refTaskUnit != null){
      this.refTaskUnit.writeOutput(outBuffer, errBuffer);
    }else if(this.stdoutstring != null && this.stderrstring != null){
      CPMTaskUnit.writeOutput(outBuffer, errBuffer,stdoutstring,stderrstring);
    }
    
    outBuffer.close();
    errBuffer.close();
    
    
   channel.disconnect();
   session.disconnect();
 
  }
}
