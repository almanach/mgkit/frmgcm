/**
 * This package contains the classes defining the server(s) run by the application. 
 */
package fr.inria.alpage.frmgcm.server;