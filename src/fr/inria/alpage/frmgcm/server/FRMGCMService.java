package fr.inria.alpage.frmgcm.server;

import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.protocol.HttpContext;

/**
 * Abstract service that are used to separate the different webservice depending on the url path
 * @author paul
 *
 */
public abstract class FRMGCMService {
  /**
   * The pattern that the url must match to enter this service
   */
  public final String uriPattern; 
  /**
   * Constructor
   * @param uriPattern pattern that the url must match to enter this service
   */
  public FRMGCMService(String uriPattern){
    this.uriPattern = uriPattern;
  }
  /**
   * Handle a single request
   * @param request the http request
   * @param requestedURI the uri that is requested
   * @param entity the entity data sent with the request
   * @param response the response to fill in return of the request
   * @param context the context of the request
   * @param corpus ???
   */
  public abstract void handleRequest(HttpRequest request,String requestedURI,HttpEntity entity,HttpResponse response, HttpContext context,StringBuilder corpus);
}
