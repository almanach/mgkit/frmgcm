package fr.inria.alpage.frmgcm.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.Collection;

import org.java_websocket.WebSocket;
import org.java_websocket.WebSocketImpl;
import org.java_websocket.framing.Framedata;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import com.mongodb.BasicDBObject;
import com.mongodb.util.JSON;

import fr.inria.alpage.frmgcm.AppMain;
import fr.inria.alpage.frmgcm.AppMain.User;

/**
 * A simple WebSocketServer implementation.
 */
public class FRMGWebSocketServer extends WebSocketServer {

  public FRMGWebSocketServer( int port ) throws UnknownHostException {
    super( new InetSocketAddress( port ) );
  }

  public FRMGWebSocketServer( InetSocketAddress address ) {
    super( address );
  }

  @Override
  public void onOpen( WebSocket conn, ClientHandshake handshake ) {
    this.sendToAll( "{\"message\":\"" +"new connection: " + handshake.getResourceDescriptor()+"\"}" );
    System.out.println( conn.getRemoteSocketAddress().getAddress().getHostAddress() + " connected" );
  }

  @Override
  public void onClose( WebSocket conn, int code, String reason, boolean remote ) {
    this.sendToAll( "{\"message\":\"" + conn + " disconnected\"}" );
    //AppMain.RemoveUser(conn);
    System.out.println( conn + " disconnected" );
  }

  @Override
  public void onMessage( WebSocket conn, String message ) {
    BasicDBObject data = null;
    String key = null;
    try{
      data = (BasicDBObject)JSON.parse(message);
      key = data.getString("authkey");
    }catch (Exception e){
      
    }
    
    
    User user = AppMain.AuthenticateConnexion(conn,key);
    
    if(user==null){
      conn.send("{\"error\":\"user not authenticated\"}");
      return;
    }
    
    this.sendToAll( message , conn );
    System.out.println( conn + ": " + message );
  }

  @Override
  public void onFragment( WebSocket conn, Framedata fragment ) {
    System.out.println( "received fragment: " + fragment );
  }

  
  @Override
  public void onError( WebSocket conn, Exception ex ) {
    ex.printStackTrace();
    if( conn != null ) {
      // some errors like port binding failed may not be assignable to a specific websocket
    }
  }

  
  public void sendToAll( String text ) {
    sendToAll(text,null);
  }
  
  /**
   * Sends <var>text</var> to all currently connected WebSocket clients.
   * 
   * @param text
   *            The String to send across the network.
   * @throws InterruptedException
   *             When socket related I/O errors occur.
   */
  public void sendToAll( String text , WebSocket except) {
    Collection<WebSocket> con = connections();
    synchronized ( con ) {
      for( WebSocket c : con ) {
        if(except == c){
          continue;
        }
        System.out.println("user : "+c.getRemoteSocketAddress().getAddress().getHostAddress());
        System.out.println("user (port) : "+c.getRemoteSocketAddress().getPort());
        
        c.send( text );
      }
    }
  }
}