package fr.inria.alpage.frmgcm.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.MethodNotSupportedException;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestHandler;

/**
 * Handle HTTP request as a webservice
 * Services :
 * - GET :
 *    - corpus => list all corpus
 *    - corpus/id => return corpus with id = id
 *    - corpus/id/status => return corpus status
 *    - corpus/id/document => return corpus document list
 *    - corpus/id/document/id => return doc
 *    - corpus/id/document/id/annotated => return annotated doc
 *    - corpus/id/document/id/sentence/id => return graph
 * - POST :
 *    - process + <json info>
 * 
 * @author paul
 *
 */
public class FRMGCMRequestHandler implements HttpRequestHandler {

  private ArrayList<FRMGCMService> services = new ArrayList<FRMGCMService>();
  public StringBuilder corpus = new StringBuilder("");
  
  @Override
  public void handle(HttpRequest request, HttpResponse response, HttpContext context)
      throws HttpException, IOException {
 
    String method = request.getRequestLine().getMethod().toUpperCase(Locale.ENGLISH);
    if (!method.equals("GET") && !method.equals("HEAD") && !method.equals("POST")) {
        throw new MethodNotSupportedException(method + " method not supported");
    }
    String target = request.getRequestLine().getUri();
    HttpEntity entity = null;
    if (request instanceof HttpEntityEnclosingRequest) {
      entity = ((HttpEntityEnclosingRequest) request).getEntity();
    }
    
    for(FRMGCMService service:services){
      if(target.matches(service.uriPattern)){
        service.handleRequest(request,target,entity,response,context,corpus);
      }
    }
    
    
    //System.out.println("Serving file " + file.getPath());
    
    /*final File file = new File(this.docRoot, URLDecoder.decode(target, "UTF-8"));
    if (!file.exists()) {

        response.setStatusCode(HttpStatus.SC_NOT_FOUND);
        StringEntity entity = new StringEntity(
                "<html><body><h1>File" + file.getPath() +
                " not found</h1></body></html>",
                ContentType.create("text/html", "UTF-8"));
        response.setEntity(entity);
        System.out.println("File " + file.getPath() + " not found");

    } else if (!file.canRead() || file.isDirectory()) {

        response.setStatusCode(HttpStatus.SC_FORBIDDEN);
        StringEntity entity = new StringEntity(
                "<html><body><h1>Access denied</h1></body></html>",
                ContentType.create("text/html", "UTF-8"));
        response.setEntity(entity);
        System.out.println("Cannot read file " + file.getPath());

    } else {

        
    }*/

  }
  
  public void addService(FRMGCMService service){
    this.services.add(service);
  }

}
