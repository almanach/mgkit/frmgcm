package fr.inria.alpage.frmgcm;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.java_websocket.WebSocket;
import org.java_websocket.WebSocketImpl;

import com.mongodb.BasicDBObject;
import com.mongodb.util.JSON;

import fr.inria.alpage.cpm.task.CPMCommand;
import fr.inria.alpage.frmgcm.corpus.format.AbstractGraph;
import fr.inria.alpage.frmgcm.server.FRMGWebSocketServer;
import fr.inria.alpage.frmgcm.server.HTTPServer;
import fr.inria.alpage.frmgcm.tools.Scripts;
import fr.inria.alpage.frmgcm.utils.Utils;

public class AppMain {
  
  public static HashMap<String,User> users = new HashMap<String,User>();
  private static HashMap<String,Object> locks = new HashMap<String, Object>();
  
  public static void main(String[] args) throws Exception {
    
    if(args.length>0){
      Configuration.Init(args[0]);
    }
    
    Init();
    
  
    WebSocketImpl.DEBUG = true;
    int port = Configuration.websocketPort; // 843 flash policy port
    try {
      port = Integer.parseInt( args[ 0 ] );
    } catch ( Exception ex ) {
    }
    FRMGWebSocketServer s = new FRMGWebSocketServer( port );
    s.start();
    System.out.println( "Websocket server started on port: " + s.getPort() );

    HTTPServer.run();
    
  
  }
  
  
  public static User AuthenticateConnexion(WebSocket conn,String key){
    if(key == null){
      return AuthenticateConnexion(conn);
    }
    Iterator<Entry<String,User>> it = AppMain.users.entrySet().iterator();
    while(it.hasNext()){
      Entry<String,User> entry = it.next();
      User u = entry.getValue();
      if(u.keys.containsKey(key)){
        u.hostname = conn.getRemoteSocketAddress().getAddress().getHostAddress();
        u.port = conn.getRemoteSocketAddress().getPort();
        return u;
      }
    }
    return null;
  }
  
  public static User AuthenticateConnexion(WebSocket conn){
    Iterator<Entry<String,User>> it = AppMain.users.entrySet().iterator();
    while(it.hasNext()){
      Entry<String,User> entry = it.next();
      User u = entry.getValue();
      if(conn.getRemoteSocketAddress().getAddress().getHostAddress().equals(u.hostname)
          && conn.getRemoteSocketAddress().getPort() == u.port){
        return u;
      }
    }
    return null;
  }
  
  public static boolean /*success*/ RemoveUser(String uid,String key){
    if(AppMain.users.containsKey(uid)){
      User user = AppMain.users.get(uid);
      user.keys.remove(key);
      Utils.Log("user "+uid+" already connected");
      return false;
    }
    return true;
  }


  public static void RemoveUser(WebSocket conn) {
    Iterator<Entry<String,User>> it = AppMain.users.entrySet().iterator();
    while(it.hasNext()){
      Entry<String,User> entry = it.next();
      User u = entry.getValue();
      if(conn.getRemoteSocketAddress().getAddress().getHostAddress().equals(u.hostname)
          && conn.getRemoteSocketAddress().getPort() == u.port){
        AppMain.users.remove(u.uid);
        break;
      }
    }
    
  }
  
  public static boolean /*success*/ AddUser(String uid,String name,String key){
    if(AppMain.users.containsKey(uid)){
      User user = AppMain.users.get(uid);
      user.keys.put(key,new Date());
      Utils.Log("user "+uid+" already connected");
      return false;
    }
    AppMain.users.put(uid, new User(uid,name,key));
    return true;
  }
  
 
  public static Object getLock(String lockName) {
    synchronized(locks){
      if(!locks.containsKey(lockName)){
        locks.put(lockName, new Object());
      }
    }
    return locks.get(lockName);
  }


  public static class User{
    
    public String uid;
    public String name;
    public String hostname;
    public int port = -1;
    public HashMap<String,Date> keys = new HashMap<String,Date>();
    
    public User(String uid,String name,String key){
      this.uid = uid;
      this.name = name;
      this.keys.put(key,new Date());
    }
  }

  public static boolean Init() throws IOException, InterruptedException{
    String logDir = Configuration.appHome+"/logs";
    if(!(new File(logDir)).exists()){
      String cmd = "mkdir "+logDir;
      String scriptfile = CPMCommand.exportToScript(cmd);
      Process process = null;
      process = Runtime.getRuntime().exec("sh ".concat(scriptfile));
      Utils.Log(String.valueOf(process.waitFor()));
      new File(scriptfile).delete();
    }
    String taskOutputsDir = Configuration.appHome+"/outputs";
    if(!(new File(taskOutputsDir)).exists()){
      String cmd = "mkdir "+taskOutputsDir;
      String scriptfile = CPMCommand.exportToScript(cmd);
      Process process = null;
      process = Runtime.getRuntime().exec("sh ".concat(scriptfile));
      Utils.Log(String.valueOf(process.waitFor()));
      new File(scriptfile).delete();
    }
    return true;
    
  }
  
  public static boolean havePermission(String user,String type,String corpusid){
    try{
      CloseableHttpClient httpClient = HttpClients.createDefault();
      HttpPost postRequest = new HttpPost(
          Configuration.permissionServiceURL);
      
      String postVal = "username="+URLEncoder.encode(user, "UTF-8")+
          "&type="+URLEncoder.encode(type, "UTF-8")+
          "&cid="+URLEncoder.encode(corpusid, "UTF-8");
      StringEntity input = new StringEntity(postVal,"utf-8");
      input.setContentType("application/x-www-form-urlencoded");
      postRequest.setEntity(input);
  
      HttpResponse response = httpClient.execute(postRequest);
  
      BufferedReader br = new BufferedReader(
                      new InputStreamReader((response.getEntity().getContent())));
  
      StringBuilder output = new StringBuilder();
      String line = null;
      while ((line = br.readLine()) != null) {
          output.append(line+"\n");
      }
      
      br.close();
      httpClient.close();
      
      BasicDBObject val = (BasicDBObject) JSON.parse(output.toString());      
      return val.getBoolean("granted");
    }catch(Exception e){
      e.printStackTrace();
      return false;
    }
  }
  
}
