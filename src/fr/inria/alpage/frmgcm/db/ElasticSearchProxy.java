package fr.inria.alpage.frmgcm.db;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.util.JSON;

import fr.inria.alpage.frmgcm.Configuration;
import fr.inria.alpage.frmgcm.utils.Utils;

/**
 * This class is used to handle connection and some operation over elasticsearch index
 * @author paul
 *
 */
public final class ElasticSearchProxy {

  /**
   * Search graphs within the index  
   * @param query the query json object as defined in elasticsearch reference
   * @return the json object result of the query as defined in elasticsearch reference
   * @throws IOException
   */
  public static BasicDBObject search(BasicDBObject query) throws IOException{
    return call("sentences/graph/_search",query);
  }
  
  /**
   * REST call to the elasticsearch index server
   * @param url the path to the webservice
   * @param postVal the value posted to the service asked
   * @return the json object result of the call to the webservice asked
   */
  public static BasicDBObject call(String url,BasicDBObject postVal){
    return call(url,postVal.toString());
    
  }
  
  /**
   * Apply bulk operation over the graph index 
   * @param bulkOperations the bulk operation string as defined in elasticsearch reference
   * @return the json object result of the bulk operation
   */
  public static BasicDBObject callBulk(String bulkOperations){
    try{
      CloseableHttpClient httpClient = HttpClients.createDefault();
      HttpPost postRequest = new HttpPost(
          "http://"+Configuration.elasticSearchHost+":"+Configuration.elasticSearchPort+"/sentences/graph/_bulk");
      
      
      StringEntity input = new StringEntity(bulkOperations,"utf-8");
      input.setContentType("text/plain");
      postRequest.setEntity(input);

      HttpResponse response = httpClient.execute(postRequest);

      BufferedReader br = new BufferedReader(
                      new InputStreamReader((response.getEntity().getContent())));

      StringBuilder output = new StringBuilder();
      String line = null;
      while ((line = br.readLine()) != null) {
          output.append(line+"\n");
      }
      
      br.close();
      httpClient.close();
      
      return (BasicDBObject) JSON.parse(output.toString());
    }catch(Exception e){
      e.printStackTrace();
      return new BasicDBObject("error",e.getMessage());
    }
  }
  

  /**
   * REST call to the elasticsearch index server
   * @param url the path to the webservice
   * @param postVal the value posted to the service asked
   * @return the json object result of the call to the webservice asked
   */
  public static BasicDBObject call(String url,String postVal){
    try{
      CloseableHttpClient httpClient = HttpClients.createDefault();
      HttpPost postRequest = new HttpPost(
          "http://"+Configuration.elasticSearchHost+":"+Configuration.elasticSearchPort+"/"+url);
      
      
      StringEntity input = new StringEntity(postVal,"utf-8");
      input.setContentType("application/json");
      postRequest.setEntity(input);

      HttpResponse response = httpClient.execute(postRequest);

      BufferedReader br = new BufferedReader(
                      new InputStreamReader((response.getEntity().getContent())));

      StringBuilder output = new StringBuilder();
      String line = null;
      while ((line = br.readLine()) != null) {
          output.append(line+"\n");
      }
      
      br.close();
      httpClient.close();
      
      return (BasicDBObject) JSON.parse(output.toString());
    }catch(Exception e){
      e.printStackTrace();
      return new BasicDBObject("error",e.getMessage());
    }
    
  }
}
