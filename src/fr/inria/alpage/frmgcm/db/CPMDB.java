package fr.inria.alpage.frmgcm.db;

import com.mongodb.*;

import fr.inria.alpage.frmgcm.corpus.Corpus;
import fr.inria.alpage.frmgcm.gm.GraphManager;

import java.net.UnknownHostException;
import java.util.Date;

/**
 * This class handle the connection instance to mongodb
 * @author paul
 *
 */
public class CPMDB {
  /**
   * Singleton instance
   */
  private static CPMDB _instance = new CPMDB();

  /**
   * The mongo db handler
   */
  public DB db = null;
  /**
   * The mongo connexion client
   */
  public MongoClient mongoClient = null;
  
  /**
   * Private constructor for the singleton
   */
  private CPMDB(){
    try {
      mongoClient = new MongoClient("localhost");
    } catch (UnknownHostException e) {
      e.printStackTrace();
    }
    this.db = mongoClient.getDB("cpmdb");
    initIndexes();
  }
  
  /**
   * Init the index for fast retrieval of graph using external id and/or collection id (id of graph is indexed by default)
   */
  private void initIndexes(){
    DBCollection coll = db.getCollection(GraphManager.graphCollection);
    coll.ensureIndex("cid");
    coll.ensureIndex("eid");
  }
  
  /**
   * Singleton instance getter
   * @return the singleton instance of the mongodb db handler
   */
  public static CPMDB GetInstance(){
    return _instance;
  }
  

  
}
