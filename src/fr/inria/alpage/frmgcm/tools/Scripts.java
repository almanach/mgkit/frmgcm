package fr.inria.alpage.frmgcm.tools;

import java.io.File;
import java.util.ArrayList;

import fr.inria.alpage.frmgcm.utils.Utils;
import fr.inria.alpage.frmgcm.utils.Utils.MapReduceFolderDelegate;

public class Scripts {
  
  public static void rename() throws Exception{
    String graphName = "corpus_a";
    String docName = "corpus_corpus_"+graphName+".txt";
    File file = new File("/var/www/mgwiki/sites/default/files/corpus/21/results");
    File[] files = file.listFiles();
    ArrayList<String> data = new ArrayList<String>();
    data.add(docName);
    data.add(graphName);
    for(File item :files){
      if(item.getName().equals(docName)){
        Utils.mapReduceFolder(item, new Scripts.RenameCB(), data);
      }
      Utils.Log(item.getCanonicalPath());
    }
  }
  
  
  public static class RenameCB implements Utils.MapReduceFolderDelegate{

    @Override
    public boolean apply(File file, ArrayList<String> args) throws Exception {
      String newname = file.getName().replace(args.get(1), args.get(0));
      String dir = file.getParent();
      //file.renameTo(dest)
      file.renameTo(new File(dir+"/"+newname));
      return true;
    }
    
  }

}
