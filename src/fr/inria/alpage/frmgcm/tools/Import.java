package fr.inria.alpage.frmgcm.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;

import fr.inria.alpage.frmgcm.corpus.SentenceCollection;
import fr.inria.alpage.frmgcm.db.CPMDB;
import fr.inria.alpage.frmgcm.gm.GraphManager;
import fr.inria.alpage.frmgcm.utils.Utils;

public class Import {
  
  public static String rootFolder = "/tmp/PassageRef/";//"/home/paul/tmp/passage_data/";//
  public static ArrayList<String> corpus = null;
  
  
  public static void main(String[] args) throws Exception {
    File rootFolder = new File(Import.rootFolder);
    File[] corpusFiles = rootFolder.listFiles(new FileFilter() {  
      @Override
      public boolean accept(File pathname) {
        return pathname.isFile();
      }
    });
    
    Import importProcess = new Import();
    for(File corpusMainFile:corpusFiles){
      importProcess.importPassageFiles(corpusMainFile);
    }
  }
  
  private HashMap<String,HashMap<String,File>> revisions = null;
  
  public Import(){
  }
  
  private void fetchRevisions(){
    this.revisions = new HashMap<String,HashMap<String,File>>();
    File backupFolder = new File(Import.rootFolder+"backup");
    File[] revisions = backupFolder.listFiles();
    for(File rev:revisions){
      String filename = rev.getName();
      String[] parts  = filename.split("_");
      String[] subparts = parts[1].split("\\.");
      if(subparts.length>3){
        Utils.Log(filename, "passage_files_import_revisions_fail");
        continue;
      }
      String eid = subparts[0];
      Integer revN = Integer.valueOf(subparts[2]);
      if(!this.revisions.containsKey(parts[0])){
        this.revisions.put(parts[0], new HashMap<String, File>());
      }
      HashMap<String,File> corpusRevision = this.revisions.get(parts[0]);
      Integer revNumber = 0;
      if(corpusRevision.containsKey(eid)){
        String revName = corpusRevision.get(eid).getName();
        String[] revParts = revName.split("_");
        String[] revSubParts = revParts[1].split("\\.");
        revNumber = Integer.valueOf(revSubParts[2]);
      }
      if(revN>revNumber){
        corpusRevision.put(eid, rev);
      }
    }
  }
  
  private String getRevision(String corpus,String eid){
    if(this.revisions == null){
      fetchRevisions();
    }
    if(this.revisions.containsKey(corpus)){
      HashMap<String,File> corpusRevision = this.revisions.get(corpus);
      if(corpusRevision.containsKey(eid)){
        try {
          return getSentenceXML(corpusRevision.get(eid));
        } catch (Exception e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return null;
  }
  
  
  private String getSentenceXML(File file) throws Exception{
    InputSource is = new InputSource(new FileReader(file));
    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    dBuilder.setEntityResolver(new EntityResolver() {
      @Override
      public InputSource resolveEntity(String publicId, String systemId)
              throws SAXException, IOException {
        return new InputSource(new StringReader(""));
      }
    });
    org.w3c.dom.Document doc = dBuilder.parse(is);
    org.w3c.dom.NodeList sentences = doc.getElementsByTagName("Sentence");
    int n = sentences.getLength();
    if(n>1){
      Utils.Log("multiple sentence in backup file "+file.getName());
    }
    for(int i = 0; i<n;i++){
      org.w3c.dom.Node sentence = sentences.item(i);
      TransformerFactory transformerFactory = TransformerFactory.newInstance();
      Transformer transformer = transformerFactory.newTransformer();
      transformer.setOutputProperty("encoding", "ISO-8859-1");
      DOMSource source = new DOMSource(sentence);
      Writer outWriter = new StringWriter();  
      StreamResult result = new StreamResult( outWriter );
      transformer.transform(source, result);
      return outWriter.toString();
    }
    return null;
  }
  
  
  public void importPassageFiles(File mainFile) throws ParserConfigurationException, SAXException, IOException, TransformerException{
    String corpusName = mainFile.getName().replace(".xml", "");
    Utils.Log(corpusName);
    
    SentenceCollection sc = SentenceCollection.Retrieve("passage_"+corpusName);
    
    if(sc==null){
      BasicDBObject sentenceCollectionDefinition = new BasicDBObject();
      sentenceCollectionDefinition.put("name","passage_"+corpusName);
      sentenceCollectionDefinition.put("namespace", "passage_group");
      sc = SentenceCollection.CreateNew(sentenceCollectionDefinition);
    }
    
    DBCollection coll = CPMDB.GetInstance().db.getCollection(GraphManager.graphCollection);
    BasicDBObject q = new BasicDBObject("cid",sc.get_id().toString());
    coll.remove(q);
    
    InputSource is = new InputSource(new FileReader(mainFile));
    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    org.w3c.dom.Document doc = dBuilder.parse(is);
    org.w3c.dom.NodeList sentences = doc.getElementsByTagName("Sentence");
    int n = sentences.getLength();
    for(int i = 0; i<n;i++){
      org.w3c.dom.Node sentence = sentences.item(i);
      String eid = sentence.getAttributes().getNamedItem("id").getNodeValue();
      
      String sentenceData = null;
      if((sentenceData=getRevision(corpusName, eid))==null){
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty("encoding", "ISO-8859-1");
        DOMSource source = new DOMSource(sentence);
        Writer outWriter = new StringWriter();  
        StreamResult result = new StreamResult( outWriter );
        transformer.transform(source, result);
        sentenceData = outWriter.toString();
      }

      sc.addResource(sentenceData, "passage", false, eid);
    }
  }
}
