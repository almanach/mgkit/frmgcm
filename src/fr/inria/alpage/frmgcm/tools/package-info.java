/**
 * This package contains unrelated entry points (scripts) using the application code as library.
 */
package fr.inria.alpage.frmgcm.tools;