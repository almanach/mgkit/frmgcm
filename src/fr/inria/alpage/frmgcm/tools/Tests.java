package fr.inria.alpage.frmgcm.tools;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.bson.types.ObjectId;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

import de.l3s.boilerpipe.extractors.ArticleExtractor;
import de.l3s.boilerpipe.extractors.DefaultExtractor;
import fr.inria.alpage.frmgcm.AppMain;
import fr.inria.alpage.frmgcm.corpus.SentenceCollection;
import fr.inria.alpage.frmgcm.corpus.format.AbstractGraph;
import fr.inria.alpage.frmgcm.corpus.format.DepXMLGraph;
import fr.inria.alpage.frmgcm.db.CPMDB;
import fr.inria.alpage.frmgcm.dpp.ASTStart;
import fr.inria.alpage.frmgcm.dpp.DPathParser;
import fr.inria.alpage.frmgcm.dpp.SimpleNode;
import fr.inria.alpage.frmgcm.gm.Graph;
import fr.inria.alpage.frmgcm.gm.GraphManager;
import fr.inria.alpage.frmgcm.gm.GraphRevision;
import fr.inria.alpage.frmgcm.search.DPathSearchMod;
import fr.inria.alpage.frmgcm.search.DPathSearchMacroManager.DPathMacro;
import fr.inria.alpage.frmgcm.services.DPathQueryMacroLibService;
import fr.inria.alpage.frmgcm.utils.Utils;
import fr.inria.alpage.frmgcm.utils.Utils.BrowseTarFileHandler;
import fr.inria.alpage.frmgcm.utils.Utils.ValidationBean;

public class Tests {

  
  
  /**
   * @param args
   */
  public static void main(String[] args) throws Exception{
    /*GraphRevision gr = new GraphRevision(new BasicDBObject("uri","file:////home/buiquang/tmp/graph_e100_lemonde_passage"));
    AbstractGraph st = AbstractGraph.Create(gr, "passage");
    System.out.println(st.getDepGraphData());*/
    
    //SentenceCollection.cleanFrmgWikiGraphs();
    // TODO Auto-generated method stub
    //nodes [@cat=\"v\"] .(out [@label=\"preparg\"] target [@cat=\"cld\"] ).(out [@label=\"object\"] target [@cat=\"cla\"] );
    //nodes [@cat=\"v\"] .(out [@label=\"object\"] target [@cat=\"nc\"] ).(out [@label=\"advneg\"] target [@cat=\"que_restr\"] );
    //
    DBCollection coll = CPMDB.GetInstance().db.getCollection("dpath_macro");
    coll.createIndex(new BasicDBObject("description","text"));
    BasicDBObject search = new BasicDBObject("search", "ff");
    BasicDBObject textSearch = new BasicDBObject("text", search);
    int matchCount = coll.find(textSearch).count();
    System.out.println("Text search matches: "+ matchCount);

    // Find using the $language operator
    textSearch = new BasicDBObject("text", search.append("$language", "english"));
    matchCount = coll.find(textSearch).count();
    System.out.println("Text search matches (english): "+ matchCount);

    // Find the highest scoring match
    BasicDBObject projection = new BasicDBObject("score", new BasicDBObject("$meta", "textScore"));
    DBObject myDoc = coll.findOne(textSearch, projection);
    System.out.println("Highest scoring document: "+ myDoc);
/*
    Utils.URI uri = new Utils.URI("file:////home/buiquang/tmp");
    ArrayList<String> params = new ArrayList<String>();
    params.add("file : ");
    if(new File(uri.locationEndPoint).isDirectory()){
      Utils.mapReduceFolder(new File(uri.locationEndPoint),new Utils.MapReduceFolderDelegate() {
        
        @Override
        public boolean apply(File file, final ArrayList<String> args) throws Exception {
          Utils.URI furi = new Utils.URI("file:///"+file.getCanonicalPath());
          Utils.browseTar(furi.getStream(), furi.locationEndPoint, new BrowseTarFileHandler() {
            
            @Override
            public boolean handleFile(String filename, InputStream fileStream) {
              System.out.println(args.get(0)+filename);
              return true;
            }
          });
          return true;
        }
      },params);
    }else{
      InputStream is = uri.getStream();
      BufferedReader br = new BufferedReader(new InputStreamReader(is));
      String line = null;
      while((line = br.readLine()) != null){
        System.out.println(line);
      }
      
    }
    
    /*java.net.URL url = new java.net.URL("http://athena.unige.ch/athena/descartes/desc_med.html");
    String text = ArticleExtractor.INSTANCE.getText(url);
    Utils.Log(text,"text");*/

    /*DBCursor cur = coll.find(new BasicDBObject("cid","53a9b346e4b0d6290416b8d3"));
    Utils.Log(cur.toArray().toString(),"wouelalala");
    
    /*SentenceCollection sc = SentenceCollection.Retrieve("EP");
    sc.addResources("file:////home/paul/tmp/EP.tar", "depxml", false, true);
    */
    //testDepGraphSrc();

    
    
    /*SentenceCollection c = SentenceCollection.Retrieve("test_v2");
    if(c == null){
      c = SentenceCollection.CreateNew(new BasicDBObject("name","test_v2"));
    }
    c.addResources("file:////home/paul/tmp/test.tar", "passage",false,true);
    
    /*BasicDBObject graphDef = new BasicDBObject();
    graphDef.put("uri", "http://localhost/mgwiki/mgwiki/ws/graph/536ceb8207da6/export");
    graphDef.put("cid", "frmgwiki");
    graphDef.put("format", "depxml");
    
    
    Graph graph = GraphManager.createGraph(graphDef);
    String uid = graph.get_id().toString();
    System.out.println(graph.getData());

    Graph savedGraph = GraphManager.getGraph(uid);
    System.out.println(savedGraph.get_id().toString());*/

    //String file = "/home/paul/tmp/test.tar#test/frwikipedia_188.tar.gz#frwikipedia_188/3/3/3/frwikipedia_188.E33383.passage.xml";
    /*SentenceCollection collection = SentenceCollection.Retrieve("test2");
    String st = collection.getSentence("/home/paul/tmp/results.frwiki.201312/frwikipedia_230.tar.gz", "E115");
    System.out.println(st);*/
    
    /*DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
    org.w3c.dom.Document doc = docBuilder.newDocument();
    Element rootElement = doc.createElement("CmaSearchResults");
    doc.appendChild(rootElement);
    
 
    for(int i=0;i<10;i++){
      
      Element sentence = doc.createElement("Sentence");
      rootElement.appendChild(sentence);
      sentence.appendChild(doc.createTextNode(i+"{\"};\\\""));
      sentence.setAttribute("test", i+"!");
    }
 
    // write the content into xml file
    TransformerFactory transformerFactory = TransformerFactory.newInstance();
    Transformer transformer = transformerFactory.newTransformer();
    transformer.setOutputProperty("encoding", "ISO-8859-1");
    DOMSource source = new DOMSource(doc);
    Writer outWriter = new StringWriter();  
    StreamResult result = new StreamResult( outWriter );  
    
 
    // Output to console for testing
    // StreamResult result = new StreamResult(System.out);
 
    transformer.transform(source, result);
    
    BasicDBObject obj = new BasicDBObject("data",outWriter.toString());
    DBCollection coll = CPMDB.GetInstance().db.getCollection("test");
    coll.save(obj);
    */
    /*DBCollection coll = CPMDB.GetInstance().db.getCollection("test");
    BasicDBObject documentObj = (BasicDBObject) coll.findOne();
    System.out.println(documentObj.getString("data"));*/
    //Utils.Log(results.toString());
    
   // on startup
    /*BasicDBList sentences = getSentences();
    Utils.Log(String.valueOf(sentences.size()));
    Utils.Log(sentences.toString());
    for(Object obj:sentences){
      CloseableHttpClient httpClient = HttpClients.createDefault();
      HttpPost postRequest = new HttpPost(
          "http://localhost/mgwiki/d3js/ws_post");
      
      
      String postVal = "action=export"+
          "&uid="+(String)obj;
      StringEntity input = new StringEntity(postVal,"utf-8");
      input.setContentType("application/x-www-form-urlencoded");
      postRequest.setEntity(input);

      HttpResponse response = httpClient.execute(postRequest);

      BufferedReader br = new BufferedReader(
                      new InputStreamReader((response.getEntity().getContent())));

      StringBuilder output = new StringBuilder();
      String line = null;
      while ((line = br.readLine()) != null) {
          output.append(line+"\n");
      }
      
      br.close();
      httpClient.close();
      
      Sentence st = Sentence.Create(output.toString(), "depxml");
      st.indexSentence("test", (String)obj, "0");
    }
        
    //System.out.println(output.toString());
    //File fXmlFile = new File("/home/buiquang/tmp/exemple.depxml");
    /*DepXMLGraph g = new DepXMLGraph(output.toString());
    System.out.println(g.graph.toString());
    
    
    /*String home = System.getenv().get("HOME");
    

    
    
    FileInputStream fin = new FileInputStream(home+"/tmp/affaires/affaires360.tar.gz");
    BufferedInputStream in = new BufferedInputStream(fin);
    
    GzipCompressorInputStream gzIn = new GzipCompressorInputStream(in);
    
    final TarArchiveInputStream tarIn =  new TarArchiveInputStream(gzIn);
    TarArchiveEntry tarEntry = tarIn.getNextTarEntry();
    while (tarEntry != null) {
        byte[] btoRead = new byte[1024];
        StringBuffer sb = new StringBuffer();
        int len = 0;
        while ((len = tarIn.read(btoRead)) != -1) {
          String s = new String(btoRead);
          sb.append(s);
        }
        //System.out.println(sb.toString());
        System.out.println(tarEntry.getName());
        if(tarEntry.getName().equals("affaires360/0/0/3/")){
          System.out.println(sb.toString());
        }
        tarEntry = tarIn.getNextTarEntry();
    }
    tarIn.close();
    
    
    
    gzIn.close();*/

  /* Node node = NodeBuilder.nodeBuilder().node();
   Client client = node.client();
   System.out.println("client iniated");
   client.admin().cluster().prepareHealth().setWaitForGreenStatus();
  
   // on shutdown
   SearchResponse response = client.prepareSearch("sentences")
       .setTypes("graph")
       .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
       .setQuery(QueryBuilders.termQuery("did", "530de34fe4b00e19e4eed40b"))             // Query
       //.setPostFilter(FilterBuilders.rangeFilter("age").from(12).to(18))   // Filter
       .setFrom(0).setSize(60).setExplain(true)
       .execute()
       .actionGet();
   
   SearchHit[] hits = response.getHits().getHits();
   for(SearchHit hit:hits){
     System.out.println(hit.sourceAsString());
   }
  
   node.close();
   */
    
        
  }
  
  public static void testDepGraphSrc(){
    HashMap<String,String> testSet = new HashMap<String,String>();
    testSet.put("conll", "file:////home/buiquang/var/testsets/test.conll");
    testSet.put("depxml", "file:////home/buiquang/var/testsets/test.depxml");
    testSet.put("depconll", "file:////home/buiquang/var/testsets/test.depconll");
    testSet.put("passage", "file:////home/buiquang/var/testsets/test.passage");
    String uid = null;
    
    Set<Entry<String,String>> set = testSet.entrySet();
    Iterator<Entry<String,String>> it = set.iterator();
    while(it.hasNext()){
      Entry<String,String> entry = it.next();
      try{
        BasicDBObject graphDef = new BasicDBObject();
        //graphDef.put("uri", );
        graphDef.put("uri",entry.getValue());
        graphDef.put("cid", "test_collection");
        graphDef.put("format", entry.getKey());
        
        
        Graph graph = GraphManager.createGraph(graphDef);
        uid = graph.get_id().toString();
        System.out.println(graph.getDepgraphSrcData());
        
      }catch(Exception e){
        
      }finally{
        if(uid!=null)
          GraphManager.deleteGraph(uid);
        
      }
    }
    
  }
  
  
  
  
  
  
  public static Utils.ValidationBean getAllGraphInFile() throws IOException{
    Utils.ValidationBean ok = new Utils.ValidationBean();
    BrowseTarFileHandler op = new BrowseTarFileHandler() {
      
      @Override
      public boolean handleFile(String filename, InputStream fileStream) {
        if(null == this.data){
          this.data = new ArrayList<String>();
        }
        String patternStr=".*?\\.(E\\d+)\\..*?";
        Pattern p = Pattern.compile(patternStr);
        Matcher m = p.matcher(filename);
        if(m.find()){
          String id = m.group(1);
          ((ArrayList<String>)this.data).add(filename);
          ((ArrayList<String>)this.data).add(id);
        }
        return false;
      }
    };
    String file = "/home/paul/tmp/test.tar";
    FileInputStream fin = new FileInputStream(file);
    try {
      Utils.browseTar(fin, file,op);
    } catch (Exception e) {
      e.printStackTrace();
    } finally{
      fin.close();
    }
    for(String s:(ArrayList<String>)op.data){
      Utils.Log(s, "getAllGraphFile");
    }
    return ok;
  }
  
    
  
  
  
  
  
  public static void dpathparsertest(){
    String query = "($lol AND edges[@label=Sé\\'re] OR nodes[@lemma=yo]) OR root[@pos=V].(out[@bla=shit])x(in[@f=r] OR out[@g=g]);";
    java.io.StringReader sr = new java.io.StringReader( query );
    java.io.Reader r = new java.io.BufferedReader( sr ); 
    DPathParser t = new DPathParser(r);
    try {
      ASTStart n = t.Start();
      Utils.Log(query,4);
      n.dump("");
    } catch (Exception e) {
      Utils.Log("Couldn't parse query",1);
      Utils.Log(e.getMessage(),1);
      e.printStackTrace();
    }

  }

}
