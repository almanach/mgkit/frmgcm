/**
 * This package contains the main entry point of the application and the global configuration.
 */
package fr.inria.alpage.frmgcm;