package fr.inria.alpage.frmgcm.corpus.format;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.xml.sax.InputSource;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;

import fr.inria.alpage.frmgcm.corpus.Document;
import fr.inria.alpage.frmgcm.corpus.format.DepXMLGraph.CompNodePosition;
import fr.inria.alpage.frmgcm.gm.GraphRevision;
import fr.inria.alpage.frmgcm.utils.Utils;

/**
 * This class handle the PASSAGE graph format
 * @author buiquang
 *
 */
public class PassageGraph extends AbstractGraph {
  
  /**
   * The json partial representation of the PASSAGE xml graph format
   */
  public BasicDBObject graph;
  /**
   * The sentence that is represented by this graph
   */
  private String sentence;
  
  /**
   * Default constructor from graph revision data
   * @param gr
   */
  public PassageGraph(GraphRevision gr) {
    this.graphRevision = gr;
    try {
      this.parseFromStringData(gr.getData().trim());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void indexSentenceContent(BasicDBObject json) {
    BasicDBList nodes_all = new BasicDBList();
    BasicDBList edges_all = new BasicDBList();
    
    BasicDBObject graph_tokens = (BasicDBObject)graph.get("tokens");
    BasicDBObject graph_chunks = (BasicDBObject)graph.get("chunks");
    BasicDBObject graph_words = (BasicDBObject)graph.get("words");
    BasicDBObject graph_links = (BasicDBObject)graph.get("links");
    
    Set<Entry<String,Object>> words = graph_words.entrySet();
    for(Entry<String,Object> obj:words){
      BasicDBObject word = (BasicDBObject)obj.getValue();
      String id = obj.getKey();
      
      String[] tokens = (String[])word.get("tokens");
      String token = "";
      for(int i = 0; i< tokens.length; i++){
        token += " "+graph_tokens.getString(tokens[i]);
      }
      token = token.trim();
      
      BasicDBObject node = new BasicDBObject();
      node.put("id", id);
      node.put("form", word.getString("form"));
      node.put("lemma", word.getString("lemma"));
      node.put("token",token);
      node.put("pos",word.getString("pos"));
      
      AbstractGraph.registerFeatureVal("passage", "node", "pos", word.getString("pos"));
      
      
      nodes_all.add(node);
    }
    
    Set<Entry<String,Object>> links = graph_links.entrySet();
    for(Entry<String,Object> obj:links){
      BasicDBObject link = (BasicDBObject)obj.getValue();
      String id = obj.getKey();
      
      String source_id = link.getString("source");
      String target_id = link.getString("target");
      if(graph_words.get(source_id)==null){
        source_id = getWordHeadFromChunk(source_id, graph_chunks);
      }
      if(graph_words.get(target_id)==null){
        target_id = getWordHeadFromChunk(target_id, graph_chunks);
      }

      BasicDBObject edge = new BasicDBObject();
      edge.put("id", id);
      edge.put("target", target_id);
      edge.put("source", source_id);
      edge.put("label", link.getString("label"));
      AbstractGraph.registerFeatureVal("passage", "edge", "label", link.getString("label"));
        
        edges_all.add(edge);
    }

    json.put("nodes", nodes_all);
    json.put("edges", edges_all);

  }
  
  /**
   * Get the word head from chunk
   * @param chunkId the id of the chunk to get the word head of
   * @param graph_chunks the hash table of chunks (indexed by id)
   * @return the id of the word that will be selected has head of the chunk
   */
  private String getWordHeadFromChunk(String chunkId,BasicDBObject graph_chunks){
    BasicDBObject chunk = (BasicDBObject)graph_chunks.get(chunkId);
    if(chunk == null){
      return "";
    }
    BasicDBList words = (BasicDBList)chunk.get("words");
    return (String)words.get(words.size()-1);
  }


  
  @Override
  protected void parseFromStringData(String data) {
    data = data.replace("&amp;","__TMP_AMP__");
    data = data.replace("&", "&amp;");
    data = data.replace("__TMP_AMP__","&amp;");

    InputSource is = new InputSource(new StringReader(data.trim()));
    try{
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      org.w3c.dom.Document doc = dBuilder.parse(is);
      parseFromXMLDoc(doc);
    }catch(Exception e){
      Utils.Log(e.getMessage());
    }
    this.parsedData = graph;
  }
  
  /**
   * Create the "passage" json representation of this graph
   * @param doc the passage xml representation of this graph
   * @throws XPathExpressionException
   */
  private void parseFromXMLDoc(org.w3c.dom.Document doc) throws XPathExpressionException{
    doc.getDocumentElement().normalize();
    
    
    
    /*$clusters = array();
    $merged_nodes = array();
    */
    graph = new BasicDBObject();
    
    BasicDBObject graph_nodes = new BasicDBObject();
    BasicDBObject graph_tokens = new BasicDBObject();
    BasicDBObject graph_edges = new BasicDBObject();
    BasicDBObject graph_chunks = new BasicDBObject();
    
    org.w3c.dom.NodeList tokens = doc.getElementsByTagName("T");
    org.w3c.dom.NodeList words = doc.getElementsByTagName("W");
    org.w3c.dom.NodeList chunks = doc.getElementsByTagName("G");
    org.w3c.dom.NodeList edges = doc.getElementsByTagName("R");
    
    int n = tokens.getLength();
    this.sentence = "";
    for(int i = 0; i<n;i++){
      org.w3c.dom.Node token = tokens.item(i);
      this.sentence += token.getTextContent().trim()+" ";
      graph_tokens.put(token.getAttributes().getNamedItem("id").getNodeValue(),token.getTextContent().trim());
    }
    
    n = words.getLength();
    for(int i = 0; i<n;i++){
      org.w3c.dom.Node word = words.item(i);
      BasicDBObject word_obj = new BasicDBObject();
      org.w3c.dom.NamedNodeMap attrs = word.getAttributes();
      String[] associatedTokens = attrs.getNamedItem("tokens").getNodeValue().split("\\s");
      word_obj.put("tokens", associatedTokens);
      org.w3c.dom.Node lemma = attrs.getNamedItem("lemma");
      if(lemma != null){
        word_obj.put("lemma", lemma.getNodeValue());
      }
      org.w3c.dom.Node pos = attrs.getNamedItem("pos");
      if(pos != null){
        word_obj.put("pos", pos.getNodeValue());
      }
      org.w3c.dom.Node form = attrs.getNamedItem("form");
      if(form!=null){
        word_obj.put("form", form.getNodeValue());
      }
      
      graph_nodes.put(attrs.getNamedItem("id").getNodeValue(),word_obj);
    }
    
    n = chunks.getLength();
    for(int i = 0; i<n;i++){
      org.w3c.dom.Node chunk = chunks.item(i);
      BasicDBObject chunk_obj = new BasicDBObject();
      org.w3c.dom.NamedNodeMap attrs = chunk.getAttributes();
      org.w3c.dom.NodeList containedWords = chunk.getChildNodes();
      BasicDBList containedWordsId = new BasicDBList();
      int l = containedWords.getLength();
      for(int j=0;j<l;j++){
        org.w3c.dom.Node containedWord = containedWords.item(j);
        if(containedWord.getNodeName().equals("W")){
          org.w3c.dom.NamedNodeMap cwattrs = containedWord.getAttributes();
          org.w3c.dom.Node attrNode = cwattrs.getNamedItem("id");
          containedWordsId.add(attrNode.getNodeValue());
        }
      }
      chunk_obj.put("words", containedWordsId);
      chunk_obj.put("type", attrs.getNamedItem("type").getNodeValue());
      graph_chunks.put(attrs.getNamedItem("id").getNodeValue(),chunk_obj);
    }
    
    n = edges.getLength();
    BasicDBObject max_edges = new BasicDBObject("n",n);
    for(int i = 0; i<n;i++){
      org.w3c.dom.Node edge = edges.item(i);
      transformRelations(edge,graph_edges,max_edges);
    }
    
    graph.put("words", graph_nodes);
    graph.put("links", graph_edges);
    graph.put("chunks", graph_chunks);
    graph.put("tokens", graph_tokens);
    
    
  }
  
  /**
   * Transform unoriented edges into oriented edges
   * @param edge the edge to transform
   * @param graph_edges the list of transformed edges
   * @param max_edges a json object containing the property n, the number of new edges created from triplet edge
   */
  private void transformRelations(org.w3c.dom.Node edge,BasicDBObject graph_edges,BasicDBObject max_edges){
    
    org.w3c.dom.NamedNodeMap attrs = edge.getAttributes();
    org.w3c.dom.NodeList relationElements = edge.getChildNodes();
    BasicDBObject relationElementsNormalized = new BasicDBObject();

    for(int j = 0 ; j < relationElements.getLength(); j++){
      org.w3c.dom.Node elt = relationElements.item(j);
      String nodename = elt.getNodeName();
      if(!nodename.equals("#text") && !nodename.equals("s-o")){
        org.w3c.dom.Node attrRel = elt.getAttributes().getNamedItem("ref");
        if(attrRel!=null){
          relationElementsNormalized.put(nodename, attrRel.getNodeValue());
        }else{
          Utils.Log(nodename+" has no attribute rel!",1);
        }
      }
    }
    
    String edgeType = attrs.getNamedItem("type").getNodeValue().trim();

    BasicDBObject edge_obj = new BasicDBObject();
    edge_obj.put("id", attrs.getNamedItem("id").getNodeValue());
    edge_obj.put("label", edgeType);
    
    String sourceKey = null;
    String targetKey = null;
    if(edgeType.equalsIgnoreCase("COORD")){
      String id = attrs.getNamedItem("id").getNodeValue();
      String id2 = "";
      int n = max_edges.getInt("n");
      n++;
      max_edges.put("n", n);
      
      String patternStr="(.*?R)\\d+";
      Pattern p = Pattern.compile(patternStr);
      Matcher m = p.matcher(id);
      if(m.find()){
        String pre = m.group(1);
        id2 = pre+n;
      }else{
        Utils.Log("error when generating new id for coordination",1);
      }
      
      BasicDBObject edge_obj1 = new BasicDBObject();
      BasicDBObject edge_obj2 = new BasicDBObject();
      edge_obj1.put("id", id);
      edge_obj1.put("label", "COORD-G");
      edge_obj2.put("id", id2);
      edge_obj2.put("label", "COORD-D");
      
      String source = relationElementsNormalized.getString("coordonnant");
      String target = relationElementsNormalized.getString("coord-g");
      if(target != null && source != null){
        edge_obj1.put("source", source);
        edge_obj1.put("target", target);
        graph_edges.put(id,edge_obj1);
        
      }
      
      target = relationElementsNormalized.getString("coord-d");
      if(target != null && source != null){
        edge_obj2.put("source", source);
        edge_obj2.put("target", target);
        graph_edges.put(id2,edge_obj2);
        
      }       
      
      
      return;
    }else if(edgeType.equalsIgnoreCase("SUJ-V")){
      sourceKey = "verbe";
      targetKey = "sujet";
    }else if(edgeType.equalsIgnoreCase("CPL-V")){
      sourceKey = "verbe";
      targetKey = "complement";
    }else if(edgeType.equalsIgnoreCase("MOD-N")){
      sourceKey = "nom";
      targetKey = "modifieur";
    }else if(edgeType.equalsIgnoreCase("AUX-V")){
      sourceKey = "verbe";
      targetKey = "auxiliaire";
    }else if(edgeType.equalsIgnoreCase("COD-V")){
      sourceKey = "verbe";
      targetKey = "cod";
    }else if(edgeType.equalsIgnoreCase("MOD-V")){
      sourceKey = "verbe";
      targetKey = "modifieur";
    }else if(edgeType.equalsIgnoreCase("COMP")){
      sourceKey = "verbe";
      targetKey = "complementeur";
    }else if(edgeType.equalsIgnoreCase("MOD-A")){
      sourceKey = "adjectif";
      targetKey = "modifieur";
    }else if(edgeType.equalsIgnoreCase("APPOS")){
      sourceKey = "premier";
      targetKey = "appose";
    }else if(edgeType.equalsIgnoreCase("JUXT")){
      sourceKey = "premier";
      targetKey = "suivant";
    }else if(edgeType.equalsIgnoreCase("MOD-R")){
      sourceKey = "adverbe";
      targetKey = "modifieur";
    }else if(edgeType.equalsIgnoreCase("MOD-P")){
      sourceKey = "preposition";
      targetKey = "modifieur";
    }else if(edgeType.equalsIgnoreCase("ATB-SO")){
      sourceKey = "verbe";
      targetKey = "attribut";
      //@todo keep s-o information
    }else{
      Utils.Log("Unknown passage relation type "+edgeType,1);
      return;
    }
    
    String source = relationElementsNormalized.getString(sourceKey);
    String target = relationElementsNormalized.getString(targetKey);
    if(target != null && source != null){
      edge_obj.put("source", source);
      edge_obj.put("target", target);
      
    }else{
      Utils.Log("Problem (missing target or source) with passage relation type "+edgeType,1);
      return;
    }
    
    graph_edges.put(attrs.getNamedItem("id").getNodeValue(),edge_obj);
      

  }
 

  @Override
  public boolean saveRevision(PrintWriter pw) {
    // TODO Auto-generated method stub
    return true;
  }

  @Override
  public String restoreRevision() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void setIDOffset(int offset) {
    // TODO Auto-generated method stub

  }

  @Override
  public ArrayList<String> getContent() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public String getSentenceString() {
    return sentence.trim();
  }

  @Override
  public BasicDBObject getDepGraphData() {
    BasicDBObject graph = new BasicDBObject();
    BasicDBObject data = new BasicDBObject("graph",graph);
    
    BasicDBList nodes_all = new BasicDBList();
    BasicDBList edges_all = new BasicDBList();
    BasicDBList chunks_all = new BasicDBList();
    
    BasicDBObject graph_tokens = (BasicDBObject)this.graph.get("tokens");
    BasicDBObject graph_chunks = (BasicDBObject)this.graph.get("chunks");
    BasicDBObject graph_words = (BasicDBObject)this.graph.get("words");
    BasicDBObject graph_links = (BasicDBObject)this.graph.get("links");
    
    Set<Entry<String,Object>> words = graph_words.entrySet();
    for(Entry<String,Object> obj:words){
      BasicDBObject word = (BasicDBObject)obj.getValue();
      String id = obj.getKey();
      
      String[] tokens = (String[])word.get("tokens");
      String token = "";
      for(int i = 0; i< tokens.length; i++){
        token += " "+graph_tokens.getString(tokens[i]);
      }
      token = token.trim();
      
      // node creation
      BasicDBObject node_data = new BasicDBObject();
      BasicDBObject node = new BasicDBObject("#data",node_data);
      
      // presentation
      node.put("id", id);
      node.put("label", "@form");
      BasicDBList sublabel = new BasicDBList();
      node.put("sublabel", sublabel);
      sublabel.add("@lemma");
      sublabel.add("@pos");
      
      // data
      node_data.put("form", word.getString("form"));
      node_data.put("lemma", word.getString("lemma"));
      node_data.put("token",token);
      node_data.put("pos",word.getString("pos"));
      
      nodes_all.add(node);
    }
    
    Set<Entry<String,Object>> chunks = graph_chunks.entrySet();
    for(Entry<String,Object> obj:chunks){
      BasicDBObject chunk = (BasicDBObject)obj.getValue();
      String id = obj.getKey();
      
      BasicDBObject group = new BasicDBObject();
      group.put("id", id);
      group.put("label", "@type");
      group.put("elements", chunk.get("words"));
      
      group.put("#data", new BasicDBObject("type",chunk.getString("type")));

      chunks_all.add(group);
    }
    
    Set<Entry<String,Object>> links = graph_links.entrySet();
    for(Entry<String,Object> obj:links){
      BasicDBObject link = (BasicDBObject)obj.getValue();
      String id = obj.getKey();
      
      String source_id = link.getString("source");
      String target_id = link.getString("target");
/*      if(graph_words.get(source_id)==null){
        source_id = getWordHeadFromChunk(source_id, graph_chunks);
      if(graph_words.get(target_id)==null){
        target_id = getWordHeadFromChunk(target_id, graph_chunks);
      }*/

      BasicDBObject edge = new BasicDBObject();
      edge.put("id", id);
      edge.put("target", target_id);
      edge.put("source", source_id);
      edge.put("label", link.getString("label"));

      edges_all.add(edge);
    }

    graph.put("words", nodes_all);
    graph.put("links", edges_all);
    graph.put("chunks", chunks_all);
    return data;
  }

  @Override
  public String getSignature(String highlightings) {
    // TODO Auto-generated method stub
    return null;
  }

}
