package fr.inria.alpage.frmgcm.corpus.format;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import com.mongodb.BasicDBObject;
import com.mongodb.util.JSON;

import fr.inria.alpage.frmgcm.gm.GraphRevision;

/**
 * This class handle json depgraph native format
 * @author buiquang
 *
 */
public class JSONGraph extends AbstractGraph {
  
  /**
   * The json depgraph reprentation
   */
  public BasicDBObject jsongraph = null;
  
  /**
   * Default constructor from graph revision data
   * @param gr
   * @throws IOException
   */
  public JSONGraph(GraphRevision gr) throws IOException{
    this.graphRevision = gr;
    try {
      this.parseFromStringData(gr.getData().trim());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  @Override
  public String getSentenceString() {
    BasicDBObject graph = (BasicDBObject)jsongraph.get("graph");
    @SuppressWarnings("unchecked")
    ArrayList<BasicDBObject> graph_words = (ArrayList<BasicDBObject>)graph.get("words");
    String sentence = "";
    for(BasicDBObject obj:graph_words){
      String token = obj.getString("label").trim();
      if(token.equals("") || token.equals("_")){
        continue;
      }
      sentence += " "+token;
    }
    return sentence.trim();
  }

  @Override
  public BasicDBObject getDepGraphData() {
    
    return jsongraph;
  }

  @Override
  public void indexSentenceContent(BasicDBObject json) {
    json.append("sentence", this.getSentenceString());

  }

  @Override
  protected void parseFromStringData(String data) throws Exception {
    jsongraph = (BasicDBObject) JSON.parse(data);
  }

  @Override
  public boolean saveRevision(PrintWriter pw) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public String restoreRevision() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void setIDOffset(int offset) {
    // TODO Auto-generated method stub

  }

  @Override
  public ArrayList<String> getContent() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public String getSignature(String highlightings) {
    // TODO Auto-generated method stub
    return null;
  }

}
