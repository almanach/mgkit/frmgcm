package fr.inria.alpage.frmgcm.corpus.format;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.bson.types.ObjectId;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

import fr.inria.alpage.frmgcm.corpus.Document;
import fr.inria.alpage.frmgcm.db.CPMDB;
import fr.inria.alpage.frmgcm.db.ElasticSearchProxy;
import fr.inria.alpage.frmgcm.gm.GraphRevision;
import fr.inria.alpage.frmgcm.utils.Utils;

/**
 * This class is the abstact base class representing a graph under a certain type of format (see fr.inria.alpage.frmgcm.corpus.format package)
 * @author paul
 *
 */
public abstract class AbstractGraph {
  /**
   * Generic object used to store formated data of the graph depending of the format
   */
  public Object parsedData = null;
  /**
   * The {@see GraphRevision} the formated graph is bound to
   */
  protected GraphRevision graphRevision = null;
  /**
   * static informations about available formats (file extension, class helper and name)
   */
  public static HashMap<String,HashMap<String,Object>> formats = new HashMap<String,HashMap<String,Object>>();
  /**
   * Datastore used to gather tagsets (should be removed when sufficient information has been retrieved)
   */
  public static BasicDBObject tagsets = new BasicDBObject();
  
  // initialize the available formats definition
  static{
    HashMap<String,Object> conllFormat = new HashMap<String,Object>();
    conllFormat.put("fileExtension", "conll");
    conllFormat.put("class", CONLLGraph.class);
    conllFormat.put("name", "conll");
    formats.put("conll", conllFormat);
    
    HashMap<String,Object> jsonFormat = new HashMap<String,Object>();
    jsonFormat.put("fileExtension", "json");
    jsonFormat.put("class", JSONGraph.class);
    jsonFormat.put("name", "json");
    formats.put("json", jsonFormat);
    
    HashMap<String,Object> passageFormat = new HashMap<String,Object>();
    passageFormat.put("fileExtension", "passage.xml");
    passageFormat.put("class", PassageGraph.class);
    passageFormat.put("name", "passage");
    formats.put("passage", passageFormat);
    
    HashMap<String,Object> depxmlFormat = new HashMap<String,Object>();
    depxmlFormat.put("fileExtension", "dis.dep.xml");
    depxmlFormat.put("class", DepXMLGraph.class);
    depxmlFormat.put("name","depxml");
    formats.put("depxml", depxmlFormat);
    formats.put("dis_xmldep", depxmlFormat);
    
    HashMap<String,Object> depconllFormat = new HashMap<String,Object>();
    depconllFormat.put("fileExtension", "dep.conll");
    depconllFormat.put("class", DepCONLLGraph.class);
    depconllFormat.put("name", "depconll");
    formats.put("depconll", depconllFormat);
    
    DBCollection coll = CPMDB.GetInstance().db.getCollection("tagsets");
    DBCursor tagsetcursor = coll.find();
    while(tagsetcursor.hasNext()){
      BasicDBObject tagset = (BasicDBObject)tagsetcursor.next();
      tagsets.put(tagset.getString("name"),tagset);
    }
    
  }
  
  /**
   * Factory
   * @return the abstract formatted graph
   * @throws SecurityException 
   * @throws NoSuchMethodException 
   * @throws InvocationTargetException 
   * @throws IllegalArgumentException 
   * @throws IllegalAccessException 
   * @throws InstantiationException 
   */
  public static AbstractGraph Create(GraphRevision data,String format) {
    try{
      HashMap<String,Object> formatInfo = AbstractGraph.formats.get(format);
      if(formatInfo!=null){
        return (AbstractGraph)((Class)formatInfo.get("class")).getConstructor(GraphRevision.class).newInstance(data);
      }
    }catch(Exception e){
      
    }
    return null;
  }
  
  /**
   * Register features values found in data => generate a tagset
   * @param objType (Node or Edge)
   * @param featureName
   * @param featureVal
   */
  public static void registerFeatureVal(String tagsetName,String objType,String featureName,String featureVal){
    Object obj = tagsets.get(tagsetName);
    BasicDBObject tagset = null;
    if(obj == null){
      tagset = new BasicDBObject("name",tagsetName);
      tagset.put("edge", new BasicDBObject());
      tagset.put("node", new BasicDBObject());
      tagsets.put(tagsetName, tagset);
    }else{
      tagset = (BasicDBObject)obj;
    }
    BasicDBObject tagsetObjFeatures = (BasicDBObject)tagset.get(objType);
    BasicDBList featureVals = (BasicDBList)tagsetObjFeatures.get(featureName);
    if(featureVals==null){
      featureVals = new BasicDBList();
      tagsetObjFeatures.put(featureName, featureVals);
    }
    if(!featureVals.contains(featureVal)){
      featureVals.add(featureVal);
    }
  }
  
  /**
   * Save the tagset in database
   */
  public static void saveTagset(){
    DBCollection coll = CPMDB.GetInstance().db.getCollection("tagsets");
    for(Object obj:tagsets.values()){
      coll.save((BasicDBObject)obj);
    }
  }
 
  /**
   * Store and index sentence with elastic search
   * @deprecated
   * @param owner the document in which the graph is referenced
   * @param the id of the graph
   * @param the version of the graph
   * @param checkReplace if true, check if previous version of the graph is already indexed and replace it, else index the graph as a new one
   */
  public void indexSentence(Document owner,String id,String vid,boolean checkReplace) throws Exception{
    indexSentence(owner.getId(),id,vid,checkReplace);
  }
  
  /**
   * Store and index sentence with elastic search
   * @param ownerId the id of the owner in which the graph is referenced (Document or SentenceCollection)
   * @param the id of the graph
   * @param the version of the graph
   * @param checkReplace if true, check if previous version of the graph is already indexed and replace it, else index the graph as a new one
   */
  public void indexSentence(String ownerId,String id,String vid,boolean checkReplace) throws Exception{
    String _id = "";
    if(checkReplace){
      BasicDBObject existingSentence = getIndexedSentence(ownerId,id);
      if(existingSentence!=null){
        Utils.Log("replacing old indexation");
        _id = existingSentence.getString("_id");
      }
    }
    BasicDBObject json = new BasicDBObject();
    json.put("sid", id);
    json.put("did", ownerId);
    indexSentenceContent(json);
    ElasticSearchProxy.call("sentences/graph/"+_id, json);
  }
  
  /**
   * String version of the indexation data used by elasticsearch
   * @param ownerId the id of the object (Document or SentenceCollection) the graph is referred
   * @param id the id of the graph
   * @param vid the version of the graph
   * @return the string version of the index object that will be passed to elasticseach index
   */
  public String indexSentenceInfo(String ownerId,String id,String vid){
    BasicDBObject json = new BasicDBObject();
    json.put("sid", id);
    json.put("did", ownerId);
    indexSentenceContent(json);
    return json.toString();
  }
  
  /**
   * The sentence that the graph represent
   * @return the sentence derived from the graph
   */
  public abstract String getSentenceString();

  /**
   * The graph data to the depgraph(json) format
   * @return the data to the depgraph format
   */
  public abstract BasicDBObject getDepGraphData();

  /**
   * Populate the indexation data with graph informations
   * @param json the data that will be send to elasticsearch for indexing (see indexing schema in mgwiki documentation)
   */
  public abstract void indexSentenceContent(BasicDBObject json);
  
  /**
   * Construct and store the data of the graph from the raw formatted data string
   * @param data the raw formatted string data
   * @throws Exception
   */
  protected abstract void parseFromStringData(String data) throws Exception;
  
  /**
   * Save the revision into a file (used in CorpusManagementApplication when saving an edited graph)
   * @param owner the document owner of the graph
   * @param fileinfo the file information about where to store the graph
   * @return "ok" if all went well, null otherwise
   */
  public String saveRevision(Document owner,Object[] fileinfo){
    try {
      PrintWriter pw = new PrintWriter(new FileWriter(((String)fileinfo[5]).concat("."+fileinfo[3])));
      
      saveRevision(pw);
      
      pw.close();
      this.indexSentence(owner, (String)fileinfo[1],String.valueOf((Integer)fileinfo[3]),true);
      return "ok";
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } 
    return null;
  }
  
  /**
   * Write the formatted content of the graph into a file (this method is yet not implemented for every formats!)
   * @param pw the printwriter that handle the file writting buffer
   * @return a boolean indication if the operation was a success
   */
  public abstract boolean saveRevision(PrintWriter pw);
  
  /**
   * 
   * @return
   */
  public abstract String restoreRevision();
  
  public abstract void setIDOffset(int offset);
  
  /**
   * Retrieve the indexed sentence defined by the couple (ownerId and id)
   * @param ownerId the {@see Document} or {@see SentenceCollection} id that own the graph
   * @param id the id of the graph
   * @return the elasticsearch result object of the query
   */
  public static BasicDBObject getIndexedSentence(String ownerId,String id){
    BasicDBObject query = new BasicDBObject();
    BasicDBObject queryInfo = new BasicDBObject();
    BasicDBObject sid = new BasicDBObject("term",new BasicDBObject("sid",id));
    BasicDBObject did = new BasicDBObject("term",new BasicDBObject("did",ownerId));
    BasicDBList match = new BasicDBList();
    match.add(sid);
    match.add(did);
    queryInfo.put("and", match);
    BasicDBObject filter = new BasicDBObject("filter", queryInfo);
    BasicDBObject const_score = new BasicDBObject("constant_score",filter);
    query.put("query", const_score);
    
    BasicDBObject result = null; 
    try {
      result = ElasticSearchProxy.search(query);
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    }
    
    BasicDBObject hits = (BasicDBObject)result.get("hits");
    if(result.get("error")!=null || hits.getInt("total")==0){
      return null;
    }else{
      return (BasicDBObject)((BasicDBList)hits.get("hits")).get(0);
    }
    
    
  }


  public abstract ArrayList<String> getContent() ;

  
  public abstract String getSignature(String highlightings);


  
}
