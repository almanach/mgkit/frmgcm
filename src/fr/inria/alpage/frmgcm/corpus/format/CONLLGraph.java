package fr.inria.alpage.frmgcm.corpus.format;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;

import fr.inria.alpage.frmgcm.corpus.Document;
import fr.inria.alpage.frmgcm.gm.GraphRevision;
import fr.inria.alpage.frmgcm.utils.Utils;

/**
 * This class handles the CONLL graph format
 * @author buiquang
 *
 */
public class CONLLGraph extends AbstractGraph {
  /**
   * The lines of the entry conll data
   */
  public ArrayList<String> tokenLines;

  /**
   * Default constructor from graph revision data
   * @param gr
   * @throws IOException
   */
  public CONLLGraph(GraphRevision gr) throws IOException{
    this.graphRevision = gr;
    try {
      this.parseFromStringData(gr.getData().trim());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  
  @Override
  protected void parseFromStringData(String data) {
    if(this.parsedData == null){
      this.tokenLines = (ArrayList<String>) (this.parsedData = new ArrayList<String>());
    }
    String[] lines = data.split("\\n");
    for(String line : lines){
      if(line.startsWith("#")){
        continue;
      }
      this.tokenLines.add(line);
    }
  }
  
  @Override
  public void setIDOffset(int offset){
    for(int i =0 ; i<tokenLines.size();i++){
      String line = tokenLines.get(i);
      String[] items = line.split("\t");
      if(!items[0].equals("0")){
        items[0]+=offset;
      }
      if(!items[6].equals("0")){
        items[6]+=offset;
      }
      String newline = "";
      for(int j=0;j<items.length;++j){
        newline += items[j]+"\t";
      }
      newline = newline.trim();
      tokenLines.set(i, newline);
    }
  }

  @Override
  public void indexSentenceContent(BasicDBObject json){
    String featureRegex="(\\w+)\\s*=\\s*(\\w+)\\s*\\|*\\s*";
    String relationRegex = "([A-Za-z0-9_\\-\\+:\\.]+)\\s*\\|*\\s*";
    
    json.append("sentence", this.getSentenceString());
    
    BasicDBList nodes_all = new BasicDBList();
    BasicDBList edges_all = new BasicDBList();
    
    for(String line:tokenLines){
      if(line.trim().isEmpty()){
        continue;
      }
      String[] items = line.split("\t");
      
      if(items.length<5){
        Utils.Log("pb with line"+line,1);
        continue;
      }
      
      BasicDBObject node = new BasicDBObject();
      node.put("id", items[0]);
      node.put("form", items[1]);
      node.put("lemma", items[2]);
      node.put("cpos",items[3]);
      node.put("pos",items[4]);
      
      AbstractGraph.registerFeatureVal("conll", "node", "cpos", items[3]);
      AbstractGraph.registerFeatureVal("conll", "node", "pos", items[4]);
      
      Pattern p = Pattern.compile(featureRegex);
      Matcher m = p.matcher(items[5]);
      BasicDBObject features = new BasicDBObject();
      while(m.find()){
        features.put(m.group(1),m.group(2));
      }
      node.put("features", features);
      
      if(items.length>7){
        p = Pattern.compile(relationRegex);
        m = p.matcher(items[6]);
        BasicDBList edges = new BasicDBList();
        while(m.find()){
          edges.add(new BasicDBObject("source",m.group(1)));
        }
        m = p.matcher(items[7]);
        int i = 0;
        while(m.find()){
          BasicDBObject edge = ((BasicDBObject)edges.get(i));
          edge.put("target", items[0]);
          edge.put("label", m.group(1));
          AbstractGraph.registerFeatureVal("conll", "edge", "label", m.group(1));

          i++;
        }
        
        edges_all.addAll(edges);
      }
      
      nodes_all.add(node);
    }

    json.put("nodes", nodes_all);
    json.put("edges", edges_all);

  }

  @Override
  public String getSentenceString(){
    StringBuilder sb = new StringBuilder();
    for(String line :this.tokenLines){
      String[] items = line.split("\t");
      if(items.length<2){
        Utils.Log("pb with line "+line,1);
        continue;
      }
      sb.append(items[1]+" ");
    }
    return sb.toString().trim();
  }

  @Override
  public boolean saveRevision(PrintWriter pw) {
    for(String line : this.tokenLines){
      pw.println(line);
    }
    return true;
  }

  @Override
  public String restoreRevision() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public ArrayList<String> getContent() {
    return this.tokenLines;
  }

  @Override
  public BasicDBObject getDepGraphData() {
    BasicDBObject graph = new BasicDBObject();
    BasicDBObject data = new BasicDBObject("graph",graph);
    
    
    String featureRegex="(\\w+)\\s*=\\s*(\\w+)\\s*\\|*\\s*";
    String relationRegex = "([A-Za-z0-9_\\-\\+:\\.]+)\\s*\\|*\\s*";
    
    BasicDBList nodes_all = new BasicDBList();
    BasicDBList edges_all = new BasicDBList();
    
    for(String line:tokenLines){
      if(line.trim().isEmpty()){
        continue;
      }
      String[] items = line.split("\t");
      
      if(items.length<5){
        Utils.Log("pb with line"+line,1);
        continue;
      }
      
      // node creation
      BasicDBObject node_data = new BasicDBObject();
      BasicDBObject node = new BasicDBObject("#data",node_data);
      
      // presentation
      node.put("id", items[0]);
      node.put("label", "@form");
      BasicDBList sublabel = new BasicDBList();
      node.put("sublabel", sublabel);
      sublabel.add("@lemma");
      sublabel.add("@cpos");
      
      // data
      node_data.put("form", items[1]);
      node_data.put("lemma", items[2]);
      node_data.put("cpos",items[3]);
      node_data.put("pos",items[4]);
      
      Pattern p = Pattern.compile(featureRegex);
      Matcher m = p.matcher(items[5]);
      BasicDBObject features = new BasicDBObject();
      while(m.find()){
        features.put(m.group(1),m.group(2));
      }
      node_data.put("features", features);

      if(items.length>7){
        p = Pattern.compile(relationRegex);
        m = p.matcher(items[6]);
        BasicDBList edges = new BasicDBList();
        while(m.find()){
          edges.add(new BasicDBObject("source",m.group(1)));
        }
        m = p.matcher(items[7]);
        int i = 0;
        while(m.find()){
          BasicDBObject edge = ((BasicDBObject)edges.get(i));
          edge.put("target", items[0]);
          edge.put("label", m.group(1));
          i++;
        }
        
        edges_all.addAll(edges);
      }
      
      nodes_all.add(node);
    }

    graph.put("words", nodes_all);
    graph.put("links", edges_all);
    return data;
  }


  @Override
  public String getSignature(String highlightings) {
    // TODO Auto-generated method stub
    return null;
  }
  
  
  
}
