package fr.inria.alpage.frmgcm.corpus.format;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.util.JSON;

import fr.inria.alpage.frmgcm.corpus.Document;
import fr.inria.alpage.frmgcm.gm.GraphRevision;
import fr.inria.alpage.frmgcm.utils.Utils;

/**
 * This class is used to handle DepXML format
 * @author buiquang
 *
 */
public class DepXMLGraph extends AbstractGraph {
  /**
   * Json partial representation of the DepXML xml format
   */
  public BasicDBObject graph;
  /**
   * The xml document containing the depxml representation
   */
  private org.w3c.dom.Document xmldoc = null;
  
  /**
   * Default constructor from graph revision data
   * @param gr
   */
  public DepXMLGraph(GraphRevision gr) {
    this.graphRevision = gr;
    try {
      this.parseFromStringData(gr.getData().trim());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void indexSentenceContent(BasicDBObject json) {
    
    json.append("sentence", this.getSentenceString());
    if(graph==null){
      return;
    }
    BasicDBList nodes_all = new BasicDBList();
    BasicDBList edges_all = new BasicDBList();
    
    ArrayList<BasicDBObject> graph_words = (ArrayList<BasicDBObject>)graph.get("words");
    ArrayList<BasicDBObject> graph_links = (ArrayList<BasicDBObject>)graph.get("links");
    
    for(Object obj:graph_words){
      BasicDBObject word = (BasicDBObject)obj;
      BasicDBObject word_data = (BasicDBObject)word.get("#data");

      
      
      
      BasicDBObject node = new BasicDBObject();
      node.put("id", word.getString("id"));
      node.put("form", word_data.getString("form"));
      node.put("lemma", word_data.getString("lemma"));
      node.put("token",word_data.getString("token"));
      node.put("cat",word_data.getString("cat"));
      node.put("xcat",word_data.getString("xcat"));
      
      AbstractGraph.registerFeatureVal("depxml", "node", "cat", word_data.getString("cat"));
      AbstractGraph.registerFeatureVal("depxml", "node", "xcat", word_data.getString("xcat"));
      
      
      nodes_all.add(node);
    }
    
    for(Object obj:graph_links){
      BasicDBObject link = (BasicDBObject)obj;

      BasicDBObject edge = new BasicDBObject();
      edge.put("id", link.getString("id"));
      edge.put("target", link.getString("target"));
      edge.put("source", link.getString("source"));
      edge.put("label", link.getString("label"));
      edge.put("type", link.getString("type"));
      AbstractGraph.registerFeatureVal("depxml", "edge", "label", link.getString("label"));
      AbstractGraph.registerFeatureVal("depxml", "edge", "type", link.getString("type"));
        
        edges_all.add(edge);
    }

    json.put("nodes", nodes_all);
    json.put("edges", edges_all);
  }



  @Override
  protected void parseFromStringData(String data) throws Exception {
    
    data = data.replace("&amp;","__TMP_AMP__");
    data = data.replace("&", "&amp;");
    data = data.replace("__TMP_AMP__","&amp;");

    InputSource is = new InputSource(new StringReader(data.trim()));
    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    org.w3c.dom.Document doc = dBuilder.parse(is);
    xmldoc = doc;
    parseFromXMLDoc(doc);
  }
  
  /**
   * Create the "depxml" json representation of this graph
   * @param doc the xml "depxml" representation of this graph
   * @throws XPathExpressionException
   */
  private void parseFromXMLDoc(org.w3c.dom.Document doc) throws XPathExpressionException{
    doc.getDocumentElement().normalize();
    
    
    /*$clusters = array();
    $merged_nodes = array();
    */
    graph = new BasicDBObject();
    
    ArrayList<BasicDBObject> graph_nodes = new ArrayList<BasicDBObject>();
    
    
    org.w3c.dom.NodeList nodes = doc.getElementsByTagName("node");
    int n = nodes.getLength();
    for(int i = 0; i<n;i++){
      org.w3c.dom.Node node = nodes.item(i);
      BasicDBObject graph_node = new BasicDBObject();
      graph_nodes.add(graph_node);
      BasicDBList sublabels = new BasicDBList();
      BasicDBObject nodedata = new BasicDBObject();
      graph_node.put("sublabel", sublabels);
      graph_node.put("#data", nodedata);
      org.w3c.dom.NamedNodeMap attributes = node.getAttributes();
      int m = attributes.getLength();
      for(int j = 0; j<m ;j++) {
        org.w3c.dom.Node attr = attributes.item(j);
        String key = attr.getNodeName();
        String value = attr.getNodeValue();
        if(key.equals("cluster")){
          nodedata.put(key, value);
          
          // merge some nodes
          /*
          $node_ids = array_keys($clusters,$value);
          $mergeHappened = false;
          foreach($node_ids as $nodeid){
            $mergeCond = $graph['words'][$nodeid]['#data']['cat'] == (string)$node['cat'];
            if($mergeCond){
              unset($graph['words'][$i]);
              $merged_nodes[(string)$node['id']] = $graph['words'][$nodeid]['id'];
              $mergeHappened = true;
              break;
            }
          }
          if($mergeHappened){
             break;
          }else{
            $clusters[$i] = $value; 
          }
          */
          XPath xPath = XPathFactory.newInstance().newXPath();
          org.w3c.dom.NodeList associated_clusters = (org.w3c.dom.NodeList)xPath.evaluate("//cluster[@id='"+value+"']", doc.getDocumentElement(),XPathConstants.NODESET);
          int l = associated_clusters.getLength();
          for(int k = 0;k<l;k++){ // should be only one
            org.w3c.dom.Node cluster = associated_clusters.item(k);
            org.w3c.dom.NamedNodeMap cluster_attributes = cluster.getAttributes();
            if(l>1 && !cluster_attributes.getNamedItem("token").getNodeValue().equals(attributes.getNamedItem("form").getNodeValue())){
              continue;
            }
            int lv = cluster_attributes.getLength();
            for(int kv=0;kv<lv;kv++){
              org.w3c.dom.Node cluster_attr = cluster_attributes.item(kv);
              String ckey = cluster_attr.getNodeName();
              if(ckey.equals("id")){ 
                continue;
              }else{
                nodedata.put(ckey, cluster_attr.getNodeValue());
              }
            }
          }
          continue;
        }else if(key.equals("id")){
          //$node_obj['label'] = $value;
          graph_node.put(key, value);
        }else{
          if(key.equals("lemma") || key.equals("cat")){
            sublabels.add(value);
          }
          nodedata.put(key, value);
        } 
      }
      if(sublabels.size()!= 2){
        for(int sbi=sublabels.size();sbi<2;sbi++){
          sublabels.add("_");
        }
      }
    }
    Collections.sort(graph_nodes,new CompNodePosition());
    
    HashMap<String,String> tokens = new HashMap<String,String>();
    for(BasicDBObject word:graph_nodes){
      String lex = ((BasicDBObject)word.get("#data")).getString("lex");
      String[] lexparts = lex.split("\\|");
      if(lexparts == null || lexparts.length<2){
        word.put("label", ((BasicDBObject)word.get("#data")).getString("token"));
      }
      
      if(!tokens.containsKey(lexparts[0]) || lexparts[0].equals("")){
        tokens.put(lexparts[0],word.getString("id"));
        if(lexparts.length ==2){
          word.put("label" ,lexparts[1]);
        }else{
          word.put("label", ((BasicDBObject)word.get("#data")).getString("token"));
        }
      }else{
        /*$duplicatedTokenEdge = $xml_data->addChild("edge");
        $duplicatedTokenEdge->addAttribute("id","C".$duplicatedTokenEdgeIndex++);
        $duplicatedTokenEdge->addAttribute("source",$tokens[$lexparts[0]]);
        $duplicatedTokenEdge->addAttribute("target",$word['id']);
        
        // type of the duplicated token (ellipsis or segmentation)
        $nodeA = getTheOnlyEltInArrayOrReturnFalse($xml_data->xpath("//node[@id = '".$tokens[$lexparts[0]]."']"));
        $nodeB = getTheOnlyEltInArrayOrReturnFalse($xml_data->xpath("//node[@id = '".$tokens[$lexparts[0]]."']"));
        $type = "ellipsis";
        if($nodeA === false || $nodeB === false){
          Logger::Log("error when retrieving nodes for type duplicated token type specification");
        }else{
          if((string) $nodeA['form'] != (string) $nodeB['form'] ){
            $type = "segmentation";
          }
        }
        
        
        $duplicatedTokenEdge->addAttribute("type",$type);*/
        word.put("label" ,"_");
      }
    }
    
    n= graph_nodes.size();
    for(int i = 0;i<n; i++){
      graph_nodes.get(i).put("#position", i);
    }
    
    ArrayList<BasicDBObject> graph_edges = new ArrayList<BasicDBObject>();
    
    org.w3c.dom.NodeList edges = doc.getElementsByTagName("edge");
    n = edges.getLength();
    for(int i =0;i<n;i++){
      org.w3c.dom.Node edge = edges.item(i);
      BasicDBObject graph_edge = new BasicDBObject();
      graph_edges.add(graph_edge);
      org.w3c.dom.NamedNodeMap attributes = edge.getAttributes();
      int m = attributes.getLength();
      for(int j = 0; j<m ;j++) {
        org.w3c.dom.Node attr = attributes.item(j);
        String key = attr.getNodeName();
        String value = attr.getNodeValue();
        if(key.equals("id")){
          graph_edge.put("id", value);
        }else if(key.equals("label") || key.equals("source") || key.equals("target")){
          /*if(key.equals("source") || key.equals("target")){
            foreach($merged_nodes as $origin => $target){
              if($origin == $value){
                $value = $target;
                break;
              }
            }
          }*/
          graph_edge.put(key,value);
        }else{
          graph_edge.put(key, value);
        }
      }
    }
    
    org.w3c.dom.NodeList costs = doc.getElementsByTagName("cost");
    n = costs.getLength();
    for(int i =0;i<n;i++){
      org.w3c.dom.Node cost = costs.item(i);
      org.w3c.dom.NamedNodeMap attributes = cost.getAttributes();
      if(attributes.getNamedItem("kept").getNodeValue().equals("yes")){
        continue; // skip, this edge is already in the graph
      }
      if(attributes.getNamedItem("eid").getNodeValue().length()>=4 && attributes.getNamedItem("eid").getNodeValue().substring(0, 4).equals("root")){
        continue; //TODO handle root edges
      }
      String id =  attributes.getNamedItem("eid").getNodeValue();
      String infostring = attributes.getNamedItem("info").getNodeValue();
      String[] info = infostring.substring(1, infostring.length()-2).split(",");
      
      BasicDBList target_nodes = retrieveCorrespondingNodes(graph_nodes, attributes.getNamedItem("target").getNodeValue());
      BasicDBObject source_node = retrieveBestCorrespondingNode(graph_nodes, attributes.getNamedItem("source").getNodeValue(), info[1]);
      if(source_node == null || source_node.getString("id") == null){
        // TODO SHOULD NOT HAPPEND
        source_node = new BasicDBObject();
        //Logger::Log($id,"source node id is null");
      }
      for(Object obj:target_nodes){
        BasicDBObject node = (BasicDBObject)obj;
        BasicDBObject alternative = null;
        if(((BasicDBObject)node.get("#data")).get("alternatives")!=null){
          BasicDBList alternatives = (BasicDBList)((BasicDBObject)node.get("#data")).get("alternatives");
          for(Object obj2:alternatives){
            BasicDBObject alt = (BasicDBObject)obj2;
            if(alt.getString("source") != null && alt.getString("source").equals(source_node.getString("id")) && 
                alt.getString("target").equals(node.getString("id")) && 
                info[2].equals(alt.getString("label"))){
              alternative = alt;
              break;
            }
          }
        }else{
          ((BasicDBObject)node.get("#data")).put("alternatives", new BasicDBList());
        }
        if(alternative != null){
          boolean found = false;
          for(Object obj3 : (BasicDBList)alternative.get("ref_ids")){
            String ref_ids = (String)obj3;
            if(ref_ids.equals(id)){
              found = true;
              break;
            }
          }
          if(!found){
            ((BasicDBList)alternative.get("ref_ids")).add(id);
          }
          continue;
        }
        
        String type = (String) info[3];
        BasicDBList alternatives = (BasicDBList) ((BasicDBObject)node.get("#data")).get("alternatives");
        BasicDBObject newalt = new BasicDBObject();
        newalt.put("id", id);
        BasicDBList altid = new BasicDBList();
        altid.add(id);
        newalt.put("ref_ids", altid);
        BasicDBObject dataobj = new BasicDBObject();
        dataobj.put("w", attributes.getNamedItem("w").getNodeValue());
        dataobj.put("virtual", true);
        newalt.put("#data", dataobj);
        newalt.put("#style", new BasicDBObject("link-color",getTypeColor(type)));
        newalt.put("label", info[2]);
        newalt.put("source", source_node.getString("id"));
        newalt.put("target", node.getString("id"));
        alternatives.add(newalt);
      }
    }
    
    
    graph.put("words", graph_nodes);
    graph.put("links", graph_edges);

    
    
  }
  
  /**
   * Get the node that should represent a certain token (cluster_id)
   * @param graph_nodes the set of the nodes
   * @param cluster_id the id of the cluster to find a corresponding node
   * @param object ???
   * @return the node selected that should match the cluster
   */
  private BasicDBObject retrieveBestCorrespondingNode(ArrayList<BasicDBObject> graph_nodes,String cluster_id,Object object){
    BasicDBObject best = null;
    String [] parts = cluster_id.split("_");
    for(Object obj:graph_nodes){
      BasicDBObject word = (BasicDBObject)obj;
      String[] bounds = ((BasicDBObject)word.get("#data")).getString("cluster").split("_");
      boolean cond = (Integer.parseInt(bounds[1])>= Integer.parseInt(parts[1]) && 
          Integer.parseInt(bounds[1]) < Integer.parseInt(parts[2])) || 
          (Integer.parseInt(bounds[2])> Integer.parseInt(parts[1]) && 
              Integer.parseInt(bounds[2]) <= Integer.parseInt(parts[2]));
      if(parts[1].equals(parts[2])){
        cond = bounds[1].equals(parts[1]) && bounds[2].equals(parts[2]);
      }
      if(cond){
        best = word;
        if(((BasicDBObject)word.get("#data")).getString("cluster").equals("cat")){
          return word;
        }
      }
    }
    return best;
  }

  /**
   * Get the nodes that span over a certain cluster
   * @param graph_nodes the set of the nodes
   * @param cluster_id the id of the cluster to find corresponding nodes
   * @return a list of nodes that span over a certain cluster
   */
  private BasicDBList retrieveCorrespondingNodes(ArrayList<BasicDBObject> graph_nodes,String cluster_id){
    BasicDBList nodes = new BasicDBList();
    String[] parts = cluster_id.split("_");
    for(Object obj:graph_nodes){
      BasicDBObject word = (BasicDBObject)obj;
      String[] bounds = ((BasicDBObject)word.get("#data")).getString("cluster").split("_");
      boolean cond = (Integer.parseInt(bounds[1])>= Integer.parseInt(parts[1]) && 
          Integer.parseInt(bounds[1]) < Integer.parseInt(parts[2])) || 
          (Integer.parseInt(bounds[2])> Integer.parseInt(parts[1]) && 
              Integer.parseInt(bounds[2]) <= Integer.parseInt(parts[2]));
      if(true || parts[1].equals(parts[2])){
        cond = bounds[1].equals(parts[1]) && bounds[2].equals(parts[2]);
      }
      if(cond){
        nodes.add(word);
      }
    }
    return nodes;
  }

  @Override
  public boolean saveRevision(PrintWriter pw) {
    // TODO Auto-generated method stub
    return true;
  }

  @Override
  public String restoreRevision() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void setIDOffset(int offset) {
    // TODO Auto-generated method stub

  }

  @Override
  public ArrayList<String> getContent() {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * User comparator function to sort the nodes in their sentence order
   * @author buiquang
   *
   */
  public class CompNodePosition implements Comparator<BasicDBObject> {

    @Override
    public int compare(BasicDBObject o1, BasicDBObject o2) {
      BasicDBObject a = (BasicDBObject)o1.get("#data");
      BasicDBObject b = (BasicDBObject)o2.get("#data");
      if(Integer.valueOf(a.getString("left"))==Integer.valueOf(b.getString("left"))){
        if(Integer.valueOf(a.getString("right"))==Integer.valueOf(b.getString("right"))){
          if(o1.getString("id").compareTo(o2.getString("id"))<0){
            return -1;
          }else{
            return 1;
          }
        }else if(Integer.valueOf(a.getString("right"))<Integer.valueOf(b.getString("right"))){
          return -1;
        }else{
          return 1;
        }
      }else if(Integer.valueOf(a.getString("left"))<Integer.valueOf(b.getString("left"))){
        return -1;
      }else{
        return 1;
      }
    }
  }

  @Override
  public String getSentenceString() {
    if(graph==null){
      return null;
    }
    ArrayList<BasicDBObject> graph_words = (ArrayList<BasicDBObject>)graph.get("words");
    String sentence = "";
    for(BasicDBObject obj:graph_words){
      String token = obj.getString("label").trim();
      if(token.equals("") || token.equals("_")){
        continue;
      }
      sentence += " "+token;
    }
    return sentence.trim();
  }

  @Override
  public BasicDBObject getDepGraphData() {
    BasicDBObject graph = new BasicDBObject();
    BasicDBObject data = new BasicDBObject("graph",graph);
    
    BasicDBList nodes_all = new BasicDBList();
    BasicDBList edges_all = new BasicDBList();
    
    ArrayList<BasicDBObject> graph_words = (ArrayList<BasicDBObject>)this.graph.get("words");
    ArrayList<BasicDBObject> graph_links = (ArrayList<BasicDBObject>)this.graph.get("links");
    
    for(Object obj:graph_words){
      BasicDBObject word = (BasicDBObject)obj;
      BasicDBObject word_data = (BasicDBObject)word.get("#data");

      // node creation
      BasicDBObject node_data = new BasicDBObject();
      BasicDBObject node = new BasicDBObject("#data",node_data);
      
      // presentation
      node.put("id", word.getString("id"));
      node.put("label", "@form");
      BasicDBList sublabel = new BasicDBList();
      node.put("sublabel", sublabel);
      sublabel.add("@lemma");
      sublabel.add("@cat");
      
      // data
      node_data.put("form", valOrDefault(word_data.getString("form")));
      node_data.put("lemma", valOrDefault(word_data.getString("lemma")));
      node_data.put("token",valOrDefault(word_data.getString("token")));
      node_data.put("cat",word_data.getString("cat"));
      node_data.put("xcat",word_data.getString("xcat"));
      node_data.put("alternatives", word_data.get("alternatives"));
      
      nodes_all.add(node);
    }
    
    for(Object obj:graph_links){
      BasicDBObject link = (BasicDBObject)obj;

      String linktype = link.getString("type");
      BasicDBObject edge = new BasicDBObject();
      edge.put("id", link.getString("id"));
      edge.put("target", link.getString("target"));
      edge.put("source", link.getString("source"));
      edge.put("label", link.getString("label"));
      edge.put("#data",new BasicDBObject("type",linktype));
      
      BasicDBObject style = new BasicDBObject("link-color",getTypeColor(linktype));
      edge.put("#style",style);
      if(linktype.equals("ellipsis")){
        style.put("oriented",false);
        style.put("stroke-dasharray","5, 5, 1, 5");
      }
      
      edges_all.add(edge);
    }
    
    graph.put("words", nodes_all);
    graph.put("links", edges_all);
    return data;
  }
  
  /**
   * Return the "_" default value if string is empty
   * @param val the entry string 
   * @return the value of the string or a default one if empty
   */
  private String valOrDefault(String val){
    if(val.equals("")){
      return "_";
    }else{
      return val;
    }
  }
  
  /**
   * Get a hex color for each type of edges
   * @param type the type of the edge
   * @return a hex color
   */
  private String getTypeColor(String type){
    String color = "black";
    if(type.equals("subst")){
      color = "#409AFF";
    }else if(type.equals("lexical")){
      color = "#FF73E4";
    }else if(type.equals("skip")){
      color = "#FF73E4";
    }else if(type.equals("adj")){
      color = "#FF4040";
    }else if(type.equals("ellipsis")){
      color = "#AAAAAA";
    }
    return color;
  }

  @Override
  public String getSignature(String highlightings) {
    // TODO Auto-generated method stub
    return null;
  }
}

