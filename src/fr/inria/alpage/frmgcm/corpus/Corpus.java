package fr.inria.alpage.frmgcm.corpus;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.bson.types.ObjectId;

import com.google.gson.Gson;
import com.jcraft.jsch.JSchException;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.ReflectionDBObject;
import com.mongodb.util.JSON;

import fr.inria.alpage.cpm.task.CPMCall;
import fr.inria.alpage.cpm.task.CPMCommand;
import fr.inria.alpage.cpm.task.CPMTask;
import fr.inria.alpage.frmgcm.Configuration;
import fr.inria.alpage.frmgcm.db.CPMDB;
import fr.inria.alpage.frmgcm.search.DPathSearchMod;
import fr.inria.alpage.frmgcm.search.DPathSearchMacroManager;
import fr.inria.alpage.frmgcm.search.IndexSearchMod;
import fr.inria.alpage.frmgcm.services.DPathQueryMacroLibService;
import fr.inria.alpage.frmgcm.utils.SSH;
import fr.inria.alpage.frmgcm.utils.Utils;
import fr.inria.alpage.frmgcm.utils.Utils.ValidationBean;

/**
 * This class reflects corpus object in db and provides all the methods related to corpus management
 * @author buiquang
 *
 */
public class Corpus extends ReflectionDBObject{
  
  /**
   * Name of the corpus
   */
  public String name;
  /**
   * Owner name (obsolete, not used) 
   */
  public String owner;
  /**
   * Local path of the directory root of the corpus
   */
  public String path;
  /**
   * List of associated {@see CPMTask} that handle the processing of the corpus
   */
  public BasicDBList tasksId; 
  /**
   * Extern id given to the corpus (id generated from MGWiki)
   */
  public String externId;
  /**
   * Current status of the corpus (waiting, processing or available)
   */
  public String status;
  /**
   * Output format chosen for the analysis
   */
  public String outputFormat;
  /**
   * Boolean indicating if the term extraction process must be launched after the syntactic analysis
   */
  public boolean termExtractProcessEnabled = false;
  /**
   * JSON object mapping id of {@see Document} and its local path
   */
  private BasicDBObject documents;
  /**
   * Boolean indicating if the documents present inside the corpus has been mapped (ie the field document has been filled)
   */
  public boolean documentsMapped; 
  /**
   * Obsolete, list of results files set to the corpus without sources
   */
  private BasicDBList resultFiles;
  
  public boolean gettermExtractProcessEnabled(){
    return this.termExtractProcessEnabled;
  }
  
  public void settermExtractProcessEnabled(boolean tepe){
    this.termExtractProcessEnabled = tepe;
  }
  
  public BasicDBList getresultFiles() {
    return resultFiles;
  }

  public void setresultFiles(BasicDBList resultFiles) {
    this.resultFiles = resultFiles;
  }
  
  public String getoutputFormat() {
    if(outputFormat == null){
      outputFormat = "conll";
    }
    return outputFormat;
  }

  public void setoutputFormat(String outputFormat) {
    this.outputFormat = outputFormat;
  }

  
  public boolean getdocumentsMapped() {
    return documentsMapped;
  }

  public void setdocumentsMapped(boolean documentsMapped) {
    this.documentsMapped = documentsMapped;
  }

  public BasicDBObject getdocuments() {
    if(documents == null){ // should be filled after convert default analysis subtask 
      try {
        documents = this.saveDocuments();
        this.save();
      } catch (Exception e) {
        Utils.Log("error while initializing corpus documents list", 1);
        e.printStackTrace();
      }
    }
    return documents;
  }

  public void setdocuments(BasicDBObject documents) {
    this.documents = documents;
  }


  
  
  public String getpath() {
    return path;
  }

  public void setpath(String path) {
    this.path = path;
  }

  public String getname() {
    return name;
  }

  public void setname(String name) {
    this.name = name;
  }

  public String getexternId() {
    return externId;
  }

  public void setexternId(String externId) {
    this.externId = externId;
  }
  
  public BasicDBList gettasksId() {
    return tasksId;
  }

  public void settasksId(BasicDBList tasksId) {
    this.tasksId = tasksId;
  }

  public Corpus(){
    set_id(ObjectId.get());
  }
  
  
  public String getstatus() {
    if(status == null){
      status = "waiting";
    }
    return status;
  }

  public void setstatus(String status) {
    this.status = status;
  }
  
  /**
   * Factory, create corpus from basic information
   * @param corpusInfos json object containing minimum information about corpus (path, cid and name)
   * @return
   */
  public static Corpus CreateCorpus(BasicDBObject corpusInfos){
    Corpus corpus = new Corpus();
    String path = corpusInfos.getString("path");
    String eid = corpusInfos.getString("cid");
    String name = corpusInfos.getString("name");
    BasicDBList resultFiles = (BasicDBList)corpusInfos.get("resultFiles");
    corpus.setname(name);
    corpus.setpath(path);
    corpus.setexternId(eid);
    corpus.setresultFiles(resultFiles);
    corpus.documentsMapped = false;

    Utils.ValidationBean valid = corpus.validate();
    if(valid.success){
      Utils.Log("saving corpus");
      corpus.save();
      if(corpus.path != null){
        corpus.startTask(0, new BasicDBObject("reset",true));
      }
    }else{
      Utils.Log(valid.message,1);
      return null;
    }
    return  corpus;
  }
  
  /**
   * Validate the corpus information (verify that it has a name, path and extern id)
   * @return a {@see Utils.ValidationBean} object
   */
  public Utils.ValidationBean validate(){
    Utils.ValidationBean validation = new Utils.ValidationBean();
    validation.success = true;
    DBCollection coll = CPMDB.GetInstance().db.getCollection("corpus");
    coll.setObjectClass(Corpus.class);
    if(this.externId == null && this.name == null){
      validation.success = false;
      validation.message = "Missing name or external Id";
      return validation;
    }
    if(this.externId != null){
      BasicDBObject q = new BasicDBObject("externId",this.externId);
      Corpus corpus = (Corpus) coll.findOne(q);
      if(corpus != null){
        validation.success = false;
        validation.message = "External Id already exists";
        return validation;
      }
    }
    if(this.name != null){
      BasicDBObject q = new BasicDBObject("name",this.name);
      Corpus corpus = (Corpus) coll.findOne(q);
      int n = 1;
      while(corpus != null){
        this.name += n++;
        q = new BasicDBObject("name",this.name);
        corpus = (Corpus) coll.findOne(q);
      }
    }
    if(this.path == null && this.resultFiles == null){
      validation.success = false;
      validation.message = "Missing path or resultFiles attributes";
      return validation;
    }
   return validation;
  }
  
  /**
   * Retrieve a corpus using its external id
   * @param id the external id given to the corpus (from mgwiki)
   * @return the corresponding corpus or null if not found
   */
  public static Corpus RetrieveFromExternalID(String id){
    DBCollection coll = CPMDB.GetInstance().db.getCollection("corpus");
    coll.setObjectClass(Corpus.class);
    BasicDBObject q = new BasicDBObject("externId",id);
    Corpus corpus = (Corpus) coll.findOne(q);
    if(corpus != null){
      Utils.Log("Retrieving corpus "+corpus.get_id().toString()+" (externId : "+corpus.externId+" )");
    }
    return corpus;
  }
  
  /**
   * Retrieve a corpus using its name (name are unique by construction (number are appended for same names))
   * @param name the name of the corpus
   * @return the corresponding corpus or null if not found
   */
  public static Corpus RetrieveFromName(String name){
    DBCollection coll = CPMDB.GetInstance().db.getCollection("corpus");
    coll.setObjectClass(Corpus.class);
    BasicDBObject q = new BasicDBObject("name",name);
    Corpus corpus = (Corpus) coll.findOne(q);
    if(corpus != null){
      Utils.Log("Retrieving corpus "+corpus.get_id().toString()+" (externId : "+corpus.externId+" )");
    }
    return corpus;
  }

  /**
   * Retrieve corpus with its mongodb id
   * @param objectId the unique mongodb id of the corpus
   * @return the corresponding corpus or null if not found
   */
  public static Corpus Retrieve(String objectId){
    DBCollection coll = CPMDB.GetInstance().db.getCollection("corpus");
    coll.setObjectClass(Corpus.class);
    BasicDBObject q = new BasicDBObject("_id",new ObjectId((String)objectId));
    Corpus corpus = (Corpus) coll.findOne(q);
    return corpus;
  }
  
  /**
   * Save modification to the corpus into db (such as output format, documents list, status, etc)
   */
  public void save(){
    DBCollection coll = CPMDB.GetInstance().db.getCollection("corpus");
    coll.setObjectClass(this.getClass());
    coll.save(this);
  }
  
  /**
   * Delete associated tasks (called when corpus is deleted)
   */
  private void deleteTasks(){
    
    for(Object obj:this.tasksId){
      String tid =(String)obj;
      DBCollection coll = CPMDB.GetInstance().db.getCollection("tasks");
      BasicDBObject q = new BasicDBObject("_id",new ObjectId(tid));
      BasicDBObject task = (BasicDBObject)coll.findOne(q);
      BasicDBList list = (BasicDBList) task.get("tasks");
      for(Object obj2:list){
        BasicDBObject taskunit = (BasicDBObject) obj2;
        String outputFile = null;
        outputFile = taskunit.getString("stderr");
        new File(outputFile).delete();
        outputFile = taskunit.getString("stdout");
        new File(outputFile).delete();
      }
      coll.remove(q);
    }
  }
  
  /**
   * Initialize the list of associated tasks (1 unpacking,conversion 2 default analysis 3 retrieve result 4 term extraction)
   */
  private void initTasks() {
    if(this.tasksId!=null){
      deleteTasks();
    }
    
    this.tasksId = new BasicDBList();

    CPMTask unpackFilesTask = new CPMTask();
    String id = this.get_id().toString();
    unpackFilesTask.addTaskUnit(new CPMCall(unpackFilesTask.id, id, "clean", null));
    unpackFilesTask.addTaskUnit(new CPMCall(unpackFilesTask.id, id, "unpack", null));
    unpackFilesTask.addTaskUnit(new CPMCall(unpackFilesTask.id, id, "convert", null));
    unpackFilesTask.addTaskUnit(new CPMCall(unpackFilesTask.id, id, "export", null));
    this.tasksId.add(unpackFilesTask.id);
    
    CPMTask defaultAnalysisTask = new CPMTask();
    defaultAnalysisTask.addTaskUnit(new CPMCall(defaultAnalysisTask.id, id, "raw2dag", null));
    defaultAnalysisTask.addTaskUnit(new CPMCall(defaultAnalysisTask.id, id, "createConfiguration", null));
    defaultAnalysisTask.addTaskUnit(new CPMCall(defaultAnalysisTask.id, id, "runDefaultAnalysis", null));
    this.tasksId.add(defaultAnalysisTask.id);
    
    CPMTask retrieveResultTask = new CPMTask();
    retrieveResultTask.addTaskUnit(new CPMCall(retrieveResultTask.id, id, "retrieveResults", null));
    this.tasksId.add(retrieveResultTask.id);

    CPMTask termExtractionTask = new CPMTask();
    termExtractionTask.addTaskUnit(new CPMCall(termExtractionTask.id, id, "termExtraction", null));
    //termExtractionTask.addTaskUnit(new CPMCall(termExtractionTask.id,id,"depExtraction",null));
    //termExtractionTask.addTaskUnit(new CPMCall(termExtractionTask.id,id,"retrieveTermExtractionResults",null));
    this.tasksId.add(termExtractionTask.id);
    
    this.save();
  }
  
  /**
   * Start a task 
   * @param taskindex the index of the task to start
   * @param options (reset : reset the task, format : define the output format)
   */
  public void startTask(int taskindex, BasicDBObject options) {
    if(this.tasksId==null || this.tasksId.size()<(taskindex+1)){
      this.initTasks();
    }
    // get the collection of tasks
    DBCollection coll = CPMDB.GetInstance().db.getCollection("tasks");

    // get task 
    BasicDBObject q = new BasicDBObject("_id",new ObjectId((String)tasksId.get(taskindex)));
    BasicDBObject data = (BasicDBObject)coll.findOne(q);
    
    String format = options.getString("format");
    if(format != null){
      this.setoutputFormat(format);
    }
    
    
    boolean reset = false;
    try{
      reset = options.getBoolean("reset");
    }catch(IllegalArgumentException e){
      reset = "true".equals(options.getString("reset")) || "1".equals(options.getString("reset"));
    }
    if(reset){
      Utils.Log("reseting task",4);
      data.put("running", false);
      BasicDBList list = (BasicDBList) data.get("tasks");
      Iterator<Object> it = list.iterator();
      while(it.hasNext()){
        BasicDBObject obj = (BasicDBObject) it.next();
        obj.put("status", "waiting");
      }
      
      coll.save(data);
    }
    
    CPMTask task = CPMTask.parseData(data);
    this.setstatus("processing");
    this.save();
    task.setPriority(Thread.MIN_PRIORITY);
    task.start();
  }

  /**
   * Clean unpacked and converted files
   * @return the output of the process
   * @throws IOException
   * @throws InterruptedException
   */
  public String[] clean() throws IOException, InterruptedException{
    String[] output = new String[]{"",""};
    String cmd = "rm -fr "+this.path+"/uncompressed/* "+this.path+"/raw/* ";
    String scriptfile = CPMCommand.exportToScript(cmd);
    Process process = null;
    process = Runtime.getRuntime().exec("sh ".concat(scriptfile));
    
    output[0] = output[0].concat(String.valueOf(process.waitFor()));

    return output;
  }
  
  
  ///////////////////////////////////////////////////////////////////////////
  //                            UNPACKING                                  //
  ///////////////////////////////////////////////////////////////////////////
  
  /**
   * unpack the files of the corpus
   * @return the output of the process
   * @throws IOException
   * @throws InterruptedException
   */
  public String[] unpack() throws IOException, InterruptedException{
    String[] output = new String[]{"",""};
    Utils.copy(this.path+"/original",this.path+"/uncompressed");
    String[] subOutput = unpack(new File(this.path+"/uncompressed"));
    output[0] = output[0].concat(subOutput[0]);
    output[1] = output[1].concat(subOutput[1]);
    return output;
  }
  
  /**
   * unpack files contained in a directory
   * @param dir the directory containing files to unpack
   * @return the output of the process
   * @throws IOException
   * @throws InterruptedException
   */
  private String[] unpack(File dir) throws IOException, InterruptedException{
    String[] output = new String[]{"",""};
    File[] files = dir.listFiles();
    for(int i=0; i< files.length;++i){
      File file = files[i];
      String[] subOutput = null;
      if(file.isFile()){
        subOutput = unpackArchive(file);
      }else{
        subOutput = unpack(file);
      }
      output[0] = output[0].concat(subOutput[0]);
      output[1] = output[1].concat(subOutput[1]);
    }
    return output;
  }
  
  /**
   * Actually unpack an archive (tar, targz or zip)
   * @param archive the file archive to unpack
   * @return the output of the process
   * @throws IOException
   * @throws InterruptedException
   */
  private String[] unpackArchive(File archive) throws IOException, InterruptedException{
    String[] output = new String[]{"",""};
    Process process = null;
    String filename = archive.getName();
    File archiveDir = null;
    String cmd = null;

    if(filename.endsWith(".tgz") || filename.endsWith(".tar.gz")){
      archiveDir = createArchiveDestDir(archive);
      cmd = "tar -xzf "+archive.getAbsolutePath()+" -C "+archiveDir.getAbsolutePath();
    }else if(filename.endsWith(".tar")){
      archiveDir = createArchiveDestDir(archive);
      cmd = "tar -xf "+archive.getAbsolutePath()+" -C "+archiveDir.getAbsolutePath();
    }else if(filename.endsWith(".zip")){
      archiveDir = createArchiveDestDir(archive);
      cmd = "unzip "+archive.getAbsolutePath()+" -d "+archiveDir.getAbsolutePath();
    }

    if(cmd != null){
      process = Runtime.getRuntime().exec(cmd);
      
      output[0] = output[0].concat(String.valueOf(process.waitFor()));
      archive.delete();
      
      String[] subOutput = this.unpack(archiveDir);
      output[0] = output[0].concat(subOutput[0]);
      output[1] = output[1].concat(subOutput[1]);
    }
    
    return output;
    
  }
  
  /**
   * create the folder that will contain the files in an archive
   * @param archive the file archive 
   * @return the output of the process
   * @throws IOException
   */
  private File createArchiveDestDir(File archive) throws IOException{
    String archiveDirPath = null;
    File archiveDir = null;
    archiveDirPath = archive.getCanonicalPath();
    int extensionIndex = archiveDirPath.lastIndexOf(".");
    if(extensionIndex != -1){
      archiveDir = new File(archiveDirPath.substring(0,extensionIndex));
      archiveDir.mkdirs();
    }
    return archiveDir;
  }
  
  
  ///////////////////////////////////////////////////////////////////////////
  //                            CONVERTING                                 //
  ///////////////////////////////////////////////////////////////////////////
  
  /**
   * Convert documents that were previously unpacked (supported file type are pdf, rtf, doc, txt and docx)
   * @return the output of the process
   * @throws Exception
   */
  public String[] convert() throws Exception{
    String[] output = new String[]{"",""};
    ArrayList<String> args = new ArrayList<String>();
    args.add(this.path);
    Utils.mapReduceFolder(new File(this.path.concat("/uncompressed")), new MapReduceFolderConvert(), args);
    ArrayList<Object[]> processes = new ArrayList<Object[]>();
    args.remove(0); // remove the input parameters which is not a output command
    Iterator<String> it = args.iterator();
    while(it.hasNext()){
      String command = it.next();
      Utils.Log(command);
      String scriptfile = CPMCommand.exportToScript(command);
      Process process = null;
      process = Runtime.getRuntime().exec("sh ".concat(scriptfile));
      Object[] pbundle = new Object[2];
      pbundle[0] = process;
      pbundle[1] = scriptfile;
      processes.add(pbundle);
      
      
    }
    
    Iterator<Object[]> pit = processes.iterator();
    while(pit.hasNext()){
      Object[] pbundle = pit.next();
      Process p = (Process) pbundle[0];
      String scriptfile = (String) pbundle[1];
      output[0] = output[0].concat(String.valueOf(p.waitFor()));
      new File(scriptfile).delete();
      Utils.Log("Deleted temporary script file",3);
    }
    
    this.documents = this.saveDocuments();
    this.save();
    
    return output;

  }
  
  /**
   * Callback used to convert files found in a directory
   * @author buiquang
   *
   */
  public static class MapReduceFolderConvert implements Utils.MapReduceFolderDelegate{

    public boolean apply(File file,ArrayList<String> args) throws IOException{
      String filename = file.getName();
      String dir = file.getParent().replaceAll("uncompressed", "raw");
      new File(dir).mkdirs();
      if(filename.endsWith(".doc") || filename.endsWith(".pdf") || filename.endsWith(".txt") || filename.endsWith(".rtf") || filename.endsWith(".docx")
          || filename.endsWith(".ods")){
        StringBuilder sb = new StringBuilder();
        sb.append("abiword --to=txt \"");
        sb.append(file.getCanonicalPath());
        sb.append("\" -o \"");
        sb.append(dir+"/");
        sb.append(Utils.getShortName(file));
        sb.append(".txt\"");
        String command = sb.toString();
        args.add(command);
      }
      return true;
    }
    
  }

  ///////////////////////////////////////////////////////////////////////////
  //                            EXPORTING                                  //
  ///////////////////////////////////////////////////////////////////////////
  /**
   * Export the prepared documents (unpacked and converted to txt) to the rioc server
   * @return the output of the process
   * @throws IOException
   * @throws JSchException
   * @throws InterruptedException
   */
  public String[] export() throws IOException, JSchException, InterruptedException{
    StringBuffer[] sbout = new StringBuffer[2];
    sbout[0]=new StringBuffer();
    sbout[1]=new StringBuffer();
    SSH ssh = new SSH(sbout[0],sbout[1]);
    String corpusRoot = Configuration.riocServerPath+"/"+this.get_id().toString();
    String cmd = "mkdir "+corpusRoot+" ; mkdir "+corpusRoot+"/dag; mkdir "+corpusRoot+"/source; mkdir "+corpusRoot+"/aligns";
    ssh.exec(cmd);
    
    String exportCmd = "scp -r -i " +Configuration.id_rioc + " " + this.path + "/raw/* buiquang@rioc:" +corpusRoot + "/source";
    Utils.Log(exportCmd);
    String scriptfile = CPMCommand.exportToScript(exportCmd);
    Process process = null;
    process = Runtime.getRuntime().exec("sh ".concat(scriptfile));
    Utils.Log(String.valueOf(process.waitFor()));
    new File(scriptfile).delete();
    return new String[]{sbout[0].toString(),sbout[1].toString()};
  }
  
///////////////////////////////////////////////////////////////////////////
//                            RAW2DAG                                    //
///////////////////////////////////////////////////////////////////////////
  
  /**
   * Call scripts in charge of creating dags for each valid files of the corpus
   * @return the output of the process
   * @throws Exception
   */
  public String[] raw2dag() throws Exception{
    StringBuffer[] sbout = new StringBuffer[2];
    sbout[0]=new StringBuffer();
    sbout[1]=new StringBuffer();

    String alpage_prefix = "/home/rioc/buiquang/exportbuild";
    String aleda_libdir = alpage_prefix + "/share/aleda"; 
    String toolsdir = alpage_prefix + "/share/sxpipe";
    String corpusRoot = Configuration.riocServerPath+"/"+this.get_id().toString();
    
    ArrayList<String> params = new ArrayList<String>();
    params.add(corpusRoot);
    params.add(alpage_prefix);
    params.add(aleda_libdir);
    params.add(toolsdir);
    params.add(new String());
    params.add(new String());
    
    Utils.mapReduceFolder(new File(this.path+"/raw"),new MapReduceFolderDagCreate(),params);

    sbout[0].append(params.get(4));
    sbout[0].append(params.get(5));

    String cmd = "/home/rioc/buiquang/corpus_analysis/extract_info.pl "+corpusRoot+"/dag > "+corpusRoot+"/dag/info.txt;";
    
    SSH ssh = new SSH(sbout[0],sbout[1]);
    ssh.exec(cmd);
    
    return new String[]{sbout[0].toString(),sbout[1].toString()};
  }

  /**
   * Callback used to actually process the dag creation for each files found in the directory containing valid files
   * @author buiquang
   *
   */
  public static class MapReduceFolderDagCreate implements Utils.MapReduceFolderDelegate{

    public boolean apply(File file,ArrayList<String> args) throws JSchException, IOException{
      String corpusRoot = args.get(0);
      String alpage_prefix = args.get(1);
      String aleda_libdir = args.get(2);
      String tools_dir = args.get(3);
      String dag_dir = corpusRoot + "/dag";
      String dag_align_script = "perl -Cio /home/rioc/buiquang/scripts/dag_align.pl";
      
      String filepath = file.getAbsolutePath();
      filepath = file.getCanonicalPath();
      
      String[] pathpart = filepath.split("/raw/");
      if(pathpart.length == 2){
        filepath = corpusRoot + "/source/" + pathpart[1];
      }
      
      String[] path = filepath.split("/source/");
      String basename = Utils.getShortName(file);
      if(path.length == 2){
        basename = path[1].replaceAll("/", "_");
      }
      StringBuilder sb = new StringBuilder();
      sb.append("source "+alpage_prefix+"/sbin/setenv.sh;");
      sb.append("cd "+corpusRoot+"/source;");
      sb.append("cat \""+filepath + "\" | ");
      sb.append("sxpipe -u -l=fr 2> \""+dag_dir+"/"+basename+".sxpipe.log\" | ");
      sb.append(tools_dir+"/np_normalizer.pl -d "+aleda_libdir+" -l fr | ");
      sb.append("yarecode -u -l fr | ");
      sb.append("ilimp -nt -nv | ");
      sb.append("dag2udag | ");
      sb.append("bzip2 > \""+dag_dir+"/"+basename+".udag.bz2\";");
      
      sb.append("bzip2 \""+dag_dir+"/"+basename+".sxpipe.log\" > \""+dag_dir+"/"+basename+".sxpipe.log.bz2\";");
      
      sb.append("bzcat \""+ dag_dir+"/"+basename+".udag.bz2\" | "+dag_align_script+" \""+filepath+"\" > \""+corpusRoot+"/aligns/"+basename+".align\" 2> \""+corpusRoot+"/aligns/"+basename+".align.log\";");
      
      
      
      String fullcmd = sb.toString();
      Utils.Log(fullcmd);
      
      StringBuffer out = new StringBuffer();
      StringBuffer err = new StringBuffer();
      SSH ssh = new SSH(out,err);
      ssh.exec(fullcmd);

      args.set(4, args.get(4).concat(out.toString()));
      args.set(5, args.get(5).concat(err.toString()));
      
      return true;
      
    }
  }
  
///////////////////////////////////////////////////////////////////////////
//                            CONFIGURATION                              //
///////////////////////////////////////////////////////////////////////////
  
  /**
   * Create the configuration file for syntactic analysis using a default template and send it to the rioc server
   * @return
   * @throws IOException
   * @throws InterruptedException
   */
  public String[] createConfiguration() throws IOException, InterruptedException{
    String[] output = new String[]{"",""};
    String baseConfigFile = Configuration.templateDispatchConfFilepath;
    String baseEndHandler = Configuration.templateEndHandlerFilepath;
    String configFile = "/tmp/dispatch."+this.get_id()+".conf";
    String endHandler = "/tmp/endHandler."+this.get_id()+".sh";
    String corpusRoot = Configuration.riocServerPath+"/"+this.get_id().toString();
    ArrayList<Object> configParams = new ArrayList<Object>();
    configParams.add(corpusRoot);
    configParams.add(new File(endHandler).getName());
    configParams.add(this.getoutputFormat());
    
    Utils.readModifyWrite(baseConfigFile, configFile, new Utils.ReadModifyWriteProcessorInterface() {
      
      public String processLine(String line,ArrayList<Object> params) {
        if(line.startsWith("dagdir")){
          line = "dagdir "+(String)params.get(0)+ "/dag\n";
        }else if(line.startsWith("results")){
          line = "results "+(String)params.get(0)+ "/results\n";
        }else if(line.startsWith("endHandler")){
          line = "endHandler "+(String)params.get(0)+ "/"+(String)params.get(1)+"\n";
        }else if(line.startsWith("format_output")){
          line = (String)params.get(2);
        }
        return line;
      }
    },configParams);


    ArrayList<Object> endHandlerParams = new ArrayList<Object>();
    endHandlerParams.add(this.externId);

    Utils.readModifyWrite(baseEndHandler, endHandler, new Utils.ReadModifyWriteProcessorInterface() {
      
      public String processLine(String line,ArrayList<Object> params) {
        if(line.startsWith("curl")){
          line = line.replace("CID", (String)params.get(0));
        }
        return line;
      }
    },endHandlerParams);

    
    String exportCmd = "scp -r -i " +Configuration.id_rioc + " " + configFile + " " + endHandler + " buiquang@rioc:" +corpusRoot + "/";
    Utils.Log(exportCmd);
    String scriptfile = CPMCommand.exportToScript(exportCmd);
    Process process = null;
    process = Runtime.getRuntime().exec("sh ".concat(scriptfile));
    output[0] = output[0].concat(String.valueOf(process.waitFor()));
    new File(scriptfile).delete();
    
    return output;
  }
  
  /**
   * Default params for cluster resource allocation
   * @author buiquang
   *
   */
  public static class OARSubParams{
    public int time = 1;
    public int nbCore = 5;
  }
  
  /**
   * Estimate the time needed to process the corpus
   * @return the resource allocation params
   */
  public OARSubParams estimateTimeRequired(){
    OARSubParams params = new OARSubParams();
    String corpusRoot = Configuration.riocServerPath+"/"+this.get_id().toString();
    UUID uid = UUID.randomUUID();
    String tmpFile = "/tmp/"+uid;
    String exportCmd = "scp -r -i " +Configuration.id_rioc + " buiquang@rioc:" +corpusRoot+"/dag/info.txt" + " " + tmpFile ;
    String scriptfile = CPMCommand.exportToScript(exportCmd);
    Process process = null;
    try {
      process = Runtime.getRuntime().exec("sh ".concat(scriptfile));
      process.waitFor();
      
      FileInputStream fs = new FileInputStream(tmpFile);
      InputStreamReader sr = new InputStreamReader(fs, "ISO-8859-1");
      BufferedReader br = new BufferedReader(sr);
      String line = null;
      while((line=br.readLine())!=null){
        if(line.startsWith("## total")){
          String[] elts = line.split("=");
          int nSentences = Integer.parseInt(elts[1]);
          if(nSentences < 30){
            params.nbCore = 1;
          }
          params.time += nSentences/15000;
          break;
        }
      }
      
    } catch (Exception e) {
      e.printStackTrace();
    }
    new File(tmpFile).delete();
    new File(scriptfile).delete();
    return params;
  }
  
  /**
   * Launch the script of syntactic parsing over the rioc server
   * @return the output of the process
   * @throws JSchException
   * @throws IOException
   */
  public String[] runDefaultAnalysis() throws JSchException, IOException{
    StringBuffer[] sbout = new StringBuffer[2];
    sbout[0]=new StringBuffer();
    sbout[1]=new StringBuffer();
    OARSubParams params = estimateTimeRequired();
    String corpusRoot = Configuration.riocServerPath+"/"+this.get_id().toString();
    String command = "cd "+corpusRoot+"; oarsub -l /nodes=5/core="+params.nbCore+",walltime="+params.time+" \"~/scripts/oar_run_script.pl --script=~/exportbuild/bin/dispatch.pl --dispatch=dispatch."
        +this.get_id()+".conf\"";
    Utils.Log(command);
    SSH ssh = new SSH(sbout[0],sbout[1]);
    ssh.exec(command);
    return new String[]{sbout[0].toString(),sbout[1].toString()};
  }
  
  public String[] analyseLog(){
    StringBuffer[] sbout = new StringBuffer[2];
    return new String[]{sbout[0].toString(),sbout[1].toString()};
  }
  
///////////////////////////////////////////////////////////////////////////
//                      SEMANTIC ANALYSIS                                //
///////////////////////////////////////////////////////////////////////////
  
  /**
   * Launch the script of term extraction and semantic distribution over the rioc server
   * @return the output of the process
   * @throws JSchException
   * @throws IOException
   */
  public String[] termExtraction() throws JSchException, IOException{
    StringBuffer[] sbout = new StringBuffer[2];
    sbout[0]=new StringBuffer();
    sbout[1]=new StringBuffer();
    String corpusRoot = Configuration.riocServerPath+"/"+this.get_id().toString();
    String command = "cd "+corpusRoot+"; oarsub -l /nodes=1/core=5,walltime=3 \"~/scripts/termextract.pl --corpusroot="+corpusRoot+" --name=corpus\"";
    Utils.Log(command);
    SSH ssh = new SSH(sbout[0],sbout[1]);
    ssh.exec(command);
    return new String[]{sbout[0].toString(),sbout[1].toString()};
  }
  
  /*
  public String[] termExtractionOld(){
    String name = "corpus";
    String dispatcherScriptPath = "~/scripts/corpus_proc/task_dispatcher.pl";
    String corpusRoot = Configuration.riocServerPath+"/"+this.get_id().toString();
    String termDir = corpusRoot + "/Terms";
    String resDir = corpusRoot + "/results";
    Utils.ssh_exec("mkdir "+termDir);
    //Utils.Log("[terms] running dispatcher task 'distrib'");
    String[] outputDistribProcess = Utils.ssh_exec(dispatcherScriptPath+" --task distrib -v "+resDir+" > "+termDir+"/"+name+".distrib");
    //Utils.Log("[terms] running dispatcher task 'term'");
    String[] outputTermProcess = Utils.ssh_exec(dispatcherScriptPath+" --task term -v "+resDir+" > "+termDir+"/"+name+".terms");
    //Utils.Log("[terms] getting sample sentences");
    String[] sidfilecreateProcess = Utils.ssh_exec("~/scripts/sidfilecreate.pl "+name+" "+termDir);
    
    String[] sentencefilecreateProcess = Utils.ssh_exec("cat "+termDir+"/"+name+".terms.sid | ~/scripts/sid2sentence_custom.pl --datadir "+resDir+ " > "+termDir+"/"+name+".terms.sentences");
    //Utils.Log("[terms] extracting terms");
    String[] ftermsProcess = Utils.ssh_exec("cd "+termDir+"; cat "+termDir+"/"+name+".terms | ~/scripts/corpus_proc/term_extraction.pl -o "+name+ " --sentences="+name+".terms.sentences > "+name+".fterms");
    //Utils.Log("[terms] done");
    String[] output = new String[2];
    output[0]="Distrib:\n==========\n"+outputDistribProcess[0]+"\n";
    output[1]="Distrib:\n==========\n"+outputDistribProcess[1]+"\n";
    output[0]+="TermProcess:\n==========\n"+outputTermProcess[0]+"\n";
    output[1]+="TermProcess:\n==========\n"+outputTermProcess[1]+"\n";
    output[0]+="SIDFileCreation:\n==========\n"+sidfilecreateProcess[0]+"\n";
    output[1]+="SIDFileCreation:\n==========\n"+sidfilecreateProcess[1]+"\n";
    output[0]+="SentenceFileCreation:\n==========\n"+sentencefilecreateProcess[0]+"\n";
    output[1]+="SentenceFileCreation:\n==========\n"+sentencefilecreateProcess[1]+"\n";
    output[0]+="FTerms:\n==========\n"+ftermsProcess[0]+"\n";
    output[1]+="FTerms:\n==========\n"+ftermsProcess[1]+"\n";
    Utils.Log(output[0]);
    Utils.Log(output[1]);
    return output;
  }
  
  public String[] depExtractionOld(){
    String name = "corpus";
    String dispatcherScriptPath = "~/scripts/corpus_proc/task_dispatcher.pl";
    String corpusRoot = Configuration.riocServerPath+"/"+this.get_id().toString();
    String depDir = corpusRoot + "/Clusters";
    String resDir = corpusRoot + "/results";
    Utils.ssh_exec("mkdir "+depDir);
    // Utils.Log("[deps] build term trie");
    String[] trieProcess = Utils.ssh_exec("cd "+depDir+"; perl ~/scripts/fterms2trie.pl -o "+name+" ../Terms/"+name+".fterms");
    // Utils.Log("[deps] create scanner info");
    String[] scannerfilecreateProcess = Utils.ssh_exec("~/scripts/scannerfilecreate.pl "+name+" "+depDir);
    // Utils.Log("[deps] collecting dependencies");
    String[] depProcess = Utils.ssh_exec("cd "+depDir+"; "+dispatcherScriptPath+" --task rsel --task_data "+name+".yml -v ../results > "+name+".rsel");
    // Utils.Log("[deps] building network"); 
    String[] networkProcess = Utils.ssh_exec("cd "+depDir+"; perl ~/scripts/dep2clusters.pl -o "+name+" --nocache "+name+".rsel");
    
    String[] output = new String[2];
    output[0]="Fterms trie:\n==========\n"+trieProcess[0]+"\n";
    output[1]="Fterms trie:\n==========\n"+trieProcess[1]+"\n";
    output[0]+="Create Scanner file:\n==========\n"+scannerfilecreateProcess[0]+"\n";
    output[1]+="Create Scanner file:\n==========\n"+scannerfilecreateProcess[1]+"\n";
    output[0]+="Dep Process:\n==========\n"+depProcess[0]+"\n";
    output[1]+="Dep Process:\n==========\n"+depProcess[1]+"\n";
    output[0]+="Network Creation:\n==========\n"+networkProcess[0]+"\n";
    output[1]+="Network Creation:\n==========\n"+networkProcess[1]+"\n";
    Utils.Log(output[0]);
    Utils.Log(output[1]);
    return output;
  }
  */
///////////////////////////////////////////////////////////////////////////
//                          CREATE DOCUMENT                              //
///////////////////////////////////////////////////////////////////////////
  
  
  /**
   * Get the information of tasks associated with this corpus
   * @return the list of list of json object representing taskunit in tasks
   */
  public String getDefaultTaskInfo() {
    BasicDBObject task = null;
    if(this.tasksId==null || this.tasksId.size()==0){
      this.initTasks();
    }
    
    DBCollection coll = CPMDB.GetInstance().db.getCollection("tasks");

    BasicDBList taskInfo = new BasicDBList();
    
    for(Object obj:tasksId){
      String taskid = (String)obj;
      BasicDBObject q = new BasicDBObject("_id",new ObjectId(taskid));
      task = (BasicDBObject)coll.findOne(q);
      if(task != null){
        taskInfo.add((BasicDBList)task.get("tasks"));
      }
    }
    
    return (taskInfo).toString();
  }

  /**
   * Retrieve the file containing the output of a particular {@see CPMTaskUnit}
   * @param taskindex the {@see CPMTask} index 
   * @param taskunitindex the {@see CPMTaskUnit} index within its task
   * @param errfile boolean indicating if the output asked for is the standard (false) or error ouput (true)
   * @return the file path of the task output
   */
  public String getTaskOutput(String taskindex,String taskunitindex, boolean errfile) {
    BasicDBObject task = null;
    if(this.tasksId==null || this.tasksId.size()==0){
      return "no task set yet";
    }
    DBCollection coll = CPMDB.GetInstance().db.getCollection("tasks");

    BasicDBObject q = new BasicDBObject("_id",new ObjectId((String)tasksId.get(taskindex)));
    task = (BasicDBObject)coll.findOne(q);
  
    
    BasicDBList list = (BasicDBList) task.get("tasks");
    BasicDBObject taskunit = (BasicDBObject) list.get(Integer.parseInt(taskunitindex));
    
    String outputFile = null;
    if(errfile){
      outputFile = taskunit.getString("stderr");
    }else{
      outputFile = taskunit.getString("stdout");
    }
    
    return outputFile;
  }
  
  /**
   * Get the syntactic analysis process report output
   * @return the output of the process
   * @throws JSchException
   * @throws IOException
   */
  public String[] getReport() throws JSchException, IOException{
    StringBuffer[] sbout = new StringBuffer[2];
    sbout[0]=new StringBuffer();
    sbout[1]=new StringBuffer();

    String corpusRoot = Configuration.riocServerPath+"/"+this.get_id().toString();
    String command = "cd "+corpusRoot+"; ~/scripts/report.pl --autorefresh=0";
    Utils.Log(command);
    SSH ssh = new SSH(sbout[0],sbout[1]);
    ssh.exec(command);
    return new String[]{sbout[0].toString(),sbout[1].toString()};
  }
  
  /**
   * Retrieve the term extraction and semantic distribution results and package the result in a tar archive ready for download
   * @return the output of the process
   */
  public String[] retrieveTermExtractionResults(){
    String[] output = new String[]{"",""};
    String corpusRoot = Configuration.riocServerPath+"/"+this.get_id().toString();
    String importCmd = "scp -r -i " +Configuration.id_rioc + " buiquang@rioc:" + corpusRoot + "/Terms buiquang@rioc:"+corpusRoot+"/Clusters "+this.path+"/results/; cd "+this.path+"; tar -cvzf "+this.path+"/cache/results.tar.gz results;" ;

    Utils.Log(importCmd);
    String scriptfile = CPMCommand.exportToScript(importCmd);
    Process process = null;
    try {
      process = Runtime.getRuntime().exec("sh ".concat(scriptfile));
    } catch (IOException e) {
      output[1] = output[1].concat(e.getMessage());
      e.printStackTrace();
    }
    try {
      BufferedReader bout = new BufferedReader(new InputStreamReader(process.getInputStream()));
      BufferedReader berr = new BufferedReader(new InputStreamReader(process.getInputStream()));
      String line = null;
      while((line = bout.readLine())!=null){
        output[0] = output[0].concat(line+"\n");
      }
      while((line = berr.readLine())!=null){
        output[1] = output[1].concat(line+"\n");
      }
      output[0] = output[0].concat(String.valueOf(process.waitFor()));
    } catch (InterruptedException e) {
      output[1] = output[1].concat(e.getMessage());
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    new File(scriptfile).delete();
    return output;
  }
  
  /**
   * Retrieve the result of the syntactic analysis (from rioc to local)
   * @return the output of the process
   */
  public String[] retrieveResults(){
    String[] output = new String[]{"",""};
    String corpusRoot = Configuration.riocServerPath+"/"+this.get_id().toString();
    String importCmd = "scp -r -i " +Configuration.id_rioc + " buiquang@rioc:" + corpusRoot + "/results buiquang@rioc:"+corpusRoot+"/aligns "+this.path+"/; cd "+this.path+"; cp aligns/*.align results/; tar -cvzf "+this.path+"/cache/results.tar.gz results;" ;

    Utils.Log(importCmd);
    String scriptfile = CPMCommand.exportToScript(importCmd);
    Process process = null;
    try {
      process = Runtime.getRuntime().exec("sh ".concat(scriptfile));
    } catch (IOException e) {
      output[1] = output[1].concat(e.getMessage());
      e.printStackTrace();
    }
    try {
      BufferedReader bout = new BufferedReader(new InputStreamReader(process.getInputStream()));
      BufferedReader berr = new BufferedReader(new InputStreamReader(process.getInputStream()));
      String line = null;
      while((line = bout.readLine())!=null){
        output[0] = output[0].concat(line+"\n");
      }
      while((line = berr.readLine())!=null){
        output[1] = output[1].concat(line+"\n");
      }
      output[0] = output[0].concat(String.valueOf(process.waitFor()));
    } catch (InterruptedException e) {
      output[1] = output[1].concat(e.getMessage());
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    new File(scriptfile).delete();
    this.setstatus("available");
    // term extraction process
    if(this.termExtractProcessEnabled){
      try {
        termExtraction();
      } catch (JSchException e1) {
        // TODO Auto-generated catch block
        e1.printStackTrace();
      } catch (IOException e1) {
        // TODO Auto-generated catch block
        e1.printStackTrace();
      }
    }
    for(Entry<String, Object> e :this.documents.entrySet()){
      Document doc = Document.Retrieve(e.getKey());
      try {
        doc.indexSentences();
      } catch (Exception e1) {
        // TODO Auto-generated catch block
        e1.printStackTrace();
      }
    }
    this.save();
    return output;
  }

  /**
   * Get the path of a graph file
   * @param did the document id owning the graph
   * @param sid the id of the sentence represented by the graph
   * @param vid the version id of the graph
   * @return the filepath of the graph
   * @todo should be moved to the class {@see Document}, even should be depreciated when using {@see GraphManager}
   */
  public String getGraphFilePath(String did,String sid,String vid){
    BasicDBObject graphBundle = new BasicDBObject();
    Document doc = Document.Retrieve(did);
    graphBundle.put("format", this.getoutputFormat());
    Object[] fileinfo = doc.getGraphFileInfo(sid,vid);
    if(fileinfo != null){
      graphBundle.put("filepath", (String)fileinfo[0]);
    }
    return graphBundle.toString();
  }
  
  /**
   * Get the html align of the raw text and syntactic parsed sentences
   * @param did the document id 
   * @return the html string of the annotated document
   * @todo should be moved to the class {@see Document}
   */
  public String getAnnotatedDocument(String did){
    Document doc = Document.Retrieve(did);
    return doc.getAnnotatedDoc();
  }
  
  /**
   * Get a graph fragment (currently works only for conll & depconll format!)
   * @param did the document id owning the graph
   * @param ts the token id at the beginning of the fragment
   * @param te the token id at the end of the fragment
   * @param slist the spanned list of sentence on the fragment
   * @return a serialized json object containing the format of the graph fragment and the data associated
   * @todo should be implemented in the class {@see Document}
   */
  public String getGraph(String did, String ts, String te, BasicDBList slist) {
    Utils.Log("getting graph fragment ("+ts+"|"+te+")");
    BasicDBObject graphBundle = new BasicDBObject();
    Document doc = Document.Retrieve(did);
    graphBundle.put("format", this.getoutputFormat());
    try {
      graphBundle.put("data", doc.getGraphFragment(ts,te,slist));
    } catch (IOException e) {
      e.printStackTrace();
    }
    return graphBundle.toString();
  }
  
  /**
   * Get the tree of documents information
   * @return the serialized json object containing the tree of documents with information present in corpus (preserve uploaded directory structure) 
   * @throws Exception
   */
  public String getDocumentList() throws Exception{
    HashMap<String,Object> jsonResource = new HashMap<String, Object>();
    jsonResource.put("path","/");
    jsonResource.put("children",new HashMap<String,Object>());
    this.getDocumentList(this.path+"/raw", jsonResource.get("children"));
    return (new BasicDBObject(jsonResource)).toString();
  }
  
  /**
   * Sub method used to construct the tree of document information of the corpus
   * @param dirPath the current path in the folder structure of uploaded files
   * @param jsonResource the json data that will be filled with the document information tree
   * @throws Exception
   */
  @SuppressWarnings("unchecked")
  public void getDocumentList(String dirPath,Object jsonResource) throws Exception{
    File dir = new File(dirPath);
    if(!dir.isDirectory()){
      throw new Exception("dirPath must be a directory");
    }
    File[] files = dir.listFiles();
    for (File file : files) {
      String filename = file.getCanonicalPath().replace(dir.getCanonicalPath()+"/", "");
      HashMap<String,Object> fileResource = new HashMap<String, Object>(); 
      ((HashMap<String,Object>)jsonResource).put(filename, fileResource);
      if(file.isDirectory()){
        fileResource.put("path", filename);
        fileResource.put("children", new HashMap<String,Object>());
        this.getDocumentList(file.getCanonicalPath(),fileResource.get("children"));
      } 
      else {
        String fullpath = file.getCanonicalPath().replace(this.path+"/raw/", "");
        String filepath = file.getCanonicalPath();
        Document doc = this.getDocumentFromPath(filepath);
        //doc.indexSentences();
        fileResource.put("path", filename);
        fileResource.put("children", new HashMap<String,Object>());
        fileResource.put("file", true);
        fileResource.put("did", doc.getId());
        SentenceCollection collection = SentenceCollection.Retrieve(doc.getId());
        if(collection!=null){
          fileResource.put("scid", collection.get_id().toString());
        }
        fileResource.put("fullpath", fullpath);
      }
    }
  }

  /**
   * Retrieve a {@see Document} from its path within the uploaded mapped structure of files in the corpus
   * @param filename the path of the document
   * @return the {@see Document} object corresponding to the file source 
   */
  private Document getDocumentFromPath(String filename) {
    BasicDBObject docs = this.getdocuments();
    Iterator<Entry<String,Object>> it = docs.entrySet().iterator();
    while(it.hasNext()){
      Entry<String,Object> entry = it.next();
      String path = (String)entry.getValue();
      if(path.equals(filename)){
        return Document.Retrieve(entry.getKey());
      }
      
    }
    return null;
  }
  
  /**
   * Retrieve the elasticsearch result of the query of particular sentences id
   * @param sids the list of sentence id
   * @param from not used
   * @param did the id of the {@see Document} whom sentence id refers to
   * @param export not used
   * @return
   */
  public String searchSID(String[] sids,Integer from,String did,String export){
    BasicDBList results = new BasicDBList();
    
    for(int i=0;i<sids.length;++i){
      BasicDBObject hit = new BasicDBObject();
      BasicDBObject sentenceSource = new BasicDBObject();
      sentenceSource.put("sid", sids[i]);
      sentenceSource.put("sentence", "sentence");
      sentenceSource.put("did", did);
      hit.put("_score", 1);
      hit.put("_source", sentenceSource);
      results.add(hit);
    }
    
    
    
    BasicDBObject json = new BasicDBObject();
    json.put("total", results.size());
    json.put("dpath", true);
    
    if(results.size()>100){
      json.put("hits", results.subList(0, 100));
    }else{
      json.put("hits", results);
    }
    String jsonString = json.toString();
    return jsonString;
  }
  
  /**
   * Generic search within a corpus
   * @param queryInput the query input
   * @param searchType the search type (sentence ids, sentence (full text match) or dpath)
   * @param from the part of the result to retrieve
   * @param max the max number of result items to retrieve
   * @param did optional precision of which {@see Document} to filter result with
   * @param export optional return the xml export of total result of the search
   * @return whether export was asked or not, return full (xml string) or part (elasticsearch json string) of the result of the search
   */
  public String search(String queryInput,String searchType,String from,String max,String did,String export){
    // get the page
    Integer fromCount = 0;
    if(from != null && !from.equals("")){
      fromCount = Integer.valueOf(from);
    }
    
    BasicDBList subcollectionIds = new BasicDBList();
    
    if(did == null){
      for(Entry<String,Object> entry:this.getdocuments().entrySet()){
        SentenceCollection coll = SentenceCollection.Retrieve(entry.getKey());
        String id = entry.getKey();
        if(coll != null){
          id = coll.get_id().toString();
        }
        subcollectionIds.add(id);
      }
    }else{
      SentenceCollection coll = SentenceCollection.Retrieve(did);
      String id = did;
      if(coll != null){
        id = coll.get_id().toString();
      }
      subcollectionIds.add(id);
    }
    
    // route to the search type
    if(searchType.equals("id")){
      String ids[] = queryInput.split("\\s");
      return searchSID(ids,fromCount,did,export);
    }else if(searchType.equals("sentence")){
      IndexSearchMod mod = new IndexSearchMod();
      
      BasicDBObject result = null;
      try {
        result = mod.fullTextSearch(queryInput, subcollectionIds);
      } catch (Exception e) {
        result = new BasicDBObject();
        e.printStackTrace();
      }
      return result.toString();
    }else {
      String result = "";
      
      Integer maxCount = 20;
      if(max != null && !max.equals("")){
        maxCount = Integer.valueOf(max);
      }
      DPathSearchMod mod = new DPathSearchMod(queryInput);
      mod.setTarget(subcollectionIds);
      try {
        mod.execute();
      } catch (Exception e1) {
        e1.printStackTrace();
      }
      
      if(export != null && (export.equals("1") || export.equals("true"))){
        try {
          result = mod.exportResult();
        } catch (Exception e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
        
      }else{
        try {
          result = mod.getJSONResults(fromCount,maxCount);
        } catch (Exception e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
      
      return result;
    }
  }

  
  /**
   * Map the valid converted documents file source found in the corpus and create the corresponding {@see Document} objects
   * @return the {@see Document} objects id keyed list of converted document filepath  
   * @throws Exception
   */
  public BasicDBObject saveDocuments() throws Exception{
    HashMap<String,Object> jsonResource = new HashMap<String, Object>();
    this.saveDocumentsSub(this.path+"/raw", jsonResource);
    this.documentsMapped = true;
    return new BasicDBObject(jsonResource);
  }

  /**
   * Sub method that create a {@see Document} for each file found in a directory
   * @param dirPath
   * @param jsonResource
   * @throws Exception
   */
  @SuppressWarnings("unchecked")
  public void saveDocumentsSub(String dirPath,HashMap<String,Object> jsonResource) throws Exception{
    File dir = new File(dirPath);
    if(!dir.isDirectory()){
      throw new Exception("dirPath must be a directory");
    }
    File[] files = dir.listFiles();
    for (File file : files) {
      if(file.isDirectory()){
        this.saveDocumentsSub(file.getCanonicalPath(),jsonResource);
      } 
      else {
        Document doc = Document.CreateNew(file.getCanonicalPath(),this.path,this.get_id().toString());
        jsonResource.put(doc.getId(),file.getCanonicalPath());
      }
    }
  }

  /**
   * Index all sentences of the corpus
   * @return serialized json validation signal
   */
  public String index() {
    Utils.Log("Start indexing corpus "+this.externId);
    for(Entry<String, Object> e :this.documents.entrySet()){
      Document doc = Document.Retrieve(e.getKey());
      try {
        doc.indexSentences();
      } catch (Exception e1) {
        e1.printStackTrace();
        return (new BasicDBObject("error",e1.getMessage())).toString();
      }
    }
    Utils.Log("Finish indexing corpus "+this.externId);
    return (new BasicDBObject("success",true)).toString();
  }

  /**
   * Delete all the documents of the corpus (called when corpus is deleted)
   */
  private void deleteDocuments(){
    BasicDBObject docs = this.getdocuments();
    Set<String> keys = docs.keySet();
    for(String key:keys){
      Document doc = Document.Retrieve(key);
      doc.delete();
    }
  }
  
  /**
   * Delete the corpus (the deletion of the local files is handled by mgwiki)
   */
  public void delete() {
    deleteDocuments();
    deleteTasks();
    DBCollection coll = CPMDB.GetInstance().db.getCollection("corpus");
    coll.setObjectClass(Corpus.class);
    BasicDBObject q = new BasicDBObject("externId",this.externId);
    coll.remove(q);
  }
  
  /**
   * Call a mgwiki webservice to assert if the corpus is public or not (permissions are handled in mgwiki db) 
   * @return a boolean indicating whether the corpus is public or not
   */
  public boolean isPublic(){
    try{
      CloseableHttpClient httpClient = HttpClients.createDefault();
      HttpPost postRequest = new HttpPost(
          Configuration.mgwikiHost+"/mgwiki_corpus/corpus_is_public");
      
      
      String postVal = "cid="+URLEncoder.encode(this.externId, "UTF-8");
      StringEntity input = new StringEntity(postVal,"utf-8");
      input.setContentType("application/x-www-form-urlencoded");
      postRequest.setEntity(input);
      
      HttpResponse response = httpClient.execute(postRequest);

      BufferedReader br = new BufferedReader(
                      new InputStreamReader((response.getEntity().getContent())));

      StringBuilder output = new StringBuilder();
      String line = null;
      while ((line = br.readLine()) != null) {
          output.append(line+"\n");
      }
      
      br.close();
      httpClient.close();
      BasicDBObject val = (BasicDBObject) JSON.parse(output.toString());
      return val.getBoolean("public");
    }catch(Exception e){
      return false;
    }
    
  }
  
  /**
   * Prepare the archive of analysis results for download
   * @return a validation object
   */
  public ValidationBean prepareDownload() {
    ValidationBean ok = new ValidationBean();
    if(this.termExtractProcessEnabled){
      retrieveTermExtractionResults();
    }
    ok.success = true;
    return ok;
  }
  
  
  
  
  
}
