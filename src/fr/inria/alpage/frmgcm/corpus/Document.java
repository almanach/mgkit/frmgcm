package fr.inria.alpage.frmgcm.corpus;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.ReflectionDBObject;

import fr.inria.alpage.cpm.task.CPMCommand;
import fr.inria.alpage.frmgcm.AppMain;
import fr.inria.alpage.frmgcm.corpus.format.AbstractGraph;
import fr.inria.alpage.frmgcm.corpus.format.CONLLGraph;
import fr.inria.alpage.frmgcm.db.CPMDB;
import fr.inria.alpage.frmgcm.db.ElasticSearchProxy;
import fr.inria.alpage.frmgcm.gm.Graph;
import fr.inria.alpage.frmgcm.gm.GraphManager;
import fr.inria.alpage.frmgcm.gm.GraphRevision;
import fr.inria.alpage.frmgcm.utils.Utils;

/**
 * This class handle a file source contained in a corpus and reflects its counter part in mongodb, it also provides method associated with the document object  
 * @author buiquang
 *
 */
public class Document {
  /**
   * The reflection of the mongo db object document
   */
  public BasicDBObject dbObject = new BasicDBObject();
  /**
   * The sentences info result of the default syntactic analysis process
   */
  private SentenceInfo[] sentencesLog = null;
  /*public String corpusPath = null;
  public String documentFilename = null;*/
  /**
   * The normalized name of the file source
   */
  private String processedFilename = null;
  /**
   * The relative file path of the document within the valid converted document folder of the corpus 
   */
  private String filerelativepath = null;
  /**
   * The associated {@see DocumentRevision} that contain information about change made to the sentences of this document
   */
  private DocumentRevision docRevision = null;
  
  
  public String getProcessedFilename() {
    if(processedFilename == null){
      processedFilename = getFilerelativepath().replace("/", "_");
    }
    return processedFilename;
  }

  public String getFilerelativepath() {
    if(filerelativepath == null){
      filerelativepath = this.dbObject.getString("documentFilename").replace(this.dbObject.getString("corpusPath")+"/raw/","");
    }
    return filerelativepath;
  }

  /**
   * Retrieve the mongo id of this document
   * @return the mondo id
   */
  public String getId(){
    ObjectId id = (ObjectId)this.dbObject.get( "_id" );
    return id.toString();
  }
  
  /**
   * Get the mongo id of the corpus that own this document 
   * @return the mongo id of a corpus
   */
  public Corpus getCorpusOwner(){
    String cid = this.dbObject.getString("cid");
    if(cid != null){
      return Corpus.Retrieve(cid);
    }
    return null;
  }
  
  /**
   * Factory, create a document object from a file source, the base root of the corpus files and the mongo id of the corpus, and save it in mongo db
   * @param filename the filepath of the file source that the document will reflect
   * @param path the path of the base root of the corpus containing this document
   * @param cid the mongo id of the corpus
   * @return the newly created {@see Document}
   */
  public static Document CreateNew(String filename,String path,String cid){
    Document doc = new Document();
    doc.dbObject.put("corpusPath", path);
    doc.dbObject.put("documentFilename", filename);
    doc.dbObject.put("cid", cid);
    
    DBCollection coll = CPMDB.GetInstance().db.getCollection("document");
    coll.insert(doc.dbObject);
    
    // Revision management
    // create new rev
    HashMap<String,Object> infos = new HashMap<String, Object>();
    infos.put("type", "manual");
    infos.put("title", "initial version");
    BasicDBList tags = new BasicDBList();
    tags.add("v0");
    infos.put("tags", tags);
    infos.put("author", 0);
    infos.put("desc", "initial version");
    doc.docRevision = DocumentRevision.CreateNew(doc.getId(),infos,null);
    String id = doc.docRevision.getId();
    doc.dbObject.put("rid-head", id);
    doc.save();
    // 
    
    
    return doc;
  }
  
  /**
   * Retrieve a document from its id
   * @param id the mongo id of the document
   * @return the {@see Document} corresponding to the id or null if not found
   */
  public static Document Retrieve(String id){
    DBCollection coll = CPMDB.GetInstance().db.getCollection("document");
    BasicDBObject q = new BasicDBObject("_id",new ObjectId(id));
    BasicDBObject documentObj = (BasicDBObject) coll.findOne(q);
    if(documentObj == null){
      return null;
    }
    Document doc = new Document();
    doc.dbObject = documentObj;
    
    /*if(!doc.dbObject.getBoolean("indexed")){
      try {
        doc.indexSentences();
      } catch (Exception e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
      doc.dbObject.put("indexed", true);
      doc.save();
    }*/
    
    //--
    if(doc.dbObject.getString("rid-head")==null){
      Object lock = AppMain.getLock(doc.getId());
      
      synchronized(lock){
        HashMap<String,Object> infos = new HashMap<String, Object>();
        infos.put("type", "manual");
        infos.put("title", "initial version");
        BasicDBList tags = new BasicDBList();
        tags.add("v0");
        infos.put("tags", tags);
        infos.put("author", 0);
        infos.put("desc", "initial version");
        doc.docRevision = DocumentRevision.CreateNew(id,infos,null);
        String rid = doc.docRevision.getId();
        doc.dbObject.put("rid-head", rid);
        doc.save();
      }
    }else{
      doc.docRevision = DocumentRevision.Retrieve(doc.dbObject.getString("rid-head"));
      if(doc.docRevision.completeState == null){
        try {
          doc.docRevision.completeState = doc.docRevision.getPrevRevisionCompleteState(doc.getId(),id);
        } catch (Exception e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
    }
    //--
    /*
    try {
      doc.indexSentences();
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }*/
    
    return doc;
  }
 
  /**
   * Save modification made to the document in db
   */
  public void save(){
    DBCollection coll = CPMDB.GetInstance().db.getCollection("document");
    coll.save(this.dbObject);
  }

  /**
   * Retrieve the number of sentences within this document
   * @return the number of sentences contained in this document
   * @throws IOException
   */
  private int getSentencesCount() throws IOException{
    String infoFilepath = this.dbObject.getString("corpusPath")+"/results/info.txt";
    BufferedReader br = new BufferedReader(new FileReader(infoFilepath));
    String line = null;
    while((line = br.readLine())!=null){
      if(line.startsWith(this.getProcessedFilename())){
        String patternStr="\\S+\\s+(\\d+)";
        Pattern p = Pattern.compile(patternStr);
        Matcher m = p.matcher(line);
        if(m.find()){
          br.close();
          return Integer.valueOf(m.group(1))+1;
        }
      }
    }
    br.close();
    return -1;
  }
  
  /**
   * Retrieve the status list of parsed sentences
   * @return the list of status of parsed sentences
   * @throws IOException
   */
  public SentenceInfo[] getSentencesLog() throws IOException{
    if(sentencesLog==null){
      int n = getSentencesCount();
      if(n==-1){
        Utils.Log("ERROR with sentences count");
        return null;
      }
      
      sentencesLog = new SentenceInfo[n];
      
      String sentencesLogFilepath = this.dbObject.getString("corpusPath")+"/results/"+this.getProcessedFilename()+".log";
      if(!(new File(sentencesLogFilepath).exists())){
        Utils.exec("bzip2 -kd "+sentencesLogFilepath+".bz2");
      }
      BufferedReader br = new BufferedReader(new FileReader(sentencesLogFilepath));
      String line = null;
      int i = -1;
      while((line = br.readLine())!=null){
        
        if(line.startsWith("ok")){
          i = createSentenceInfo(line,0);
        }else if(line.startsWith("robust")){
          i = createSentenceInfo(line,1);
        }else if(line.startsWith("fail")){
          i = createSentenceInfo(line,2);
        }else{
          if(i!=-1){
            String sbuf = new String(line);
            sbuf=sbuf.replace("<", "&lt;");
            sbuf=sbuf.replace(">", "&gt;");
            sbuf=sbuf.replace("\n", "<br>");
            sentencesLog[i].info += sbuf+"<br>";             
          }
        }
      }
      br.close();
      
    }
    return sentencesLog;
  }
  
  /**
   * Create a sentence summary info 
   * @param line the line of the log currently read 
   * @param status the status of the sentence (robust, failed or success)
   * @return the sid of the sentence
   */
  private int createSentenceInfo(String line,int status){
    int i = getSID(line);
    if(i<sentencesLog.length && i != -1){
      if(sentencesLog[i]==null){
        sentencesLog[i] = new SentenceInfo();
      }
      sentencesLog[i].status = status;
    }else{
      Utils.Log("ERROR sid ("+i+") greater than total sentence count");
      i=-1;
    }
    return i;
  }
  
  /**
   * Retrieve the sid of a sentence when parsing an info.txt result file line
   * @param line a line of the file info.txt generated by the syntactic parsing process
   * @return the sid of the sentence
   */
  private int getSID(String line){
    String patternStr="\\w+\\s+(\\d+)\\s+";
    Pattern p = Pattern.compile(patternStr);
    Matcher m = p.matcher(line);
    if(m.find()){
      return Integer.valueOf(m.group(1));
    }
    return -1;
  }
  
  /**
   * Get the dag file corresponding to the document file source
   * @param filename the file source path of this document
   * @return the file path of the dag file constructed from the document source file
   */
  private String getDagAlignFilepath(String filename){
    String dagfileinfo = filename.replace(this.dbObject.getString("corpusPath")+"/raw/","");
    String dagfile = dagfileinfo.replace("/", "_").concat(".align");
    String dagaligndir = this.dbObject.getString("corpusPath")+"/aligns/"; 
    return dagaligndir + dagfile;
  }
  
  /**
   * Retrieve a formatted graph from a graph(sentence) id 
   * @param gid the sentence id to retrieve the graph from
   * @return the formatted graph object
   */
  public AbstractGraph getSentence(String gid){
    String vid = this.docRevision.completeState.getString(gid);
    return getSentence(gid,vid);
  }
  
  /**
   * Retrieve a formatted graph from a sentence id and revision number
   * @param gid the sentence id to retrieve the graph from
   * @param vid the version/revision number of the graph
   * @return the formatted graph object
   */
  public AbstractGraph getSentence(String gid,String vid) {
    Object[] fileinfo = getGraphFileInfo(gid,vid);
    GraphRevision gr = new GraphRevision(new BasicDBObject("uri","file:///"+(String)fileinfo[0]));
    try {
      return AbstractGraph.Create(gr, (String)((HashMap<String,Object>)fileinfo[2]).get("name"));
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return null;
  }
  
 /**
  * Get a graph fragment (currently only works for conll and depconll formats!)
  * @param ts the token id at the beginning of the fragment
  * @param te the token id at the end of the fragment
  * @param slist the list of spanned sentence within this fragment
  * @return the data representing the graph fragment selected
  * @throws IOException
  */
  public String getGraphFragment(String ts, String te, BasicDBList slist) throws IOException {
    String[] tsInfo = ts.split("F");
    String[] teInfo = te.split("F");
    int tsId = Integer.valueOf(tsInfo[1]);
    int teId = Integer.valueOf(teInfo[1]);

    ArrayList<ArrayList<String>> graphs = new ArrayList<ArrayList<String>>();
    
    Iterator<Object> it = slist.iterator();
    int offset = 0;
    while(it.hasNext()){
      String sid = (String)it.next();
      AbstractGraph st = this.getSentence(sid);
      st.setIDOffset(offset);
      
      ArrayList<String> graph = st.getContent();
      offset+=graph.size();
      graphs.add(graph);
    }
    
    StringBuilder sb = new StringBuilder();
    for(int i=0;i<graphs.size();++i){
      ArrayList<String> graph = graphs.get(i);
      int startOffset = 0;
      int endOffset = graph.size();
      if(i==0){
        startOffset = tsId-1;
      }
      if(i==graphs.size()-1){
        endOffset = teId;
      }
      for(int j=startOffset;j<endOffset;j++){
        sb.append(graph.get(j)+"\n");
      }
    }
    return sb.toString();
  }
  
  /**
   * Get the annotated document (the html containing the original text aligned with sentences parses)
   * @return the annotated html document string
   */
  public String getAnnotatedDoc(){
    StringBuilder doc = new StringBuilder();
    String dagalignfile = getDagAlignFilepath(this.dbObject.getString("documentFilename"));
    BufferedReader brdag = null;
    BufferedWriter bwout = null;
    
    File outFile = new File(this.dbObject.getString("corpusPath")+"/cache/"+dagalignfile.replace(this.dbObject.getString("corpusPath")+"/aligns/", ""));
    
    
    /*
    // retrieve cache 
    if(outFile.exists()){
      CPMUtils.Log("retrieving cached annotated document");
      BufferedReader reader;
      StringBuilder  stringBuilder = new StringBuilder();
      try {
        reader = new BufferedReader( new FileReader (outFile));
        String         line = null;
        String         ls = System.getProperty("line.separator");

        while( ( line = reader.readLine() ) != null ) {
            stringBuilder.append( line );
            stringBuilder.append( ls );
        }
      } catch (IOException e) {
        e.printStackTrace();
      }

      return stringBuilder.toString();
    }*/
    
    try {
      bwout = new BufferedWriter(new FileWriter(outFile));
      BufferedReader frdoc = new BufferedReader(new InputStreamReader(new FileInputStream(this.dbObject.getString("documentFilename")),"UTF-8"));
      brdag = new BufferedReader(new FileReader(dagalignfile));
      
      int docCurrentOffset = 0;
      int charRead = 0;
      
      String currentSentence = null;
      Integer currentSID = null;
      boolean enteringSentence = false;
      Integer startOffset = null;
      Integer endOffset = null;
      String tokenEndOffset = null;
      String prevTokenEndOffset = null;
      
       String line;
       while ((line = brdag.readLine()) != null) {
         if(line.startsWith("##DAG BEGIN")){
           enteringSentence = true;
         }else if(line.startsWith("##OFFSET")){
           String[] elts = line.split("\\s+");
           prevTokenEndOffset = tokenEndOffset;
           tokenEndOffset = elts[3];
           startOffset = Integer.parseInt(elts[2]);
           if(enteringSentence){
             currentSentence = elts[1].split("F")[0];
             currentSID = Integer.valueOf(currentSentence.substring(1));
             enteringSentence = false;
             
             int length = startOffset-docCurrentOffset;
             if(length >0){
               char[] cbuf = new char[length];
               int cread = frdoc.read(cbuf, 0, length);
               String sbuf = new String(cbuf);
               sbuf=sbuf.replace("<", "&lt;");
               sbuf=sbuf.replace(">", "&gt;");
               sbuf=sbuf.replace("\n", "<br>");
               docCurrentOffset = startOffset;
               charRead+=length;
               bwout.write(sbuf);
               doc.append(sbuf);
             }
             SentenceInfo stinfo = getSentencesLog()[currentSID];
             String info = "";
             int status = 0;
             if(stinfo!=null){
               info = stinfo.info;
               status = stinfo.status;
             }
             
             doc.append("<span id=\""+currentSentence+"\" infos=\""+
                 info+"\" class=\"cma-sentence sentence-status-"+
                 status+"\">");
             bwout.write("<span id=\""+currentSentence+"\" infos=\""+
                 info+"\" class=\"cma-sentence sentence-status-"+
                 status+"\">");
           }else{
             docCurrentOffset+=writeOffset(startOffset-docCurrentOffset, doc, bwout, frdoc);
           }
           
           docCurrentOffset+=writeToken(Integer.parseInt(tokenEndOffset)-docCurrentOffset, doc, bwout, frdoc,elts[1]);
           
         }else if(line.startsWith("##DAG END")){
           if(tokenEndOffset != null){
             /*endOffset = Integer.parseInt(tokenEndOffset);
             int length = endOffset-docCurrentOffset;
             if(length >0){
               byte[] cbuf = new byte[length];
               int cread = frdoc.read(cbuf, 0, length);
               String sbuf = new String(cbuf);
               sbuf=sbuf.replace("<", "&lt;");
               sbuf=sbuf.replace(">", "&gt;");
               sbuf=sbuf.replace("\n", "<br>");
               charRead+=length;
               docCurrentOffset = endOffset;
               bwout.write(sbuf);
               doc.append(sbuf);
             }*/
             doc.append("</span>");
             bwout.write("</span>");
           }
         }
       }
       
       frdoc.close();
       brdag.close();
       bwout.close();
    } catch (Exception e) {
       e.printStackTrace();
    }    
    return doc.toString();
  }

  /**
   * (Obsolete) Get the annotated document (the html containing the original text aligned with sentences parses)
   * @return the annotated html document string
   */
  @SuppressWarnings("unused")
  public String getAnnotatedDocByte() {
    StringBuilder doc = new StringBuilder();
    String dagalignfile = getDagAlignFilepath(this.dbObject.getString("documentFilename"));
    BufferedReader brdag = null;
    BufferedWriter bwout = null;
    
    File outFile = new File(this.dbObject.getString("corpusPath")+"/cache/"+dagalignfile.replace(this.dbObject.getString("corpusPath")+"/aligns/", ""));
    
    
    
    // retrieve cache 
    /*if(outFile.exists()){
      Utils.Log("retrieving cached annotated document");
      BufferedReader reader;
      StringBuilder  stringBuilder = new StringBuilder();
      try {
        reader = new BufferedReader( new FileReader (outFile));
        String         line = null;
        String         ls = System.getProperty("line.separator");

        while( ( line = reader.readLine() ) != null ) {
            stringBuilder.append( line );
            stringBuilder.append( ls );
        }
      } catch (IOException e) {
        e.printStackTrace();
      }

      return stringBuilder.toString();
    }*/
    
    try {
      bwout = new BufferedWriter(new FileWriter(outFile));
      FileInputStream frdoc = new FileInputStream(this.dbObject.getString("documentFilename"));
      brdag = new BufferedReader(new FileReader(dagalignfile));
      
      int docCurrentOffset = 0;
      int charRead = 0;
      
      String currentSentence = null;
      Integer currentSID = null;
      boolean enteringSentence = false;
      Integer startOffset = null;
      Integer endOffset = null;
      String tokenEndOffset = null;
      String prevTokenEndOffset = null;
      
       String line;
       while ((line = brdag.readLine()) != null) {
         if(line.startsWith("##DAG BEGIN")){
           enteringSentence = true;
         }else if(line.startsWith("##OFFSET")){
           String[] elts = line.split("\\s+");
           prevTokenEndOffset = tokenEndOffset;
           tokenEndOffset = elts[3];
           startOffset = Integer.parseInt(elts[2]);
           if(enteringSentence){
             currentSentence = elts[1].split("F")[0];
             currentSID = Integer.valueOf(currentSentence.substring(1));
             enteringSentence = false;
             
             int length = startOffset-docCurrentOffset;
             if(length >0){
               byte[] cbuf = new byte[length];
               int cread = frdoc.read(cbuf, 0, length);
               String sbuf = new String(cbuf);
               sbuf=sbuf.replace("<", "&lt;");
               sbuf=sbuf.replace(">", "&gt;");
               sbuf=sbuf.replace("\n", "<br>");
               docCurrentOffset = startOffset;
               charRead+=length;
               bwout.write(sbuf);
               doc.append(sbuf);
             }
             doc.append("<span id=\""+currentSentence+"\" infos=\""+
                 getSentencesLog()[currentSID].info+"\" class=\"cma-sentence sentence-status-"+
                 getSentencesLog()[currentSID].status+"\">");
             bwout.write("<span id=\""+currentSentence+"\" infos=\""+
                 getSentencesLog()[currentSID].info+"\" class=\"cma-sentence sentence-status-"+
                 getSentencesLog()[currentSID].status+"\">");
           }else{
             docCurrentOffset+=writeOffsetByte(startOffset-docCurrentOffset, doc, bwout, frdoc);
           }
           
           docCurrentOffset+=writeTokenByte(Integer.parseInt(tokenEndOffset)-docCurrentOffset, doc, bwout, frdoc,elts[1]);
           
         }else if(line.startsWith("##DAG END")){
           if(tokenEndOffset != null){
             /*endOffset = Integer.parseInt(tokenEndOffset);
             int length = endOffset-docCurrentOffset;
             if(length >0){
               byte[] cbuf = new byte[length];
               int cread = frdoc.read(cbuf, 0, length);
               String sbuf = new String(cbuf);
               sbuf=sbuf.replace("<", "&lt;");
               sbuf=sbuf.replace(">", "&gt;");
               sbuf=sbuf.replace("\n", "<br>");
               charRead+=length;
               docCurrentOffset = endOffset;
               bwout.write(sbuf);
               doc.append(sbuf);
             }*/
             doc.append("</span>");
             bwout.write("</span>");
           }
         }
       }
       
       frdoc.close();
       brdag.close();
       bwout.close();
    } catch (Exception e) {
       e.printStackTrace();
    }    
    return doc.toString();
  }
  
  private int writeOffset(int length,StringBuilder doc,BufferedWriter out,BufferedReader source) throws IOException{
    if(length>0){
      char[] cbuf = new char[length];
      int byteRead = source.read(cbuf, 0, length);
      String sbuf = new String(cbuf);
      sbuf=sbuf.replace("<", "&lt;");
      sbuf=sbuf.replace(">", "&gt;");
      sbuf=sbuf.replace("\n", "<br>");
      doc.append(sbuf);
      out.write(sbuf);
      return byteRead;
    }
    return 0;
  }
  
  private int writeOffsetByte(int length,StringBuilder doc,BufferedWriter out,FileInputStream source) throws IOException{
    if(length>0){
      byte[] cbuf = new byte[length];
      int byteRead = source.read(cbuf, 0, length);
      String sbuf = new String(cbuf);
      sbuf=sbuf.replace("<", "&lt;");
      sbuf=sbuf.replace(">", "&gt;");
      sbuf=sbuf.replace("\n", "<br>");
      doc.append(sbuf);
      out.write(sbuf);
      return byteRead;
    }
    return 0;
  }
  
  private int writeToken(int length,StringBuilder doc,BufferedWriter out,BufferedReader source,String tokenId) throws IOException{
    if(length>0){
      char[] cbuf = new char[length];
      int byteRead = source.read(cbuf, 0, length);
      String sbuf = new String(cbuf);
      sbuf=sbuf.replace("<", "&lt;");
      sbuf=sbuf.replace(">", "&gt;");
      sbuf=sbuf.replace("\n", "<br>");
      doc.append("<token ref=\""+tokenId+"\">"+sbuf+"</token>");
      out.write("<token ref=\""+tokenId+"\">"+sbuf+"</token>");
      return byteRead;
    }
    return 0;
  }
  
  private int writeTokenByte(int length,StringBuilder doc,BufferedWriter out,FileInputStream source,String tokenId) throws IOException{
    if(length>0){
      byte[] cbuf = new byte[length];
      int byteRead = source.read(cbuf, 0, length);
      String sbuf = new String(cbuf);
      sbuf=sbuf.replace("<", "&lt;");
      sbuf=sbuf.replace(">", "&gt;");
      sbuf=sbuf.replace("\n", "<br>");
      doc.append("<token ref=\""+tokenId+"\">"+sbuf+"</token>");
      out.write("<token ref=\""+tokenId+"\">"+sbuf+"</token>");
      return byteRead;
    }
    return 0;
  }

  
  public static class SentenceInfo{
    public int status = -1;
    public String info = "";
  }
  
  /**
   * Delete from the elasticsearch index the sentences of this document
   */
  public void deleteIndex(){
    BasicDBObject query = new BasicDBObject();
    BasicDBObject queryInfo = new BasicDBObject();
    SentenceCollection coll = SentenceCollection.Retrieve(this.getId());
    
    // this is legacy when sentences were indexed with the document id and not within a collection
    String id = this.getId();
    if(coll != null){
      id = coll.get_id().toString();
    }
    
    BasicDBObject did = new BasicDBObject("term",new BasicDBObject("did",id));
    BasicDBList match = new BasicDBList();
    match.add(did);
    queryInfo.put("and", match);
    BasicDBObject filter = new BasicDBObject("filter", queryInfo);
    BasicDBObject const_score = new BasicDBObject("constant_score",filter);
    query.put("query", const_score);
    
    BasicDBObject result = null; 
    try {
      result = ElasticSearchProxy.search(query);
    } catch (IOException e) {
      e.printStackTrace();
    }
    
    BasicDBObject hitsInfo = (BasicDBObject)result.get("hits");
    if(result.get("error")!=null || hitsInfo.getInt("total")==0){
      return;
    }
    
    query.put("size", hitsInfo.getInt("total"));
    
    try {
      result = ElasticSearchProxy.search(query);
    } catch (IOException e) {
      e.printStackTrace();
    }
    
    BasicDBList hits = (BasicDBList)((BasicDBObject)result.get("hits")).get("hits");
    String bulkDelete = "";
    for(Object hit:hits){
      bulkDelete += "{\"delete\":{\"_id\":\""+((BasicDBObject) hit).getString("_id")+"\"}}\n";
    }
    ElasticSearchProxy.callBulk(bulkDelete);
    //Utils.Log(bulkDelete, "testDeleteIndex");
  }
  
  /**
   * Index the sentences of this document
   * @throws Exception
   */
  public void indexSentences() throws Exception{
    deleteIndex();
    String resultfileinfo = this.dbObject.getString("documentFilename").replace(this.dbObject.getString("corpusPath")+"/raw/","");
    String resultfile = resultfileinfo.replace("/", "_");
    String resultdir = this.dbObject.getString("corpusPath")+"/results/";
    String dir = resultdir+resultfile+"/";
    if(!(new File(dir).exists())){
      dir = resultdir+resultfile+".tar.gz";
      
      SentenceCollection collection = null;
      String format;
      Corpus c = Corpus.Retrieve(this.dbObject.getString("cid"));
      format = c.getoutputFormat();
      
      collection = SentenceCollection.Retrieve(this.getId());
      if(collection==null){
        BasicDBObject def = new BasicDBObject("name",this.getId());
        def.put("isCMADocument", true);
        def.put("cmaDocumentId", this.getId());
        collection = SentenceCollection.CreateNew(def);
      }else{
        collection.isCMADocument = true;
        collection.cmaDocumentId = this.getId();
        collection.save();
      }
      this.dbObject.put("collection", this.getId());
      this.save();
      collection.addResources("file:///"+(new File(dir)).getAbsolutePath(), format, false, true);
    }else{
      ArrayList<String> params = new ArrayList<String>();
      params.add(this.getProcessedFilename());
      
      Utils.mapReduceFolder(new File(dir),new MapReduceIndexFiles(this),params);
    }
    
    //AbstractGraph.saveTagset();
  }
  
  /**
   * Callback used to index sentence found in the syntactic analysis process result folder corresponding to this document 
   * @author buiquang
   *
   */
  class MapReduceIndexFiles implements Utils.MapReduceFolderDelegate{

    private Integer id = 0;
    private Document caller;
    private HashMap<String,Object> formatInfo;
    
    private SentenceCollection collection = null;
    private String format;
    
    public MapReduceIndexFiles(Document caller){
      this.caller = caller;
      Corpus c = Corpus.Retrieve(caller.dbObject.getString("cid"));
      format = c.getoutputFormat();
      formatInfo = AbstractGraph.formats.get(format);
      
      collection = SentenceCollection.Retrieve(this.caller.getId());
      if(collection==null){
        BasicDBObject def = new BasicDBObject("name",this.caller.getId());
        def.put("isCMADocument", true);
        def.put("cmaDocumentId", this.caller.getId());
        collection = SentenceCollection.CreateNew(def);
      }else{
        collection.isCMADocument = true;
        collection.cmaDocumentId = this.caller.getId();
        collection.save();
      }
      this.caller.dbObject.put("collection", this.caller.getId());
      this.caller.save();
    }
    
    @Override
    public boolean apply(File file, ArrayList<String> args) throws Exception {
      collection.addResources("file:///"+file.getAbsolutePath(), format, false, true);
      
      /*if(fileinfo.length>2 && fileinfo[fileinfo.length-1].equals("bz2")){
        file = new File(Utils.uncompressFile(file));
        fileinfo[2] = "0";
      }
      
      Class formatGraph = (Class)formatInfo.get("class");
      try {
        AbstractGraph st = (AbstractGraph) formatGraph.getConstructor(File.class).newInstance(file);
        st.indexSentence(this.caller, fileinfo[0],"0",true);
      } catch (Exception e) {
        e.printStackTrace();
        return false;
      }*/
      
      return true;
    }
    
  }
  
  /**
   * Retrieve information about a graph
   * @param sid the id of the sentence
   * @param vid the version of the graph
   * @return an array containing 0 the filepath of the graph 1 the id of the graph 2 the format of the graph 3 the revision number of the graph 4 the last revision number of the graph 5 the basefilename (without revision number) of the graph
   */
  public Object[] getGraphFileInfo(String sid,String vid) {
    boolean vidNull = (vid==null);
    String resultfilepath = this.dbObject.getString("documentFilename").replace(this.dbObject.getString("corpusPath")+"/raw/","");
    String resultfile = resultfilepath.replace("/", "_");
    String resultdir = this.dbObject.getString("corpusPath")+"/results/";
    
    Object[] info = new Object[6];
    
    String dir = resultdir+resultfile+"/";
    // problem with option tar (used for semantic analysis), that let the folder compressed
    if(!(new File(dir).exists())){
      String cmd = "cd "+resultdir+"; tar -xzf "+resultfile+".tar.gz";
      String scriptfile = CPMCommand.exportToScript(cmd);
      Process process = null;
      try {
        process = Runtime.getRuntime().exec("sh ".concat(scriptfile));
        process.waitFor();
      } catch (InterruptedException | IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
    
    int n = Integer.valueOf(sid.substring(1));
    String a = String.valueOf(n/10000);
    String b = String.valueOf((n%10000)/1000);
    String c = String.valueOf((n%1000)/100);
    
    dir += a+"/"+b+"/"+c;
    
    for(HashMap<String,Object> formatInfo:AbstractGraph.formats.values()){
      String fileExtension = (String)formatInfo.get("fileExtension");
      final String graphFileName = this.getProcessedFilename()+"."+sid+"."+fileExtension;
      String graphFilePath = dir + "/" + graphFileName;
      String graphFileNameWithRev = graphFilePath + ((vidNull || vid.equals("0"))?"":("."+vid));
      File graphFile = new File(graphFileNameWithRev);
      String graphFileNameFound = graphFileName;
      if(!graphFile.exists()){
        graphFile = new File(graphFilePath+".bz2"+ ((vidNull || vid.equals("0"))?"":("."+vid)));
        graphFileNameFound = graphFileName+".bz2";
      }
      if(graphFile.exists()){
        try {
          info[0] = graphFile.getCanonicalPath();
        } catch (IOException e) {
          e.printStackTrace();
        }
        info[1] = sid;
        info[2] = formatInfo;
        info[5] = graphFilePath;
        
        int lastRev = 0;
        File[] files = (new File(dir)).listFiles(new FilenameFilter(){
          @Override
          public boolean accept(File dir, String name) {
            if(name.startsWith(graphFileName)){
              return true;
            }
            return false;
          }});
        for(File file:files){
          String revinfo = file.getName().replace(graphFileNameFound, "");
          int rev = 0;
          if(revinfo.length()>1){
            rev = Integer.parseInt(revinfo.substring(1));
          }
          if(lastRev<rev){
            lastRev = rev;
          }
        }
        
        if(vidNull){
          if(lastRev != 0){
            try {
              info[0]= new File(graphFileNameWithRev + "." + String.valueOf(lastRev)).getCanonicalPath();
            } catch (IOException e) {
              e.printStackTrace();
            }
          }
          info[3] = lastRev;
        }else{
          info[3] = Integer.parseInt(vid);
        }
        
        info[4] = lastRev;
        return info;
      }
    }
    
    
    /*
    
    for(int i = 0 ; i< files.length ; ++i){
      File file = files[i];
      String filename = file.getName();
      String filepath = null;
      String filepathwithoutrev = null;
      try {
        filepathwithoutrev = filepath = file.getCanonicalPath();
      } catch (IOException e1) {
        // TODO Auto-generated catch block
        e1.printStackTrace();
      }
      String[] fileinfo = filename.replace(this.getProcessedFilename(), "").substring(1).split("\\.");
      int rev = 0;
      if(fileinfo.length > 2){
        filepathwithoutrev = filepath.replace("."+fileinfo[2], "");
        if(fileinfo[fileinfo.length-1].equals("bz2")){
          filepath = Utils.uncompressFile(file);
          if(fileinfo.length==4){
            fileinfo[2]=fileinfo[3];
          }else{
            fileinfo[2] = "0";
          }
        }
        
        rev = Integer.valueOf(fileinfo[2]);
      }
      
      if(fileinfo[0].equals(sid)){
        if(rev>=lastRev){
          lastRev = rev;
          if(vidNull){
            vid = String.valueOf(lastRev);
          }
        }
        
        if(String.valueOf(rev).equals(vid)){
          
          info[0] = filepath;
          info[1] = fileinfo[0];
          info[2] = fileinfo[1];
          info[3] = String.valueOf(rev);
          info[4] = String.valueOf(lastRev); // last revision
          info[5] = filepathwithoutrev;
        } 
      }

      
    }*/
    
    return null;
  }
  
  /**
   * Retrieve information about a graph
   * @param sid the id of the sentence
   * @return an array containing 0 the filepath of the graph 1 the id of the graph 2 the format of the graph 3 the revision number of the graph 4 the last revision number of the graph 5 the basefilename (without revision number) of the graph
   */
  public Object[] getGraphFileInfo(String sid){
    String vid = this.docRevision.completeState.getString(sid);
    if(vid==null){
      vid = "0";
    }
    return getGraphFileInfo(sid, vid);
  }
  
  /**
   * This class is used to store revision information of a document, in particular changes in graph revisions
   * @author buiquang
   *
   */
  protected static class DocumentRevision{
    /**
     * The object reflecting the stored mongo db object
     */
    public BasicDBObject dbObject = new BasicDBObject();
    /**
     * Complete mapping between changes operated to the graph (sid => current revision)
     */
    public BasicDBObject completeState = null;
    /**
     * Get the mongo db id of this document revision
     * @return
     */
    public String getId() {
      ObjectId id = (ObjectId)this.dbObject.get( "_id" );
      return id.toString();
    }

    /**
     * Retrieve the mapping (sid => revision number) of all modified graphs from a particular revision to this revision
     * @param docId the document id 
     * @param fromRev the revision to gather the changes from
     * @return the stacked mapping of all changes
     * @throws Exception
     */
    private static BasicDBObject getPrevRevisionCompleteState(String docId,String fromRev) throws Exception{
      DBCollection coll = CPMDB.GetInstance().db.getCollection("document-rev");
      BasicDBObject q = new BasicDBObject("timedate",-1);
      DBCursor cursor = coll.find().sort( q );
      BasicDBObject stackedChanges = new BasicDBObject();
      boolean start = false;
      while(cursor.hasNext()){
        BasicDBObject obj = (BasicDBObject)cursor.next();
        if(!docId.equals(obj.getString("docId"))){
          continue;
        }
        
        if(!start && fromRev != null){
          if(obj.get("_id").equals(fromRev)){
            start = true;
          }else{
            continue;
          }
        }
        
        BasicDBObject changes = (BasicDBObject)obj.get("state");
        Iterator<Entry<String,Object>> it = changes.entrySet().iterator();
        while(it.hasNext()){
          Entry<String,Object> change = it.next();
          if(!stackedChanges.containsField(change.getKey())){
            stackedChanges.put(change.getKey(), change.getValue());
          }
        }
        if(obj.getString("type").equals("manual")){
          break;
        }
      }
      return stackedChanges;
    }
    
    /**
     * Factory, create a new document revision 
     * @param docId the document id that the revision refers to
     * @param infos infos concerning this revision (author, type (manual or automatic), date, title, tags, description)
     * @param changes the mapping (sid => revision number) of sentence changes
     * @return a new document revision
     */
    public static DocumentRevision CreateNew(String docId,HashMap<String,Object> infos,BasicDBObject changes){
      if(changes == null){
        changes = new BasicDBObject();
      }
      
      BasicDBObject stackedChanges = new BasicDBObject();
      try {
        stackedChanges = DocumentRevision.getPrevRevisionCompleteState(docId,null);
      } catch (Exception e) {
        e.printStackTrace();
      }
      Iterator<Entry<String,Object>> it = changes.entrySet().iterator();
      while(it.hasNext()){
        Entry<String,Object> change = it.next();
        stackedChanges.put(change.getKey(), change.getValue());
      }
      
      String type = (String)infos.get("type");
      if(type.equals("manual")){
        changes = stackedChanges;
      }
      
      DocumentRevision rev = new DocumentRevision();
      rev.dbObject.put("docId", docId);
      rev.dbObject.put("state", changes);
      rev.dbObject.put("timedate", (new Date()).getTime());
      rev.dbObject.put("type", type);
      rev.dbObject.put("author", infos.get("author"));
      rev.dbObject.put("title", infos.get("title"));
      rev.dbObject.put("tags", infos.get("tags"));
      rev.dbObject.put("desc", infos.get("desc"));
      
      rev.completeState = stackedChanges;
      
      DBCollection coll = CPMDB.GetInstance().db.getCollection("document-rev");
      coll.insert(rev.dbObject);
      return rev;
    }
    
    /**
     * Retrieve a document revision from its mongo id
     * @param id the mongo id of the revision
     * @return the document revision 
     */
    public static DocumentRevision Retrieve(String id){
      DBCollection coll = CPMDB.GetInstance().db.getCollection("document-rev");
      BasicDBObject q = new BasicDBObject("_id",new ObjectId(id));
      BasicDBObject documentObj = (BasicDBObject) coll.findOne(q);
      
      
      
      DocumentRevision rev = new DocumentRevision();
      rev.dbObject = documentObj;
      
      try {
        rev.completeState = getPrevRevisionCompleteState(rev.dbObject.getString("docId"),id);
      } catch (Exception e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
      
      return rev;
    }
    
    /**
     * Get the revision number of a sentence (default is 0 if no changes were made)
     * @param sid the sentence id to get the revision number of
     * @return the revision number of a sentence in regards of the changes made to this document revision
     */
    public int getSentenceRId(String sid){
      BasicDBObject state = (BasicDBObject)this.dbObject.get("state");
      Object info = null;
      if((info = state.get(sid))!=null){
        return ((BasicDBObject)info).getInt("rid");
      }
      return 0;
    }
  }
  
  /**
   * Manual creation of a document revision by the user (every graph modification create an automatic revision)
   * @param title the title of the revision
   * @param user the user that created the revision
   * @return a boolean indicating if the revision were successfuly created
   */
  public boolean saveRevision(String title,String description,String user){
    Object lock = AppMain.getLock(this.getId());
    
    synchronized(lock){
      HashMap<String,Object> infos = new HashMap<String, Object>();
      infos.put("type", "manual");
      infos.put("title", title);
      infos.put("author", user);
      if(description != null){
        infos.put("desc", description);
      }
      DocumentRevision rev = DocumentRevision.CreateNew(this.getId(),infos,null);
      
      this.dbObject.put("rid-head", rev.getId());
      
      save();
      
    }
    return true;
  }

  /**
   * Update a graph with new data
   * @param sid the id of the sentence to update
   * @param data the new data
   * @param user_name the name of the user that made the change
   * @return a boolean indicating if the update process went well
   */
  public boolean updateSentence(String sid, String data, String user_name) {
    
    Object lock = AppMain.getLock(this.getId());
    
    synchronized(lock){
      Object[] sentenceInfo = getGraphFileInfo(sid);
      sentenceInfo[3] = (Integer)sentenceInfo[4]+1;
      
      try {
        GraphRevision gr = new GraphRevision(new BasicDBObject("data",data));
        AbstractGraph st = AbstractGraph.Create(gr, (String)((HashMap<String,Object>)sentenceInfo[2]).get("name"));
        st.saveRevision(this,sentenceInfo);
      } catch (Exception e) {
        e.printStackTrace();
        return false;
      }
      
      HashMap<String,Object> infos = new HashMap<String, Object>();
      infos.put("type", "auto");
      infos.put("title", "changes in sentence "+sid);
      infos.put("author", user_name);
      
      BasicDBObject changes = new BasicDBObject();
      changes.put(sid, sentenceInfo[3]);
      
      DocumentRevision rev = DocumentRevision.CreateNew(this.getId(),infos,changes);
      
      this.dbObject.put("rid-head", rev.getId());
      
      save();
    }

    return true;
  }

  /**
   * Retrieve the ordered list of this document revisions (from most recent to oldest)
   * @return the ordered list of this document revisions
   */
  public BasicDBList getRevisions() {
    String id = this.getId();
    DBCollection coll = CPMDB.GetInstance().db.getCollection("document-rev");
    BasicDBObject q = new BasicDBObject("timedate",-1);
    DBCursor cursor = coll.find().sort( q );
    BasicDBList revisionList = new BasicDBList();
    while(cursor.hasNext()){
      BasicDBObject obj = (BasicDBObject)cursor.next();
      if(id.equals(obj.getString("docId"))){
        revisionList.add(obj);
      }
    }
    return revisionList;
  }

  /**
   * delete this document alongside with revisions and elasticsearch index
   */
  public void delete(){
    deleteIndex();
    // delete graphs
    DBCollection coll = CPMDB.GetInstance().db.getCollection(GraphManager.graphCollection);
    coll.setObjectClass(Graph.class);
    SentenceCollection associatedSC = SentenceCollection.Retrieve(this.getId());
    if(associatedSC!=null){
      BasicDBObject qu = new BasicDBObject("cid",associatedSC.get_id().toString());
      DBCursor cursor = coll.find(qu);
      ArrayList<String> ids = new ArrayList<String>();
      while(cursor.hasNext()){
        Graph g = (Graph) cursor.next();
        ids.add(g.get_id().toString());
      }
      for(String uid:ids){
        GraphManager.deleteGraph(uid);
      }
      
    }
    // delete document revisions
    BasicDBList revs = getRevisions();
    coll = CPMDB.GetInstance().db.getCollection("document-rev");
    for(Object obj:revs){
      BasicDBObject rev = (BasicDBObject)obj;
      BasicDBObject q = new BasicDBObject("_id",rev.get("_id"));
      coll.remove(q);
    }
    coll = CPMDB.GetInstance().db.getCollection("document");
    BasicDBObject q = new BasicDBObject("_id",new ObjectId(this.getId()));
    coll.remove(q);
  }
}
