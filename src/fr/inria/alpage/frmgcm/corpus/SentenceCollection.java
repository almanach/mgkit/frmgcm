package fr.inria.alpage.frmgcm.corpus;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.bson.types.ObjectId;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.ReflectionDBObject;
import com.mongodb.util.JSON;

import fr.inria.alpage.frmgcm.Configuration;
import fr.inria.alpage.frmgcm.corpus.format.AbstractGraph;
import fr.inria.alpage.frmgcm.db.CPMDB;
import fr.inria.alpage.frmgcm.db.ElasticSearchProxy;
import fr.inria.alpage.frmgcm.gm.Graph;
import fr.inria.alpage.frmgcm.gm.GraphManager;
import fr.inria.alpage.frmgcm.gm.GraphRevision;
import fr.inria.alpage.frmgcm.search.DPathSearchMod;
import fr.inria.alpage.frmgcm.search.DPathSearchMacroManager;
import fr.inria.alpage.frmgcm.search.DPathSearchMacroManager.DPathMacro;
import fr.inria.alpage.frmgcm.utils.Utils;
import fr.inria.alpage.frmgcm.utils.Utils.BrowseTarFileHandler;
import fr.inria.alpage.frmgcm.utils.Utils.URI;
import fr.inria.alpage.frmgcm.utils.Utils.ValidationBean;
import fr.inria.alpage.frmgcm.utils.Utils.URI.URI_TYPE;

/**
 * This class allows handling multiple graphs as a collection (coherence conflict with {@see Document} class...)
 * @author buiquang
 *
 */
public class SentenceCollection extends ReflectionDBObject{
  /**
   * The name of the collection (unique)
   */
  public String name;
  /**
   * The formats of the graphs contained in this collection
   */
  public BasicDBList formats;
  /**
   * A boolean indicating if this collection is bound to a corpus {@see Document}
   */
  public Boolean isCMADocument;
  /**
   * (optional) the mongo id of the {@see Document} if bounded to one
   */
  public String cmaDocumentId;
  /**
   * (optional) namespace in which this collection may belong (used to cluster some collection for application related permissions (exemple groupe passage))
   */
  public String namespace;
  
  public String getnamespace(){
    return namespace;
  }
  
  public void setnamespace(String namespace){
    this.namespace = namespace;
  }
  
  public String getcmaDocumentId(){
    return cmaDocumentId;
  }
  
  public void setcmaDocumentId(String cmaDocumentId){
    this.cmaDocumentId = cmaDocumentId;
  }
  
  public Boolean getisCMADocument(){
    return isCMADocument;
  }
  
  public void setisCMADocument(Boolean isCMADocument){
    this.isCMADocument = isCMADocument;
  }
  
  public BasicDBList getformats() {
    return formats;
  }
  
  public void setformats(BasicDBList formats){
    this.formats = formats;
  }
  
  public String getname(){
    return name;
  }
  
  public void setname(String name){
    this.name = name;
  }
  
  /**
   * get all graph from this collection and rebuild available formats
   */
  public void updateAvailableFormat(){
    
  }
  
  /**
   * add a available format
   * @param format
   */
  public void updateAvailableFormat(String format){
    if(formats == null){
      formats = new BasicDBList();
    }
    if(!formats.contains(format)){
      formats.add(format);
      this.save();
    }
    
  }
  
  
  public SentenceCollection(){
    
  }
  
  /**
   * Factory, create a new collection from a json object definition
   * @param definition json object definition containing at least the field 'name'
   * @return
   */
  public static SentenceCollection CreateNew(BasicDBObject definition){
    SentenceCollection collection = new SentenceCollection();
    // collection set defintion
    if(definition == null){
      return null;
    }
    collection.name = definition.getString("name");
    collection.namespace = definition.getString("namespace");
    if(definition.get("isCMADocument")!=null){
      collection.isCMADocument = definition.getBoolean("isCMADocument");
      collection.cmaDocumentId = definition.getString("cmaDocumentId");
    }
    Utils.ValidationBean valid = collection.validate();
    if(valid.success){
      Utils.Log("creating new collection "+collection.name);
      collection.save();
      return collection;
    }
    return null;
  }
  
  /**
   * Retrieve every {@see SentenceCollection}
   * @return the list of every {@see SentenceCollection}
   */
  public static BasicDBList RetrieveAll(){
    DBCollection coll = CPMDB.GetInstance().db.getCollection("stcollection");
    coll.setObjectClass(SentenceCollection.class);
    DBCursor cursor = coll.find();
    BasicDBList all = new BasicDBList();
    while(cursor.hasNext()){
      SentenceCollection doc = (SentenceCollection)cursor.next();
      all.add(doc);
    }
    return all;
  }
  
  /**
   * Filter out sentence collections from db
   * @param condition the json object that will filter which collection should be retrieved
   * @return the list of filtered sentence collection
   */
  public static BasicDBList RetrieveAll(BasicDBObject condition){
    DBCollection coll = CPMDB.GetInstance().db.getCollection("stcollection");
    coll.setObjectClass(SentenceCollection.class);
    DBCursor cursor = coll.find(condition);
    BasicDBList all = new BasicDBList();
    while(cursor.hasNext()){
      SentenceCollection doc = (SentenceCollection)cursor.next();
      all.add(doc);
    }
    return all;
  }
  
  /**
   * Retrieve a particular collection from its name
   * @param collection_name the name of the collection
   * @return the corresponding collection or null if not found
   */
  public static SentenceCollection Retrieve(String collection_name){
    DBCollection coll = CPMDB.GetInstance().db.getCollection("stcollection");
    coll.setObjectClass(SentenceCollection.class);
    BasicDBObject q = new BasicDBObject("name",collection_name);
    SentenceCollection collection = (SentenceCollection) coll.findOne(q);
    return collection;
  }
  
  /**
   * Validate an instance of SentenceCollection (before saving it)
   * @return a validation bean
   */
  public Utils.ValidationBean validate(){
    Utils.ValidationBean validation = new Utils.ValidationBean();
    if(name==null){
      validation.success = false;
      validation.message = "Name is null!";
      return validation;
    }
    if(name.startsWith("_")){
      validation.success = false;
      validation.message = "Name cannot start with \"_\".";
      return validation;
    }
    String patternStr="[^a-zA-Z0-9_]";
    Pattern p = Pattern.compile(patternStr);
    Matcher m = p.matcher(name);
    if(m.find()){
      validation.success = false;
      validation.message = "Name must contains only characters ranging from a to z (ignoring case), numbers and \"_\".";
      return validation;
    }
    SentenceCollection existingCollection = SentenceCollection.Retrieve(name);
    if (existingCollection != null) {
      validation.success = false;
      validation.message = "Name already exist. Please choose another name.";
      return validation;
    }
    validation.success = true;
    return validation;
  }
  
  /**
   * Save the modification to the collection into the db
   */
  public void save(){
    DBCollection coll = CPMDB.GetInstance().db.getCollection("stcollection");
    coll.setObjectClass(SentenceCollection.class);
    coll.save(this);
  }
  
  /**
   * Removed costly or unused data from elasticsearch query result
   * @param results the elasticsearch query result object
   * @return the lightweighted query result
   */
  public BasicDBList formatResults(BasicDBList results){
    BasicDBList formattedResults = new BasicDBList();
    for(Object obj:results){
      BasicDBObject result = (BasicDBObject)obj;
      BasicDBObject resultSource = (BasicDBObject)result.get("_source");
      BasicDBObject formattedResult = new BasicDBObject();
      formattedResult.put("sid", resultSource.getString("sid"));
      String did = resultSource.getString("did");
      formattedResults.add(formattedResult);
    }
    return formattedResults;
  }
  
  /**
   * Index all the graph in this collection
   * @param overwrite re index all graph even if they are already indexed
   */
  public void index(boolean overwrite){
    
    DBCollection coll = CPMDB.GetInstance().db.getCollection(GraphManager.graphCollection);
    coll.setObjectClass(Graph.class);
    
    BasicDBObject q = new BasicDBObject();
    
    BasicDBList filters = new BasicDBList();
    filters.add(new BasicDBObject("cid",this.get_id().toString()));
    
    q.put("$and", filters);

    DBCursor curs = coll.find(q); 
    while(curs.hasNext()){
      Graph graph = (Graph)curs.next();
      graph.index(overwrite);
    }
  }
  
  /**
   * Add a graph to this collection
   * @param data the data defining the graph structure
   * @param format the format of the graph
   * @param autoIndex a boolean indicating if the graph must be indexed once added
   * @param eid an external id used to identify the graph
   * @return a validation bean indicating if the addition was a success
   */
  public Utils.ValidationBean addResource(String data,String format,boolean autoIndex,String eid){
    Utils.ValidationBean ok = new ValidationBean();
    BasicDBObject graphDefinition = new BasicDBObject();
    graphDefinition.put("format", format);
    graphDefinition.put("cid", this.get_id().toString());
    graphDefinition.put("eid", eid);
    
    Graph existingGraph = GraphManager.getGraph(graphDefinition);
    graphDefinition.put("data", data);
    Graph g = null;
    if(existingGraph!=null){
      g = existingGraph;
      g.update(graphDefinition);
      g.save();
    }else{
      g = GraphManager.createGraph(graphDefinition);
    }
    if(g != null){
      if(autoIndex){
        if(!g.bIndexed){
          g.index();
        }
      }
    }else{
      ok.success = false;
      Utils.Log("Failed adding graph "+eid,1);
    }
    return ok;
  }
  
  /**
   * Add a graph or multiple graph found at a certain resource location (url or local archive/directory)
   * @param resourcePath the resource location (url or file (must begin with 'http://' or 'file:///'))
   * @param format the format of the graph(s)
   * @param saveLocalCopy a boolean indicating if the data of the graph(s) must be locally saved
   * @param autoIndex a boolean indicating if the graph(s) should be indexed once added
   * @return a validation bean indicating if import was a success
   */
  public Utils.ValidationBean addResources(String resourcePath,final String format,final boolean saveLocalCopy,final boolean autoIndex){
    return addResources(resourcePath,format,saveLocalCopy,autoIndex,null);
  }
  
  /**
   * Add a graph or multiple graph found at a certain resource location (url or local archive/directory)
   * @param resourcePath the resource location (url or file (must begin with 'http://' or 'file:///'))
   * @param format the format of the graph(s)
   * @param saveLocalCopy a boolean indicating if the data of the graph(s) must be locally saved
   * @param autoIndex a boolean indicating if the graph(s) should be indexed once added
   * @param eid if resource is a single graph, the external id of this graph (otherwise it is generated from the path of the graph within the archive/directory)
   * @return a validation bean indicating if import was a success
   */
  public Utils.ValidationBean addResources(String resourcePath,final String format,final boolean saveLocalCopy,final boolean autoIndex,final String eid){
    Utils.Log("adding resource to collection "+this.name);
    this.updateAvailableFormat(format);
    Utils.ValidationBean ok = new ValidationBean();
    Utils.URI uri = new URI(resourcePath);
    if(uri.type == URI_TYPE.FILEPATH){
      
      HashMap<String,Object> formatInfo = AbstractGraph.formats.get(format);
      final String fileExtension = ((String)formatInfo.get("fileExtension")).replace(".", "\\.");
      // pattern used to select only the files that represent graphs
      String patternStr=".*?\\.(E\\d+)\\."+fileExtension+".*?";
      final Pattern p = Pattern.compile(patternStr);
      
      // temporary stores for batch indexing
      final ArrayList<String> bulkIndex = new ArrayList<String>();
      final ArrayList<Graph> graphToIndex = new ArrayList<Graph>();
      
      // handle files found if they match the pattern
      final BrowseTarFileHandler op = new BrowseTarFileHandler(this.get_id().toString()) {
        private int counter = 0;
        
        @Override
        public boolean handleFile(String filename, InputStream fileStream) {
          try{
            Matcher m = p.matcher(filename);
            if(m.find()){
              BasicDBObject graphDefinition = new BasicDBObject();
              graphDefinition.put("format", format);
              graphDefinition.put("cid", (String)this.eData);
              if(eid==null){
                graphDefinition.put("eid",filename);
              }else{
                graphDefinition.put("eid", eid);
              }
              
              Graph existingGraph = GraphManager.getGraph(graphDefinition);
              GraphRevision contentToIndex = null;
              byte[] btoRead = new byte[1024];
              StringBuffer sb = new StringBuffer();
              int len = 0;
              try {
                while ((len = fileStream.read(btoRead)) != -1) {
                  String s = new String(btoRead,Charset.forName("ISO-8859-1"));
                  sb.append(s);
                  btoRead = new byte[1024];
                }
              } catch (IOException e) {
                e.printStackTrace();
              }
              
              if(saveLocalCopy){
                graphDefinition.put("data", sb.toString());
              }else{
                graphDefinition.put("uri", "file:///"+filename);
              }
              
              Graph g = null;
              if(existingGraph!=null){
                g = existingGraph;
                g.bIndexed = true;
                g.update(graphDefinition);
                g.save();
              }else{
                g = GraphManager.createGraph(graphDefinition);
              }
              
              if(g != null){
                if(autoIndex){
                  if(!g.bIndexed && existingGraph==null){
                    contentToIndex = new GraphRevision(new BasicDBObject("data",sb.toString()));
                    AbstractGraph graph = AbstractGraph.Create(contentToIndex, format);
                    String[] info = new String[2];
                    info[0] = "{\"index\":{}}"; 
                    info[1] = graph.indexSentenceInfo((String)this.eData, g.get_id().toString(), "0");
                    if(info != null){
                      bulkIndex.add(info[0]);
                      bulkIndex.add(info[1]);
                    }
                    graphToIndex.add(g);
                  }
                }
              }else{
                Utils.Log("Failed adding graph "+filename,1);
              }
              if(bulkIndex.size()>1000){
                String bulkop = "";
                for(String s:bulkIndex){
                  bulkop += s+"\n";
                }
                ElasticSearchProxy.callBulk(bulkop);
                for(Graph gr:graphToIndex){
                  gr.bIndexed = true;
                  gr.save();
                }
                graphToIndex.clear();
                bulkIndex.clear();
              }
              counter++;  
              if(counter>500 && counter%500 == 0){
                Utils.Log(counter+" currently added to collection (id="+this.eData+")");
              }
            }
          }catch(Exception e){
            Utils.Log(e.getMessage(),1);
          }
          
          return true;
        }
      };
      
      if(new File(uri.locationEndPoint).isDirectory()){
        try {
          Utils.mapReduceFolder(new File(uri.locationEndPoint),new Utils.MapReduceFolderDelegate() {
            
            @Override
            public boolean apply(File file, final ArrayList<String> args) throws Exception {
              Utils.URI furi = new Utils.URI("file:///"+file.getCanonicalPath());
              Utils.browseTar(furi.getStream(), furi.locationEndPoint, op);
              return true;
            }
          },new ArrayList<String>());
        } catch (Exception e) {
          ok.message = "error when browsing file archive";
          e.printStackTrace();
        }
      }else{
        InputStream is = uri.getStream();
        try {
          Utils.browseTar(is, uri.locationEndPoint, op);
        } catch (Exception e) {
          ok.message = "error when browsing file archive";
          e.printStackTrace();
        }
        try {
          is.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
      
      
      // index remaining graphs
      if(bulkIndex.size()>0){
        String bulkop = "";
        for(String s:bulkIndex){
          bulkop += s+"\n";
        }
        ElasticSearchProxy.callBulk(bulkop);
        for(Graph gr:graphToIndex){
          gr.bIndexed = true;
          gr.save();
        }
        graphToIndex.clear();
        bulkIndex.clear();
      }
      ok.success = true;
      
      
    }else if(uri.type == URI_TYPE.URL){
      BasicDBObject graphDefinition = new BasicDBObject();
      if(eid==null){
        graphDefinition.put("eid", resourcePath);
      }else{
        graphDefinition.put("eid", eid);
      }
      
      graphDefinition.put("format", format);
      graphDefinition.put("cid", this.get_id().toString());
      
      Graph existingGraph = GraphManager.getGraph(graphDefinition);

      if(saveLocalCopy){
        graphDefinition.put("data", Utils.fetchFromURL(uri));
      }else{
        graphDefinition.put("uri", resourcePath);
      }
      
      
      Graph g = null;
      if(existingGraph!=null){
        g = existingGraph;
        g.update(graphDefinition);
      }else{
        g = GraphManager.createGraph(graphDefinition);
      }
      if(g != null){
        if(autoIndex){
          g.index(true);
        }
        ok.success = true;
      }else{
        ok.success = false;
      }
    }else{
      ok.message = "Couldn't reach resource";
    }
    Utils.Log("finish adding resource to collection "+this.name);
    return ok;
  }
  
  
  
  /**
   * delete index of the sentences from the collection
   * @param collectionid the collection id 
   */
  public static void deleteIndex(String collectionid){
    BasicDBObject query = new BasicDBObject();
    BasicDBObject queryInfo = new BasicDBObject();
    BasicDBObject did = new BasicDBObject("term",new BasicDBObject("did",collectionid));
    BasicDBList match = new BasicDBList();
    match.add(did);
    queryInfo.put("and", match);
    BasicDBObject filter = new BasicDBObject("filter", queryInfo);
    BasicDBObject const_score = new BasicDBObject("constant_score",filter);
    query.put("query", const_score);
    
    BasicDBObject result = null; 
    try {
      result = ElasticSearchProxy.search(query);
    } catch (IOException e) {
      e.printStackTrace();
    }
    
    BasicDBObject hitsInfo = (BasicDBObject)result.get("hits");
    if(result.get("error")!=null || hitsInfo.getInt("total")==0){
      return;
    }
    
    query.put("size", hitsInfo.getInt("total"));
    
    try {
      result = ElasticSearchProxy.search(query);
    } catch (IOException e) {
      e.printStackTrace();
    }
    
    BasicDBList hits = (BasicDBList)((BasicDBObject)result.get("hits")).get("hits");
    String bulkDelete = "";
    for(Object hit:hits){
      bulkDelete += "{\"delete\":{\"_id\":\""+((BasicDBObject) hit).getString("_id")+"\"}}\n";
    }
    ElasticSearchProxy.callBulk(bulkDelete);
    //Utils.Log(bulkDelete, "testDeleteIndex");
  }
  
  /**
   * custom indexation of mgwiki sentences, also create the signature - page bindings
   * @throws Exception
   */
  public static void MGWikiIndex() throws Exception{
    SentenceCollection collection = SentenceCollection.Retrieve("frmgwiki");
    deleteIndex("frmgwiki"); // to remove
    if(collection == null){
      collection = SentenceCollection.CreateNew(new BasicDBObject("name","frmgwiki"));
    }
    deleteIndex(collection.get_id().toString());
    deleteSignatures();
    cleanFrmgWikiGraphs();
    Utils.Log("Started indexing mgwiki sentences");
    
    
    BasicDBObject graphs = SentenceCollection.getMGWikiSentences();
    Set<Entry<String,Object>> entrySet = graphs.entrySet();
    Iterator<Entry<String,Object>> it = entrySet.iterator();
    int stCount = 0;
    while(it.hasNext()){
      Entry<String,Object> entry = it.next();
      String uid = entry.getKey();
      String url = Configuration.mgwikiHost+"/mgwiki/ws/graph/"+uid+"/export";
      collection.addResources(url, "depxml", true, true,uid);
      stCount++;
      String signature = getMGWikiGraphSignature(uid);
      BasicDBList list = (BasicDBList)((BasicDBObject)entry.getValue()).get("relatedPages");
      String[] relatedPages = new String[list.size()];
      for(int i=0;i<list.size();i++){
        relatedPages[0] = String.valueOf(list.get(i));
      }
      if(!signature.trim().equals("")){
        collection.updateSignature(uid, signature, "mgwiki", relatedPages, ((BasicDBObject)entry.getValue()).getString("title"));
      }
    }
    Utils.Log("Finished indexing mgwiki sentences");
    Utils.Log("Setting signature weight");
    setSignatureWeight(collection,stCount);
    Utils.Log("Finished signature weight");
  }
  
  /**
   * Update signature info (used only for mgwiki..)
   * @param graph_eid the mgwiki (or collection external graph id) uid of the graph
   * @param signature the dpath signature to save
   * @param user the user that will be the owner of the signature
   * @param page_ref_ids the list of page id (mgwiki nid) that will be associated with this signature
   * @param signatureDescription the optional description of the signature
   */
  public void updateSignature(String graph_eid,String signature,String user, String[] page_ref_ids, String signatureDescription){
    String name = this.name+"_"+graph_eid;
    DPathMacro existingMacro = DPathMacro.RetrieveFromName(name);
    DPathMacro macro = null;
    if(existingMacro!=null){
      macro = existingMacro;
    }else{
      macro = new DPathMacro();
      macro.created = new java.util.Date().toString();
      macro.name = name;
    }
    if(signatureDescription!=null){
      macro.description = signatureDescription;
    }
    macro.lastModified = new java.util.Date().toString();
    macro.query = signature;
    macro.user = user;
    macro.ispublic = true;
    macro.mgwikiPageRelated = new BasicDBList();
    if(page_ref_ids != null){
      for(String page_ref_id:page_ref_ids){
        macro.mgwikiPageRelated.add(page_ref_id);
      }
    }
    Utils.ValidationBean validation = macro.validate(false);
    if(validation.success){
      macro.save();
      Utils.Log("Registred signature for (uid:"+graph_eid+")");
    }
  }
  
  /**
   * delete mgwiki signatures
   */
  public static void deleteSignatures(){
    DBCollection coll = CPMDB.GetInstance().db.getCollection("dpath_macro");
    coll.setObjectClass(DPathMacro.class);
    BasicDBObject q = new BasicDBObject("user", "mgwiki");
    coll.remove(q);
  }
  
  
  /**
   * delete the mgwiki graphs stored
   */
  public static void cleanFrmgWikiGraphs(){
    SentenceCollection collection = SentenceCollection.Retrieve("frmgwiki");
    deleteIndex("frmgwiki"); // to remove
    if(collection == null){
      collection = SentenceCollection.CreateNew(new BasicDBObject("name","frmgwiki"));
    }
    deleteIndex(collection.get_id().toString());
    DBCollection coll = CPMDB.GetInstance().db.getCollection(GraphManager.graphCollection);
    coll.setObjectClass(Graph.class);
    BasicDBObject q = new BasicDBObject("cid",collection.get_id().toString());
    DBCursor cursor = coll.find(q);
    ArrayList<String> ids = new ArrayList<String>();
    while(cursor.hasNext()){
      Graph g = (Graph) cursor.next();
      ids.add(g.get_id().toString());
    }
    for(String uid:ids){
      GraphManager.deleteGraph(uid);
    }
  }
  
  /**
   * compute signature weights
   * @param collection the collection to evaluate the importance of signature with
   * @param totalSentence the number of sentences of the collection
   */
  public static void setSignatureWeight(SentenceCollection collection,int totalSentence){
    DBCollection coll = CPMDB.GetInstance().db.getCollection("dpath_macro");
    //coll.setObjectClass(DPathMacro.class);
    BasicDBObject q = new BasicDBObject("user", "mgwiki");
    DBCursor cur = coll.find(q);
    while(cur.hasNext()){
      DPathMacro macro = (DPathMacro)cur.next();
      DPathSearchMod mod = new DPathSearchMod(macro.query);
      BasicDBList subcollectionIds = new BasicDBList();
      subcollectionIds.add(collection.get_id().toString());
      mod.setTarget(subcollectionIds);
      
      try {
        mod.execute();
      } catch (Exception e) {
        e.printStackTrace();
      }
      if(mod.getResults()!= null && mod.getResults().size()>0){
        int resultSize = mod.getResults().size();
        float diff = (float)(totalSentence-resultSize);
        macro.weight = String.valueOf((diff/(float)totalSentence));
      }else{
        macro.weight = "0";
      }
      macro.save();
    }
    
    
  }
  
  /**
   * Retrieve the related pages associated to a sentence 
   * @param sentence the sentence to parse
   * @param params the param sent to the parser
   * @return the list of pages weighted by their signature binding
   * @throws Exception
   */
  public static BasicDBList MGWikiMatchSignatures(String sentence,String params) throws Exception{
    BasicDBList relatedPages = new BasicDBList();
    CloseableHttpClient httpClient = HttpClients.createDefault();
    String getVal = "?sentence="+URLEncoder.encode(sentence,"utf-8")+"&options=utf8+xml"+URLEncoder.encode(params,"utf-8")+"&schema=dep&view=txt";
    HttpGet getRequest = new HttpGet(Configuration.frmgparserproxyurl+getVal);
    


    HttpResponse response = httpClient.execute(getRequest);

    BufferedReader br = new BufferedReader(
                    new InputStreamReader((response.getEntity().getContent())));

    StringBuilder output = new StringBuilder();
    String line = null;
    while ((line = br.readLine()) != null) {
        output.append(line+"\n");
    }
    
    br.close();
    httpClient.close();
    String stdata = output.toString();
    if(!stdata.trim().equals("")){
      GraphRevision gr = new GraphRevision(new BasicDBObject("data",stdata));
      AbstractGraph st = AbstractGraph.Create(gr, "depxml");
      BasicDBObject jsonSentence = new BasicDBObject();
      st.indexSentenceContent(jsonSentence);
      
      BasicDBList sentencesSearchPool = new BasicDBList();
      sentencesSearchPool.add(new BasicDBObject("_source",jsonSentence));
      
      BasicDBList signatures = new BasicDBList();
      DBCollection coll = CPMDB.GetInstance().db.getCollection("dpath_macro");
      coll.setObjectClass(DPathMacro.class);
      BasicDBObject q = new BasicDBObject("user", "mgwiki");
      DBCursor cursor = coll.find(q);
      List<DBObject> sigs = cursor.toArray();
      for(DBObject sig:sigs){
        signatures.add((DPathMacro)sig);
      }
      

      for(Object obj:signatures){
        DPathMacro signature = (DPathMacro)obj;
        DPathSearchMod mod = new DPathSearchMod(signature.query);
        
        try {
          mod.execute(sentencesSearchPool);
        } catch (Exception e) {
          e.printStackTrace();
        }
        if(mod.getResults()!= null && mod.getResults().size()>0){
          if(signature.mgwikiPageRelated!=null){
            Float weight = Float.valueOf(signature.getweight());
            if(weight == null){
              weight = (float) 0;
            }
            BasicDBObject pages = new BasicDBObject("weight",weight);
            pages.put("pages", signature.mgwikiPageRelated);
            relatedPages.add(pages);
          }
        }
      }
      
    }
    return relatedPages;
  }
  
  /**
   * Retrieve graph data from mgwiki graph uid
   * @param uid the uid of the graph in mgwiki
   * @return the data string of the graph
   * @throws ClientProtocolException
   * @throws IOException
   */
  public static String getMGWikiGraphData(String uid) throws ClientProtocolException, IOException{
    CloseableHttpClient httpClient = HttpClients.createDefault();
    HttpPost postRequest = new HttpPost(
        Configuration.d3jsUrl);
    
    
    String postVal = "action=export"+
        "&uid="+uid;
    StringEntity input = new StringEntity(postVal,"utf-8");
    input.setContentType("application/x-www-form-urlencoded");
    postRequest.setEntity(input);

    HttpResponse response = httpClient.execute(postRequest);

    BufferedReader br = new BufferedReader(
                    new InputStreamReader((response.getEntity().getContent())));

    StringBuilder output = new StringBuilder();
    String line = null;
    while ((line = br.readLine()) != null) {
        output.append(line+"\n");
    }
    
    br.close();
    httpClient.close();
    return output.toString();
  }
  
  /**
   * Retrieve the signature of a mgwiki graph using its uid
   * @param uid the uid of the mgwiki graph
   * @return the signature of the graph
   * @throws ClientProtocolException
   * @throws IOException
   */
  public static String getMGWikiGraphSignature(String uid) throws ClientProtocolException, IOException{
    CloseableHttpClient httpClient = HttpClients.createDefault();
    HttpPost postRequest = new HttpPost(
        Configuration.d3jsUrl);
    
    
    String postVal = "action=_getSig"+
        "&uid="+uid;
    StringEntity input = new StringEntity(postVal,"utf-8");
    input.setContentType("application/x-www-form-urlencoded");
    postRequest.setEntity(input);

    HttpResponse response = httpClient.execute(postRequest);

    BufferedReader br = new BufferedReader(
                    new InputStreamReader((response.getEntity().getContent())));

    StringBuilder output = new StringBuilder();
    String line = null;
    while ((line = br.readLine()) != null) {
        output.append(line+"\n");
    }
    
    br.close();
    httpClient.close();
    return output.toString();
  }
  
  /**
   * Retrieve all the mgwiki graphs
   * @return 
   * @throws ClientProtocolException
   * @throws IOException
   */
  public static BasicDBObject getMGWikiSentences() throws ClientProtocolException, IOException{
    CloseableHttpClient httpClient = HttpClients.createDefault();
    HttpPost postRequest = new HttpPost(
        Configuration.mgwikiSentencesUrl);
    
    

    HttpResponse response = httpClient.execute(postRequest);

    BufferedReader br = new BufferedReader(
                    new InputStreamReader((response.getEntity().getContent())));

    StringBuilder output = new StringBuilder();
    String line = null;
    while ((line = br.readLine()) != null) {
      output.append(line+"\n");
    }
    
    br.close();
    httpClient.close();
    return (BasicDBObject) JSON.parse(output.toString());
  }

  /**
   * delete this collection alongside with the indexed sentences and stored graphs
   */
  public void delete() {
    deleteIndex(this.get_id().toString());
    DBCollection coll = CPMDB.GetInstance().db.getCollection(GraphManager.graphCollection);
    coll.setObjectClass(Graph.class);
    BasicDBObject q = new BasicDBObject("cid",this.get_id().toString());
    DBCursor cursor = coll.find(q);
    ArrayList<String> ids = new ArrayList<String>();
    while(cursor.hasNext()){
      Graph g = (Graph) cursor.next();
      ids.add(g.get_id().toString());
    }
    for(String uid:ids){
      GraphManager.deleteGraph(uid);
    }
    coll = CPMDB.GetInstance().db.getCollection("stcollection");
    coll.setObjectClass(SentenceCollection.class);
    q = new BasicDBObject("name",this.name);
    coll.remove(q);
  }


  

}
