/**
 * This package encompass the classes handling search operations over elasticsearch index using dpath query language.
 */
package fr.inria.alpage.frmgcm.search;