package fr.inria.alpage.frmgcm.search;

import java.util.ArrayList;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;

import fr.inria.alpage.frmgcm.search.DPathSearchMod.DPathSearchBaseElement;
import fr.inria.alpage.frmgcm.utils.Utils;

/**
 * Unit of search for DPath query language
 * @author buiquang
 *
 */
public class DPathSearchUnit {
  /**
   * source input of the search unit
   */
  private ArrayList<BasicDBObject> sourceElts;
  /**
   * raw output (mapped with the source input)
   */
  private ArrayList<ArrayList<BasicDBObject>> mappedResultElts;
  /**
   * unified output
   */
  private ArrayList<BasicDBObject> resultElts;
  /**
   * Associated function to this search unit
   */
  private DPathSearchFunction function;
  
  /**
   * Construct a search unit from a {@see DPathSearchFunction}
   * @param function the {@see DPathSearchFunction} that will define this search unit
   */
  public DPathSearchUnit(DPathSearchFunction function){
    this.function = function; 
  }

  /**
   * Execute this search unit
   * @param sentence the base sentence ({@see DPathSearchBaseElement}) that is used as a context for the entrySet
   * @param entrySet the entry set of nodes or edges to apply the search unit over
   */
  public void execute(DPathSearchBaseElement sentence,ArrayList<BasicDBObject> entrySet){
    sourceElts = entrySet;
    //Utils.Log("executing filter "+this.function.getClass().getName());
    //Utils.Log("entry set is "+entrySet);
    mappedResultElts = this.function.apply(sentence, entrySet);
  }
  
  /**
   * Get the result of this search unit
   * @return the list of nodes or edges that resulted from this search unit
   */
  public ArrayList<BasicDBObject> getResult(){
    resultElts = new ArrayList<BasicDBObject>();
    for(ArrayList<BasicDBObject> subset : mappedResultElts){
      for(BasicDBObject item:subset){
        if(resultElts.indexOf(item)==-1){
          resultElts.add(item);
        }
      }
    }
    return resultElts;
  }
  
  /**
   * Get the mapped result of this search unit
   * @return the result list of lists of nodes or edges that are mapped to their originated input
   */
  public ArrayList<ArrayList<BasicDBObject>> getMappedResult(){
    return mappedResultElts;
  }
  
  
 
  /**
   * Common interface for function that define a search unit (navigation, filter, test)
   * @author buiquang
   *
   */
  public static interface DPathSearchFunction{
    /**
     * Apply the function over an entry set
     * @param sentence the base sentence, context of the entry
     * @param entry the entry set, list of nodes or edges
     * @return the mapped result of the function applied over the set
     */
    public ArrayList<ArrayList<BasicDBObject>> apply(DPathSearchBaseElement sentence,ArrayList<BasicDBObject> entry);
  }
  
  /**
   * Test a property of node/edge and consequently filter the entry input 
   * @author buiquang
   *
   */
  public static class DPathTest implements DPathSearchFunction{
    /**
     * The attribute to be tested
     */
    private String attr = "";
    /**
     * The value reference 
     */
    private String val = "";
    /**
     * The operator that define the test of the attribute against the value of reference
     */
    private String operator = "";
    
    public DPathTest(String attr,String val,String operator){
      this.attr = attr;
      this.val = val;
      this.operator = operator;
    }

    @Override
    public ArrayList<ArrayList<BasicDBObject>> apply(DPathSearchBaseElement sentence, ArrayList<BasicDBObject> entry) {
      //Utils.Log("testing "+attr+operator+val);
      ArrayList<ArrayList<BasicDBObject>> results = new ArrayList<ArrayList<BasicDBObject>>();
      for(BasicDBObject elt:entry){
        ArrayList<BasicDBObject> identity = new ArrayList<BasicDBObject>();
        // compare the number of edges in or out of a node to the reference value
        if(this.attr.equals("eout") || this.attr.equals("ein")){
          try{
            int val = Integer.parseInt(this.val);
            
            ArrayList<BasicDBObject> edgeCount = null;
            if(this.attr.equals("ein")){
              edgeCount = sentence.getIn(elt.getString("id"));
            }else{
              edgeCount = sentence.getOut(elt.getString("id"));
            }
            if(this.operator.equals("=") && edgeCount.size()==val){
              identity.add(elt);
            }else if(this.operator.equals("<") && edgeCount.size()<val){
              identity.add(elt);
            }else if(this.operator.equals(">") && edgeCount.size()>val){
              identity.add(elt);
            }
          }
          catch(Exception e){
            
          }
        }else if(this.attr.equals("dir")){
          // test the direction of the edge
          try{
            boolean direction = sentence.getEdgeDirection(elt);
            if(this.val.equals("left") && !direction){
              identity.add(elt);
            }else if(this.val.equals("right") && direction){
              identity.add(elt);
            }
          }
          catch(Exception e){
            
          }
        }else{
          // default nodes/edges attribute testing
          String eltVal = elt.getString(this.attr);
          if(eltVal!=null && !this.val.equals("")){
            if(eltVal.equals(this.val)){
              identity.add(elt);
            }
          }
          else{
            if(this.attr.equals("")){
              
            }else{
              Utils.Log(this.attr+" does not exist in "+elt.toString(),1);
            }
            
            /*ArrayList<BasicDBObject> identity = new ArrayList<BasicDBObject>();
            identity.add(elt);
            results.add(identity);*/
          }
        }
        
        results.add(identity);
        
      }
      
      return results;
    }
    
  }
  
  /**
   * Evaluate a dpath expression
   * @author buiquang
   *
   */
  public static class DPathExpression implements DPathSearchFunction{
    /**
     * The entry set
     */
    public String entrySet = null;
    /**
     * The search unit contained in this expression
     */
    public ArrayList<DPathSearchUnit> elts = null;
    
    public DPathExpression(ArrayList<DPathSearchUnit> elements,String entrySet){
      this.elts = elements;
      this.entrySet = entrySet;
    }
    
    public DPathExpression(ArrayList<DPathSearchUnit> elements){
      this.elts = elements;
    }
    
    @Override
    public ArrayList<ArrayList<BasicDBObject>> apply(DPathSearchBaseElement sentence, ArrayList<BasicDBObject> entry) {
      if(elts.size()<1){
        Utils.Log("No elts defined",1);
        return null;
      }
      
      ArrayList<BasicDBObject> input = entry;
      if(entrySet != null){
        input = sentence.getEntrySet(entrySet);
      }
      
      DPathSearchUnit firstFilter = elts.get(0);
      firstFilter.execute(sentence, input);
      ArrayList<ArrayList<BasicDBObject>> mappedResult = firstFilter.getMappedResult();
      
      for(int i =1; i< elts.size();++i){
        DPathSearchUnit filter = elts.get(i);
        for(int j = 0; j<mappedResult.size();++j){
          ArrayList<BasicDBObject> mappedEntry = mappedResult.get(j);
          filter.execute(sentence, mappedEntry);
          mappedEntry = filter.getResult();
          mappedResult.set(j, mappedEntry);
        }
      }
      
      return mappedResult;
    }
    
  }
  
  /**
   * Apply union or intersection between dpath expression
   * @author buiquang
   *
   */
  public static class DPathCoord implements DPathSearchFunction{
    /**
     * List of coordinators between expressions
     */
    public ArrayList<String> coordinators = null;
    /**
     * List of expressions
     */
    private ArrayList<DPathSearchUnit> coords = null;
    
    public DPathCoord(ArrayList<DPathSearchUnit> coords,ArrayList<String> coordinators){
      this.coordinators = coordinators;
      this.coords = coords;
    }
    
    /**
     * Intersection between two sets
     * @param listA
     * @param listB
     * @param type if true, override default intersection behavior (return union if both set are non empty, empty set otherwise)
     * @return the intersection of two set (except if type is true)
     */
    public static ArrayList<BasicDBObject> intersect(ArrayList<BasicDBObject> listA,ArrayList<BasicDBObject> listB,boolean type){
      if(type){
        if(listA.size()>0 && listB.size()>0){
          return join(listA,listB);
        }else{
          return new ArrayList<BasicDBObject>();
        }
      }
      ArrayList<BasicDBObject> intersection = new ArrayList<BasicDBObject>();
      for(BasicDBObject objA:listA){
        for(BasicDBObject objB:listB){
          if(objA.equals(objB)){
            intersection.add(objA);
          }
        }
      }
      return intersection;
    }
    
    /**
     * Union between two sets
     * @param listA
     * @param listB
     * @return the union of two set
     */
    public static ArrayList<BasicDBObject> join(ArrayList<BasicDBObject> listA,ArrayList<BasicDBObject> listB){
      ArrayList<BasicDBObject> union = new ArrayList<BasicDBObject>();
      for(BasicDBObject objA:listA){
        union.add(objA);
      }
      for(BasicDBObject objB:listB){
        boolean exist = false;
        for(BasicDBObject objC:union){
          if(objC.equals(objB)){
            exist=true;
            break;
          }
        }
        if(!exist){
          union.add(objB);
        }
      }
      return union;
    }

    @Override
    public ArrayList<ArrayList<BasicDBObject>> apply(DPathSearchBaseElement sentence, ArrayList<BasicDBObject> entry) {
      if(coords.size()-1!=this.coordinators.size()){
        Utils.Log("Incoherent coordinators/coordinated definition!!",1);
        return null;
      }
      
      DPathSearchUnit firstCoord = coords.get(0);
      firstCoord.execute(sentence, entry);
      ArrayList<ArrayList<BasicDBObject>> mappedResult = firstCoord.getMappedResult();
      
      boolean joinType = (entry == null);
      
      for(int i =1; i< coords.size();++i){
        DPathSearchUnit coord = coords.get(i);
        coord.execute(sentence, entry);
        ArrayList<ArrayList<BasicDBObject>> coordMappedResult = coord.getMappedResult();
        if(coordinators.get(i-1).equalsIgnoreCase("or")){
          for(int k = 0 ; k < mappedResult.size(); ++k){
            mappedResult.set(k, DPathCoord.join(mappedResult.get(k), coordMappedResult.get(k)));
          }
        }else{ // and
          for(int k = 0 ; k < mappedResult.size(); ++k){
            mappedResult.set(k, DPathCoord.intersect(mappedResult.get(k), coordMappedResult.get(k),joinType));
          }
        }
      }
      
      return mappedResult;
    }
  }
  
  
  /**
   * Filter an entry set using the mapped result of search units process list applied over the entry set
   * @author buiquang
   *
   */
  public static class DPathFilter implements DPathSearchFunction{
    
    public String type = ".";
    private ArrayList<DPathSearchUnit> filters = null;
    
    
    public DPathFilter(ArrayList<DPathSearchUnit> filters){
      this.filters = filters;
    }
   

    @Override
    public ArrayList<ArrayList<BasicDBObject>> apply(DPathSearchBaseElement sentence, ArrayList<BasicDBObject> entry) {
      //Utils.Log("filtering  with "+type);
      if(filters.size()<1){
        Utils.Log("Filter with no filters!!",1);
        return null;
      }
      
      DPathSearchUnit firstFilter = filters.get(0);
      firstFilter.execute(sentence, entry);
      ArrayList<ArrayList<BasicDBObject>> mappedResult = firstFilter.getMappedResult();
      
      for(int i =1; i< filters.size();++i){
        DPathSearchUnit filter = filters.get(i);
        for(int j = 0; j<mappedResult.size();++j){
          ArrayList<BasicDBObject> mappedEntry = mappedResult.get(j);
          filter.execute(sentence, mappedEntry);
          mappedEntry = filter.getResult();
          mappedResult.set(j, mappedEntry);
        }
      }
      
      // apply filter :
      ArrayList<ArrayList<BasicDBObject>> filteredEntry = new ArrayList<ArrayList<BasicDBObject>>();
      for(int i=0;i<mappedResult.size();++i){
        ArrayList<BasicDBObject> identity = new ArrayList<BasicDBObject>();
        ArrayList<BasicDBObject> subset = mappedResult.get(i);
        // keep if result set is non empty
        if(type.equals(".")){
          if(subset.size()>0){
            identity.add(entry.get(i));
          }
        }else if(type.equals("a")){
          // keep every entries if all result set are non empty
          if(subset.size()==0){
            ArrayList<ArrayList<BasicDBObject>> aFailed = new ArrayList<ArrayList<BasicDBObject>>();
            for(int j=0;j<entry.size();++j){
              aFailed.add(new ArrayList<BasicDBObject>());
            }
            return aFailed;
          }
          identity.add(entry.get(i));
        }else if(type.equals("x")){
          // keep every entries if at least of result set is non empty
          if(subset.size()>0){
            ArrayList<ArrayList<BasicDBObject>> xSuccess = new ArrayList<ArrayList<BasicDBObject>>();
            for(int j=0;j<entry.size();++j){
              ArrayList<BasicDBObject> tmp = new ArrayList<BasicDBObject>();
              tmp.add(entry.get(j));
              xSuccess.add(tmp);
            }
            return xSuccess;
          }
        }else if(type.equals("n")){
          // keep every entries iff every result set is empty
          if(subset.size()>0){
            ArrayList<ArrayList<BasicDBObject>> nFailed = new ArrayList<ArrayList<BasicDBObject>>();
            for(int j=0;j<entry.size();++j){
              nFailed.add(new ArrayList<BasicDBObject>());
            }
            return nFailed;
          }
          identity.add(entry.get(i));
        }
        filteredEntry.add(identity);
      }
      
      
      
      return filteredEntry;
    }
    
  }
  
  /**
   * Generate a new set from the entry set using predefined navigation axis
   * @author buiquang
   *
   */
  public static class DPathNavigate implements DPathSearchFunction{
    /**
     * The axis of navigation (out, in, target, source, prev, next, parents, children)
     */
    private String axis;
    
    public DPathNavigate(String axis){
      this.axis = axis;
    }

    @Override
    public ArrayList<ArrayList<BasicDBObject>> apply(DPathSearchBaseElement sentence,ArrayList<BasicDBObject> entry) {
      //Utils.Log("navigation within axis "+axis);
      ArrayList<ArrayList<BasicDBObject>> results = new ArrayList<ArrayList<BasicDBObject>>();
      for(BasicDBObject elt:entry){
        ArrayList<BasicDBObject> subset = new ArrayList<BasicDBObject>();
        
        if(axis.equals("out")){
          subset.addAll(sentence.getOut(elt.getString("id")));
        }else if(axis.equals("in")){
          subset.addAll(sentence.getIn(elt.getString("id")));
        }else if(axis.equals("target")){
          subset.add((BasicDBObject)sentence.getNode(elt.getString("target")));
        }else if(axis.equals("source")){
          subset.add((BasicDBObject)sentence.getNode(elt.getString("source")));
        }else if(axis.equals("prev")){
          BasicDBObject node = sentence.getPrev(elt);
          if(node!=null){
            subset.add(node);
          }
        }else if(axis.equals("next")){
          BasicDBObject node = sentence.getNext(elt);
          if(node!=null){
            subset.add(node);
          }
        }else if(axis.equals("parents")){
          subset.addAll(sentence.getAscendants(elt.getString("id")));
        }else if(axis.equals("children")){
          subset.addAll(sentence.getDescendants(elt.getString("id")));
        }
        results.add(subset);
      }
      return results;
    }
  }
  

}
