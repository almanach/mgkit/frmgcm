package fr.inria.alpage.frmgcm.search;

import com.mongodb.BasicDBObject;

import fr.inria.alpage.frmgcm.utils.Utils.ValidationBean;

/**
 * This abstract class define a common interface for search modules
 * @author buiquang
 *
 */
public abstract class SearchMod {
  
  private Thread searchThread = null;
  private Integer _stopSignal = 0;
  
  public int get_stopSignal() {
    int stopSignal;
    synchronized(_stopSignal){
      stopSignal = _stopSignal;
    }
    return stopSignal;
  }


  public void set_stopSignal(int _stopSignal) {
    synchronized(this._stopSignal){
      this._stopSignal = _stopSignal;
    }
  }

  /**
   * The status of the search (0 means processing, 1 process finished, 2 process ended with error)
   */
  public int status;

  /**
   * Retrieve result
   * @param from
   * @param max
   * @return
   */
  public abstract BasicDBObject getResult(int from,int max);

  
  public Thread getSearchThread() {
    return searchThread;
  }

  public void setSearchThread(Thread searchThread) {
    this.searchThread = searchThread;
  }

  public abstract String exportResult();

}
