package fr.inria.alpage.frmgcm.search;

import java.util.HashMap;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;

import fr.inria.alpage.frmgcm.utils.DBCacheManager;
import fr.inria.alpage.frmgcm.utils.StaticCacheManager;
import fr.inria.alpage.frmgcm.utils.Utils;
import fr.inria.alpage.frmgcm.utils.AbstractCacheManager.Cacheable;
import fr.inria.alpage.frmgcm.utils.Utils.ValidationBean;

public class AsyncSearch {
  
  public static HashMap<String,SearchMod> searches = new HashMap<String,SearchMod>();
  
  public String query;
  public BasicDBList targets;
  public String searchType;
  
  public static class QueryCacheIndex{
    public String query;
    public BasicDBList targetDocuments;
    
    
    public QueryCacheIndex(String thequery,BasicDBList thetargetdocuments){
      query = thequery;
      targetDocuments = thetargetdocuments;
    }
    
    public String toString(){
      String retVal = query;
      for(Object obj:targetDocuments){
        String s = (String)obj;
        retVal+=s;
      }
      return retVal;
    }
  }
  
  public AsyncSearch(String query,BasicDBList targets,String searchType){
    this.query = query;
    this.targets = targets;
    this.searchType = searchType;
  }
  
  public String export(){
    String qid = (new QueryCacheIndex(query,targets)).toString();
    Cacheable cache = DBCacheManager.getCache(qid);
    SearchMod mod = searches.get(qid);
    if(cache != null){
      return DPathSearchMod.exportResults((BasicDBList)cache.data); 
    }else if(mod!=null){
      return mod.exportResult();
    }else{
      this.execute(0, 10);
      return "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><error>Cache expired, please retry later</error>";
    }
  }
  
  public BasicDBObject execute(int from,int max){
    String qid = (new QueryCacheIndex(query,targets)).toString();
    Cacheable cache = DBCacheManager.getCache(qid);
    SearchMod mod = searches.get(qid);
    if(cache != null){
      BasicDBList results = (BasicDBList)cache.data;
      if(mod!=null){
        searches.remove(qid);
      }
      return DPathSearchMod.formatResult(results,from,max,1,1); 
    }
    
    
    if(mod!=null){
      if(mod.status != 0){
        searches.remove(qid);
      }
      Thread t = mod.getSearchThread();
      if(t!=null){
        return mod.getResult(from,max);
      }
    }
    
    if(searchType.equals("dpath")){
      final DPathSearchMod mode = new DPathSearchMod(query);
      mode.setTarget(targets);
      searches.put(qid, mode);
      Runnable searchingProcess = new Runnable(){

        @Override
        public void run() {
          try {
            mode.execute();
          } catch (Exception e) {
            e.printStackTrace();
          }
          
        }
        
      };
      Thread t = new Thread(searchingProcess);
      mode.setSearchThread(t);
      t.start();
      return mode.getResult(from,max);
    }else if(searchType.equals("fulltext")){
      IndexSearchMod mode = new IndexSearchMod();
      /*BasicDBObject result = null;
      try {
        result = mod.fullTextSearch(query, targets);
      } catch (Exception e) {
        result = new BasicDBObject();
        e.printStackTrace();
      }
      return result;*/
      // @todo
    }
    return null;
  }
  
  public ValidationBean abort(){
    ValidationBean response = new ValidationBean();
    
    String qid = (new QueryCacheIndex(query,targets)).toString();
    SearchMod mod = searches.get(qid);
    
    if(mod!=null){
      searches.remove(qid);
      if(mod.getSearchThread() != null){
        mod.set_stopSignal(1);
        mod.setSearchThread(null);
      }
    }
    response.success = true;
    return response;
  }
  

}
