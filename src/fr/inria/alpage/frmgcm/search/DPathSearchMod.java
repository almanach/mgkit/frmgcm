package fr.inria.alpage.frmgcm.search;

import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;

import fr.inria.alpage.frmgcm.Configuration;
import fr.inria.alpage.frmgcm.dpp.ASTCoordExpression;
import fr.inria.alpage.frmgcm.dpp.ASTDPathFilter;
import fr.inria.alpage.frmgcm.dpp.ASTDPathNavigation;
import fr.inria.alpage.frmgcm.dpp.ASTDPathTest;
import fr.inria.alpage.frmgcm.dpp.ASTExpression;
import fr.inria.alpage.frmgcm.dpp.ASTGroupedExpression;
import fr.inria.alpage.frmgcm.dpp.ASTMacroExpression;
import fr.inria.alpage.frmgcm.dpp.ASTStart;
import fr.inria.alpage.frmgcm.dpp.DPathParser;
import fr.inria.alpage.frmgcm.dpp.Node;
import fr.inria.alpage.frmgcm.dpp.SimpleNode;
import fr.inria.alpage.frmgcm.utils.AbstractCacheManager.CompositeCacheable;
import fr.inria.alpage.frmgcm.utils.DBCacheManager;
import fr.inria.alpage.frmgcm.utils.StaticCacheManager;
import fr.inria.alpage.frmgcm.utils.AbstractCacheManager.Cacheable;
import fr.inria.alpage.frmgcm.utils.Utils;

/**
 * This class handle dpath query search
 * @author buiquang
 *
 */
public class DPathSearchMod extends SearchMod {

  public BasicDBList targetDocuments = null;
  public String query = null;
  /**
   * This is the original query obtained (without macro expansion)
   */
  public String originalQuery = null;
  public String entrySet = null;
  public BasicDBObject prefilter = null;
  public BasicDBList pfFilters = null;
  public BasicDBList curPfFilters = null;
  public BasicDBObject pfQuery = null;
  private BasicDBList _result = null;
  public String neo4jQuery;
  
  private DPathSearchUnit dpathProcesses = null;
  
  /**
   * Construct a the helper with a query
   * @param query the dpath query
   */
  public DPathSearchMod(String query){
    this.originalQuery = query; 
    this.query = query;
    initPreFilter();
  }
  
  /**
   * Set the targeted collections ids
   * @param subcollectionIds
   */
  public void setTarget(BasicDBList subcollectionIds){
    this.targetDocuments = subcollectionIds;
    // add a filter for document ids
    if(this.targetDocuments.size()==1){
      this.addPreFilter(new BasicDBObject("term",new BasicDBObject("did",subcollectionIds.get(0))),null);
    }else{
      this.addPreFilter(new BasicDBObject("terms",new BasicDBObject("did",subcollectionIds)),null);
    }
    
  }
  
  /**
   * Construct the base structure for elasticsearch filter
   */
  private void initPreFilter(){
    pfFilters = new BasicDBList();
    curPfFilters = pfFilters;
    pfQuery = new BasicDBObject();
    BasicDBObject filteredQuery = new BasicDBObject();
    filteredQuery.put("filter",
        new BasicDBObject("and",pfFilters));
    filteredQuery.put("query", pfQuery);
    prefilter = new BasicDBObject("query",
        new BasicDBObject("filtered",filteredQuery));
  }

  /**
   * Expand macro present in the query (ie name starting with '$')
   * @todo move this to DPathSearchMacroManager ?
   * @param query the dpath query
   * @return the dpath query with macro fully expanded
   * @throws Exception
   */
  public static String expandMacros(String query) throws Exception{
    final Matcher matcher = Pattern.compile("(\\$[A-Za-z0-9_]*)").matcher(query);
    String expandedQuery = query;
    while(matcher.find())
    {
        final MatchResult matchResult = matcher.toMatchResult();
        final String replacement = DPathSearchMacroManager.expandMacro(matchResult.group(1).substring(1));
        expandedQuery = expandedQuery.substring(0, matchResult.start()) +
                 replacement + expandedQuery.substring(matchResult.end());
        matcher.reset(expandedQuery);
    }
    return expandedQuery;
  }
  
  
  
  /**
   * Parse the input query and returns a list of Search Unit process
   * @return the {@see DPathSearchUnit} tree root
   */
  public DPathSearchUnit parseQuery(){
    try {
      query = expandMacros(query);
      query = query.trim();
      if(!query.endsWith(";")){
        query += ";";
      }
      java.io.StringReader sr = new java.io.StringReader( query );
      java.io.Reader r = new java.io.BufferedReader( sr ); 
      DPathParser t = new DPathParser(r);
      ASTStart n = t.Start();
      
      return processParseTree((SimpleNode)n.children[0],null);
    } catch (Exception e) {
      Utils.Log("Couldn't parse query "+query,1);
      Utils.Log(e.getMessage(),1);
      e.printStackTrace();
    }
    return null;
  }
  
  /**
   * Parse the tree structure of the query
   * @param tree the current root of the tree structure 
   * @param filter the mutable list of filters that will be added while parsing the query parts
   * @return the {@see DPathSearchUnit} tree root
   */
  private DPathSearchUnit processParseTree(SimpleNode tree,BasicDBList filter){
    return processParseTree(tree,true,filter);
  }

  /**
   * Parse a node of the tree structure of the query
   * @param tree the node of the tree structure
   * @param addPreFilter a boolean indicating whether prefilter should be added when parsing the node (set to false when negative filter is applied)
   * @param prefilter the mutable list of filters that will be added while parsing the query parts
   * @return a {@see DPathSearchUnit} tree
   */
  private DPathSearchUnit processParseTree(SimpleNode tree,boolean addPreFilter,BasicDBList prefilter){
    DPathSearchUnit queryElements = null;
    Class nodeType = tree.getClass();
    
    if(nodeType.equals(ASTCoordExpression.class)){
      ArrayList<DPathSearchUnit> coordElements = new ArrayList<DPathSearchUnit>();
      ASTCoordExpression coordination = (ASTCoordExpression) tree;
      BasicDBList curPreFilter = prefilter;
      for(int i = 0 ; i< tree.children.length; ++i){
        SimpleNode sn = (SimpleNode)tree.children[i];
        BasicDBList[] newprefilters = null;
        if(i<coordination.coords.size()){
          String coord = coordination.coords.get(i);
          if(coord.equalsIgnoreCase("or")){
            newprefilters = this.addUnionFilter(prefilter);
          }
        }
        BasicDBList newprefilter = null;
        if(newprefilters!=null){
          newprefilter = newprefilters[0];
          curPreFilter = newprefilters[1];
        }else{
          newprefilter = curPreFilter;
        }
        coordElements.add(processParseTree(sn,addPreFilter,newprefilter));
      }
      DPathSearchUnit.DPathCoord coord = new DPathSearchUnit.DPathCoord(coordElements,coordination.coords);
      DPathSearchUnit coordUnit = new DPathSearchUnit(coord);
      queryElements = coordUnit;
    }
    else if(nodeType.equals(ASTGroupedExpression.class)){
      queryElements = processParseTree((SimpleNode)tree.children[0],addPreFilter,prefilter);
    }
    else if(nodeType.equals(ASTMacroExpression.class)){
      // should not happend => preprocessed beforehand
    }
    else if(nodeType.equals(ASTDPathFilter.class)){
      ArrayList<DPathSearchUnit> filterElements = new ArrayList<DPathSearchUnit>();
      String filtertype = ((ASTDPathFilter)tree).name;
      boolean doNotAddPreFilterToNegationFilter = !filtertype.equals("n");
      for(Node node:tree.children){
        SimpleNode sn = (SimpleNode)node;
        filterElements.add(processParseTree(sn,doNotAddPreFilterToNegationFilter,prefilter));
      }
      DPathSearchUnit.DPathFilter filter = new DPathSearchUnit.DPathFilter(filterElements);
      filter.type = filtertype;
      DPathSearchUnit filterUnit = new DPathSearchUnit(filter);
      queryElements = filterUnit;
      
    }else if(nodeType.equals(ASTExpression.class)){
      ASTExpression expr = (ASTExpression)tree;
      String entryset = null;
      if(!expr.name.equals("")){
        entryset = expr.name;
      }
      ArrayList<DPathSearchUnit> expressionElements = new ArrayList<DPathSearchUnit>();
      for(Node node:tree.children){
        SimpleNode sn = (SimpleNode)node;
        expressionElements.add(processParseTree(sn,addPreFilter,prefilter));
      }
      DPathSearchUnit.DPathExpression expression = new DPathSearchUnit.DPathExpression(expressionElements,entryset);
      DPathSearchUnit expressionUnit = new DPathSearchUnit(expression);
      queryElements = expressionUnit;
    }else if(nodeType.equals(ASTDPathTest.class)){
      ASTDPathTest test = (ASTDPathTest)tree;
      DPathSearchUnit testUnit = new DPathSearchUnit(new DPathSearchUnit.DPathTest(test.name,test.val,test.operator));
      if(addPreFilter && test.isAttr){
        BasicDBList qfilter = new BasicDBList();
        qfilter.add(new BasicDBObject("term",new BasicDBObject("nodes."+test.name,test.val)));
        qfilter.add(new BasicDBObject("term",new BasicDBObject("edges."+test.name,test.val)));
        this.addPreFilter(new BasicDBObject("or",qfilter),prefilter);
      }
      queryElements = testUnit;
    }else if(nodeType.equals(ASTDPathNavigation.class)){
      DPathSearchUnit testUnit = new DPathSearchUnit(new DPathSearchUnit.DPathNavigate(((ASTDPathNavigation)tree).name));
      queryElements = testUnit;
    }
    
    return queryElements;
  }
  
  /**
   * Depending on the entryset defined by the query (nodes, edges or root), return the corresponding elements from the current sentence
   * @param sentence the current sentence to apply the query on
   * @return the list of elements corresponding to the entryset
   */
  public ArrayList<BasicDBObject> getEntrySet(DPathSearchBaseElement sentence) {
    ArrayList<BasicDBObject> entrySetList = new ArrayList<BasicDBObject>();
    if(entrySet.equals("root")){
      entrySetList.addAll(sentence.getRoots());
    }else if(entrySet.equals("nodes")){
      for(Object o :sentence.nodes){
        BasicDBObject dbo = (BasicDBObject)o;
        entrySetList.add(dbo);
      }
    }else if(entrySet.equals("edges")){
      for(Object o :sentence.edges){
        BasicDBObject dbo = (BasicDBObject)o;
        entrySetList.add(dbo);
      }
    }
    
    return entrySetList;
  }
  
  /**
   * Add a OR branch in the elasticsearch filter
   * @param filters the current filter object
   * @return the two new spawning 'OR' branches of the filter
   */
  public BasicDBList[] addUnionFilter(BasicDBList filters){
    if(filters == null){
      filters = pfFilters;
    }
    BasicDBList ands = new BasicDBList();
    BasicDBList newfilters0 = new BasicDBList();
    BasicDBList newfilters1 = new BasicDBList();
    BasicDBObject union = new BasicDBObject("or",ands);
    ands.add(new BasicDBObject("and",newfilters0));
    ands.add(new BasicDBObject("and",newfilters1));
    filters.add(union);
    BasicDBList[] unionFilters = new BasicDBList[2];
    unionFilters[0] = newfilters0;
    unionFilters[1] =newfilters1;
    return unionFilters;
  }
  
  /**
   * Append a filter to the list of filters
   * @param query the new filter (see examples in {@see #processParseTree(SimpleNode, boolean, BasicDBList)} and {@see #setTarget(BasicDBList)})
   * @param filters the current list of filters (default is the main list of filters {@see #pfFilters})
   */
  public void addPreFilter(BasicDBObject query,BasicDBList filters){
    if(filters == null){
      filters = pfFilters;
    }
    filters.add(query);
  }

  /**
   * Set the query entry set 
   * @param entrySet the entry set (root, edges or nodes)
   */
  public void setEntrySet(String entrySet) {
    this.entrySet = entrySet;
  }


  /**
   * Using information from {@see #parseQuery()}, retrieve a set of sentences from elasticsearch index matching some properties of the query
   * @return the complete list of sentences filtered by elasticsearch for this query
   * @throws Exception 
   */
  private BasicDBList preFilter() throws Exception{
    IndexSearchMod mod = new IndexSearchMod();
    if(this.pfQuery.isEmpty()){
      ((BasicDBObject)((BasicDBObject)this.prefilter.get("query")).get("filtered")).remove("query");
    }
    mod.setQuery(this.prefilter).execute();
    return mod.getFullResults();
  }
  
  /**
   * get the result for the query fetched so far
   * @return the result of the query
   */
  public BasicDBList getResults(){
    return _result;
  }
  
  /**
   * get a serialized json string of the results
   * @param from the index of the first result to get
   * @param max the number of results to get
   * @return a serialized json string of a segment of the results
   */
  public String getJSONResults(int from,int max){
    BasicDBObject json = new BasicDBObject();
    if(_result == null){
      _result = new BasicDBList();
    }
    json.put("total", _result.size());
    json.put("dpath", true);
    json.put("status", status);
    
    int to = java.lang.Math.min(_result.size(),from+max);
    json.put("hits", _result.subList(from, to));
    String jsonString = json.toString();
    return jsonString;
  }

  /**
   * 
   * @param list
   * @return
   */
  public static String exportResults(BasicDBList list){
    try{
      

      DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
   
      // root elements
      org.w3c.dom.Document doc = docBuilder.newDocument();
      Element rootElement = doc.createElement("CmaSearchResults");
      doc.appendChild(rootElement);
      
   
      java.util.Iterator<Object> it = list.iterator();
      while(it.hasNext()){
        BasicDBObject item = (BasicDBObject)it.next();
        BasicDBObject info = (BasicDBObject)item.get("_source");
        Element sentence = doc.createElement("Sentence");
        rootElement.appendChild(sentence);
        
        String straw = info.getString("sentence");
        String st = new String(straw.getBytes(),"ISO-8859-1");
        sentence.appendChild(doc.createTextNode(st));
       
        Attr attr = doc.createAttribute("id");
        attr.setValue(info.getString("sid"));
        sentence.setAttributeNode(attr);
        
        attr = doc.createAttribute("did");
        attr.setValue(info.getString("did"));
        sentence.setAttributeNode(attr);
      }
   
      // write the content into xml file
      TransformerFactory transformerFactory = TransformerFactory.newInstance();
      Transformer transformer = transformerFactory.newTransformer();
      transformer.setOutputProperty("encoding", "ISO-8859-1");
      DOMSource source = new DOMSource(doc);
      Writer outWriter = new StringWriter();  
      StreamResult result = new StreamResult( outWriter );  
      
   
      // Output to console for testing
      // StreamResult result = new StreamResult(System.out);
   
      transformer.transform(source, result);
   
      return outWriter.toString();
      
    }catch(Exception e){
      return "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><error>"+e.getMessage()+"</error>";
    }
  }
  
  @Override
  public String exportResult(){
    BasicDBList list = _result;
    if(list == null){
      list = new BasicDBList();
    }
    
    return DPathSearchMod.exportResults(list);

  }
  
  
  
  /**
   * Get from the result of an elasticsearch hit (from the prefetch), the base material for the query
   * @author buiquang
   *
   */
  public static class DPathSearchBaseElement{
    public BasicDBList nodes;
    public BasicDBList edges;
    public String sid;
    public String sentence;
    public ArrayList<String> nodeids = null;
    public ArrayList<BasicDBObject> roots = null;
    
    public HashMap<String,ArrayList<BasicDBObject>> cacheTable = new HashMap<String,ArrayList<BasicDBObject>>();
    
    public DPathSearchBaseElement(BasicDBObject indexedSentence){
      BasicDBObject sentenceobj = (BasicDBObject) indexedSentence.get("_source");
      nodes = (BasicDBList)sentenceobj.get("nodes");
      edges = (BasicDBList)sentenceobj.get("edges");
      sid = sentenceobj.getString("sid");
      sentence = sentenceobj.getString("sentence");
      // list all node ids
      nodeids = new ArrayList<String>();
      for(Object o:nodes){
        BasicDBObject node = (BasicDBObject)o;
        nodeids.add(node.getString("id"));
      }
    }
    
    private ArrayList<BasicDBObject> getRoots(){
      if(this.roots == null){
        ArrayList<BasicDBObject> roots = new ArrayList<BasicDBObject>();
        ArrayList<String> rootsId = new ArrayList<String>();
        ArrayList<String> noRootIds = new ArrayList<String>();
        for(Object o :this.edges){
          BasicDBObject edge = (BasicDBObject)o;
          String source = edge.getString("source");
          String target = edge.getString("target");
          if(!nodeids.contains(source)){
            if(!rootsId.contains(target)){
              rootsId.add(target);
            }
          }else{
            if(!noRootIds.contains(target)){
              noRootIds.add(target);
            }
          }
        }
        for(Object o:this.nodes){
          BasicDBObject node = (BasicDBObject)o;
          String nodeid = node.getString("id");
          if(rootsId.contains(nodeid) || !noRootIds.contains(nodeid)){
            roots.add(node);
          }
        }
        this.roots = roots;
      }
      return this.roots;
    }
    
    public int getNodePosition(String id){
      for(int i =0; i<nodes.size();++i){
        BasicDBObject node = (BasicDBObject)nodes.get(i);
        if(node.getString("id").equals(id)){
          return i;
        };
      }
      return -1;
    }
    
    /**
     * 
     * @param edge
     * @return true if directed to the right, false otherwise
     */
    public boolean getEdgeDirection(BasicDBObject edge){
      String source_id = edge.getString("source");
      String target_id = edge.getString("target");
      return getNodePosition(target_id)>getNodePosition(source_id);
    }
    
    public BasicDBObject getNode(String id){
      for(Object o:nodes){
        BasicDBObject node = (BasicDBObject)o;
        if(node.getString("id").equals(id)){
          return node;
        };
      }
      return null;
    }
    
    public ArrayList<BasicDBObject> getEntrySet(String entrySet) {
      ArrayList<BasicDBObject> entrySetList = new ArrayList<BasicDBObject>();
      if(entrySet.equals("root")){
        entrySetList = getRoots();
      }else if(entrySet.equals("nodes")){
        for(Object o :this.nodes){
          BasicDBObject dbo = (BasicDBObject)o;
          entrySetList.add(dbo);
        }
      }else if(entrySet.equals("edges")){
        for(Object o :this.edges){
          BasicDBObject dbo = (BasicDBObject)o;
          entrySetList.add(dbo);
        }
      }
      
      return entrySetList;
    }
    
    public ArrayList<BasicDBObject> getOut(String nodeid){
      if(cacheTable.containsKey("out-"+nodeid)){
        return cacheTable.get("out-"+nodeid);
      }
      ArrayList<BasicDBObject> result = new ArrayList<BasicDBObject>();
      for(Object o:edges){
        BasicDBObject edge = (BasicDBObject)o;
        if(edge.getString("source").equals(nodeid)){
          result.add(edge);
        }
      }
      cacheTable.put("out-"+nodeid, result);
      return result;
    }
    
    public ArrayList<BasicDBObject> getDescendants(String nodeid){
      ArrayList<String> descendants_id = new ArrayList<String>();
      ArrayList<BasicDBObject> edgeIn = getOut(nodeid);
      while(edgeIn.size()>0){
        ArrayList<String> children = new ArrayList<String>();
        for(BasicDBObject edge:edgeIn){
          String targetid = edge.getString("target");
          if(!descendants_id.contains(targetid)){
            descendants_id.add(targetid);
            children.add(targetid);
          }
        }
        edgeIn = new ArrayList<BasicDBObject>();
        for(String sid:children){
          ArrayList<BasicDBObject> newEdgeInPart = getOut(sid);
          edgeIn.addAll(newEdgeInPart);
        }
      }
      ArrayList<BasicDBObject> descendants = new ArrayList<BasicDBObject>();
      for(String nid:descendants_id){
        descendants.add(getNode(nid));
      }
      return descendants;
    }
    
    public ArrayList<BasicDBObject> getAscendants(String nodeid){
      ArrayList<String> ascendants_id = new ArrayList<String>();
      ArrayList<BasicDBObject> edgeIn = getIn(nodeid);
      while(edgeIn.size()>0){
        ArrayList<String> parents = new ArrayList<String>();
        for(BasicDBObject edge:edgeIn){
          String sourceid = edge.getString("source");
          if(!ascendants_id.contains(sourceid)){
            ascendants_id.add(sourceid);
            parents.add(sourceid);
          }
        }
        edgeIn = new ArrayList<BasicDBObject>();
        for(String sid:parents){
          ArrayList<BasicDBObject> newEdgeInPart = getIn(sid);
          edgeIn.addAll(newEdgeInPart);
        }
      }
      ArrayList<BasicDBObject> ascendants = new ArrayList<BasicDBObject>();
      for(String nid:ascendants_id){
        ascendants.add(getNode(nid));
      }
      return ascendants;
    }
    
    public ArrayList<BasicDBObject> getIn(String nodeid){
      if(cacheTable.containsKey("in-"+nodeid)){
        return cacheTable.get("in-"+nodeid);
      }
      ArrayList<BasicDBObject> result = new ArrayList<BasicDBObject>();
      for(Object o:edges){
        BasicDBObject edge = (BasicDBObject)o;
        if(edge.getString("target").equals(nodeid)){
          result.add(edge);
        }
      }
      cacheTable.put("in-"+nodeid, result);
      return result;
    }
    
    public BasicDBObject getPrev(BasicDBObject node){
      for(int i=1;i<nodes.size();i++){
        BasicDBObject n = (BasicDBObject)nodes.get(i);
        BasicDBObject prev = (BasicDBObject)nodes.get(i-1);
        if(n.equals(node)){
          return prev;
        }
        
      }
      return null;
    }

    public BasicDBObject getNext(BasicDBObject node){
      for(int i=0;i<nodes.size()-1;i++){
        BasicDBObject n = (BasicDBObject)nodes.get(i);
        BasicDBObject next = (BasicDBObject)nodes.get(i+1);
        if(n.equals(node)){
          return next;
        }
        
      }
      return null;
    }
  }

  public void execute(BasicDBList sentences) throws Exception{
    //DPathSearchMacroManager.putHistory(query, "");
    if(dpathProcesses==null){
      dpathProcesses = this.parseQuery();
      if(dpathProcesses ==null){
        throw new Exception();
      }
    }
    if(_result == null){
      _result  = new BasicDBList();
    }
    //Utils.Log("Elasticsearch pre filter returned "+n+" sentences");
    for(Object elt:sentences){
      DPathSearchBaseElement sentence = new DPathSearchBaseElement((BasicDBObject)elt);
      dpathProcesses.execute(sentence, null);
      ArrayList<BasicDBObject> result = dpathProcesses.getResult();
      //Utils.Log("Applying dpath query on  sentences "+(i++)+"/"+n);
      if(result.size()>0){
        synchronized (_result){
          BasicDBObject eltobj = (BasicDBObject) ((BasicDBObject)elt).copy();
          BasicDBObject source = (BasicDBObject)eltobj.get("_source");
          source.remove("nodes");
          source.remove("edges");
          _result.add(eltobj);
          
        }
        //Utils.Log("Success for sentence "+sentence.sentence);
      }
    }
  }

  public BasicDBList filter() throws Exception{
    this.parseQuery();
    BasicDBList sentences = this.preFilter();
    return sentences;
  }
  
  /**
   * Estimation factor (allow estimating number of result from current number of result)
   */
  private float _ef = 1;

  public void execute() throws Exception {
    
    
    dpathProcesses = this.parseQuery();
    if(dpathProcesses==null){
      status = 2;
      return;
    }
    //Utils.Log("DPath query successfully parsed : "+this.query);
    //BasicDBList sentences = this.preFilter();
    IndexSearchMod mod = new IndexSearchMod();
    if(this.pfQuery.isEmpty()){
      ((BasicDBObject)((BasicDBObject)this.prefilter.get("query")).get("filtered")).remove("query");
    }
    mod.setQuery(this.prefilter).execute();
    while(mod.hasNext()){
      // to end the search if async on a thread
      if(get_stopSignal()==1){
        return;
      }
      BasicDBList sentences = mod.next();
      _ef = mod.getEstimationFactor();
      this.execute(sentences);
    }
    
    status = 1;
    if(_result!=null){
      if(_result.size()>Configuration.DPATH_RESULT_CACHE_MAX_SIZE){
        DBCacheManager.putCache(new CompositeCacheable(_result,(new AsyncSearch.QueryCacheIndex(originalQuery,targetDocuments)).toString(),Configuration.DPATH_RESULT_CACHE_MAX_SIZE,15));
      }else{
        DBCacheManager.putCache(new Cacheable(_result,(new AsyncSearch.QueryCacheIndex(originalQuery,targetDocuments)).toString(),15));
      }
    }
    

    
  }

  @Override
  public BasicDBObject getResult(int from,int max) {
    if(_result!=null){
      synchronized (_result) {
        return formatResult(_result, from, max,status,_ef);
      }
    }else{
      return formatResult(new BasicDBList(), from, max,status,_ef);
    }
  }
  
  public static BasicDBObject formatResult(BasicDBList result,int from,int max,int status,float ef){
    BasicDBObject json = new BasicDBObject();
    if(result == null){
      result = new BasicDBList();
    }
    json.put("e_total", (int)ef*result.size());
    json.put("total", result.size());
    json.put("dpath", true);
    json.put("status", status);
    
    int to = java.lang.Math.min(result.size(),from+max);
    List<Object> sublist = result.subList(from, to);
    // creating copy : prevents from synchronisation error of result being updated in an other thread
    BasicDBList copy = new BasicDBList();
    copy.addAll(sublist);
    json.put("hits", copy);
    return json;
  }

}
