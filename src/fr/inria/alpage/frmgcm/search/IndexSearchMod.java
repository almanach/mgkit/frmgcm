package fr.inria.alpage.frmgcm.search;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.text.html.HTMLDocument.Iterator;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.util.JSON;

import fr.inria.alpage.frmgcm.Configuration;
import fr.inria.alpage.frmgcm.corpus.Corpus;
import fr.inria.alpage.frmgcm.corpus.SentenceCollection;
import fr.inria.alpage.frmgcm.search.DPathSearchMod.DPathSearchBaseElement;
import fr.inria.alpage.frmgcm.utils.Utils;

public class IndexSearchMod extends SearchMod {
  
  private BasicDBObject _query = null;
  private BasicDBObject _results;
  private String _tmpStringResult = null;
  
  private static Integer batchSize = 50;
  private Integer fromResult;
  private Integer total;
  
  public float getEstimationFactor(){
    return (float)total/(float)(fromResult+batchSize);
  }
  
  public boolean hasNext(){
    if(_tmpStringResult!=null){
      BasicDBObject json = (BasicDBObject) JSON.parse(_tmpStringResult);
      if(total==null){
        BasicDBObject jsonhits = ((BasicDBObject)json.get("hits"));
        total = jsonhits.getInt("total");
      }
      int from = 0;
      if(fromResult!=null){
        from = fromResult+batchSize;
      }
      if(from<total){
        return true;
      }else{
        return false;
      }
    }else{
      return false;
    }
  }
  
  public BasicDBList next(){
    if(_tmpStringResult!=null){
      BasicDBObject json = (BasicDBObject) JSON.parse(_tmpStringResult);
      if(total==null){
        BasicDBObject jsonhits = ((BasicDBObject)json.get("hits"));
        total = jsonhits.getInt("total");
      }
      int size = batchSize;
      if(fromResult==null){
        fromResult = -batchSize;
      }
      int newFromResult = fromResult + batchSize;
      if((newFromResult+batchSize)>total){
        size = total-newFromResult;
      }
      fromResult = newFromResult;
      _query.put("size", size);
      _query.put("from", fromResult);
      
      try {
        this.execute();
      } catch (Exception e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
      
      
      _results = (BasicDBObject) JSON.parse(_tmpStringResult);
      BasicDBObject resultHits = ((BasicDBObject)_results.get("hits"));
      BasicDBList list = (BasicDBList) resultHits.get("hits");
      return list;
    }else{
      return new BasicDBList();
    }
  }
  
  
  
  /**
   * Execute the query
   * @return
   */
  public String execute() throws Exception{
    if(_query == null){
      return "{\"error\":\"no queries\"}";
    }
    
    CloseableHttpClient httpClient = HttpClients.createDefault();
    HttpPost postRequest = new HttpPost("http://"+Configuration.elasticSearchHost+":"+Configuration.elasticSearchPort+"/sentences/graph/_search");
    Gson jsonFactory = new Gson();
    String json = jsonFactory.toJson(_query);
    Utils.Log(json,4);
    StringEntity input = new StringEntity(json,"utf-8");
    input.setContentType("application/json");
    postRequest.setEntity(input);
    HttpResponse response = httpClient.execute(postRequest);
    
    /*if (response.getStatusLine().getStatusCode() != 201) {
      System.err.println("Failed : HTTP error code : "
          + response.getStatusLine().getStatusCode());
    }*/

    BufferedReader br = new BufferedReader(
                    new InputStreamReader((response.getEntity().getContent())));
  
    StringBuilder output = new StringBuilder();
    String line = null;
    while ((line = br.readLine()) != null) {
        output.append(line+"\n");
    }
    
    br.close();

    httpClient.close();
    
    _tmpStringResult = output.toString(); 
    return _tmpStringResult;
  }
  
  /**
   * set the query object
   * @param query
   * @return
   */
  public IndexSearchMod setQuery(BasicDBObject query){
    _query = query;
    return this;
  }
  
  
  
  /**
   * Used for exportResults
   * @throws Exception 
   */
  public BasicDBList getFullResults() throws Exception{
    // check if _tmpStringResult not null
    
    BasicDBObject json = (BasicDBObject) JSON.parse(_tmpStringResult);
    BasicDBObject jsonhits = ((BasicDBObject)json.get("hits"));
    int total = jsonhits.getInt("total");
    
    _query.put("size", total);
    
    this.execute();
    
    
    _results = (BasicDBObject) JSON.parse(_tmpStringResult);
    BasicDBObject resultHits = ((BasicDBObject)_results.get("hits"));
    BasicDBList list = (BasicDBList) resultHits.get("hits");
    return list;
  }

  
  public BasicDBObject fullTextSearch(String match,BasicDBList subcollectionIds) throws Exception{
    Integer fromCount = 0;
    
    
    
    Map<String, Object> query = new HashMap<String, Object>();
    Map<String,Object> queryInfo = new HashMap<String, Object>();
    Map<String,Object> queryElt = new HashMap<String, Object>();
    queryElt.put("query", match);
    queryInfo.put("query_string", queryElt);
    query.put("from", fromCount);
    query.put("size", 10);
    BasicDBObject queryParts = new BasicDBObject("query",queryInfo);
    String term = "terms";
    if(subcollectionIds.size()==1){
      term = "term";
    }
    queryParts.put("filter", new BasicDBObject(term,new BasicDBObject("did",subcollectionIds)));
    query.put("query",new BasicDBObject("filtered",queryParts));

    String returnval = this.setQuery(new BasicDBObject(query)).execute();
    return (BasicDBObject)JSON.parse(returnval);
  }
  

  
  @Override
  public BasicDBObject getResult(int from,int max) {
    
    return null;
  }

  @Override
  public String exportResult() {
    // TODO Auto-generated method stub
    return null;
  }
  
}
