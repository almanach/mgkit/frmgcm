package fr.inria.alpage.frmgcm.search;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.ReflectionDBObject;

import fr.inria.alpage.frmgcm.Configuration;
import fr.inria.alpage.frmgcm.corpus.Corpus;
import fr.inria.alpage.frmgcm.db.CPMDB;
import fr.inria.alpage.frmgcm.utils.StaticCacheManager;
import fr.inria.alpage.frmgcm.utils.AbstractCacheManager.Cacheable;
import fr.inria.alpage.frmgcm.utils.Utils;

/**
 * This class handles operations over dpath macro/signatures
 * @author buiquang
 *
 */
public class DPathSearchMacroManager {
  
  /**
   * Retrieve every macro saved in db
   * @return the list of every {@see DPathMacro}
   */
  public static BasicDBList getAll() {
    BasicDBList _all = new BasicDBList();
    DBCollection coll = CPMDB.GetInstance().db.getCollection("dpath_macro");
    coll.setObjectClass(DPathMacro.class);
    DBCursor cursor = coll.find();
    while (cursor.hasNext()) {
      DPathMacro macro = (DPathMacro) cursor.next();
      _all.add(macro);
    }
    return _all;
  }
  
  public static BasicDBList search(String query,String owner){
    DBCollection coll = CPMDB.GetInstance().db.getCollection("dpath_macro");
    String regex = "(";
    String[] words = query.split("\\s");
    for(String word:words){
      regex += "\\s*"+word+"\\s*|";
    }
    regex = regex.substring(0, regex.length()-1);
    regex += ")+";
    BasicDBObject queryObject = new BasicDBObject();
    BasicDBList fields = new BasicDBList();
    BasicDBObject regexQuery = new BasicDBObject("$regex",regex);
    regexQuery.put("$options","i");
    fields.add(new BasicDBObject("name",regexQuery));
    fields.add(new BasicDBObject("description",regexQuery));
    queryObject.put("$or", fields);
    //queryObject.put("score", new BasicDBObject("meta","textScore"));
    DBCursor cursor = coll.find(queryObject);//.sort(new BasicDBObject("score",new BasicDBObject ("meta","textScore")));
    BasicDBList _all = new BasicDBList();
    while (cursor.hasNext()) {
      DBObject macro =  cursor.next();
      if(owner!=null && !owner.equals("admin")){
        if(!owner.equals(macro.get("user")) && !(Boolean)macro.get("ispublic")){
          continue;
        }
      }
      _all.add(macro);
    }
    
    return _all;
  }
  
  /**
   * Retrieve every macro owned by a particular user
   * @param owner the user who saved the macro
   * @return the list of every {@see DPathMacro} saved by a particular user
   */
  public static BasicDBList getAll(String owner) {
    BasicDBList _all = new BasicDBList();
    DBCollection coll = CPMDB.GetInstance().db.getCollection("dpath_macro");
    coll.setObjectClass(DPathMacro.class);
    DBCursor cursor = coll.find();
    while (cursor.hasNext()) {
      DPathMacro macro = (DPathMacro) cursor.next();
      if(owner.equals(macro.user) || macro.ispublic){
        _all.add(macro);
      }
    }
    return _all;
  }
  
  /**
   * Enqueue into per user history a query
   * @param query the original query string
   * @param owner the current user
   */
  public static void putHistory(String query,String owner) {
    BasicDBList history = getHistory(owner);
    if(history.size()>Configuration.dpathQueryHistoryMaxSize){
      history.remove(0);
    }
    if(history.contains(query)){
      history.remove(query);
    }
    history.add(query);
    Cacheable cache = new Cacheable(history,"dpathquery-"+owner,0);
    StaticCacheManager.putCache(cache);
  }

  /**
   * Retrieve the history of queries for a particular user
   * @param owner the user to retrieve the history for
   * @return the list of query string searched by a particular user
   */
  public static BasicDBList getHistory(String owner) {
    BasicDBList list = new BasicDBList();
    Cacheable cache = StaticCacheManager.getCache("dpathquery-"+owner);
    if(cache != null){
      list = (BasicDBList)cache.data;
    }
    return list;
  }

  /**
   * Retrieve the full expression of a macro
   * @param macroName the macro name to expand
   * @return the query string associated with this macro
   * @throws Exception
   */
  public static String expandMacro(String macroName) throws Exception {
    DPathMacro macro = DPathMacro.RetrieveFromName(macroName);
    if (macro == null) {
      throw new Exception("Error, no macro found with the name " + macroName);
    }
    return macro.query;
  }
  
  /**
   * This class reflects the mongo db object used to store a dpath macro
   * @author buiquang
   *
   */
  public static class DPathMacro extends ReflectionDBObject {
    /**
     * the query string associated to this macro (usually a dpath query)
     */
    public String query;
    /**
     * The unique name of the macro
     */
    public String name;
    /**
     * The user who created the macro
     */
    public String user;
    /**
     * The optional description of the macro
     */
    public String description;
    /**
     * The date of the last modification of the macro
     */
    public String lastModified;
    /**
     * The date of creation of the macro
     */
    public String created;
    /**
     * (specific for mgwiki) The list of mgwiki pages related to this macro (here it is a signature) 
     */
    public BasicDBList mgwikiPageRelated;
    /**
     * A boolean indicating if the macro is public and should therefore be visible and usable by anyone
     */
    public boolean ispublic;
    /**
     * (specific for mgwiki) The weight of the macro, the lesser the less specific
     */
    public String weight;

    public DPathMacro() {
      set_id(ObjectId.get());
    }

    /**
     * Validate the content of a macro (name is correct and query is defined)
     * @param checkName override the process of verifying the name (used for update a macro)
     * @return a validation object
     */
    public Utils.ValidationBean validate(boolean checkName) {
      Utils.ValidationBean validation = new Utils.ValidationBean();
      if(name == null || name.trim().equals("")){
        validation.success = false;
        validation.message = "Name is empty!";
        return validation;
      }
      if (checkName) {
        if(name.startsWith("_")){
          validation.success = false;
          validation.message = "Name cannot start with \"_\".";
          return validation;
        }
        String patternStr="[^a-zA-Z0-9_]";
        Pattern p = Pattern.compile(patternStr);
        Matcher m = p.matcher(name);
        if(m.find()){
          validation.success = false;
          validation.message = "Name must contains only characters ranging from a to z (ignoring case), numbers and \"_\".";
          return validation;
        }
        DPathMacro existingMacro = DPathMacro.RetrieveFromName(name);
        if (existingMacro != null) {
          validation.success = false;
          validation.message = "Name already exist. Please choose another name.";
          return validation;
        }
      }
      if (query == null || query.trim().equals("")) {
        validation.success = false;
        validation.message = "Query is empty!";
        return validation;
      }
      validation.success = true;
      return validation;
    }

    /**
     * Delete a macro 
     * @param macroName the name of the macro to delete
     */
    public static void Delete(String macroName) {
      DBCollection coll = CPMDB.GetInstance().db.getCollection("dpath_macro");
      BasicDBObject q = new BasicDBObject("name", macroName);
      coll.remove(q);
    }

    /**
     * Retrieve a macro by its name
     * @param macroName the name of the macro to retrieve
     * @return the macro corresponding or null if not found
     */
    public static DPathMacro RetrieveFromName(String macroName) {
      DBCollection coll = CPMDB.GetInstance().db.getCollection("dpath_macro");
      coll.setObjectClass(DPathMacro.class);
      BasicDBObject q = new BasicDBObject("name", macroName);
      DPathMacro macro = (DPathMacro) coll.findOne(q);
      return macro;
    }
    
    /**
     * Retrieve a macro by its id
     * @param objectId the string mongo id of the macro to retrieve
     * @return the corresponding macro or null if not found
     */
    public static DPathMacro Retrieve(String objectId) {
      DBCollection coll = CPMDB.GetInstance().db.getCollection("dpath_macro");
      coll.setObjectClass(DPathMacro.class);
      BasicDBObject q = new BasicDBObject("_id",
          new ObjectId((String) objectId));
      DPathMacro macro = (DPathMacro) coll.findOne(q);
      return macro;
    }

    /**
     * Save to db this macro
     */
    public void save() {
      DBCollection coll = CPMDB.GetInstance().db.getCollection("dpath_macro");
      coll.setObjectClass(this.getClass());
      coll.save(this);
    }
    
    public BasicDBList getmgwikiPageRelated() {
      return mgwikiPageRelated;
    }

    public void setmgwikiPageRelated(BasicDBList mgwikiPageRelated) {
      this.mgwikiPageRelated = mgwikiPageRelated;
    }

    public String getquery() {
      return query;
    }

    public void setquery(String query) {
      this.query = query;
    }
    
    public boolean getispublic() {
      return ispublic;
    }

    public void setispublic(boolean ispublic) {
      this.ispublic = ispublic;
    }
    
    public String getweight() {
      if(weight == null){
        this.weight = "0";
      }
      return this.weight;
    }

    public void setweight(String weight) {
      this.weight = weight;
    }

    public String getname() {
      return name;
    }

    public void setname(String name) {
      this.name = name;
    }

    public String getuser() {
      return user;
    }

    public void setuser(String user) {
      this.user = user;
    }

    public String getdescription() {
      return description;
    }

    public void setdescription(String description) {
      this.description = description;
    }

    public String getlastModified() {
      return lastModified;
    }

    public void setlastModified(String lastModified) {
      this.lastModified = lastModified;
    }

    public String getcreated() {
      return created;
    }

    public void setcreated(String created) {
      this.created = created;
    }
  }


}
